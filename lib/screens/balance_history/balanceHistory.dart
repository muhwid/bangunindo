import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/withdraw_body.dart';
import 'package:e_commerce_app/model/object/store_model.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/balance_history/balance_controller.dart';
import 'package:e_commerce_app/screens/main/tab/profile/bloc/profile_bloc.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BalanceHistory extends StatefulWidget {
  int saldo;
  BalanceHistory({Key key, this.saldo}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BalanceHistoryState();
  }
}

class BalanceHistoryState extends State<BalanceHistory>
    with TickerProviderStateMixin {
  BalanceController ctrl = Get.put(BalanceController());
  final box = GetStorage();
  var pembeli = true;
  var selectedTab = 1;
  var name = "";
  StoreModel store;
  int saldo = 0;
  int saldoSeller = 0;
  String errorlog;
  bool loading = false;
  Rx<TextEditingController> wd = TextEditingController().obs;
  Rx<TextEditingController> rek = TextEditingController().obs;
  String bank;
  ProfileBloc _bloc;
  ApiDomain _domain;
  PersistentBottomSheetController _controller; // <------ Instance variable
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Timer _timer;
  Dio dio = ApiServices().launch();

  @override
  void initState() {
    saldoSeller = widget.saldo;
    ctrl.apiHistory();
    super.initState();
  }

  withdraw(context) async {
    if (int.parse(wd.value.text) < 10000) {
      _controller.setState(() {
        errorlog = "Minimal penarikan adalah Rp 10.000";
        _timer = new Timer(const Duration(milliseconds: 2000), () {
          _controller.setState(() {
            errorlog = null;
          });
        });
      });
    } else if (int.parse(wd.value.text) > saldoSeller) {
      _controller.setState(() {
        errorlog = "Nominal penarikan lebih besar dari saldo yang anda miliki";
        _timer = new Timer(const Duration(milliseconds: 2000), () {
          _controller.setState(() {
            errorlog = null;
          });
        });
      });
    } else {
      loading = true;
      WithdrawBody body = WithdrawBody.fromJson({
        "bank": bank,
        "account_number": rek.value.text,
        "amount": wd.value.text
      });
      final resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_withdraw}",
          data: body.toJson());
      BaseResponse response = BaseResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        Navigator.pop(context, "Hello world");
        _controller.setState(() {
          loading = false;
        });
        setState(() {
          ctrl.apiHistory();
        });
        CustomDialog().showDialogConfirms(
            Get.context,
            "${getTranslated(Get.context, "wd")}",
            null,
            "${getTranslated(Get.context, "close")}",
            null, () {
          Get.back();
        });
        //GlobalHelper().showSnackBar("Pesanan berhasil diterima",context);
      } else {
        _controller.setState(() {
          loading = false;
        });
      }
    }
  }

  void _createBottomSheet() {
    _controller = _scaffoldKey.currentState.showBottomSheet((context) =>
        StatefulBuilder(
          builder: (BuildContext context, setState) => SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                (errorlog != null)
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.red[900],
                        padding: EdgeInsets.all(20),
                        child: Text(
                          errorlog,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    : Container(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[200],
                  padding: EdgeInsets.all(20),
                  child: Text(
                      'Permintaan penarikan dana hanya dapat dilakukan pada jam kerja dengan waktu penanganan 3x24 jam',
                      textAlign: TextAlign.center),
                ),
                Container(
                    padding: EdgeInsets.all(20),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DropdownButton<String>(
                          isExpanded: true,
                          hint: Text('Pilih Bank tujuan'),
                          value: bank,
                          items: <String>['BCA', 'MANDIRI', 'BNI', 'BRI']
                              .map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (velu) {
                            setState(() {
                              bank = velu;
                            });
                          },
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Text("Nomor Rekening"),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: rek.value,
                          textAlign: TextAlign.left,
                          style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                          decoration: new InputDecoration(
                              hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                              hintText: "Masukkan nomor rekening",
                              fillColor: bgPage),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Text("Nominal"),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: wd.value,
                          textAlign: TextAlign.left,
                          style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                          decoration: new InputDecoration(
                              hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                              hintText: "Masukkan nominal penarikan",
                              fillColor: bgPage),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text("${getTranslated(context, "imb_fee")}",
                            style: TextStyle(fontSize: 13, color: Colors.grey))
                      ],
                    )),
                (loading)
                    ? Center(child: CircularProgressIndicator())
                    : WidgetComponent.primaryButton(
                        onPress: () {
                          withdraw(context);
                        },
                        title: "KIRIM"),
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                )
              ],
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: primaryColor,
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                _createBottomSheet();
              },
              child: Image.asset(
                "assets/images/withdrawal.png",
                height: 25,
              ),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          title: Text("Balance"),
        ),
        body: Obx(() => (ctrl.isLoading.value)
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(width: 1, color: Colors.grey[300]),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              ctrl.productRating.value.data[index].description,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              ctrl.productRating.value.data[index].created_at,
                              style: TextStyle(
                                  color: Colors.grey.shade400, fontSize: 12),
                            )
                          ],
                        ),
                        Text(
                            int.parse(ctrl.productRating.value.data[index]
                                        .amount) <
                                    0
                                ? "- Rp. ${GlobalHelper().formattingNumber(int.parse(ctrl.productRating.value.data[index].amount).abs())}"
                                : "Rp. ${GlobalHelper().formattingNumber(int.parse(ctrl.productRating.value.data[index].amount).abs())}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 13,
                                color: int.parse(ctrl.productRating.value
                                            .data[index].amount) <
                                        0
                                    ? Colors.red
                                    : Colors.green))
                      ],
                    ),
                  );
                },
                itemCount: ctrl.productRating.value.data.length)));
  }
}
