import 'package:dio/dio.dart' as dd;
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/response/balance_history.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BalanceController extends GetxController {
  RxBool isLoading = false.obs;
  RxString errorMessage = "".obs;
  final box = GetStorage();

  Rx<BalanceHistoryResponse> productRating = BalanceHistoryResponse().obs;
  RxBool isDone = false.obs;
  dd.Dio dio = ApiServices().launch();

  @override
  void onInit() {
    super.onInit();
  }

  void setErrorMessage(String msg) async {
    errorMessage.value = msg;
  }

  void apiHistory() async {
    isLoading = isLoading.toggle();
    setErrorMessage("");
    var resp;
    try {
      print(Endpoint.api_balance_store);
      resp = await dio.get("${Endpoint.api_balance_store}");
      BalanceHistoryResponse response =
          BalanceHistoryResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        productRating.value = response;
      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiHistory();
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiHistory();
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiHistory();
      }, () {
        Get.back();
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
