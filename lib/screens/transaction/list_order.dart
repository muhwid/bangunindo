import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/transaction/bloc/list_order_event.dart';
import 'package:e_commerce_app/screens/transaction/bloc/list_order_state.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/list_order_bloc.dart';
import 'detail_transaction/detail_transaction.dart';

class ListOrder extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListOrdertate();
  }
}

class ListOrdertate extends State<ListOrder> {
  ListOrderBloc _bloc;
  ApiDomain _domain;
  List<DataListTransaction> transactions = List();

  // injectResult(){
  //   transactions.add(TransactionModel("https://muhwid.com/projects/bangunindo/images/img_besi.png", "Selesai", "LIVING ROOM", "08 Jul 2020", 82.23));
  //   transactions.add(TransactionModel("https://muhwid.com/projects/bangunindo/images/img_besi.png", "Diproses", "LIVING ROOM", "08 Jul 2020", 19.23));
  //   transactions.add(TransactionModel("https://muhwid.com/projects/bangunindo/images/img_besi.png", "Menunggu", "LIVING ROOM", "08 Jul 2020",  23.12));
  // }

  @override
  void initState() {
    // injectResult();
    _domain = ApiDomain(ApiRepository());
    _bloc = ListOrderBloc(domain: _domain);

    reqListOrder();
    super.initState();
  }

  void reqListOrder() {
    _bloc.add(ReqListOrder());
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (BuildContext context) {
                return _bloc;
              },
            )
          ],
          child: BlocListener<ListOrderBloc, ListOrderState>(
            bloc: _bloc,
            listener: (context, state) {
              if (state is ReqLoading) {
                CustomDialog().loading(
                    context, getTranslated(context, "processing_request"));
              } else if (state is ReqListOrderError) {
                Navigator.pop(context);
                CustomDialog().showDialogConfirms(
                    context,
                    state.error,
                    "${getTranslated(context, "retry")}",
                    "${getTranslated(context, "close")}", () {
                  Navigator.of(context, rootNavigator: true).pop();
                  // _reqGetExplore();
                }, () {
                  Navigator.of(context, rootNavigator: true).pop();
                });
              } else if (state is ReqListOrderSuccess) {
                Navigator.pop(context);
                transactions.clear();
                transactions?.addAll(state.resp.data);
              }
            },
            child: BlocBuilder<ListOrderBloc, ListOrderState>(
              bloc: _bloc,
              builder: (context, state) {
                return Scaffold(
                  backgroundColor: bgPage,
                  body: Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${getTranslated(context, "list_transaction")}",
                          style: boldMuliFont.copyWith(
                              fontSize: ScreenUtil().setSp(16)),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(15),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: _viewTrending(),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  _viewTrending() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [
          for (DataListTransaction item in transactions)
            itemTransaction(item, context)
        ],
      ),
    );
  }

  itemTransaction(DataListTransaction item, BuildContext context) {
    return InkWell(
      onTap: () async {
        var result = await Navigator.of(context).push(MaterialPageRoute(
            builder: (ctx) => DetailTransactionPage(item: item, store: false)));
        print(result);
        setState(() {
          _bloc.add(ReqListOrder());
        });
      },
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(ScreenUtil().setWidth(10)),
                        color: Colors.black),
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/img_placeholder.png",
                      image: (item.detail != null)
                          ? (item.detail[0].images.length > 0)
                              ? item.detail[0].images[0]
                              : ""
                          : "",
                      fit: BoxFit.contain,
                    ),
                  ),
                  flex: 2,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                        color: greyMenu,
                        child: Container(
                          padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(15),
                              right: ScreenUtil().setWidth(15),
                              top: ScreenUtil().setWidth(2),
                              bottom: ScreenUtil().setWidth(2)),
                          child: Text(
                            "${item.status_text}",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(10), color: green),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(5),
                      ),
                      Text(
                        (item.detail != null)
                            ? item.detail[0].name
                            : "${getTranslated(context, "product_has_deleted")}",
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12)),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(5),
                      ),
                      Text(
                        "${GlobalHelper().convertDateFromString(item.created_at)}",
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(10), color: subtitle),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(5),
                      ),
                      Text(
                        "Rp. ${GlobalHelper().formattingNumber(int.parse(item.total))}",
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(10),
                            color: primaryColor),
                      ),
                    ],
                  ),
                  flex: 6,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Divider(),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
          ],
        ),
      ),
    );
  }
}
