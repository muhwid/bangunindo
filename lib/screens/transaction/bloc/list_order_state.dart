
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:e_commerce_app/model/response/va_response.dart';
import 'package:equatable/equatable.dart';

abstract class ListOrderState extends Equatable {
  const ListOrderState();
}

class InitPage extends ListOrderState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends ListOrderState {
  @override
  List<Object> get props => [];
}

class ReqListOrderSuccess extends ListOrderState {
  final ListOrderResponse resp;

  const ReqListOrderSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqListOrderError extends ListOrderState {
  final String error;

  const ReqListOrderError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqListOrderError { error: $error }';
}