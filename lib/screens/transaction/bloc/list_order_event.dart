import 'package:equatable/equatable.dart';

abstract class ListOrderEvent extends Equatable {
  const ListOrderEvent();

  @override
  List<Object> get props => [];
}

class ReqListOrder extends ListOrderEvent {
  const ReqListOrder();

  @override
  List<Object> get props => [];
}
