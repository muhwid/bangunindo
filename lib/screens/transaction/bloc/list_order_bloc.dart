import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:flutter/material.dart';

import 'list_order_event.dart';
import 'list_order_state.dart';

class ListOrderBloc extends Bloc<ListOrderEvent, ListOrderState> {
  final ApiDomain domain;

  ListOrderBloc({@required this.domain}) : assert(domain != null);

  @override
  ListOrderState get initialState => InitPage();

  @override
  Stream<ListOrderState> mapEventToState(ListOrderEvent event) async* {
    if (event is ReqListOrder) {
      yield ReqLoading();
      try {
        ListOrderResponse resp = await domain.reqListOrder();
        if (resp.status.startsWith("20"))
          yield ReqListOrderSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqListOrderError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqListOrderError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqListOrderError(error: e.toString());
      }
    }
  }
}
