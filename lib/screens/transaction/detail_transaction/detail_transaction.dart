import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/order_status_body.dart';
import 'package:e_commerce_app/model/body/review_body.dart';
import 'package:e_commerce_app/model/body/review_main_body.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/main/main_page.dart';
import 'package:e_commerce_app/screens/success/success_page.dart';
import 'package:e_commerce_app/screens/transaction/detail_transaction/detail_transaction_controller.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/widget/source/source_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_viewer/image_viewer.dart';
import 'package:e_commerce_app/views/widget/video_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailTransactionPage extends StatefulWidget {
  final DataListTransaction item;
  final bool store;

  DetailTransactionPage({Key key, @required this.item, @required this.store})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DetailTransactionState();
  }
}

class DetailTransactionState extends State<DetailTransactionPage> {
  var controller = DetailTransactionController();

  List<DetailTransaction> carts = List();

  final picker = ImagePicker();
  PersistentBottomSheetController _controller; // <------ Instance variable
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  MediaSource source;

  @override
  void initState() {
    controller.onStart();
    controller.apiDetailOrder(widget.item.id_transaction);
    loadAllCarts();
    super.initState();
  }

  loadAllCarts() {
    controller.total.value = 0;
    controller.shipping.value = int.parse(widget.item.shipping);
    controller.total.value =
        int.parse(widget.item.shipping) + int.parse(widget.item.total);
    carts = widget.item.detail;
    for (DetailTransaction item in carts) {
      // var value = {"product": item.id_product, "rate": "4.0", "review": '${controller.textReasonController.value.text}'};
      controller.productRating
          .add(ReviewBody.static(item.id_product, "4.0", "", "", "", ""));
    }
  }

  Future getImage(source) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    _controller.setState(() {
      if (pickedFile != null) {
        if (source == 'permit') {
          controller.selectedPermit.value = File(pickedFile.path);
        } else {
          controller.selectedShipping.value = File(pickedFile.path);
        }
      } else {
        print('No image selected.');
      }
    });
  }

  Set<Marker> _markers = {};

  void _onMapCreated(GoogleMapController mapController) {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId('idm'),
          position: LatLng(
              double.tryParse(controller.dataOrder.value.data.address.latitude),
              double.tryParse(
                  controller.dataOrder.value.data.address.longitude))));
    });
  }

  _createBottomSheet() {
    _controller = _scaffoldKey.currentState
        .showBottomSheet((context) => Obx(() => SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.grey[200],
                    padding: EdgeInsets.all(20),
                    child: Text(
                        '${getTranslated(context, "make_sure_data_correct")}',
                        textAlign: TextAlign.center),
                  ),
                  Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("${getTranslated(context, "delivery_receipt")}"),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            controller: controller.resi.value,
                            textAlign: TextAlign.left,
                            style: normalTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12)),
                            decoration: new InputDecoration(
                                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                                hintText:
                                    "${getTranslated(context, "input_delivery_receipt")}",
                                fillColor: bgPage),
                          )
                        ],
                      )),
                  (widget.item.shipping == 'Expedisi mandiri' ||
                          widget.item.shipping == 'Gojek')
                      ? Container(
                          padding: EdgeInsets.all(20),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${getTranslated(context, "price")}"),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                controller: controller.priceTxtCtrl.value,
                                textAlign: TextAlign.left,
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12)),
                                decoration: new InputDecoration(
                                    hintStyle:
                                        TextStyle(color: Color(0xffD1D1D1)),
                                    hintText:
                                        "${getTranslated(context, "input_price")}",
                                    fillColor: bgPage),
                              )
                            ],
                          ))
                      : Container(),
                  (widget.item.shipping == 'Expedisi mandiri' ||
                          widget.item.shipping == 'Gojek')
                      ? Container(
                          padding: EdgeInsets.all(20),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  "${getTranslated(context, "delivery_photo")}"),
                              InkWell(
                                onTap: () {
                                  getImage('shipping');
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(top: 10),
                                    padding: EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.grey[200])),
                                    child: Column(children: <Widget>[
                                      (controller.selectedShipping.value !=
                                              null)
                                          ? Image.file(
                                              controller.selectedShipping.value,
                                              width: 50)
                                          : Image.asset(
                                              "assets/images/uplaod.png",
                                              width: 50),
                                      Text(
                                          '${getTranslated(context, "upload_delivery_photo")}')
                                    ])),
                              )
                            ],
                          ))
                      : Container(),
                  (widget.item.shipping == 'Expedisi mandiri' ||
                          widget.item.shipping == 'Gojek')
                      ? Container(
                          padding: EdgeInsets.all(20),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  "${getTranslated(context, "travel_document")}"),
                              InkWell(
                                onTap: () {
                                  getImage('permit');
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(top: 10),
                                    padding: EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.grey[200])),
                                    child: Column(children: <Widget>[
                                      (controller.selectedPermit.value != null)
                                          ? Image.file(
                                              controller.selectedPermit.value,
                                              width: 50)
                                          : Image.asset(
                                              "assets/images/uplaod.png",
                                              width: 50),
                                      Text(
                                          '${getTranslated(context, "upload_travel_document")}')
                                    ])),
                              )
                            ],
                          ))
                      : Container(),
                  WidgetComponent.primaryButton(
                      onPress: () {
                        controller.uploadImage(OrderStatusBody(
                            widget.item.id_transaction,
                            '3',
                            '',
                            controller.priceTxtCtrl.value.text,
                            controller.basepermit.value,
                            controller.baseshipping.value,
                            controller.resi.value.text));
                      },
                      title: (controller.isLoading.value)
                          ? "${getTranslated(context, "uploading")}..."
                          : "${getTranslated(context, "send").toUpperCase()}"),
                  SizedBox(
                    height: ScreenUtil().setHeight(25),
                  )
                ],
              ),
            )));
  }

  void _createRatingSheet() {
    _controller = _scaffoldKey.currentState
        .showBottomSheet((context) => Obx(() => SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.grey[200],
                    padding: EdgeInsets.all(20),
                    child: Text(
                        '${getTranslated(context, "desc_complete_order")}',
                        textAlign: TextAlign.center),
                  ),
                  for (int i = 0; i < carts.length; i++)
                    ratingView(i, carts[i]),
                  WidgetComponent.primaryButton(
                      onPress: () {
                        print(controller);
                        if (!controller.isClosed)
                          controller.apiFinish(ReviewMainBody(
                              widget.item.id_transaction,
                              controller.productRating));
                      },
                      title: (controller.isLoading.value)
                          ? "Loading..."
                          : "${getTranslated(context, "receipt_of_goods").toUpperCase()}"),
                  SizedBox(
                    height: ScreenUtil().setHeight(20),
                  )
                ],
              ),
            )));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: bgPage,
          body: Obx(() => SingleChildScrollView(
                child: Stack(
                  children: [
                    ClipPath(
                        clipper: OvalBottomBorderClipper(),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: ScreenUtil().setHeight(200),
                          color: primaryColor,
                        )),
                    Container(
                      margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(30),
                          left: ScreenUtil().setWidth(15)),
                      child: Text(
                        "${getTranslated(context, "detail_transaction")}",
                        style: boldMuliFont.copyWith(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18)),
                      ),
                    ),
                    (carts.length <= 0) ? emptyCart() : viewCartList(context)
                  ],
                ),
              )),
        ),
      ),
    );
  }

  recepientView() {
    return Container(
      margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
      width: Get.width,
      child: Card(
        color: Colors.white,
        child: Container(
          margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${getTranslated(context, "recepient_detail")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
              ),
              Divider(),
              Text(
                "${getTranslated(context, "name")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              controller.dataOrder.value.data != null
                  ? Text(
                      "${controller.dataOrder.value.data.user.name}",
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12)),
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Text(
                "${getTranslated(context, "phone_number")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              controller.dataOrder.value.data != null
                  ? Text(
                      "${controller.dataOrder.value.data.address.phone}",
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12)),
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Text(
                "${getTranslated(context, "address")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              controller.dataOrder.value.data != null
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${controller.dataOrder.value.data.address.name}",
                          style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        ),
                        Text(
                          "${controller.dataOrder.value.data.address.address}",
                          style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        ),
                      ],
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Text(
                "${getTranslated(context, "sub")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              controller.dataOrder.value.data != null
                  ? Text(
                      "${controller.dataOrder.value.data.address.districtName}",
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12)),
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Text(
                "${getTranslated(context, "dis")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              controller.dataOrder.value.data != null
                  ? Text(
                      "${controller.dataOrder.value.data.address.cityName}",
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12)),
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Text(
                "${getTranslated(context, "prov")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              controller.dataOrder.value.data != null
                  ? Text(
                      "${controller.dataOrder.value.data.address.provinceName}",
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12)),
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              controller.dataOrder.value.data != null
                  ? Container(
                      width: MediaQuery.of(context).size.width - 2 * 25,
                      height: MediaQuery.of(context).size.height / 4,
                      child: GoogleMap(
                          onMapCreated: _onMapCreated,
                          markers: _markers,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(
                                  double.tryParse(controller
                                      .dataOrder.value.data.address.latitude),
                                  double.tryParse(controller
                                      .dataOrder.value.data.address.longitude)),
                              zoom: 13)))
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  emptyCart() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
      child: Column(
        children: [
          //header
          Container(
            padding: EdgeInsets.all(ScreenUtil().setSp(15)),
            margin: EdgeInsets.only(
                left: ScreenUtil().setHeight(15),
                right: ScreenUtil().setHeight(15),
                bottom: ScreenUtil().setHeight(15)),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ],
                borderRadius: BorderRadius.circular(12.0)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(80),
                  height: ScreenUtil().setHeight(80),
                  child: SvgPicture.asset("assets/images/ic_empty_cart.svg"),
                ),
                Text(
                  "Empty Cart",
                  style:
                      boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(25)),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                // Center(
                //   child: Text(
                //       "it's a Long Established fact that a reader will be distracted by the readable content",
                //       textAlign: TextAlign.center,
                //       style: normalTextFont.copyWith(
                //         fontSize: ScreenUtil().setSp(12),
                //       )),
                // ),
                // SizedBox(
                //   height: ScreenUtil().setHeight(15),
                // ),
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => MainPage()),
                        ModalRoute.withName("/Home"));
                  },
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          color: Color(0xffFB4C5A),
                          child: Center(
                            child: Container(
                                margin:
                                    EdgeInsets.all(ScreenUtil().setWidth(10)),
                                child: Text(
                                  "${getTranslated(context, "browse_more")}"
                                      .toUpperCase(),
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      color: Colors.white),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.0,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _viewTotal(context) {
    return Column(
      children: [
        SizedBox(
          height: ScreenUtil().setHeight(5),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Divider(),
        ),
        (widget.item.shipping_number != null)
            ? Container(
                margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(15),
                    right: ScreenUtil().setWidth(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Text(
                      "${getTranslated(context, "receipt")}",
                      style: boldTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          color: primaryColor),
                    )),
                    Expanded(
                        child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        widget.item.shipping_number,
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12),
                            color: primaryColor),
                      ),
                    )),
                  ],
                ),
              )
            : Container(),
        (widget.item.shipping_photo != null)
            ? SizedBox(
                height: ScreenUtil().setHeight(15),
              )
            : Container(),
        (widget.item.shipping_photo != null)
            ? Container(
                margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(15),
                    right: ScreenUtil().setWidth(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Text(
                      "${getTranslated(context, "delivery_photo")}",
                      style: boldTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          color: primaryColor),
                    )),
                    Expanded(
                        child: Align(
                      alignment: Alignment.centerRight,
                      child: InkWell(
                          onTap: () {
                            print("image url ${widget.item.shipping_photo}");
                            ImageViewer.showImageSlider(
                              images: [
                                widget.item.shipping_photo,
                              ],
                              startingPosition: 1,
                            );
                          },
                          child: Text(
                            "${getTranslated(context, "click_to_see")}",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: primaryColor),
                          )),
                    )),
                  ],
                ),
              )
            : Container(),
        (widget.item.shipping_photo != null)
            ? SizedBox(
                height: ScreenUtil().setHeight(15),
              )
            : Container(),
        (widget.item.shipping_permit != null)
            ? Container(
                margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(15),
                    right: ScreenUtil().setWidth(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Text(
                      "${getTranslated(context, "travel_document")}",
                      style: boldTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          color: primaryColor),
                    )),
                    Expanded(
                        child: Align(
                      alignment: Alignment.centerRight,
                      child: InkWell(
                          onTap: () {
                            print("image url ${widget.item.shipping_permit}");
                            ImageViewer.showImageSlider(
                              images: [
                                widget.item.shipping_permit,
                              ],
                              startingPosition: 1,
                            );
                          },
                          child: Text(
                            "${getTranslated(context, "click_to_see")}",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: primaryColor),
                          )),
                    )),
                  ],
                ),
              )
            : Container(),
        (widget.item.shipping_permit != null)
            ? SizedBox(
                height: ScreenUtil().setHeight(15),
              )
            : Container(),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(
                "${getTranslated(context, "shipping")} (" +
                    widget.item.shipping_service +
                    ")",
                style: boldTextFont.copyWith(
                    fontSize: ScreenUtil().setSp(12), color: primaryColor),
              )),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Rp. ${GlobalHelper().formattingNumber(controller.shipping.value)}",
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12), color: primaryColor),
                ),
              )),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(
                "Total",
                style: boldTextFont.copyWith(
                    fontSize: ScreenUtil().setSp(12), color: primaryColor),
              )),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Rp. ${GlobalHelper().formattingNumber(int.parse(widget.item.total))}",
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12), color: primaryColor),
                ),
              )),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(35),
        ),
        (widget.store)
            ? (controller.isLoading.value)
                ? Center(child: CircularProgressIndicator())
                : (controller.isDone.value &&
                        controller.dataOrder.value.data != null &&
                        controller.dataOrder.value.data.status != '5')
                    ? Center(
                        child: Text(
                            "${getTranslated(context, "desc_order_accept")}"))
                    : (controller.isDone.value &&
                            controller.dataOrder.value.data != null &&
                            controller.dataOrder.value.data.status == '5')
                        ? Text(
                            "${getTranslated(context, "order_cancel_by_seller")}",
                            style: boldTextFont.copyWith(color: primaryColor))
                        : (controller.dataOrder.value.data != null &&
                                controller.dataOrder.value.data.status == '1')
                            ? Container(
                                margin: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(15),
                                    right: ScreenUtil().setWidth(15)),
                                child: InkWell(
                                  onTap: () {
                                    //Navigator.push(context, MaterialPageRoute(builder: (context) => CheckoutPage(total: total,)));
                                  },
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {
                                          controller.apiUpdate(OrderStatusBody(
                                              widget.item.id_transaction,
                                              '2',
                                              '0',
                                              widget.item.reason));
                                        },
                                        child: Card(
                                          color: green,
                                          child: Center(
                                            child: Container(
                                                margin: EdgeInsets.all(
                                                    ScreenUtil().setWidth(10)),
                                                child: Text(
                                                  "${getTranslated(context, "accept")}",
                                                  style: boldTextFont.copyWith(
                                                      fontSize: ScreenUtil()
                                                          .setSp(12),
                                                      color: Colors.white),
                                                )),
                                          ),
                                        ),
                                      )),
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {
                                          showInformationDialog(context);
                                        },
                                        child: Card(
                                          color: primaryColor,
                                          child: Center(
                                            child: Container(
                                                margin: EdgeInsets.all(
                                                    ScreenUtil().setWidth(10)),
                                                child: Text(
                                                  "${getTranslated(context, "decline")}",
                                                  style: boldTextFont.copyWith(
                                                      fontSize: ScreenUtil()
                                                          .setSp(12),
                                                      color: Colors.white),
                                                )),
                                          ),
                                        ),
                                      )),
                                    ],
                                  ),
                                ),
                              )
                            : (controller.dataOrder.value.data != null &&
                                    controller.dataOrder.value.data.status ==
                                        '2')
                                ? Container(
                                    margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(15),
                                        right: ScreenUtil().setWidth(15)),
                                    child: InkWell(
                                      onTap: () {
                                        //Navigator.push(context, MaterialPageRoute(builder: (context) => CheckoutPage(total: total,)));
                                      },
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: InkWell(
                                            onTap: () {
                                              _createBottomSheet();
                                            },
                                            child: Card(
                                              color: green,
                                              child: Center(
                                                child: Container(
                                                    margin: EdgeInsets.all(
                                                        ScreenUtil()
                                                            .setWidth(10)),
                                                    child: Text(
                                                      "${getTranslated(context, "send_item")}",
                                                      style:
                                                          boldTextFont.copyWith(
                                                              fontSize:
                                                                  ScreenUtil()
                                                                      .setSp(
                                                                          12),
                                                              color:
                                                                  Colors.white),
                                                    )),
                                              ),
                                            ),
                                          )),
                                        ],
                                      ),
                                    ),
                                  )
                                : Container()
            : (controller.dataOrder.value.data != null &&
                    controller.dataOrder.value.data.status == '3')
                ? (controller.isLoading.value)
                    ? Center(child: CircularProgressIndicator())
                    : Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(15),
                            right: ScreenUtil().setWidth(15)),
                        child: InkWell(
                          onTap: () {
                            //Navigator.push(context, MaterialPageRoute(builder: (context) => CheckoutPage(total: total,)));
                          },
                          child: Row(
                            children: [
                              Expanded(
                                  child: InkWell(
                                onTap: () {
                                  _createRatingSheet();
                                },
                                child: Card(
                                  color: green,
                                  child: Center(
                                    child: Container(
                                        margin: EdgeInsets.all(
                                            ScreenUtil().setWidth(10)),
                                        child: Text(
                                          "${getTranslated(context, "item_received")}",
                                          style: boldTextFont.copyWith(
                                              fontSize: ScreenUtil().setSp(12),
                                              color: Colors.white),
                                        )),
                                  ),
                                ),
                              )),
                            ],
                          ),
                        ),
                      )
                : (controller.dataOrder.value.data != null &&
                        controller.dataOrder.value.data.status == '0' &&
                        widget.item.payment != '0')
                    ? (controller.isLoading.value)
                        ? Center(child: CircularProgressIndicator())
                        : Container(
                            margin: EdgeInsets.only(
                                left: ScreenUtil().setWidth(15),
                                right: ScreenUtil().setWidth(15)),
                            child: InkWell(
                              onTap: () {
                                //Navigator.push(context, MaterialPageRoute(builder: (context) => CheckoutPage(total: total,)));
                              },
                              child: Row(
                                children: [
                                  Expanded(
                                      child: InkWell(
                                    onTap: () async {
                                      if (await canLaunch(
                                          "https://checkout.xendit.co/web/" +
                                              controller
                                                  .dataOrder.value.data.xendit +
                                              "#" +
                                              controller
                                                  .dataOrder.value.data.payment
                                                  .toLowerCase())) {
                                        await launch(
                                            "https://checkout.xendit.co/web/" +
                                                controller.dataOrder.value.data
                                                    .xendit +
                                                "#" +
                                                controller.dataOrder.value.data
                                                    .payment
                                                    .toLowerCase());
                                      } else {
                                        throw 'Could not launch';
                                      }
                                    },
                                    child: Card(
                                      color: green,
                                      child: Center(
                                        child: Container(
                                            margin: EdgeInsets.all(
                                                ScreenUtil().setWidth(10)),
                                            child: Text(
                                              "Bayar Pesanan",
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(12),
                                                  color: Colors.white),
                                            )),
                                      ),
                                    ),
                                  )),
                                ],
                              ),
                            ),
                          )
                    : Container(),
        SizedBox(
          height: ScreenUtil().setHeight(25),
        )
      ],
    );
  }

  viewCartList(context) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(ScreenUtil().setSp(15))
                .copyWith(top: ScreenUtil().setWidth(5)),
            margin: EdgeInsets.only(
                left: ScreenUtil().setHeight(15),
                right: ScreenUtil().setHeight(15),
                bottom: ScreenUtil().setHeight(5)),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ],
                borderRadius: BorderRadius.circular(12.0)),
            child: Column(
              children: [
                for (int i = 0; i < carts.length; i++) itemCart(i, carts[i])
              ],
            ),
          ),
          controller.dataOrder.value.data != null
              ? recepientView()
              : Container(),
          _viewTotal(context)
        ],
      ),
    );
  }

// TODO: Review disini
  Widget ratingView(int index, DetailTransaction item) {
    return Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(item.name, style: new TextStyle(fontWeight: FontWeight.bold)),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            RatingBar.builder(
              initialRating: 4,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemSize: 30.0,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                setState(() {
                  controller.productRating[index].rate = rating.toString();
                });
              },
            ),
            SizedBox(
              height: ScreenUtil().setHeight(20),
            ),
            TextField(
              onChanged: (text) {
                setState(() {
                  controller.productRating[index].review = text;
                });
              },
              decoration: new InputDecoration(
                border: new OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.grey[200])),
                hintText:
                    '${getTranslated(context, "your_comment_about_product")}',
              ),
              maxLines: 1,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(20),
            ),
            Card(
              child: Container(
                margin: EdgeInsets.all(ScreenUtil().setWidth(25)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            child: controller.productRating[index].photo == ""
                                ? Container(
                                    child: Column(
                                    children: [
                                      SvgPicture.asset(
                                          "assets/images/ic_add_image.svg"),
                                      Text(
                                        "${getTranslated(context, "img_empty")}",
                                        style: normalTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(10)),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ))
                                : Image.file(File(
                                    controller.productRating[index].photo)),
                            onTap: () {
                              capture(index, MediaSource.image);
                            },
                          ),
                          flex: 1,
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(5),
                        ),
                        Expanded(
                          child: InkWell(
                            child: controller.productRating[index].video == ""
                                ? Container(
                                    child: Column(
                                    children: [
                                      SvgPicture.asset(
                                          "assets/images/ic_add_image.svg"),
                                      Text(
                                        "${getTranslated(context, "video_empty")}",
                                        style: normalTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(10)),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ))
                                : VideoWidget(
                                    file: File(
                                        controller.productRating[index].video),
                                  ),
                            onTap: () {
                              capture(index, MediaSource.video);
                            },
                          ),
                          flex: 1,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(10),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    Text(
                      "${getTranslated(context, "additional_image_review_desc")}",
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(10)),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }

  Future capture(int index, MediaSource source) async {
    setState(() {
      this.source = source;
      controller.file.value = null;
    });

    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => SourceScreen(),
        settings: RouteSettings(arguments: source)));

    // var value = {"photo": controller.reviewPhoto.value};

    if (result == null) {
      return;
    } else {
      _controller.setState(() {
        controller.file.value = result;
        if (source == MediaSource.image)
          controller.productRating[index].photo = result.path;
        else
          controller.productRating[index].video = result.path;
      });
    }
  }

  Widget itemCart(int index, DetailTransaction item) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(7)),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  height: ScreenUtil().setHeight(70),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setWidth(10)),
                      color: Colors.black),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/img_placeholder.png",
                    image: (item.images.length > 0)
                        ? item.images[0]
                        : "assets/images/img_placeholder.png",
                    fit: BoxFit.cover,
                  ),
                ),
                flex: 2,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Text(
                          item.name,
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        )),
                        SizedBox(
                          width: ScreenUtil().setWidth(5),
                        ),
                        Row(
                          children: [
                            Text(
                                "Rp. ${GlobalHelper().formattingNumber(int.parse(item.price))}",
                                style: boldTextFont.copyWith(
                                    color: primaryColor,
                                    fontSize: ScreenUtil().setSp(10))),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            width: ScreenUtil().setWidth(35),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.black),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: Text(
                                        "${carts[index].qty}",
                                        textAlign: TextAlign.center,
                                        style: boldTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(12)),
                                      )),
                                ],
                              ),
                            )),
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    (controller.dataOrder.value.data != null)
                        ? Container(
                            child: (controller.dataOrder.value.data != null &&
                                    controller.dataOrder.value.data.reason !=
                                        null)
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${getTranslated(context, "reason_cancelation")} :",
                                        style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12),
                                        ),
                                      ),
                                      Text(
                                        "${controller.dataOrder.value.data.reason}",
                                        style: normalTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(10),
                                            color: Colors.black),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                      ),
                                    ],
                                  )
                                : SizedBox(),
                          )
                        : SizedBox()
                  ],
                ),
                flex: 6,
              )
            ],
          ),
          SizedBox(
            height: ScreenUtil().setHeight(5),
          ),
          Container(
            margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(10),
                right: ScreenUtil().setWidth(10)),
            child: Divider(),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(5),
          )
        ],
      ),
    );
  }

  Future<void> showInformationDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                        '${getTranslated(context, "what_reason_cancelation")} ?',
                        style: normalTextFont.copyWith(fontSize: 14)),
                    TextFormField(
                        controller: controller.textReasonController.value,
                        validator: (value) {
                          return value.isNotEmpty
                              ? null
                              : "${getTranslated(context, "cant_empty")}";
                        }),
                  ],
                ),
              ),
              actions: [
                FlatButton(
                    color: primaryColor,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        //Do Something
                        // apiUpdateTolak(DataListTransaction(), context);
                        // TODO: Add function, get value text, update model
                        controller.apiUpdateTolak(OrderStatusBody(
                            widget.item.id_transaction,
                            '5',
                            controller.textReasonController.value.text,
                            '0'));

                        Navigator.of(context).pop();
                      }
                    },
                    child: Text('${getTranslated(context, "send")}',
                        style: normalTextFont.copyWith(
                            fontSize: 14, color: Colors.white)))
              ],
            );
          });
        });
  }
}
