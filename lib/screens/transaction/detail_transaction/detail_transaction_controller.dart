import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' as dd;
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/order_status_body.dart';
import 'package:e_commerce_app/model/body/review_body.dart';
import 'package:e_commerce_app/model/body/review_main_body.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/detail_order_response.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DetailTransactionController extends GetxController {
  RxBool isLoading = false.obs;
  RxString errorMessage = "".obs;
  final box = GetStorage();

  RxList<ReviewBody> productRating = List<ReviewBody>().obs;
  Rx<DetailOrderResponse> dataOrder = DetailOrderResponse().obs;
  RxBool isDone = false.obs;
  dd.Dio dio = ApiServices().launch();
  Rx<TextEditingController> resi = TextEditingController().obs;
  Rx<TextEditingController> priceTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> textReasonController = TextEditingController().obs;
  RxString status = "".obs;
  RxString statusText = "".obs;
  RxString reason = "".obs;
  RxInt total = 0.obs;
  RxInt shipping = 0.obs;
  Rx<File> selectedShipping = File("").obs;
  Rx<File> selectedPermit = File("").obs;
  Rx<File> file = File("").obs;
  RxString baseshipping = "".obs;
  RxString basepermit = "".obs;
  RxString reviewPhoto = "".obs;

  @override
  void onInit() {
    super.onInit();
  }

  void setErrorMessage(String msg) async {
    errorMessage.value = msg;
  }

  void apiDetailOrder(String id) async {
    isLoading = isLoading.toggle();
    setErrorMessage("");
    var resp;
    try {
      resp = await dio.get("${Endpoint.api_detail_order}/$id");
      DetailOrderResponse response = DetailOrderResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        dataOrder.value = response;
      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiDetailOrder(id);
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiDetailOrder(id);
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiDetailOrder(id);
      }, () {
        Get.back();
      });
    }
  }

  void apiUpdateTolak(OrderStatusBody body) async {
    isLoading = isLoading.toggle();
    setErrorMessage("");
    var resp;
    try {
      resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_order}/edit",
          data: body.toJson());
      BaseResponse response = BaseResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        isDone.value = true;
        status.value = '5';
        statusText.value = '${getTranslated(Get.context, "canceled")}';
        reason.value = textReasonController.value.text;
      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiUpdateTolak(body);
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiUpdateTolak(body);
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiUpdateTolak(body);
      }, () {
        Get.back();
      });
    }
  }

  void apiUpdate(OrderStatusBody body) async {
    isLoading = isLoading.toggle();
    setErrorMessage("");
    var resp;
    try {
      resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_order}/edit",
          data: body.toJson());
      BaseResponse response = BaseResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        isDone.value = true;
        status.value = '2';
        statusText.value = '${getTranslated(Get.context, "processing")}';
      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiUpdate(body);
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiUpdate(body);
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiUpdate(body);
      }, () {
        Get.back();
      });
    }
  }

  void apiFinish(ReviewMainBody body) async {
    isLoading = isLoading.toggle();
    setErrorMessage("");

    Map<String, dynamic> bodyJson = {
      "id_transaction": "${body.id_transaction}"
    };

    var index = 0;
    for (var item in body.product) {
      bodyJson.putIfAbsent('product[$index][product]', () => item.product);
      bodyJson.putIfAbsent('product[$index][rate]', () => item.rate);
      bodyJson.putIfAbsent('product[$index][review]', () => item.review);
      if (item.photo == "") {
        bodyJson.putIfAbsent('product[$index][photo]', () => "");
      } else {
        bodyJson.putIfAbsent('product[$index][photo]',
            () async => await dd.MultipartFile.fromFile(item.photo));
      }

      if (item.video == "") {
        bodyJson.putIfAbsent('product[$index][video]', () => "");
      } else {
        bodyJson.putIfAbsent('product[$index][video]',
            () async => await dd.MultipartFile.fromFile(item.video));
      }
      index++;
    }

    dd.FormData data;
    data = dd.FormData.fromMap(bodyJson);

    print(data);

    var resp;
    try {
      resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_review}",
          data: data);
      BaseResponse response = BaseResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        Get.back();
        dataOrder.value.data.status = '4';
        FlushBarMessage().toast(
            Get.context,
            "${getTranslated(Get.context, "success")}",
            "Pesanan berhasil diterima",
            Colors.green,
            2);
      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiFinish(body);
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiFinish(body);
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiFinish(body);
      }, () {
        Get.back();
      });
    }
  }

  void uploadImage(OrderStatusBody body) async {
    // Navigator.pop(context, "Hello world");
    if (!selectedShipping.value.path.isEmpty) {
      baseshipping.value = convertImagetoBase64(selectedShipping.value);
    }
    if (!selectedPermit.value.path.isEmpty) {
      basepermit.value = convertImagetoBase64(selectedPermit.value);
    }

    // log("body ${bd.toJson().toString()}");
    final resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_order}/edit",
        data: body.toJson());
    BaseResponse response = BaseResponse.fromJson(resp.data);
    isLoading = isLoading.toggle();
    if (response.status.startsWith("20")) {
      status.value = '3';
      statusText.value = '${getTranslated(Get.context, "sent")}';
      isLoading = isLoading.toggle();
      Get.back();
    } else {}
  }

  String convertImagetoBase64(File fileData) {
    List<int> imageBytes = fileData.readAsBytesSync();
    return base64Encode(imageBytes);
  }

  @override
  void onClose() {
    super.onClose();
  }
}
