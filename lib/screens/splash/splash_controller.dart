import 'dart:async';

import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  RxBool isLogin = false.obs;

  @override
  void onInit() {
    Timer(Duration(seconds: 3), navigatePage);
    super.onInit();
  }

  void navigatePage() async {
    FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    if (BoxStorage().getFirst() == null) {
      Get.offNamed("/introPage");
    } else {
      if (BoxStorage().getLogin() == null || BoxStorage().getLogin() == false) {
        _firebaseMessaging.getToken().then((token) async {
          assert(token != null);
          await GlobalHelper.saveFcmToken(token).then((value) {
            Get.offAllNamed("/loginPage");
          });
        });
      } else {
        isLogin.value = BoxStorage().getLogin();
        _firebaseMessaging.getToken().then((token) async {
          assert(token != null);
          await GlobalHelper.saveFcmToken(token).then((value) {
            if (isLogin.value) {
              Get.offAllNamed("/mainPage");
            } else {
              Get.offAllNamed("/loginPage");
            }
          });
        });
      }
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
