import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/splash/splash_controller.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';


class SplashPage extends StatelessWidget {
  SplashController ctrl = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 360, height: 640, allowFontScaling: true)..init(context);

    return Container(
      color: bgSplash,
      child: Center(
          child: SizedBox(
            height: ScreenUtil().setHeight(40),
            child: Image.asset(URL.COMPANY_LOGO),
          )),
    );
  }
}
