import 'package:e_commerce_app/model/body/order_body.dart';
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:equatable/equatable.dart';

abstract class CheckoutEvent extends Equatable {
  const CheckoutEvent();

  @override
  List<Object> get props => [];
}

class ReqListBank extends CheckoutEvent {
  const ReqListBank();

  @override
  List<Object> get props => [];
}

class ReqShipping extends CheckoutEvent {
  final AddressesModel body;
  final String store;

  const ReqShipping(this.body,this.store);

  @override
  List<Object> get props => [body];
}

class ReqOrder extends CheckoutEvent {
  final OrderBody body;

  const ReqOrder(this.body);

  @override
  List<Object> get props => [body];
}
