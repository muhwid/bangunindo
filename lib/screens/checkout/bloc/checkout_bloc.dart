import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/list_bank_response.dart';
import 'package:e_commerce_app/model/response/order_response.dart';
import 'package:e_commerce_app/model/response/shipping_response.dart';
import 'package:flutter/material.dart';

import 'checkout_event.dart';
import 'checkout_state.dart';

class CheckoutBloc extends Bloc<CheckoutEvent, CheckoutState> {
  final ApiDomain domain;

  CheckoutBloc({@required this.domain}) : assert(domain != null);

  @override
  CheckoutState get initialState => InitPage();

  @override
  Stream<CheckoutState> mapEventToState(CheckoutEvent event) async* {
    if (event is ReqListBank) {
      yield ReqLoading();
      try {
        ListBankResponse resp = await domain.reqListBank();
        if (resp.status.startsWith("20"))
          yield GetBanksSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield GetBanksError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield GetBanksError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield GetBanksError(error: e.toString());
      }
    }

    if (event is ReqShipping) {
      yield ReqShippingLoading();
      try {
        ShippingResponse resp = await domain.reqShipping(
            event.body.district.id_district, event.store);
        if (resp.status.startsWith("20"))
          yield ReqShippingSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqShippingError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqShippingError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqShippingError(error: e.toString());
      }
    }

    if (event is ReqOrder) {
      yield ReqLoading();
      try {
        OrderResponse resp = await domain.reqOrder(event.body);
        if (resp.status.startsWith("20"))
          yield ReqOrderSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqOrderError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqOrderError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqOrderError(error: e.toString());
      }
    }
  }
}
