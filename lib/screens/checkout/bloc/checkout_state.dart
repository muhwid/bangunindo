
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/list_bank_response.dart';
import 'package:e_commerce_app/model/response/order_response.dart';
import 'package:e_commerce_app/model/response/shipping_response.dart';
import 'package:equatable/equatable.dart';

abstract class CheckoutState extends Equatable {
  const CheckoutState();
}

class InitPage extends CheckoutState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends CheckoutState {
  @override
  List<Object> get props => [];
}

class GetBanksSuccess extends CheckoutState {
  final ListBankResponse resp;

  const GetBanksSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class GetBanksError extends CheckoutState {
  final String error;

  const GetBanksError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetBanksError { error: $error }';
}

class ReqShippingLoading extends CheckoutState {
  @override
  List<Object> get props => [];
}

class ReqShippingSuccess extends CheckoutState {
  final ShippingResponse resp;

  const ReqShippingSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqShippingError extends CheckoutState {
  final String error;

  const ReqShippingError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqShippingError { error: $error }';
}

class ReqOrderSuccess extends CheckoutState {
  final OrderResponse resp;

  const ReqOrderSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqOrderError extends CheckoutState {
  final String error;

  const ReqOrderError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqOrderError { error: $error }';
}