import 'package:clipboard/clipboard.dart';
import 'package:e_commerce_app/data/database_helper.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/order_body.dart';
import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/model/object/bank_model.dart';
import 'package:e_commerce_app/model/object/data_shipping_model.dart';
import 'package:e_commerce_app/model/object/master_cost_model.dart';
import 'package:e_commerce_app/model/object/product_cart_model.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/response/order_response.dart';
import 'package:e_commerce_app/model/response/shipping_response.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/list/list_address_page.dart';
import 'package:e_commerce_app/screens/checkout/bloc/checkout_bloc.dart';
import 'package:e_commerce_app/screens/main/main_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bloc/checkout_event.dart';
import 'bloc/checkout_state.dart';

class CheckoutPage extends StatefulWidget {
  int total = 0;
  CheckoutPage({Key key, @required this.total}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CheckoutPageState();
  }
}

class CheckoutPageState extends State<CheckoutPage> {
  CheckoutBloc _bloc;
  ApiDomain _domain;
  List<BankModel> paymentMethods = List();
  final f = new DateFormat('dd-MM-yyyy hh:mm');
  AddressesModel selectedLocation;
  ShippingResponse shippingResponse;
  MasterCostModel selectedTypeShipping;
  DataShippingModel selectedCourier;
  BankModel selectedPayment;
  OrderResponse orderResponse;
  int totalBayar = 0;
  OrderBody body;
  List<ProductCartModel> details = List();
  int selectedIndex = 0;
  final keyOne = GlobalKey();
  final keyTwo = GlobalKey();
  final keyThree = GlobalKey();
  final keyFour = GlobalKey();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = CheckoutBloc(domain: _domain);
    loadAllCarts();
    _reqGetBanks();

    WidgetsBinding.instance.addPostFrameCallback((_) =>
        ShowCaseWidget.of(context)
            .startShowCase([keyOne, keyTwo, keyThree, keyFour]));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<CheckoutBloc, CheckoutState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqShippingLoading || state is ReqLoading) {
                  CustomDialog().loading(context,
                      "${getTranslated(context, "processing_request")}");
                } else if (state is GetBanksError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqGetBanks();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is GetBanksSuccess) {
                  Navigator.pop(context);
                  paymentMethods.clear();
                  paymentMethods.addAll(state.resp.data);
                }

                if (state is ReqShippingSuccess) {
                  Navigator.pop(context);
                  shippingResponse = state.resp;
                  selectedCourier = shippingResponse.data[0];
                  selectedTypeShipping = shippingResponse.data[0].costs[0];

                  totalBayar =
                      widget.total + selectedTypeShipping.cost[0].value;
                } else if (state is ReqShippingError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqGetShipping();
                  }, () {
                    Navigator.pop(context);
                  });
                }

                if (state is ReqOrderSuccess) {
                  Navigator.pop(context);
                  DatabaseHelper.instance.clearTable("cart");
                } else if (state is ReqOrderError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      "${getTranslated(context, state.error)}",
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqOrder();
                  }, () {
                    Navigator.pop(context);
                  });
                }
              },
              child: BlocBuilder<CheckoutBloc, CheckoutState>(
                bloc: _bloc,
                builder: (context, state) {
                  return SingleChildScrollView(
                    child: Stack(
                      children: [
                        ClipPath(
                            clipper: OvalBottomBorderClipper(),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil().setHeight(200),
                              color: primaryColor,
                            )),
                        Container(
                          margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(30),
                              left: ScreenUtil().setWidth(15)),
                          child: Text(
                            getTranslated(context, 'checkout'),
                            style: boldMuliFont.copyWith(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(18)),
                          ),
                        ),
                        (state is ReqOrderSuccess)
                            ? viewSuccessOrder(state)
                            : checkOut()
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _reqGetBanks() {
    _bloc.add(ReqListBank());
  }

  _reqGetShipping() {
    _bloc.add(ReqShipping(selectedLocation, details[0].store));
  }

  _reqOrder() {
    body = OrderBody(
        selectedLocation.id_address,
        details,
        selectedPayment.code,
        "",
        "${widget.total}",
        "${selectedTypeShipping.cost[0].value}",
        "${selectedTypeShipping.service}",
        "$totalBayar",
        GlobalHelper().formatTimestamp(
            GlobalHelper().getCurrentTimestamp(), "yyyy-MM-dd HH:mm:ss"));
    _bloc.add(ReqOrder(body));
  }

  loadAllCarts() {
    DatabaseHelper.instance.queryAllRows().then((value) {
      details.clear();
      for (var item in value) {
        CartDbModel it = CartDbModel().fromMap(item);
        details.add(ProductCartModel.insert(
            it.id_product, it.id_store, "${it.qty}", it.price));
      }

      setState(() {});
    });
  }

  Widget viewSuccessOrder(CheckoutState state) {
    orderResponse = state.props[0];
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
      child: Column(
        children: [
          //header
          (orderResponse.payment != null) ? payment(state) : Container(),
          (orderResponse.payment == null)
              ? Container(
                  padding: EdgeInsets.all(ScreenUtil().setSp(15)),
                  margin: EdgeInsets.only(
                      left: ScreenUtil().setHeight(15),
                      right: ScreenUtil().setHeight(15),
                      bottom: ScreenUtil().setHeight(15)),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 5,
                          offset: Offset(0, 3),
                          color: Colors.grey.withOpacity(0.2),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(12.0)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: ScreenUtil().setHeight(25),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(80),
                        height: ScreenUtil().setHeight(80),
                        child:
                            SvgPicture.asset("assets/images/ic_empty_cart.svg"),
                      ),
                      Text(
                        getTranslated(context, "thank"),
                        style: boldMuliFont.copyWith(
                            fontSize: ScreenUtil().setSp(25)),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(15),
                      ),
                      Center(
                        child: Text(
                            (state is ReqOrderSuccess)
                                ? "${state.resp.message}"
                                : "${getTranslated(context, "your_order_is_complete")}",
                            textAlign: TextAlign.center,
                            style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12),
                            )),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(15),
                      ),
                      InkWell(
                        onTap: () {
                          Get.offAllNamed("/mainPage");
                        },
                        child: Row(
                          children: [
                            Expanded(
                              child: Card(
                                color: primaryColor,
                                child: Center(
                                  child: Container(
                                      margin: EdgeInsets.all(
                                          ScreenUtil().setWidth(10)),
                                      child: Text(
                                        getTranslated(context, "back_to_home")
                                            .toUpperCase(),
                                        style: boldTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(12),
                                            color: Colors.white),
                                      )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      )
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Widget payment(CheckoutState state) {
    orderResponse = state.props[0];
    return Container(
      child: Column(
        children: [
          //header
          Container(
            padding: EdgeInsets.all(ScreenUtil().setSp(15)),
            margin: EdgeInsets.only(
                left: ScreenUtil().setHeight(15),
                right: ScreenUtil().setHeight(15),
                bottom: ScreenUtil().setHeight(15)),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ],
                borderRadius: BorderRadius.circular(12.0)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                ),
                Text(
                  orderResponse.payment.bank_code,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                Text("${getTranslated(context, "complete_order_before")}",
                    style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12),
                    )),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                Text(
                  f.format(DateTime.parse(orderResponse.expired)),
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                Text("${getTranslated(context, "amount_should_paid")}",
                    style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12),
                    )),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                Text(
                  "Rp.${GlobalHelper().formattingNumber(orderResponse.payment.transfer_amount)}",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                      color: Colors.red),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(30),
                ),
                InkWell(
                  onTap: () async {
                    if (await canLaunch(orderResponse.url)) {
                      await launch(orderResponse.url);
                    } else {
                      throw 'Could not launch $orderResponse.url';
                    }
                  },
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          color: primaryColor,
                          child: Center(
                            child: Container(
                                margin:
                                    EdgeInsets.all(ScreenUtil().setWidth(10)),
                                child: Text(
                                  "${getTranslated(context, "pay_now")}"
                                      .toUpperCase(),
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      color: Colors.white),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.0,
                )
              ],
            ),
          ),

          InkWell(
            onTap: () {
              Get.offAllNamed("/mainPage");
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Card(
                    color: primaryColor,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                          child: Text(
                            "${getTranslated(context, "back_to_home")}"
                                .toUpperCase(),
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: Colors.white),
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),
          // for (DetailInstruction how in orderResponse.instruction)
          //   Container(
          //     padding: EdgeInsets.all(ScreenUtil().setSp(15)),
          //     margin: EdgeInsets.only(
          //         left: ScreenUtil().setHeight(15),
          //         right: ScreenUtil().setHeight(15),
          //         bottom: ScreenUtil().setHeight(5)),
          //     width: MediaQuery.of(context).size.width,
          //     decoration: BoxDecoration(
          //         color: Colors.white,
          //         boxShadow: [
          //           BoxShadow(
          //             spreadRadius: 2,
          //             blurRadius: 5,
          //             offset: Offset(0, 3),
          //             color: Colors.grey.withOpacity(0.2),
          //           ),
          //         ],
          //         borderRadius: BorderRadius.circular(12.0)),
          //     child: ExpandablePanel(
          //       header: Text(how.name,
          //           style:
          //               TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
          //       expanded: Html(data: how.text),
          //       tapHeaderToExpand: true,
          //       hasIcon: false,
          //     ),
          //   )
        ],
      ),
    );
  }

  Widget checkOut() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(ScreenUtil().setSp(15)),
            margin: EdgeInsets.only(
                left: ScreenUtil().setHeight(15),
                right: ScreenUtil().setHeight(15),
                bottom: ScreenUtil().setHeight(15)),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ],
                borderRadius: BorderRadius.circular(12.0)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                ),
                _shippingAddressSection(),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                _shippingSection(),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                _paymentMethodSection(),
                // Showcase(
                //     key: keyFour,
                //     title: '${getTranslated(context, "step_4")}',
                //     description:
                //         '${getTranslated(context, "click_here_to_complete")}',
                //     showArrow: false,
                //     child: Container())
                _newContainer()
              ],
            ),
          ),
          _viewTotal()
        ],
      ),
    );
  }

  _shippingAddressSection() {
    return CustomShowcase(
      globalKey: keyOne,
      title: '${getTranslated(context, "step_1")}',
      description:
          '${getTranslated(context, "please_select_shipping_address")}',
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            getTranslated(context, "shippinga"),
            style: boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(14)),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(15),
          ),
          InkWell(
            onTap: () async {
              AddressesModel result = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ListAddressPage(
                          needRefresh: true,
                        )),
              );
              if (result != null) {
                selectedLocation = result;
                setState(() {});
                _reqGetShipping();
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    flex: 8,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        (selectedLocation == null)
                            ? SizedBox()
                            : Text(
                                selectedLocation.phone,
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(10),
                                    color: Color(0xff979797)),
                              ),
                        Text(
                          (selectedLocation == null)
                              ? getTranslated(context, 'choosea')
                              : selectedLocation.address,
                          style: normalMuliFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        )
                      ],
                    )),
                Expanded(
                    flex: 2,
                    child: Container(
                      margin: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.arrow_forward_ios,
                          size: ScreenUtil().setWidth(15),
                        ),
                      ),
                    ))
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Divider(),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
        ],
      ),
    );
  }

  _shippingSection() {
    return CustomShowcase(
      globalKey: keyTwo,
      title: '${getTranslated(context, "step_2")}',
      description: '${getTranslated(context, "choose_courier")}',
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            // TODO: NANTI
            getTranslated(context, 'shipping'),
            style: boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(14)),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(15),
          ),
          GestureDetector(
            onTap: () {
              if (selectedLocation == null) {
                FlushBarMessage().error(context, "Opps",
                    getTranslated(context, "choose_address_first"), 2);
                return;
              }

              _showCourier();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    flex: 12,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            (selectedTypeShipping == null)
                                ? SizedBox()
                                : Image.asset(
                                    'assets/images/' +
                                        selectedCourier.name +
                                        '.png',
                                    width: 50),
                            SizedBox(
                              width: 5,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  (selectedCourier == null)
                                      ? getTranslated(context, 'cshipping')
                                      : "${selectedCourier.code.toUpperCase()}",
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12)),
                                ),
                                // TODO: 1
                                Text(
                                  (selectedCourier == null)
                                      ? "-"
                                      : "${selectedTypeShipping.service} ${(selectedCourier.name.toLowerCase() == "kargo" || selectedCourier.name.toLowerCase() == "instant") ? "" : "(Rp. ${GlobalHelper().formattingNumber(selectedTypeShipping.cost[0].value)} - ${selectedTypeShipping.cost[0].etd} days)"}",
                                  style: normalMuliFont.copyWith(
                                      fontSize: ScreenUtil().setSp(10)),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    )),
                Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(right: ScreenUtil().setWidth(5)),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.arrow_forward_ios,
                          size: ScreenUtil().setWidth(15),
                        ),
                      ),
                    ))
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Divider(),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
        ],
      ),
    );
  }

  _paymentMethodSection() {
    return CustomShowcase(
        globalKey: keyThree,
        title: '${getTranslated(context, "step_3")}',
        description: '${getTranslated(context, "choose_payment_method")}',
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              getTranslated(context, 'checkout'),
              style: boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(14)),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(15),
            ),

            InkWell(
              onTap: () {
                _showListBank();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      flex: 8,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          (selectedPayment == null)
                              ? Text(getTranslated(context, 'cpay'))
                              : Row(
                                  children: [
                                    Image.asset(
                                        'assets/images/' +
                                            selectedPayment.code.toLowerCase() +
                                            '.png',
                                        height: 20),
                                    SizedBox(width: 10),
                                    Text(
                                      "${selectedPayment.name.toUpperCase()}",
                                      style: normalMuliFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                    ),
                                  ],
                                )
                        ],
                      )),
                  Expanded(
                      flex: 2,
                      child: Container(
                        margin:
                            EdgeInsets.only(right: ScreenUtil().setWidth(10)),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: ScreenUtil().setWidth(15),
                          ),
                        ),
                      ))
                ],
              ),
            ),
            // ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Divider(),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
          ],
        ));
  }

  _viewTotal() {
    return Column(
      children: [
        SizedBox(
          height: ScreenUtil().setHeight(35),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(
                "Subtotal",
                style:
                    normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              )),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Rp.${GlobalHelper().formattingNumber(widget.total)}",
                  style:
                      boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                ),
              )),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(
                getTranslated(context, 'shipping'),
                style:
                    normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              )),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  (selectedTypeShipping == null)
                      ? "Rp.0"
                      : "Rp.${GlobalHelper().formattingNumber(selectedTypeShipping.cost[0].value)}",
                  style:
                      boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                ),
              )),
            ],
          ),
        ),
        (selectedTypeShipping?.service == 'Expedisi mandiri' ||
                selectedTypeShipping?.service == 'Gojek' ||
                selectedTypeShipping?.service == 'Grab')
            ? Container(
                margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(15),
                    top: 10,
                    right: ScreenUtil().setWidth(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                        child: Text(
                      selectedTypeShipping.description,
                      style: normalTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(12)),
                    )),
                  ],
                ),
              )
            : Container(),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Divider(),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(
                "Total",
                style: boldTextFont.copyWith(
                    fontSize: ScreenUtil().setSp(12), color: primaryColor),
              )),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Rp. ${GlobalHelper().formattingNumber(totalBayar)}",
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12), color: primaryColor),
                ),
              )),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(35),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: InkWell(
            onTap: () {
              if (selectedCourier == null ||
                  selectedTypeShipping == null ||
                  selectedLocation == null ||
                  selectedPayment == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "fill_all_form"), 2);
                return;
              }
              _reqOrder();
            },
            child: Row(
              children: [
                _newContainer(),
                Expanded(
                  child: Card(
                    color: primaryColor,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                          child: Text(
                            getTranslated(context, 'placeo').toUpperCase(),
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: Colors.white),
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(25),
        )
      ],
    );
  }

  // TODO: 2
  void _showCourier() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (builder) {
          return new SingleChildScrollView(
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: transparent,
                child: new Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15))),
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "${getTranslated(context, "choose_courier")}",
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(16),
                                    color: primaryColor),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // TODO: 3
                              for (DataShippingModel dt
                                  in shippingResponse.data)
                                itemShipping(dt, null)
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                        ],
                      ),
                    )),
              );
            }),
          );
        });
  }

  // // TODO: 4
  void _showTypeShip(DataShippingModel selectedCourier) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (builder) {
          return new SingleChildScrollView(
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: transparent,
                child: new Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15))),
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "${getTranslated(context, "choose_courier")}",
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(16),
                                    color: primaryColor),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              for (MasterCostModel dt in selectedCourier.costs)
                                itemShipping(dt, selectedCourier)
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          )
                        ],
                      ),
                    )),
              );
            }),
          );
        });
  }

  Widget itemShipping(dynamic data, DataShippingModel dataCourier) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
      child: InkWell(
        onTap: () {
          if (data is DataShippingModel) {
            Navigator.of(context, rootNavigator: true).pop();
            _showTypeShip(data);
            // ShowCourierScreen(selectedCouriers: shippingResponse.data);
          } else if (data is BankModel) {
            selectedPayment = data;
            setState(() {});
            Navigator.of(context, rootNavigator: true).pop();
          } else {
            selectedCourier = dataCourier;
            selectedTypeShipping = data;
            totalBayar = widget.total + selectedTypeShipping.cost[0].value;
            setState(() {});
            Navigator.of(context, rootNavigator: true).pop();
          }
        },
        child: Row(
          children: [
            (data is DataShippingModel)
                ? Image.asset('assets/images/' + data.name + '.png', width: 60)
                : (data is BankModel)
                    ? Image.asset(
                        'assets/images/' +
                            "${data.code.toLowerCase()}" +
                            '.png',
                        width: 60)
                    : SizedBox(),
            SizedBox(width: 10),
            Text(
              (data is DataShippingModel)
                  ? "${data.name.toUpperCase()}"
                  : (data is BankModel)
                      ? "${data.name.toUpperCase()}"
                      : (data.cost[0].value == 0)
                          ? "${data.service}"
                          : "${data.service} (Rp. ${GlobalHelper().formattingNumber(data.cost[0].value)} - ${data.cost[0].etd} days)",
              style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
            ),
          ],
        ),
      ),
    );
  }

  void _showListBank() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (builder) {
          return new SingleChildScrollView(
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: transparent,
                child: new Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15))),
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "${getTranslated(context, "choose_payment_method")}",
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(16),
                                    color: primaryColor),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              for (BankModel dt in paymentMethods)
                                itemShipping(dt, null)
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                        ],
                      ),
                    )),
              );
            }),
          );
        });
  }

  _newContainer() {
    return Showcase(
        key: keyFour,
        title: '${getTranslated(context, "step_4")}',
        description: '${getTranslated(context, "click_here_to_complete")}',
        descTextStyle:
            TextStyle(fontSize: ScreenUtil().setSp(14), color: Colors.white),
        contentPadding: EdgeInsets.all(20),
        showcaseBackgroundColor: primaryColor,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        child: Container());
  }
}

class CustomShowcase extends StatelessWidget {
  final Widget child;
  final String description;
  final GlobalKey globalKey;
  final String title;

  const CustomShowcase({
    @required this.description,
    @required this.globalKey,
    @required this.child,
    this.title,
  });

  @override
  Widget build(BuildContext context) => Showcase(
        title: title,
        key: globalKey,
        description: description,
        descTextStyle:
            TextStyle(fontSize: ScreenUtil().setSp(14), color: Colors.white),
        contentPadding: EdgeInsets.all(20),
        showcaseBackgroundColor: primaryColor,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        child: child,
      );
}
