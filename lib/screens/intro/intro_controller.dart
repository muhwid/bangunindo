
import 'package:e_commerce_app/base/box_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IntroController extends GetxController {
  PageController pageController;
  RxInt currentIndex = 0.obs;
  var kDuration = const Duration(milliseconds: 300);
  var kCurve = Curves.ease;
  RxBool isLogin = false.obs;

  @override
  void onInit() {
    pageController = PageController();

    if(BoxStorage().getLogin() == null) {
      isLogin.value = false;
    }else{
      isLogin.value = BoxStorage().getLogin();
    }

    super.onInit();
  }

  void navigatePage() async {
    if (isLogin.value) {
      Get.offAllNamed("/mainPage");
    } else {
      Get.offAllNamed("/loginPage");
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}