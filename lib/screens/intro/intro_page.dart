import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/screens/intro/intro_controller.dart';
import 'package:e_commerce_app/utils/component.dart';
import 'package:e_commerce_app/views/indicator_intro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class IntroPage extends StatelessWidget {
  IntroController ctrl = Get.put(IntroController());

  nextFunction() {
    ctrl.pageController.nextPage(duration: ctrl.kDuration, curve: ctrl.kCurve);
  }

  previousFunction() {
    ctrl.pageController
        .previousPage(duration: ctrl.kDuration, curve: ctrl.kCurve);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Obx(()=> Stack(
        children: <Widget>[
          PageView(
            controller: ctrl.pageController,
            onPageChanged: onChangedFunction,
            children: <Widget>[
              Container(
                  child: Image.asset(
                    "assets/images/intro_1.png",
                    fit: BoxFit.fill,
                  )),
              Container(
                  child: Image.asset(
                    "assets/images/intro_2.png",
                    fit: BoxFit.fill,
                  )),
              Container(
                  child: Image.asset(
                    "assets/images/intro_3.png",
                    fit: BoxFit.fill,
                  ))
            ],
          ),
          Visibility(child: Positioned(right : ScreenUtil().setWidth(15), top: ScreenUtil().setWidth(30),
              child: WidgetComponent.primaryButton(onPress: (){
                BoxStorage().setFirst(false);
                ctrl.navigatePage();
              }, title: "Next")), visible: ctrl.currentIndex.value == 2,),
          Positioned(
            bottom: ScreenUtil().setHeight(60),
            child: Container(
              width: Get.width,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Indicator(
                    positionIndex: 0,
                    currentIndex: ctrl.currentIndex.value,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Indicator(
                    positionIndex: 1,
                    currentIndex: ctrl.currentIndex.value,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Indicator(
                    positionIndex: 2,
                    currentIndex: ctrl.currentIndex.value,
                  ),
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }

  onChangedFunction(int index) {
    ctrl.currentIndex.value = index;
  }
}
