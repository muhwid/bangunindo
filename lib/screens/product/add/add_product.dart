import 'dart:convert';

import 'package:e_commerce_app/helper/decorations.dart';
import 'package:e_commerce_app/model/image_model.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/product/add/add_controller.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_category_horizontal.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as pth;

class AddProduct extends StatefulWidget {
  final ProductModel product;

  AddProduct({Key key, this.product}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddProductState();
  }
}

class AddProductState extends State<AddProduct> {
  var controller = AddController();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => initConfig());
    super.initState();
  }

  void initConfig() {
    if (widget.product != null) {
      print('product id ${widget.product.id_product}');
      controller.currentData.value = widget.product;
    }
    controller.initController();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Obx(() => Scaffold(
              appBar: WidgetComponent.appBar(
                  text: (widget.product != null)
                      ? "${getTranslated(context, "edit_product")}"
                      : "${getTranslated(context, "add_product")}"),
              backgroundColor: bgPage,
              body: Container(
                margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [_viewForm()],
                  ),
                ),
              ),
            )),
      ),
    );
  }

  _viewForm() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              _showChooseDialog();
            },
            child: (widget.product == null)
                ? _checkChangeImage()
                : _checkExistImage(),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "item_name")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            controller: controller.nameTxtCtrl.value,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            textAlign: TextAlign.left,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: "${getTranslated(context, "input_item_name")}",
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "category")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              _chooseCategoryDialog();
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: controller.categoryTxtCtrl.value,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: "${getTranslated(context, "choose_category")}",
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "desc_item")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            controller: controller.descriptionTxtCtrl.value,
            maxLines: null,
            minLines: 5,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            textAlign: TextAlign.left,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: "${getTranslated(context, "input_desc_item")}",
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "stock")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            controller: controller.stockTxtCtrl.value,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            textAlign: TextAlign.left,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: "${getTranslated(context, "input_stock")}",
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "item_weight")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            controller: controller.weightTxtCtrl.value,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            textAlign: TextAlign.left,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: "${getTranslated(context, "input_item_weight")}",
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "item_price")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            controller: controller.priceTxtCtrl.value,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            textAlign: TextAlign.left,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: "${getTranslated(context, "input_item_price")}",
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            "${getTranslated(context, "video_url")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            controller: controller.videoTxtCtrl.value,
            keyboardType: TextInputType.text,
            textAlign: TextAlign.left,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: "${getTranslated(context, "input_video_url")}",
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          InkWell(
            onTap: () {
              controller.submitProduct();
            },
            child: Container(
              width: Get.width,
              child: Card(
                color: primaryColor,
                child: Center(
                  child: Container(
                      margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                      child: Text(
                        "${getTranslated(context, "submit")}".toUpperCase(),
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12),
                            color: Colors.white),
                      )),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _chooseCategoryDialog() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(builder: (context, state) {
            return SingleChildScrollView(
                child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              decoration: CustomDecoration.createBox(),
              child: Padding(
                padding: EdgeInsets.all(ScreenUtil().setWidth(15)),
                child: Wrap(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${getTranslated(context, "choose_category")}',
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(16)),
                        ),
                        for (CategoryModel item in controller.categories)
                          itemCategoryHorizontal(item, () {
                            Get.back();
                            controller.selectedCategory.value = item;
                            controller.categoryTxtCtrl.value.text =
                                "${item.name}";
                          })
                      ],
                    )
                  ],
                ),
              ),
            ));
          });
        });
  }

  Future getImage(int type) async {
    PickedFile pickedFile;
    if (type == 0)
      pickedFile =
          await controller.picker.value.getImage(source: ImageSource.camera);
    else
      pickedFile =
          await controller.picker.value.getImage(source: ImageSource.gallery);

    File finalFile = File(pickedFile.path);

    print("pickedfile ${pickedFile.path}");

    final bytes = finalFile.readAsBytesSync();
    String img64 = base64Encode(bytes);
    controller.selectedImage.value = ImageModel(
        name: pth.basename(pickedFile.path),
        path: pickedFile.path,
        base64: img64);
  }

  void _showChooseDialog() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
      backgroundColor: Colors.white,
      context: Get.context,
      isScrollControlled: false,
      builder: (context) => Wrap(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: ScreenUtil().setWidth(35),
              right: ScreenUtil().setWidth(35),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: ScreenUtil().setHeight(40),
                ),
                InkWell(
                  onTap: () {
                    getImage(0);
                    Navigator.pop(context);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.camera),
                      SizedBox(
                        width: ScreenUtil().setWidth(5),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${getTranslated(context, "camera")}",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(14)),
                          ),
                          Text(
                            '${getTranslated(context, "camera_desc")}',
                            style: normalTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(10)),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(23),
                ),
                InkWell(
                  onTap: () {
                    getImage(1);
                    Navigator.pop(context);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.photo),
                      SizedBox(
                        width: ScreenUtil().setWidth(5),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            '${getTranslated(context, "gallery")}',
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(14)),
                          ),
                          Text(
                            '${getTranslated(context, "gallery_desc")}',
                            style: normalTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(10)),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(40),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _defaultImage() {
    return Card(
      child: Container(
        margin: EdgeInsets.all(ScreenUtil().setWidth(25)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset("assets/images/ic_add_image.svg"),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Text("${getTranslated(context, "additional_image")}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10))),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            Text(
              "${getTranslated(context, "additional_image_desc")}",
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  _checkExistImage() {
    return (controller.selectedImage.value.path != null)
        ? Image.file(
            File(controller.selectedImage.value.path),
            width: Get.width,
            height: ScreenUtil().setHeight(150),
            fit: BoxFit.cover,
          )
        : (widget.product.images.length > 0)
            ? Container(
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.circular(ScreenUtil().setWidth(10)),
                    color: Colors.white),
                child: FadeInImage.assetNetwork(
                  placeholder: "assets/images/img_placeholder.png",
                  image: widget.product.images[0],
                  fit: BoxFit.contain,
                ),
              )
            : _defaultImage();
  }

  _checkChangeImage() {
    return (controller.selectedImage.value.path != null)
        ? Image.file(
            File(controller.selectedImage.value.path),
            width: Get.width,
            height: ScreenUtil().setHeight(150),
            fit: BoxFit.cover,
          )
        : _defaultImage();
  }
}
