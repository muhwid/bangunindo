import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/add_product_body.dart';
import 'package:e_commerce_app/model/body/edit_product_body.dart';
import 'package:e_commerce_app/model/body/login_body.dart';
import 'package:e_commerce_app/model/image_model.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';

class AddController extends GetxController {
  RxBool isLoading = false.obs;
  RxString errorMessage = "".obs;
  RxString deviceName = "".obs;
  final box = GetStorage();

  RxList<CategoryModel> categories = List<CategoryModel>().obs;
  Rx<ImagePicker> picker = ImagePicker().obs;
  Rx<CategoryModel> selectedCategory = CategoryModel().obs;
  Rx<ImageModel> selectedImage = ImageModel().obs;

  Rx<ProductModel> currentData = ProductModel().obs;

  Rx<TextEditingController> nameTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> descriptionTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> categoryTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> stockTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> weightTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> priceTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> priceGrosirTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> minGrosirTxtCtrl = TextEditingController().obs;
  Rx<TextEditingController> videoTxtCtrl = TextEditingController().obs;

  Dio dio = ApiServices().launch();


  @override
  void onInit() {
    super.onInit();
  }

  void initController(){
    apiGetCategory();

    if(currentData.value.name != null){
      nameTxtCtrl.value.text = currentData.value.name;
      descriptionTxtCtrl.value.text = currentData.value.description;
      categoryTxtCtrl.value.text = currentData.value.category;
      stockTxtCtrl.value.text = currentData.value.stock;
      weightTxtCtrl.value.text = currentData.value.weight;
      priceTxtCtrl.value.text = currentData.value.price;
      videoTxtCtrl.value.text = (currentData.value.video != null) ? currentData.value.video : "";
    }
  }

  void submitProduct(){
    if(nameTxtCtrl.value.text.isEmpty){
      Get.snackbar("Gagal", "Nama Barang tidak boleh kosong", snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.all(ScreenUtil().setWidth(15)));
      return;
    }

    if(categoryTxtCtrl.value.text.isEmpty){
      Get.snackbar("Gagal", "Kategori tidak boleh kosong", snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.all(ScreenUtil().setWidth(15)));
      return;
    }

    if(descriptionTxtCtrl.value.text.isEmpty){
      Get.snackbar("Gagal", "Deskripsi tidak boleh kosong", snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.all(ScreenUtil().setWidth(15)));
      return;
    }

    if(stockTxtCtrl.value.text.isEmpty){
      Get.snackbar("Gagal", "Stock tidak boleh kosong", snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.all(ScreenUtil().setWidth(15)));
      return;
    }

    if(weightTxtCtrl.value.text.isEmpty){
      Get.snackbar("Gagal", "Berat tidak boleh kosong", snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.all(ScreenUtil().setWidth(15)));
      return;
    }

    if(priceTxtCtrl.value.text.isEmpty){
      Get.snackbar("Gagal", "Harga tidak boleh kosong", snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.all(ScreenUtil().setWidth(15)));
      return;
    }
    if(currentData.value.id_product != null){
      apiEditProduct(EditProductBody(currentData.value.id_product, nameTxtCtrl.value.text, descriptionTxtCtrl.value.text, selectedCategory.value.id_category,
          priceTxtCtrl.value.text, stockTxtCtrl.value.text, minGrosirTxtCtrl.value.text, priceGrosirTxtCtrl.value.text, weightTxtCtrl.value.text, selectedImage.value.base64,
          (videoTxtCtrl.value.text.isEmpty) ? "" : videoTxtCtrl.value.text));
    }else{
      apiAddProduct(AddProductBody(nameTxtCtrl.value.text, descriptionTxtCtrl.value.text, selectedCategory.value.id_category,
          priceTxtCtrl.value.text, stockTxtCtrl.value.text, minGrosirTxtCtrl.value.text, priceGrosirTxtCtrl.value.text, weightTxtCtrl.value.text, selectedImage.value.base64,
          (videoTxtCtrl.value.text.isEmpty) ? "" : videoTxtCtrl.value.text));
    }

  }

  void apiAddProduct(AddProductBody body) async {
    CustomDialog().loading(Get.context, "Adding Product...");
    var resp;
    try{
      resp = await dio.post("${Endpoint.api_add_product}", data: body.toJson());
      LoginResponse response = LoginResponse.fromJson(resp.data);
      Get.back();
      if(response.status.startsWith("20")){
        Get.back(result: true);
        showSnackSuccess('${getTranslated(Get.context, "success")}', '${getTranslated(Get.context, "success_add_product")}');
      }else{
        if(response.invalid != null) {
          CustomDialog().showDialogConfirms(Get.context, response.invalid, "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
            Get.back();
            apiAddProduct(body);
          }, (){
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(Get.context, response.message.toString(),"${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
            Get.back();
            apiAddProduct(body);
          }, (){
            Get.back();
          });
        }
      }
    }catch(e){
      Get.back();
      CustomDialog().showDialogConfirms(Get.context, "${e.toString()}", "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
        Get.back();
        apiAddProduct(body);
      }, (){
        Get.back();
      });
    }
  }

  void apiEditProduct(EditProductBody body) async {
    CustomDialog().loading(Get.context, "Updating Product...");
    var resp;
    try{
      resp = await dio.post("${Endpoint.api_edit_product}", data: body.toJson());
      LoginResponse response = LoginResponse.fromJson(resp.data);
      Get.back();
      if(response.status.startsWith("20")){
        Get.back(result: true);
        showSnackSuccess('success'.tr, 'Success updated product');
      }else{
        if(response.invalid != null) {
          CustomDialog().showDialogConfirms(Get.context, response.invalid, "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
            Get.back();
            apiEditProduct(body);
          }, (){
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(Get.context, response.message.toString(), "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
            Get.back();
            apiEditProduct(body);
          }, (){
            Get.back();
          });
        }
      }
    }catch(e){
      Get.back();
      CustomDialog().showDialogConfirms(Get.context, "${e.toString()}", "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
        Get.back();
        apiEditProduct(body);
      }, (){
        Get.back();
      });
    }
  }

  void apiGetCategory() async {
    CustomDialog().loading(Get.context, "${getTranslated(Get.context, "processing_request")}");
    var resp;
    try{
      resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_category}");
      Get.back();
      CategoryResponse response = CategoryResponse.fromJson(resp.data);
      if(response.status.startsWith("20")){
        categories.clear();
        categories.addAll(response.data);

        if(currentData.value != null){
          for(CategoryModel item in categories){
            if(currentData.value.category == item.id_category){
              selectedCategory.value = item;
              categoryTxtCtrl.value.text = selectedCategory.value.name;
            }
          }
        }
      }else{
        if(response.invalid != null) {
          CustomDialog().showDialogConfirms(Get.context, response.invalid, "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
            Get.back();
            apiGetCategory();
          }, (){
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(Get.context, response.message.toString(), "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
            Get.back();
            apiGetCategory();
          }, (){
            Get.back();
          });
        }
      }
    }catch(e){
      Get.back();
      CustomDialog().showDialogConfirms(Get.context, "${e.toString()}", "${getTranslated(Get.context, "retry")}", "${getTranslated(Get.context, "close")}", (){
        Get.back();
        apiGetCategory();
      }, (){
        Get.back();
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}