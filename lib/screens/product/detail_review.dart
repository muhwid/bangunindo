import 'package:e_commerce_app/model/object/comment_model.dart';
import 'package:e_commerce_app/model/object/detail_product_model.dart';
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/horizontal_bar.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_review.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailReviews extends StatefulWidget {

  final DetailProductModel resp;

  DetailReviews({Key key, this.resp}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DetailReviewState();
  }
}

class DetailReviewState extends State<DetailReviews> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                ClipPath(
                    clipper: OvalBottomBorderClipper(),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: ScreenUtil().setHeight(200),
                      color: primaryColor,
                    )),
                Container(
                  margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(30),
                      left: ScreenUtil().setWidth(15)),
                  child: Text(
                    "${getTranslated(context, "reviews")}",
                    style: boldMuliFont.copyWith(
                        color: Colors.white, fontSize: ScreenUtil().setSp(18)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(80)),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setHeight(15),
                            right: ScreenUtil().setHeight(15),
                            bottom: ScreenUtil().setHeight(15)),
                        child: Card(
                          child: Container(
                            margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                            child: _viewReview(),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(25),
                            right: ScreenUtil().setWidth(25)),
                        child: Column(
                          children: [
                            for (CommentModel dt in widget.resp.comments) itemReviews(dt)
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _viewReview() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 1,
                child: Text(
                  "${getTranslated(context, "reviews")}",
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(14), color: Colors.black),
                )),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailReviews()));
                      },
                      child: Text(
                        "${widget.resp.comments.length} ${getTranslated(context, "reviews")}",
                        style: normalTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(10),
                            color: Colors.red),
                      )),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: ScreenUtil().setHeight(10),
        ),
        Row(
          children: [
            Expanded(
                flex: 4,
                child: Column(
                  children: [
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                    HorizontalBar(widget.resp.excelent, "${getTranslated(context, "excelent")}", Colors.green),
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                    HorizontalBar(widget.resp.very, "${getTranslated(context, "very_good")}", Colors.red),
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                    HorizontalBar(widget.resp.average, "${getTranslated(context, "average")}", Colors.yellow),
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                    HorizontalBar(widget.resp.bad, "${getTranslated(context, "bad")}", Colors.grey),
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                  ],
                )),
            Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(
                      left: ScreenUtil().setWidth(15),
                      right: ScreenUtil().setWidth(5)),
                  child: SizedBox(
                    height: ScreenUtil().setHeight(120),
                    child: Card(
                      color: Color(0xffEEBD57),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "${widget.resp.review}",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(16),
                                color: Colors.black),
                          ),
                          Text(
                            "out of 5",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                )),
          ],
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        Text(
          "Based on ${widget.resp.comments.length} ${getTranslated(context, "reviews")}",
          style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(11)),
        ),
      ],
    );
  }
}
