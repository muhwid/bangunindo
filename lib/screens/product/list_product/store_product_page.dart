import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_bloc.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_state.dart';
import 'package:e_commerce_app/screens/product/detail_product/detail_product.dart';
import 'package:e_commerce_app/screens/product/list_product/bloc/list_product_bloc.dart';
import 'package:e_commerce_app/screens/product/list_product/bloc/list_product_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_product_grid.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import 'bloc/list_product_state.dart';

class StoreProductPage extends StatefulWidget {
  List<ProductModel> products = List();
  String title;
  String id;
  String last_seen;
  bool store;
  String image;
  StoreProductPage(
      {Key key,
      this.products,
      @required this.title,
      this.id,
      this.store,
      this.image,
      this.last_seen})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return StoreProductPageState();
  }
}

class StoreProductPageState extends State<StoreProductPage> {
  ListProductBloc _bloc;
  ApiDomain _domain;
  List<ProductModel> products = List();
  RxBool isLoadingCategory = false.obs;
  RxString errorMessage = "".obs;
  RxList<CategoryModel> categories = List<CategoryModel>().obs;
  Dio dio = ApiServices().launch();
  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = ListProductBloc(domain: _domain);

    if (widget.products != null) {
      print("product size ${products.length}");
      _bloc.add(ReqListAllProduct());
    } else if (widget.id != null && widget.store == null) {
      apiGetStore(widget.id);
      _reqLoadProduct();
    } else {
      _bloc.add(ReqListProductStore(widget.id));
    }

    super.initState();
  }

  _reqLoadProduct() {
    _bloc.add(ReqListProduct(widget.id));
  }

  void apiGetStore(id) async {
    isLoadingCategory = isLoadingCategory.toggle();
    final resp = await dio.get("${Endpoint.baseUrl}api/product/bystore/" + id);
    isLoadingCategory = isLoadingCategory.toggle();
    CategoryResponse response = CategoryResponse.fromJson(resp.data);
    if (response.status.startsWith("20")) {
      categories.clear();
      categories.addAll(response.data);
    } else {
      if (response.invalid != null)
        errorMessage.value = response.invalid;
      else
        errorMessage.value = response.message.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          appBar: WidgetComponent.appBar(text: widget.title),
          body: SingleChildScrollView(
            child: MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (BuildContext context) {
                    return _bloc;
                  },
                )
              ],
              child: BlocListener<ListProductBloc, ListProductState>(
                bloc: _bloc,
                listener: (context, state) {
                  if (state is ReqLoading) {
                    CustomDialog().loading(context,
                        "${getTranslated(context, "processing_request")}");
                  } else if (state is ReqListProductError) {
                    Navigator.pop(context);
                  } else if (state is ReqListProductSuccess) {
                    products.clear();
                    products.addAll(state.resp.data);
                    Navigator.pop(context);
                  }
                },
                child: BlocBuilder<ListProductBloc, ListProductState>(
                  bloc: _bloc,
                  builder: (context, state) {
                    return Container(
                      margin: (categories.length > 0)
                          ? EdgeInsets.all(ScreenUtil().setSp(10))
                          : EdgeInsets.all(ScreenUtil().setSp(0)),
                      child: (state is! ReqLoading)
                          ? (state is ReqListProductError)
                              ? message(state.error)
                              : Column(children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Image(
                                        image: CachedNetworkImageProvider(
                                            widget.image),
                                        height: 200,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        fit: BoxFit.cover),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(
                                  //       horizontal: 10),
                                  //   child: Row(
                                  //       crossAxisAlignment:
                                  //           CrossAxisAlignment.start,
                                  //       mainAxisAlignment:
                                  //           MainAxisAlignment.spaceBetween,
                                  //       children: [
                                  //         Text(
                                  //           'Terakhir dilihat',
                                  //           style: boldTextFont.copyWith(
                                  //               fontSize:
                                  //                   ScreenUtil().setSp(14),
                                  //               color: Colors.black),
                                  //         ),
                                  //         Text(widget.last_seen,
                                  //             style: normalTextFont.copyWith(
                                  //                 fontSize:
                                  //                     ScreenUtil().setSp(12),
                                  //                 color: Colors.grey))
                                  //       ]),
                                  // ),
                                  Container(
                                      child: GridView.count(
                                    crossAxisCount: 2,
                                    mainAxisSpacing: 3.0,
                                    //MENGATUR JARAK ANTARA OBJEK ATAS DAN BAWAH
                                    crossAxisSpacing: 3,
                                    //MENGATUR JARAK ANTARA OBJEK KIRI DAN KANAN
                                    childAspectRatio: 0.6,
                                    //A
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    children:
                                        List.generate(products.length, (index) {
                                      return itemProduct(
                                          products[index], context, () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailProduct(
                                                      id: products[index]
                                                          .id_product,
                                                    )));
                                      });
                                    }),
                                  ))
                                ])
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget message(text) {
    return Container(
        margin: EdgeInsets.all(ScreenUtil().setWidth(0)),
        width: MediaQuery.of(context).size.width,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset("assets/images/img_empty.svg"),
              SizedBox(
                height: ScreenUtil().setHeight(40),
              ),
              Text(
                text,
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
              )
            ]));
  }
}
