import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:flutter/material.dart';

import 'list_product_event.dart';
import 'list_product_state.dart';

class ListProductBloc extends Bloc<ListProductEvent, ListProductState> {
  final ApiDomain domain;

  ListProductBloc({@required this.domain}) : assert(domain != null);

  @override
  ListProductState get initialState => InitPage();

  @override
  Stream<ListProductState> mapEventToState(ListProductEvent event) async* {
    if (event is ReqListProduct) {
      yield ReqLoading();
      try {
        ProductResponse resp = await domain.reqProductByCat(event.id);
        if (resp.status.startsWith("20"))
          yield ReqListProductSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqListProductError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqListProductError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqListProductError(error: e.toString());
      }
    }

    if (event is ReqListProductStore) {
      yield ReqLoading();
      try {
        ProductResponse resp = await domain.reqProductByStore(event.id);
        if (resp.status.startsWith("20"))
          yield ReqListProductSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqListProductError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqListProductError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqListProductError(error: e.toString());
      }
    }
    
    if (event is ReqListAllProduct) {
      yield ReqLoading();
      try {
        ProductResponse resp = await domain.reqProductTrending();
        if (resp.status.startsWith("20"))
          yield ReqListProductSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqListProductError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqListProductError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqListProductError(error: e.toString());
      }
    }
  }
}
