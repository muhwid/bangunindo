import 'package:equatable/equatable.dart';

abstract class ListProductEvent extends Equatable {
  const ListProductEvent();

  @override
  List<Object> get props => [];
}

class ReqListProduct extends ListProductEvent {
  final String id;

  const ReqListProduct(this.id);

  @override
  List<Object> get props => [id];
}
class ReqListProductStore extends ListProductEvent {
  final String id;

  const ReqListProductStore(this.id);

  @override
  List<Object> get props => [id];
}
class ReqListAllProduct extends ListProductEvent {

  const ReqListAllProduct();

  @override
  List<Object> get props => [];
}
