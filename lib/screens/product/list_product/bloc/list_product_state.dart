
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:equatable/equatable.dart';

abstract class ListProductState extends Equatable {
  const ListProductState();
}

class InitPage extends ListProductState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends ListProductState {
  @override
  List<Object> get props => [];
}

class ReqListProductSuccess extends ListProductState {
  final ProductResponse resp;

  const ReqListProductSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqListProductError extends ListProductState {
  final String error;

  const ReqListProductError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqListProductError { error: $error }';
}