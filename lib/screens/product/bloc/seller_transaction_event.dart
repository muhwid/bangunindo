import 'package:equatable/equatable.dart';

abstract class SellerTransactionEvent extends Equatable {
  const SellerTransactionEvent();

  @override
  List<Object> get props => [];
}

class ReqSellerTransaction extends SellerTransactionEvent {
  const ReqSellerTransaction();

  @override
  List<Object> get props => [];
}
