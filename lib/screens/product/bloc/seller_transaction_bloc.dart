import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:flutter/material.dart';

import 'seller_transaction_event.dart';
import 'seller_transaction_state.dart';

class SellerTransactionBloc extends Bloc<SellerTransactionEvent, SellerTransactionState> {
  final ApiDomain domain;

  SellerTransactionBloc({@required this.domain}) : assert(domain != null);

  @override
  SellerTransactionState get initialState => InitPage();

  @override
  Stream<SellerTransactionState> mapEventToState(SellerTransactionEvent event) async* {
    if (event is ReqSellerTransaction) {
      yield ReqLoading();
      try {
        ListOrderResponse resp = await domain.reqSellerTransaction();
        if (resp.status.startsWith("20"))
          yield ReqSellerTransactionSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqSellerTransactionError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqSellerTransactionError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqSellerTransactionError(error: e.toString());
      }
    }
  }
}
