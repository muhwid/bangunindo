
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:e_commerce_app/model/response/va_response.dart';
import 'package:equatable/equatable.dart';

abstract class SellerTransactionState extends Equatable {
  const SellerTransactionState();
}

class InitPage extends SellerTransactionState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends SellerTransactionState {
  @override
  List<Object> get props => [];
}

class ReqSellerTransactionSuccess extends SellerTransactionState {
  final ListOrderResponse resp;

  const ReqSellerTransactionSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqSellerTransactionError extends SellerTransactionState {
  final String error;

  const ReqSellerTransactionError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqSellerTransactionError { error: $error }';
}