import 'package:e_commerce_app/model/body/wishlist_body.dart';
import 'package:equatable/equatable.dart';

abstract class DetailProductEvent extends Equatable {
  const DetailProductEvent();

  @override
  List<Object> get props => [];
}

class ReqDetailProduct extends DetailProductEvent {
  final String id;

  const ReqDetailProduct(this.id);

  @override
  List<Object> get props => [id];
}

class ReqAddWishlist extends DetailProductEvent {
  final WishlistBody body;

  const ReqAddWishlist(this.body);

  @override
  List<Object> get props => [body];
}
