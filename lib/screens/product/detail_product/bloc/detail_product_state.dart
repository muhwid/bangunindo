
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:equatable/equatable.dart';

abstract class DetailProductStates extends Equatable {
  const DetailProductStates();
}

class InitPage extends DetailProductStates{
  @override
  List<Object> get props => [];

}

class ReqLoading extends DetailProductStates {
  @override
  List<Object> get props => [];
}

class ReqDetailSuccess extends DetailProductStates {
  final DetailProductResponse resp;

  const ReqDetailSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqDetailError extends DetailProductStates {
  final String error;

  const ReqDetailError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DetailProductFailure { error: $error }';
}

class ReqWishlistSuccess extends DetailProductStates {
  const ReqWishlistSuccess();

  @override
  List<Object> get props => [];
}

class ReqWishlistError extends DetailProductStates {
  final String error;

  const ReqWishlistError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqWishlistError { error: $error }';
}