import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:flutter/material.dart';

import 'detail_product_event.dart';
import 'detail_product_state.dart';

class DetailProductBloc extends Bloc<DetailProductEvent, DetailProductStates> {
  final ApiDomain domain;

  DetailProductBloc({@required this.domain}) : assert(domain != null);

  @override
  DetailProductStates get initialState => InitPage();

  @override
  Stream<DetailProductStates> mapEventToState(DetailProductEvent event) async* {
    if (event is ReqDetailProduct) {
      yield ReqLoading();
      try {
        DetailProductResponse resp = await domain.reqDetailProduct(event.id);
        if (resp.status.startsWith("20"))
          yield ReqDetailSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqDetailError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqDetailError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqDetailError(error: e.toString());
      }
    }

    if (event is ReqAddWishlist) {
      yield ReqLoading();
      try {
        BaseResponse resp = await domain.reqAddWishlist(event.body);
        if (resp.status.startsWith("20"))
          yield ReqWishlistSuccess();
        else {
          if(resp.invalid != null){
            yield ReqWishlistError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqWishlistError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqWishlistError(error: e.toString());
      }
    }
  }
}
