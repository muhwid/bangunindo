import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/data/database_helper.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/wishlist_body.dart';
import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:e_commerce_app/model/object/comment_model.dart';
import 'package:e_commerce_app/model/object/detail_product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/chat/chat_screen.dart';
import 'package:e_commerce_app/screens/product/detail_review.dart';
import 'package:e_commerce_app/screens/product/list_product/store_product_page.dart';
import 'package:e_commerce_app/screens/shopping_cart/shopping_cart.dart';
import 'package:e_commerce_app/utils/horizontal_bar.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_review.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bloc/detail_product_bloc.dart';
import 'bloc/detail_product_event.dart';
import 'bloc/detail_product_state.dart';

class DetailProduct extends StatefulWidget {
  final String id;

  DetailProduct({Key key, @required this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DetailProductState();
  }
}

class DetailProductState extends State<DetailProduct> {
  List<CommentModel> reviews = List();
  DetailProductBloc _bloc;
  ApiDomain _domain;
  DetailProductModel data;
  // List<bool> _isWishlist = [];
  List<DetailProductModel> detail = List();
  var box = GetStorage();
  List<CartDbModel> carts = List();
  int total = 0;
  // var isWishlist = false.obs;

  @override
  void initState() {
    loadAllCarts();
    _domain = ApiDomain(ApiRepository());
    _bloc = DetailProductBloc(domain: _domain);

    _reqDetailProduct();

    super.initState();
  }

  loadAllCarts() {
    DatabaseHelper.instance.queryAllRows().then((value) {
      total = 0;
      carts.clear();
      for (var item in value) {
        carts.add(CartDbModel().fromMap(item));
        int harga = item["price"] * item["qty"];
        total = total + harga;
      }

      setState(() {});
    });
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.red,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<DetailProductBloc, DetailProductStates>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqLoading) {
                  CustomDialog().loading(
                      context, getTranslated(context, "processing_request"));
                } else if (state is ReqDetailError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      "${getTranslated(context, "failed_get")}",
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
//                    _reqLogin();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqDetailSuccess) {
                  Navigator.pop(context);
                  data = state.resp.data;
                }

                if (state is ReqWishlistError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqAddWishlist();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqWishlistSuccess) {
                  Navigator.pop(context);
                  FlushBarMessage().toast(
                      context,
                      "${getTranslated(context, "success")}",
                      "${getTranslated(context, "success_add_to_wishlist")}",
                      Colors.green,
                      2);
                }
              },
              child: BlocBuilder<DetailProductBloc, DetailProductStates>(
                bloc: _bloc,
                builder: (context, state) {
                  return (state is ReqLoading)
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : SingleChildScrollView(
                          child: newUi(),
                        );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _reqDetailProduct() {
    _bloc.add(ReqDetailProduct(widget.id));
  }

  _reqAddWishlist() {
    _bloc.add(ReqAddWishlist(WishlistBody(widget.id)));
  }

  Widget newUi() {
    return Stack(
      children: [
        ClipPath(
            // OvalClipper()
            clipper: OvalBottomBorderClipper(),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: ScreenUtil().setHeight(180),
              color: primaryColor,
            )),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15),
              top: ScreenUtil().setHeight(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(getTranslated(context, 'detail'),
                      style: boldTextFont.copyWith(
                          fontSize: ScreenUtil().setSp(18),
                          color: Colors.white)),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(
                    right: ScreenUtil().setWidth(8),
                    top: ScreenUtil().setWidth(10)),
                child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ShoppingCart()));
                    },
                    child: Stack(
                      children: [
                        Container(
                            width: ScreenUtil().setWidth(30),
                            height: ScreenUtil().setWidth(30),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child:
                                Icon(Icons.shopping_cart, color: Colors.red)),
                        (carts.length >= 1)
                            ? Container(
                                width: ScreenUtil().setWidth(30),
                                height: ScreenUtil().setWidth(30),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          right: ScreenUtil().setSp(0),
                                          top: ScreenUtil().setHeight(3)),
                                      width: ScreenUtil().setWidth(7),
                                      height: ScreenUtil().setWidth(7),
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      )),
                                ),
                              )
                            : SizedBox(),
                      ],
                    )),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(95),
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(ScreenUtil().setSp(20)),
                  child: Container(
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/img_placeholder.png",
                      image: (data.images.length > 0) ? data.images[0] : "",
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(25),
              ),
              Text(
                "${data.name}",
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(16)),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 5,
                    child: Row(
                      children: [
                        Text(
                          "Rp. ${GlobalHelper().formattingNumber(int.parse(data.price))}",
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(10),
                              color: primaryColor),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(5),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                        icon: Icon(Icons.chat, color: primaryColor),
                        onPressed: () {
                          if (data.store.user == BoxStorage().getId()) {
                            FlushBarMessage().toast(
                                context,
                                "${getTranslated(context, "error")}",
                                "${getTranslated(context, "deny_chat")}",
                                primaryColor,
                                2);
                          } else {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChatScreenPage(
                                        product: data,
                                        title: data.store.name)));
                          }
                        },
                        color: primaryColor),
                  ),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                        icon: box.read(data.id_product) != null
                            // icon: box.read("isWishlist")
                            ? Icon(Icons.favorite_rounded, color: primaryColor)
                            : Icon(Icons.favorite_border, color: primaryColor),
                        onPressed: () {
                          if (box.read(data.id_product) == null) {
                            setState(() {
                              box.writeIfNull(data.id_product, true);
                              _reqAddWishlist();
                              print("isWishlist selected");
                            });
                          } else {
                            setState(() {
                              box.remove(data.id_product);
                              _reqAddWishlist();
                              print("isWishlist removed");
                            });
                          }
                        },
                        color: primaryColor),
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => StoreProductPage(
                              id: data.store.id_store,
                              title: data.store.name,
                              store: true,
                              image: data.store.photo,
                              last_seen: data.store.last_seen)));
                },
                child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[200])),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 23.0,
                          backgroundImage: NetworkImage(data.store.photo != null
                              ? data.store.photo
                              : 'https://via.placeholder.com/150'),
                          backgroundColor: Colors.transparent,
                        ),
                        SizedBox(
                          width: ScreenUtil().setHeight(15),
                        ),
                        Text(
                          data.store.name,
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(15)),
                        )
                      ],
                    )),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(25),
              ),
              Text(
                getTranslated(context, 'desc'),
                style: boldTextFont.copyWith(
                    fontSize: ScreenUtil().setSp(14), color: Colors.black),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
              Html(
                data: "${data.description.replaceAll("\n", "<br>")}",
              ),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              _itemDetail("SKU", "${data.id_product}"),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              _itemDetail(
                  "${getTranslated(context, "store")}", "${data.store.name}"),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              _itemDetail(
                  "${getTranslated(context, "stock")}", "${data.stock}"),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              _itemDetail(
                  "${getTranslated(context, "weight")}", "${data.weight}"),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              _itemDetail("${getTranslated(context, "category")}",
                  "${data.category.name}"),
              (data.video.isNotEmpty)
                  ? SizedBox(
                      height: ScreenUtil().setHeight(15),
                    )
                  : Container(),
              (data.video.isNotEmpty)
                  ? GestureDetector(
                      onTap: () {
                        _launchURL(data.video);
                      },
                      child: _itemDetail("Video", "Lihat video"),
                    )
                  : Container(),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      flex: 1,
                      child: Text(
                        "${getTranslated(context, "reviews")}",
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(14),
                            color: Colors.black),
                      )),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: InkWell(
                            onTap: () {
                              Get.to(DetailReviews(
                                resp: data,
                              ));
                            },
                            child: Text(
                              "${data.comments.length} ${getTranslated(context, "reviews")}",
                              style: normalTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(10),
                                  color: Colors.red),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Row(
                children: [
                  Expanded(
                      flex: 4,
                      child: Column(
                        children: [
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          HorizontalBar(
                              data.excelent,
                              "${getTranslated(context, "excelent")}",
                              Colors.green),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          HorizontalBar(
                              data.very,
                              "${getTranslated(context, "very_good")}",
                              Colors.red),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          HorizontalBar(
                              data.average,
                              "${getTranslated(context, "average")}",
                              Colors.yellow),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          HorizontalBar(data.average,
                              "${getTranslated(context, "bad")}", Colors.grey),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                        ],
                      )),
                  Expanded(
                      flex: 2,
                      child: Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(15),
                            right: ScreenUtil().setWidth(5)),
                        child: SizedBox(
                          height: ScreenUtil().setHeight(120),
                          child: Card(
                            color: bgCard,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "${data.review}",
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(16),
                                      color: Colors.white),
                                ),
                                Text(
                                  "${getTranslated(context, "out_of")}" + " 5",
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setHeight(15),
              ),
              Container(
                child: Column(
                  children: [for (CommentModel dt in reviews) itemReviews(dt)],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              InkWell(
                onTap: () {
                  DatabaseHelper.instance.queryAllRows().then((value) {
                    if (value.isNotEmpty) {
                      var contain = value.where((element) =>
                          element['id_store'].toString() ==
                          data.store.id_store);
                      if (contain.isEmpty) {
                        FlushBarMessage().toast(
                            context,
                            "${getTranslated(context, "cannot_make_purchase")}",
                            "Failed Add ${data.name} to cart",
                            Colors.red,
                            2);
                      } else {
                        DatabaseHelper.instance
                            .getSpecificData(data.id_product)
                            .then((value) {
                          if (value.isNotEmpty) {
                            int currentQty = value[0]["qty"];
                            DatabaseHelper.instance.updateQty(
                                CartDbModel.insert(
                                    data.id_product,
                                    data.store.id_store,
                                    data.name,
                                    data.description,
                                    data.category.name,
                                    (data.brand == null) ? "" : data.brand,
                                    data.price,
                                    data.stock,
                                    data.min_grosir,
                                    data.grosir,
                                    data.weight,
                                    data.expired,
                                    data.created_at,
                                    (data.images.length > 0)
                                        ? data.images[0]
                                        : "",
                                    data.review,
                                    currentQty + 1));
                          } else {
                            var i = DatabaseHelper.instance.insert(
                                CartDbModel.insert(
                                    data.id_product,
                                    data.store.id_store,
                                    data.name,
                                    data.description,
                                    data.category.name,
                                    (data.brand == null) ? "" : data.brand,
                                    data.price,
                                    data.stock,
                                    data.min_grosir,
                                    data.grosir,
                                    data.weight,
                                    data.expired,
                                    data.created_at,
                                    (data.images.length > 0)
                                        ? data.images[0]
                                        : "",
                                    data.review,
                                    1));
                          }
                          loadAllCarts();
                          FlushBarMessage().toast(
                              context,
                              "${getTranslated(context, "success")}",
                              "${getTranslated(context, "success_add_product")} ${data.name}",
                              Colors.green,
                              2);
                        });
                      }
                    } else {
                      DatabaseHelper.instance
                          .getSpecificData(data.id_product)
                          .then((value) {
                        if (value.isNotEmpty) {
                          int currentQty = value[0]["qty"];
                          DatabaseHelper.instance.updateQty(CartDbModel.insert(
                              data.id_product,
                              data.store.id_store,
                              data.name,
                              data.description,
                              data.category.name,
                              (data.brand == null) ? "" : data.brand,
                              data.price,
                              data.stock,
                              data.min_grosir,
                              data.grosir,
                              data.weight,
                              data.expired,
                              data.created_at,
                              (data.images.length > 0) ? data.images[0] : "",
                              data.review,
                              currentQty + 1));
                        } else {
                          var i = DatabaseHelper.instance.insert(
                              CartDbModel.insert(
                                  data.id_product,
                                  data.store.id_store,
                                  data.name,
                                  data.description,
                                  data.category.name,
                                  (data.brand == null) ? "" : data.brand,
                                  data.price,
                                  data.stock,
                                  data.min_grosir,
                                  data.grosir,
                                  data.weight,
                                  data.expired,
                                  data.created_at,
                                  (data.images.length > 0)
                                      ? data.images[0]
                                      : "",
                                  data.review,
                                  1));
                        }
                        loadAllCarts();
                        FlushBarMessage().toast(
                            context,
                            "${getTranslated(context, "success")}",
                            "${getTranslated(context, "success_add_product")} ${data.name}",
                            Colors.green,
                            2);
                      });
                    }
                  });
                },
                child: Row(
                  children: [
                    Expanded(
                      child: Card(
                        color: Color(0xffFB4C5A),
                        child: Center(
                          child: Container(
                              margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                              child: Text(
                                getTranslated(context, 'addto').toUpperCase(),
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12),
                                    color: Colors.white),
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
            ],
          ),
        )
      ],
    );
  }

  _itemDetail(String title, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            flex: 1,
            child: Text(
              title,
              style: boldTextFont.copyWith(
                  fontSize: ScreenUtil().setSp(12), color: Colors.grey),
            )),
        Expanded(
            flex: 1,
            child: Text((title == 'Weight') ? value + " gram" : value,
                style: boldTextFont.copyWith(
                    fontSize: ScreenUtil().setSp(12), color: Colors.black))),
      ],
    );
  }

//  _itemColor(int index, Color color) {
//    return InkWell(
//        onTap: () {
//          selectedColor = index;
//          setState(() {});
//        },
//        child: Container(
//          height: ScreenUtil().setWidth(22),
//          width: ScreenUtil().setHeight(22),
//          margin: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
//          decoration: BoxDecoration(
//              shape: BoxShape.circle,
//              border: Border.all(
//                  width: (selectedColor == index) ? 2 : 0,
//                  color: Colors.black,
//                  style: BorderStyle.solid)),
//          child: Container(
//            margin: EdgeInsets.all(2),
//            width: ScreenUtil().setWidth(20),
//            height: ScreenUtil().setHeight(20),
//            decoration: BoxDecoration(
//              color: color,
//              shape: BoxShape.circle,
//            ),
//          ),
//        ));
//  }
}
