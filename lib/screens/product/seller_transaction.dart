import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/transaction_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/product/add/add_product.dart';
import 'package:e_commerce_app/screens/product/bloc/seller_transaction_bloc.dart';
import 'package:e_commerce_app/screens/product/bloc/seller_transaction_event.dart';
import 'package:e_commerce_app/screens/product/bloc/seller_transaction_state.dart';
import 'package:e_commerce_app/screens/transaction/detail_transaction/detail_transaction.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/widget/refresh_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class TransactionSeller extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TransactionSellerState();
  }
}

class TransactionSellerState extends State<TransactionSeller> {
  SellerTransactionBloc _bloc;
  ApiDomain _domain;
  List<DataListTransaction> transactions = List();
  // injectResult(){
  //   transactions.add(TransactionModel("https://muhwid.com/projects/bangunindo/images/img_besi.png", "Selesai", "LIVING ROOM", "08 Jul 2020", 82.23));
  //   transactions.add(TransactionModel("https://muhwid.com/projects/bangunindo/images/img_besi.png", "Diproses", "LIVING ROOM", "08 Jul 2020", 19.23));
  //   transactions.add(TransactionModel("https://muhwid.com/projects/bangunindo/images/img_besi.png", "Menunggu", "LIVING ROOM", "08 Jul 2020",  23.12));
  // }

  @override
  void initState() {
    //injectResult();
    _domain = ApiDomain(ApiRepository());
    _bloc = SellerTransactionBloc(domain: _domain);

    reqSeller();
    super.initState();
  }

  Future reqSeller() async {
    await Future.delayed(Duration(milliseconds: 200));

    setState(() {
      _bloc.add(ReqSellerTransaction());
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return RefreshWidget(
      onRefresh: reqSeller,
      child: Container(
        color: primaryColor,
        child: SafeArea(
            child: MultiBlocProvider(
                providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
                child: BlocListener<SellerTransactionBloc,
                        SellerTransactionState>(
                    bloc: _bloc,
                    listener: (context, state) {
                      if (state is ReqLoading) {
                        CustomDialog().loading(context,
                            "${getTranslated(context, "processing_request")}");
                      } else if (state is ReqSellerTransactionError) {
                        Navigator.pop(context);
                        // CustomDialog().showDialogConfirms(context, state.error, "Retry", "Close", (){
                        //   Navigator.of(context, rootNavigator: true).pop();
                        //   // _reqGetExplore();
                        // }, (){
                        //   Navigator.of(context, rootNavigator: true).pop();
                        // });
                      } else if (state is ReqSellerTransactionSuccess) {
                        Navigator.pop(context);
                        transactions.clear();
                        transactions?.addAll(state.resp.data);
                      }
                    },
                    child: BlocBuilder<SellerTransactionBloc,
                            SellerTransactionState>(
                        bloc: _bloc,
                        builder: (context, state) {
                          return Scaffold(
                            backgroundColor: bgPage,
                            body: Container(
                              margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                              child: SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "${getTranslated(context, "sales_transaction")}",
                                          style: boldMuliFont.copyWith(
                                              fontSize: ScreenUtil().setSp(16)),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color: primaryColor),
                                          child: IconButton(
                                              icon: Icon(Icons.refresh_rounded),
                                              color: Colors.white,
                                              onPressed: reqSeller),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().setHeight(15),
                                    ),
                                    (state is! ReqLoading)
                                        ? (state is ReqSellerTransactionError)
                                            ? message(state.error)
                                            : _viewTrending()
                                        : Container()
                                  ],
                                ),
                              ),
                            ),
                          );
                        })))),
      ),
    );
  }

  _viewTrending() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [
          for (DataListTransaction item in transactions) itemResult(item)
        ],
      ),
    );
  }

  itemResult(DataListTransaction item) {
    return InkWell(
        onTap: () async {
          var result = await Navigator.of(context).push(MaterialPageRoute(
              builder: (ctx) =>
                  DetailTransactionPage(item: item, store: true)));
          print(_bloc);
          setState(() {
            _bloc.add(ReqSellerTransaction());
          });
        },
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(ScreenUtil().setWidth(10)),
                          color: Colors.black),
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/images/img_placeholder.png",
                        image: (item.detail != null)
                            ? (item.detail[0].images.length > 0)
                                ? item.detail[0].images[0]
                                : ""
                            : "",
                        fit: BoxFit.contain,
                      ),
                    ),
                    flex: 2,
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Wrap(
                          children: [
                            Container(
                                width: ScreenUtil().setWidth(130),
                                padding: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(5),
                                    bottom: ScreenUtil().setHeight(5)),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  color: greyMenu,
                                ),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    item.status_text,
                                    style: boldTextFont.copyWith(
                                        fontSize: ScreenUtil().setSp(10),
                                        color: green),
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Text(
                          (item.detail != null)
                              ? item.detail[0].name
                              : "${getTranslated(context, "product_has_deleted")}",
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Text(
                          "${GlobalHelper().convertDateFromString(item.created_at)}",
                          style: normalMuliFont.copyWith(
                              fontSize: ScreenUtil().setSp(10)),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Text(
                          "Rp. ${GlobalHelper().formattingNumber(int.parse(item.total))}",
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(10),
                              color: primaryColor),
                        ),
                      ],
                    ),
                    flex: 6,
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
              ),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Divider(),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
            ],
          ),
        ));
  }

  Widget message(text) {
    return Container(
        margin: EdgeInsets.only(top: 100),
        width: MediaQuery.of(context).size.width,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset("assets/images/img_empty.svg"),
              SizedBox(
                height: ScreenUtil().setHeight(40),
              ),
              Text(
                text,
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
              )
            ]));
  }
}
