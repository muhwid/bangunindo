import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/forgot_pass/bloc/forgot_pass_bloc.dart';
import 'package:e_commerce_app/screens/forgot_pass/bloc/forgot_pass_event.dart';
import 'package:e_commerce_app/screens/otp_screen/otp_screen_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/forgot_pass_state.dart';

class ForgotPassPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ForgotPassPageState();
  }
}

class ForgotPassPageState extends State<ForgotPassPage> {
  ForgotPassBloc _bloc;
  ApiDomain _domain;
  TextEditingController phoneCtrl = new TextEditingController();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = ForgotPassBloc(domain: _domain);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
            backgroundColor: bgPage,
            body: MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (BuildContext context) {
                    return _bloc;
                  },
                )
              ],
              child: BlocListener<ForgotPassBloc, ForgotPassState>(
                bloc: _bloc,
                listener: (context, state) {
                  if (state is ReqLoading) {
                    CustomDialog().loading(context,
                        "${getTranslated(context, "processing_request")}");
                  } else if (state is ForgotPassError) {
                    Navigator.pop(context);
                    CustomDialog().showDialogConfirms(
                        context,
                        state.error,
                        "${getTranslated(context, "retry")}",
                        "${getTranslated(context, "close")}", () {
                      Navigator.pop(context);
                      _reqForgotPass();
                    }, () {
                      Navigator.pop(context);
                    });
                  } else if (state is ForgotPassSuccess) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OtpScreenPage(
                                phoneNUmber: phoneCtrl.text, isforgot: true)));
                  }
                },
                child: BlocBuilder<ForgotPassBloc, ForgotPassState>(
                  bloc: _bloc,
                  builder: (context, state) {
                    return SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: ScreenUtil().setHeight(75)),
                                  Center(
                                      child: Image.asset(
                                    URL.COMPANY_LOGO,
                                    width: 200.0,
                                  )),
                                  SizedBox(height: ScreenUtil().setHeight(85)),
                                  Text(
                                      getTranslated(context, "forgot_password")
                                          .toUpperCase(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: ScreenUtil().setSp(18))),
                                  SizedBox(
                                    height: ScreenUtil().setHeight(10),
                                  ),
                                  Text(
                                      "${getTranslated(context, "please_enter_email")}",
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                  SizedBox(
                                    height: ScreenUtil().setHeight(10),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(15),
                                        bottom: ScreenUtil().setHeight(8),
                                        top: ScreenUtil().setHeight(16)),
                                    child: Text(
                                        "${getTranslated(context, "email")}*"
                                            .toUpperCase(),
                                        style: boldTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(10))),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      border: Border.all(color: Colors.grey),
                                      color: bgPage,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: ScreenUtil().setWidth(10),
                                          right: ScreenUtil().setWidth(10)),
                                      child: TextFormField(
                                        keyboardType: TextInputType.text,
                                        controller: phoneCtrl,
                                        textAlign: TextAlign.left,
                                        style: normalTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(12)),
                                        decoration: new InputDecoration(
                                            hintStyle: TextStyle(
                                                color: Color(0xffD1D1D1)),
                                            border: InputBorder.none,
                                            hintText:
                                                "${getTranslated(context, "input_email")}",
                                            fillColor: bgPage),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ScreenUtil().setHeight(20),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: WidgetComponent.primaryButton(
                                            onPress: () {
                                              _reqForgotPass();
                                            },
                                            title:
                                                "${getTranslated(context, "reset_your_password")}"
                                                    .toUpperCase()),
                                      )
                                    ],
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: ScreenUtil().setHeight(45),
                          ),
                          Center(
                              child: InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 35,
                                    child: Center(
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                              text: getTranslated(context,
                                                  "already_have_account"),
                                              style: normalTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                          TextSpan(
                                              text: getTranslated(
                                                      context, "sign_in")
                                                  .toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  color: primaryColor,
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ]),
                                      ),
                                    ),
                                  ))),
                          SizedBox(
                            height: ScreenUtil().setHeight(50),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            )),
      ),
    );
  }

  _reqForgotPass() {
    if (phoneCtrl.text.isEmpty) {
      FlushBarMessage().error(context, "Oops",
          "${getTranslated(context, "please_enter_email")}", 2);
      return;
    }

    _bloc.add(ReqForgotPass(phoneCtrl.text));
  }
}
