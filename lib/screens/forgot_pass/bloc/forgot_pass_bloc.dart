import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:flutter/material.dart';

import 'forgot_pass_event.dart';
import 'forgot_pass_state.dart';

class ForgotPassBloc extends Bloc<ForgotPassEvent, ForgotPassState> {
  final ApiDomain domain;

  ForgotPassBloc({@required this.domain}) : assert(domain != null);

  @override
  ForgotPassState get initialState => InitPage();

  @override
  Stream<ForgotPassState> mapEventToState(ForgotPassEvent event) async* {
    if (event is ReqForgotPass) {
      yield ReqLoading();
      try {
        BaseResponse resp = await domain.reqForgotPass(event.phone);
        if (resp.status.startsWith("20"))
          yield ForgotPassSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ForgotPassError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ForgotPassError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ForgotPassError(error: e.toString());
      }
    }
  }
}
