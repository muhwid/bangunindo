
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:equatable/equatable.dart';

abstract class ForgotPassState extends Equatable {
  const ForgotPassState();
}

class InitPage extends ForgotPassState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends ForgotPassState {
  @override
  List<Object> get props => [];
}

class ForgotPassSuccess extends ForgotPassState {
  final BaseResponse resp;

  const ForgotPassSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ForgotPassError extends ForgotPassState {
  final String error;

  const ForgotPassError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}