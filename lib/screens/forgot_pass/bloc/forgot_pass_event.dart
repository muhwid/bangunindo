import 'package:e_commerce_app/model/body/login_body.dart';
import 'package:equatable/equatable.dart';

abstract class ForgotPassEvent extends Equatable {
  const ForgotPassEvent();

  @override
  List<Object> get props => [];
}

class ReqForgotPass extends ForgotPassEvent {
  final String phone;

  const ReqForgotPass(this.phone);

  @override
  List<Object> get props => [phone];
}
