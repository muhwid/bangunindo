import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/login_body.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/forgot_pass/forgot_pass_page.dart';
import 'package:e_commerce_app/screens/login/login_controller.dart';
import 'package:e_commerce_app/screens/register/register_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginController ctrl = Get.put(LoginController());

  bool _showPassword = true;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: Obx(() => SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: ScreenUtil().setHeight(75)),
                            Center(
                                child: Image.asset(
                              URL.COMPANY_LOGO,
                              width: 200.0,
                            )),
                            SizedBox(height: ScreenUtil().setHeight(85)),
                            Text(
                                getTranslated(context, "sign_in").toUpperCase(),
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(18))),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(15),
                                  bottom: ScreenUtil().setHeight(8),
                                  top: ScreenUtil().setHeight(16)),
                              child: Text(
                                  "${getTranslated(context, "phone_number")}*"
                                      .toUpperCase(),
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(10))),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(color: Colors.grey),
                                color: bgPage,
                              ),
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(10),
                                    right: ScreenUtil().setWidth(10)),
                                child: TextFormField(
                                  keyboardType: TextInputType.number,
                                  controller: ctrl.phoneController.value,
                                  textAlign: TextAlign.left,
                                  style: normalTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12)),
                                  decoration: new InputDecoration(
                                      hintStyle:
                                          TextStyle(color: Color(0xffD1D1D1)),
                                      border: InputBorder.none,
                                      hintText:
                                          "${getTranslated(context, "input_phone_number")}",
                                      fillColor: bgPage),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(15),
                                  bottom: ScreenUtil().setHeight(8),
                                  top: ScreenUtil().setHeight(16)),
                              child: Text(
                                  "${getTranslated(context, "password").toUpperCase()}*",
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(10))),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(color: Colors.grey),
                                color: bgPage,
                              ),
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(10),
                                    right: ScreenUtil().setWidth(10)),
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  obscureText: _showPassword,
                                  controller: ctrl.passController.value,
                                  textAlign: TextAlign.left,
                                  style: normalTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12)),
                                  decoration: new InputDecoration(
                                      hintStyle:
                                          TextStyle(color: Color(0xffD1D1D1)),
                                      border: InputBorder.none,
                                      hintText:
                                          "${getTranslated(context, "input_password")}",
                                      fillColor: bgPage,
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _showPassword = !_showPassword;
                                          });
                                        },
                                        child: Icon(
                                          _showPassword
                                              ? Icons.visibility_off_outlined
                                              : Icons.visibility_outlined,
                                          color: primaryColor,
                                        ),
                                      )),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: ScreenUtil().setHeight(15),
                                  bottom: ScreenUtil().setHeight(15)),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ForgotPassPage()));
                                  },
                                  child: Text(
                                    "${getTranslated(context, "forgot_password")}?",
                                    style: boldTextFont.copyWith(
                                        fontSize: ScreenUtil().setSp(11)),
                                  ),
                                ),
                              ),
                            ),
                            (ctrl.isLoading.value)
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : Row(
                                    children: [
                                      Expanded(
                                        child: WidgetComponent.primaryButton(
                                            onPress: () {
                                              _reqLogin(context);
                                            },
                                            title: getTranslated(
                                                    context, "sign_in")
                                                .toUpperCase()),
                                      )
                                    ],
                                  ),
                            Visibility(
                              child: Text(
                                "${ctrl.errorMessage.value}",
                                style: normalTextFont.copyWith(
                                    color: Colors.red,
                                    fontSize: ScreenUtil().setSp(11)),
                              ),
                              visible: ctrl.errorMessage.value != "",
                            )
                          ],
                        )),
                    SizedBox(
                      height: ScreenUtil().setHeight(25),
                    ),
                    Center(
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => RegisterPage()));
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 35,
                              child: Center(
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                      text:
                                          "${getTranslated(context, "dont_have_account")}",
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10)),
                                    ),
                                    TextSpan(
                                      text: getTranslated(context, "sign_up")
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10),
                                          color: primaryColor),
                                    ),
                                  ]),
                                ),
                              ),
                            ))),
                    SizedBox(
                      height: ScreenUtil().setHeight(50),
                    )
                  ],
                ),
              )),
        ),
      ),
    );
  }

  _reqLogin(BuildContext context) {
    if (ctrl.phoneController.value.text.isEmpty) {
      FlushBarMessage().error(context, "Oops",
          "${getTranslated(context, "phone_number_cant_empty")}", 2);
      return;
    }

    if (ctrl.passController.value.text.isEmpty) {
      FlushBarMessage().error(context, "Oops",
          "${getTranslated(context, "password_cant_empty")}", 2);
      return;
    }

    ctrl.apiLogin(LoginBody(ctrl.phoneController.value.text,
        ctrl.passController.value.text, "", ctrl.deviceName.value));
  }
}
