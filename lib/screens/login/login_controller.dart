import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/body/login_body.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  RxBool isLoading = false.obs;
  RxString errorMessage = "".obs;
  RxString deviceName = "".obs;
  final box = GetStorage();

  Rx<TextEditingController> phoneController = TextEditingController().obs;
  Rx<TextEditingController> passController = TextEditingController().obs;

  Dio dio = ApiServices().launch();

  getDeviceName() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceName.value = androidInfo.model;
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceName.value = iosInfo.utsname.machine;
    }
  }

  @override
  void onInit() {
    super.onInit();
  }

  void setErrorMessage(String msg) async {
    errorMessage.value = msg;
  }

  void apiLogin(LoginBody body) async {
    isLoading = isLoading.toggle();
    setErrorMessage("");
    var resp;
    try {
      resp = await dio.post("${Endpoint.api_login}", data: body.toJson());
      LoginResponse response = LoginResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        BoxStorage().setToken(response.data.token);
        BoxStorage().setLogin(true);
        BoxStorage().setId(response.data.id);
        BoxStorage().setName(response.data.name);
        Get.offAllNamed("/mainPage");
      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiLogin(body);
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiLogin(body);
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiLogin(body);
      }, () {
        Get.back();
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
