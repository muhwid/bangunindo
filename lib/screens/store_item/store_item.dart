import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/product/add/add_product.dart';
import 'package:e_commerce_app/screens/store_item/bloc/store_item_bloc.dart';
import 'package:e_commerce_app/screens/store_item/bloc/store_item_event.dart';
import 'package:e_commerce_app/screens/store_item/bloc/store_item_state.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class StoreItem extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StoreItemsState();
  }
}

class StoreItemsState extends State<StoreItem> {
  StoreItemBloc _bloc;
  ApiDomain _domain;
  List<ProductModel> transactions = List();

  @override
  void initState() {
    // injectResult();
    _domain = ApiDomain(ApiRepository());
    _bloc = StoreItemBloc(domain: _domain);

    reqStoreItem();
    super.initState();
  }

  void reqStoreItem() {
    _bloc.add(ReqStoreItem());
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (BuildContext context) {
                return _bloc;
              },
            )
          ],
          child: BlocListener<StoreItemBloc, StoreItemState>(
            bloc: _bloc,
            listener: (context, state) {
              if (state is ReqLoading) {
                CustomDialog().loading(
                    context, getTranslated(context, "processing_request"));
              } else if (state is ReqStoreItemError) {
                Navigator.pop(context);
                CustomDialog().showDialogConfirms(
                    context,
                    state.error,
                    "${getTranslated(context, "retry")}",
                    "${getTranslated(context, "close")}", () {
                  Navigator.of(context, rootNavigator: true).pop();
                  // _reqGetExplore();
                }, () {
                  Navigator.of(context, rootNavigator: true).pop();
                });
              } else if (state is ReqStoreItemSuccess) {
                Navigator.pop(context);
                transactions.clear();
                transactions?.addAll(state.resp.data);
              }
            },
            child: BlocBuilder<StoreItemBloc, StoreItemState>(
              bloc: _bloc,
              builder: (context, state) {
                return Scaffold(
                  backgroundColor: bgPage,
                  body: Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "${getTranslated(context, "list_item")}",
                              style: boldMuliFont.copyWith(
                                  fontSize: ScreenUtil().setSp(16)),
                            ),
                            InkWell(
                                onTap: () async {
                                  var result = await Get.to(AddProduct());
                                  if (result == true) {
                                    reqStoreItem();
                                  }
                                },
                                child: Text(
                                  "${getTranslated(context, "add")}",
                                  style: boldMuliFont.copyWith(fontSize: 14),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(15),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: _viewTrending(),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  _viewTrending() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [
          for (ProductModel item in transactions) itemTransaction(item, context)
        ],
      ),
    );
  }

  itemTransaction(ProductModel item, BuildContext context) {
    return InkWell(
      onTap: () async {
        var result = await Get.to(AddProduct(
          product: item,
        ));
        // setState(() {
        if (result == true) {
          _bloc.add(ReqStoreItem());
        }
        // });
      },
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(ScreenUtil().setWidth(10)),
                        color: Colors.black),
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/img_placeholder.png",
                      image: (item.images != null)
                          ? (item.images.length > 0)
                              ? item.images[0]
                              : ""
                          : "",
                      fit: BoxFit.contain,
                    ),
                  ),
                  flex: 2,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        (item.name != null)
                            ? item.name
                            : "${getTranslated(context, "product_has_deleted")}",
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12)),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(5),
                      ),
                      Text(
                        "Rp. ${GlobalHelper().formattingNumber(int.parse(item.price))}",
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(10),
                            color: primaryColor),
                      ),
                    ],
                  ),
                  flex: 6,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Divider(),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
          ],
        ),
      ),
    );
  }
}
