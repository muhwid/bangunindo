
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:equatable/equatable.dart';

abstract class StoreItemState extends Equatable {
  const StoreItemState();
}

class InitPage extends StoreItemState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends StoreItemState {
  @override
  List<Object> get props => [];
}

class ReqStoreItemSuccess extends StoreItemState {
  final ProductResponse resp;
  
  const ReqStoreItemSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqStoreItemError extends StoreItemState {
  final String error;

  const ReqStoreItemError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqStoreItemError { error: $error }';
}