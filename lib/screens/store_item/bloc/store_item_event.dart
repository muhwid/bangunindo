import 'package:equatable/equatable.dart';

abstract class StoreItemEvent extends Equatable {
  const StoreItemEvent();

  @override
  List<Object> get props => [];
}

class ReqStoreItem extends StoreItemEvent {
  const ReqStoreItem();

  @override
  List<Object> get props => [];
}
