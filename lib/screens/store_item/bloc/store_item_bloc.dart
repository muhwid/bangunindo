import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:flutter/material.dart';

import 'store_item_event.dart';
import 'store_item_state.dart';

class StoreItemBloc extends Bloc<StoreItemEvent, StoreItemState> {
  final ApiDomain domain;

  StoreItemBloc({@required this.domain}) : assert(domain != null);

  @override
  StoreItemState get initialState => InitPage();

  @override
  Stream<StoreItemState> mapEventToState(StoreItemEvent event) async* {
    if (event is ReqStoreItem) {
      yield ReqLoading();
      try {
        ProductResponse resp = await domain.reqStoreItem();
        if (resp.status.startsWith("20"))
          yield ReqStoreItemSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqStoreItemError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqStoreItemError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqStoreItemError(error: e.toString());
      }
    }
  }
}
