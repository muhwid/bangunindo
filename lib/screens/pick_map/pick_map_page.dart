import 'dart:ffi';

import 'package:e_commerce_app/model/object/shipping_location_model.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PickMapPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PickMapPageState();
  }
}

class PickMapPageState extends State<PickMapPage> {
  Completer<GoogleMapController> _controller = Completer();
  Position currentPositon;
  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 18.0,
  );
  String address = "";
  LatLng centerPos;

  @override
  void initState() {
    _getCurrentPos();
    super.initState();
  }

  Future<void> _goToCurrentPos() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kGooglePlex));
  }

  _getCurrentPos() async {
    currentPositon = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      _kGooglePlex = CameraPosition(
        target: LatLng(currentPositon.latitude, currentPositon.longitude),
        zoom: 18.0,
      );

      _goToCurrentPos();
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: Column(
            children: [
              Container(
                height: ScreenUtil().setHeight(55),
                padding: EdgeInsets.only(
                    left: ScreenUtil().setSp(20),
                    right: ScreenUtil().setSp(20)),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back_ios),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(15),
                    ),
                    Text(
                      "${getTranslated(context, "pick_address")}",
                      style: boldMuliFont.copyWith(
                          fontSize: ScreenUtil().setSp(16)),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Stack(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height -
                          ScreenUtil().setHeight(80),
                      child: GoogleMap(
                        mapType: MapType.normal,
                        initialCameraPosition: _kGooglePlex,
                        onMapCreated: (GoogleMapController controller) {
                          _controller.complete(controller);
                        },
                        onCameraMove: (CameraPosition position) {
                          print(
                              "position ${position.target.latitude} ${position.target.longitude}");
                          centerPos = LatLng(position.target.latitude,
                              position.target.longitude);
                        },
                        onCameraIdle: () {
                          Geocoder.local
                              .findAddressesFromCoordinates(Coordinates(
                                  centerPos.latitude, centerPos.longitude))
                              .then((value) {
                            address = value.first.addressLine;
                            print("address ${value.first.toMap().toString()}");
                            setState(() {});
                          });
                        },
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height -
                          ScreenUtil().setHeight(80),
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Icon(
                          Icons.location_on,
                          color: primaryColor,
                          size: ScreenUtil().setSp(30),
                        ),
                      ),
                    ),
                    Positioned(
                      top: ScreenUtil().setHeight(5),
                      left: ScreenUtil().setHeight(15),
                      right: ScreenUtil().setHeight(15),
                      child: Card(
                        child: Container(
                          margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                          child: Column(
                            children: [
                              Text(
                                address,
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12)),
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: WidgetComponent.primaryButton(
                                        onPress: () {
                                          Navigator.pop(
                                              context,
                                              ShippingLocationModel.insert(
                                                  address,
                                                  currentPositon.latitude,
                                                  currentPositon.longitude));
                                        },
                                        title: getTranslated(
                                                context, "use_this_address")
                                            .toUpperCase()),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                flex: 1,
              )
            ],
          ),
        ),
      ),
    );
  }
}
