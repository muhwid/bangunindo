import 'package:e_commerce_app/data/database_helper.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/checkout/checkout_page.dart';
import 'package:e_commerce_app/screens/main/main_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:showcaseview/showcase_widget.dart';

class ShoppingCart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ShoppingCartState();
  }
}

class ShoppingCartState extends State<ShoppingCart> {
  List<CartDbModel> carts = List();
  int total = 0;

  @override
  void initState() {
    loadAllCarts();
    super.initState();
  }

  loadAllCarts() {
    DatabaseHelper.instance.queryAllRows().then((value) {
      total = 0;
      carts.clear();
      for (var item in value) {
        carts.add(CartDbModel().fromMap(item));
        int harga = item["price"] * item["qty"];
        total = total + harga;
      }

      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                ClipPath(
                    clipper: OvalBottomBorderClipper(),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: ScreenUtil().setHeight(200),
                      color: primaryColor,
                    )),
                Container(
                  margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(30),
                      left: ScreenUtil().setWidth(15)),
                  child: Text(
                    (carts.length <= 0)
                        ? getTranslated(context, 'empty')
                        : getTranslated(context, 'shopping'),
                    style: boldMuliFont.copyWith(
                        color: Colors.white, fontSize: ScreenUtil().setSp(18)),
                  ),
                ),
                (carts.length <= 0) ? emptyCart() : viewCartList()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget emptyCart() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
      child: Column(
        children: [
          //header
          Container(
            padding: EdgeInsets.all(ScreenUtil().setSp(15)),
            margin: EdgeInsets.only(
                left: ScreenUtil().setHeight(15),
                right: ScreenUtil().setHeight(15),
                bottom: ScreenUtil().setHeight(15)),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ],
                borderRadius: BorderRadius.circular(12.0)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(80),
                  height: ScreenUtil().setHeight(80),
                  child: SvgPicture.asset("assets/images/ic_empty_cart.svg"),
                ),
                Text(
                  getTranslated(context, 'empty'),
                  style:
                      boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(25)),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                // Center(
                //   child: Text(getTranslated(context, 'its'),
                //       textAlign: TextAlign.center,
                //       style: normalTextFont.copyWith(
                //         fontSize: ScreenUtil().setSp(12),
                //       )),
                // ),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => MainPage()),
                        ModalRoute.withName("/Home"));
                  },
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          color: Color(0xffFB4C5A),
                          child: Center(
                            child: Container(
                                margin:
                                    EdgeInsets.all(ScreenUtil().setWidth(10)),
                                child: Text(
                                  getTranslated(context, 'browse')
                                      .toUpperCase(),
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      color: Colors.white),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.0,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _viewTotal() {
    return Column(
      children: [
        SizedBox(
          height: ScreenUtil().setHeight(35),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Divider(),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(
                "Total",
                style: boldTextFont.copyWith(
                    fontSize: ScreenUtil().setSp(12), color: primaryColor),
              )),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Rp. ${GlobalHelper().formattingNumber(total)}",
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12), color: primaryColor),
                ),
              )),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(35),
        ),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ShowCaseWidget(
                            builder: Builder(
                                builder: (_) => CheckoutPage(
                                      total: total,
                                    )),
                          )));
            },
            child: Row(
              children: [
                Expanded(
                  child: Card(
                    color: primaryColor,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                          child: Text(
                            getTranslated(context, 'checkout').toUpperCase(),
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: Colors.white),
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(25),
        )
      ],
    );
  }

  Widget viewCartList() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(95)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(ScreenUtil().setSp(15)),
            margin: EdgeInsets.only(
                left: ScreenUtil().setHeight(15),
                right: ScreenUtil().setHeight(15),
                bottom: ScreenUtil().setHeight(15)),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ],
                borderRadius: BorderRadius.circular(12.0)),
            child: Column(
              children: [
                for (int i = 0; i < carts.length; i++) itemCart(i, carts[i])
              ],
            ),
          ),
          _viewTotal()
        ],
      ),
    );
  }

  Widget itemCart(int index, CartDbModel item) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(7)),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setWidth(10)),
                      color: Colors.black),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/img_placeholder.png",
                    image: item.image,
                    fit: BoxFit.contain,
                  ),
                ),
                flex: 2,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Text(
                          item.name,
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        )),
                        Row(
                          children: [
                            Text(
                                "Rp. ${GlobalHelper().formattingNumber(int.parse(item.price))}",
                                style: boldTextFont.copyWith(
                                    color: primaryColor,
                                    fontSize: ScreenUtil().setSp(10))),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                    Wrap(
                      children: [
                        Container(
                            width: ScreenUtil().setWidth(80),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.black),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: ScreenUtil().setWidth(15),
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: InkWell(
                                        onTap: () {
                                          if (carts[index].qty > 0) {
                                            setState(() {
                                              carts[index].qty =
                                                  carts[index].qty - 1;
                                              DatabaseHelper.instance.updateQty(
                                                  CartDbModel.insert(
                                                      carts[index].id_product,
                                                      carts[index].id_store,
                                                      carts[index].name,
                                                      carts[index].description,
                                                      carts[index].category,
                                                      carts[index].brand,
                                                      carts[index].price,
                                                      carts[index].stock,
                                                      carts[index].min_grosir,
                                                      carts[index].grosir,
                                                      carts[index].weight,
                                                      carts[index].expired,
                                                      carts[index].created_at,
                                                      carts[index].image,
                                                      carts[index].review,
                                                      carts[index].qty));
                                            });
                                          }

                                          if (carts[index].qty == 0) {
                                            DatabaseHelper.instance.delete(
                                                carts[index].id_product);
                                          }

                                          loadAllCarts();
                                        },
                                        child: Container(
                                          child: Text(
                                            "-",
                                            style: boldTextFont.copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(12)),
                                          ),
                                        ),
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: Text(
                                        "${carts[index].qty}",
                                        style: boldTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(12)),
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            carts[index].qty =
                                                carts[index].qty + 1;
                                            DatabaseHelper.instance.updateQty(
                                                CartDbModel.insert(
                                                    carts[index].id_product,
                                                    carts[index].id_store,
                                                    carts[index].name,
                                                    carts[index].description,
                                                    carts[index].category,
                                                    carts[index].brand,
                                                    carts[index].price,
                                                    carts[index].stock,
                                                    carts[index].min_grosir,
                                                    carts[index].grosir,
                                                    carts[index].weight,
                                                    carts[index].expired,
                                                    carts[index].created_at,
                                                    carts[index].image,
                                                    carts[index].review,
                                                    carts[index].qty));
                                          });

                                          loadAllCarts();
                                        },
                                        child: Container(
                                          child: Text(
                                            "+",
                                            style: boldTextFont.copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(12)),
                                          ),
                                        ),
                                      )),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ],
                ),
                flex: 6,
              )
            ],
          ),
          SizedBox(
            height: ScreenUtil().setHeight(5),
          ),
          Container(
            margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(10),
                right: ScreenUtil().setWidth(10)),
            child: Divider(),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(5),
          )
        ],
      ),
    );
  }
}
