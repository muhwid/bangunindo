import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/object/detail_product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:dash_chat/dash_chat.dart';

class ChatScreenPage extends StatefulWidget {
  DetailProductModel product;
  String title;
  String room;
  String store_id;
  ChatScreenPage({Key key, this.product, this.title, this.room, this.store_id})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return ChatScreenPageState();
  }
}

class ChatScreenPageState extends State<ChatScreenPage> {
  final GlobalKey<DashChatState> _chatViewKey = GlobalKey<DashChatState>();

  List<ChatMessage> messages = List<ChatMessage>();
  var m = List<ChatMessage>();
  String doc;
  String chattId;
  var i = 0;
  Color bgclr;

  @override
  void initState() {
    super.initState();
    providerChatId();
  }

  providerChatId() async {
    if (widget.product != null && widget.product.id_product.isNotEmpty) {
      chattId = widget.product.store.user;
    } else if (widget.store_id != null) {
      chattId = widget.store_id;
    } else {
      var token = BoxStorage().getId();
      setState(() {
        chattId = token;
      });
    }
    if (widget.room != null) {
      doc = widget.room;
    } else {
      doc = 'user-' + BoxStorage().getId() + '#store-' + chattId;
    }
  }

  void systemMessage() {
    Timer(Duration(milliseconds: 300), () {
      if (i < 6) {
        setState(() {
          messages = [...messages, m[i]];
        });
        i++;
      }
      Timer(Duration(milliseconds: 300), () {
        _chatViewKey.currentState.scrollController
          ..animateTo(
            _chatViewKey.currentState.scrollController.position.maxScrollExtent,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
      });
    });
  }

  void onSend(ChatMessage message) async {
    var documentReference = Firestore.instance
        // .collection('rooms')
        // .document(doc)
        .collection('messages')
        .document(DateTime.now().millisecondsSinceEpoch.toString());

    var roomReference = Firestore.instance
        // .collection('rooms')
        // .document(doc)
        .collection('rooms')
        .document(doc);
    if (widget.room == null) {
      var roomData = {
        "user": BoxStorage().getName(),
        "user_id": BoxStorage().getId(),
        "store": widget.product.store.name,
        "store_id": widget.product.store.user,
        "store_image": widget.product.store.photo
      };
      await Firestore.instance.runTransaction((transaction) async {
        await transaction.set(
          roomReference,
          roomData,
        );
      });
    }
    message.id = doc;
    message.customProperties = {
      'status': '0',
      'product_name': widget.product != null ? widget.product.name : null,
      'product_image': widget.product != null ? widget.product.images[0] : null,
      'product_price': widget.product != null ? widget.product.price : null
    };
    await Firestore.instance.runTransaction((transaction) async {
      await transaction.set(
        documentReference,
        message.toJson(),
      );
    });
    /* setState(() {
      messages = [...messages, message];
      print(messages.length);
    });

    if (i == 0) {
      systemMessage();
      Timer(Duration(milliseconds: 600), () {
        systemMessage();
      });
    } else {
      systemMessage();
    } */
  }

  @override
  Widget build(BuildContext context) {
    // final ChatUser otherUser = ChatUser(
    //   name: "Mr.Fat",
    //   uid: "25649654",
    // );

    final ChatUser user = ChatUser(
      name: BoxStorage().getName(),
      uid: BoxStorage().getId(),
      avatar: "https://www.wrappixel.com/ampleadmin/assets/images/users/4.jpg",
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: primaryColor,
      ),
      body: StreamBuilder(
          stream: Firestore.instance
              // .collection('rooms')
              // .document(doc)
              .collection('messages')
              .where('id', isEqualTo: doc)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).primaryColor,
                  ),
                ),
              );
            } else {
              List<DocumentSnapshot> items = snapshot.data.documents;
              var messages =
                  items.map((i) => ChatMessage.fromJson(i.data)).toList();
              messages.forEach((element) {});
              return DashChat(
                key: _chatViewKey,
                inverted: false,
                onSend: onSend,
                sendOnEnter: true,
                textInputAction: TextInputAction.send,
                user: user,
                inputDecoration: InputDecoration.collapsed(
                    hintText: getTranslated(context, "add_message"),
                    hintStyle: normalTextFont.copyWith(
                        fontSize: ScreenUtil().setSp(14), color: Colors.black)),
                dateFormat: DateFormat('yyyy-MMM-dd'),
                timeFormat: DateFormat('HH:mm'),
                messages: messages,
                messageContainerDecoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                ),
                showUserAvatar: false,
                showAvatarForEveryMessage: false,
                scrollToBottom: true,
                onPressAvatar: (ChatUser user) {
                  print("OnPressAvatar: ${user.name}");
                },
                onLongPressAvatar: (ChatUser user) {
                  print("OnLongPressAvatar: ${user.name}");
                },
                inputMaxLines: 5,
                messageContainerPadding: EdgeInsets.only(left: 5.0, right: 5.0),
                alwaysShowSend: true,
                inputTextStyle: TextStyle(fontSize: 16.0),
                inputContainerStyle: BoxDecoration(
                  border: Border.all(width: 0.0),
                  color: Colors.white,
                ),
                onQuickReply: (Reply reply) {
                  setState(() {
                    messages.add(ChatMessage(
                        text: reply.value,
                        createdAt: DateTime.now(),
                        user: user));

                    messages = [...messages];
                  });

                  Timer(Duration(milliseconds: 300), () {
                    _chatViewKey.currentState.scrollController
                      ..animateTo(
                        _chatViewKey.currentState.scrollController.position
                            .maxScrollExtent,
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 300),
                      );

                    if (i == 0) {
                      systemMessage();
                      Timer(Duration(milliseconds: 600), () {
                        systemMessage();
                      });
                    } else {
                      systemMessage();
                    }
                  });
                },
                onLoadEarlier: () {
                  print("loading...");
                },
                messageTextBuilder: messageTextBuilder(),
                shouldShowLoadEarlier: false,
                showTraillingBeforeSend: false,
                // trailing: <Widget>[
                //   IconButton(
                //     icon: Icon(Icons.photo),
                //     onPressed: () async {
                //       File result = await ImagePicker.pickImage(
                //         source: ImageSource.gallery,
                //         imageQuality: 80,
                //         maxHeight: 400,
                //         maxWidth: 400,
                //       );

                //       if (result != null) {
                //         final StorageReference storageRef =
                //             FirebaseStorage.instance.ref().child("chat_images");

                //         StorageUploadTask uploadTask = storageRef.putFile(
                //           result,
                //           StorageMetadata(
                //             contentType: 'image/jpg',
                //           ),
                //         );
                //         StorageTaskSnapshot download =
                //             await uploadTask.onComplete;

                //         String url = await download.ref.getDownloadURL();

                //         ChatMessage message =
                //             ChatMessage(text: "", user: user, image: url);

                //         var documentReference = Firestore.instance
                //             .collection('messages')
                //             .document(DateTime.now()
                //                 .millisecondsSinceEpoch
                //                 .toString());

                //         Firestore.instance.runTransaction((transaction) async {
                //           await transaction.set(
                //             documentReference,
                //             message.toJson(),
                //           );
                //         });
                //       }
                //     },
                //   )
                // ],
              );
            }
          }),
    );
  }

  messageTextBuilder() {
    return (String text, [ChatMessage message]) => chatMessage(text, message);
  }

  placeholder() async {
    return (await rootBundle.load('assets/images/logo.png'))
        .buffer
        .asUint8List();
  }

  Widget chatMessage(text, message) {
    return Container(
      child: Column(
        crossAxisAlignment: message.user.uid == BoxStorage().getId()
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: [
          message.customProperties['product_name'] != null
              ? Container(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0.2)),
                  margin: EdgeInsets.only(bottom: 20),
                  clipBehavior: Clip.hardEdge,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Image.network(
                          message.customProperties['product_image'],
                          width: 50.0,
                          height: 50,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            Text(message.customProperties['product_name'],
                                maxLines: 2,
                                style: TextStyle(
                                    color:
                                        message.user.uid == BoxStorage().getId()
                                            ? Colors.white70
                                            : Colors.black87)),
                            SizedBox(height: 5),
                            Text(
                                "Rp. ${GlobalHelper().formattingNumber(int.parse(message.customProperties['product_price']))}",
                                style: TextStyle(
                                    color:
                                        message.user.uid == BoxStorage().getId()
                                            ? Colors.white70
                                            : Colors.black87))
                          ])
                    ],
                  ),
                )
              : Container(),
          ParsedText(
            text: message.text,
            style: TextStyle(
                color: message.user.uid == BoxStorage().getId()
                    ? Colors.white70
                    : Colors.black87),
          )
        ],
      ),
    );
  }
}
