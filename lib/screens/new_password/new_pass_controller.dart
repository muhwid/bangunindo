import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/update_password_body.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class NewPasswordController extends GetxController {
  RxBool isLoading = false.obs;
  RxString errorMessage = "".obs;
  final box = GetStorage();

  Rx<TextEditingController> passController = TextEditingController().obs;
  Rx<TextEditingController> confirmPassController = TextEditingController().obs;

  Dio dio = ApiServices().launch();

  @override
  void onInit() {
    super.onInit();
  }

  void setErrorMessage(String msg) async {
    errorMessage.value = msg;
  }

  void apiUpdatePass(UpdatePasswordBody body) async {
    isLoading = isLoading.toggle();
    setErrorMessage("");
    var resp;
    try {
      resp = await dio.post("${Endpoint.api_user_update}", data: body.toJson());
      BaseResponse response = BaseResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        isLoading = isLoading.toggle();
        //CustomDialog().showDialogConfirms(Get.context, "${getTranslated(Get.context, "${getTranslated(Get.context, "success_reset_password")}")}", "${getTranslated(Get.context, "sign_in")}", null, (){
        Get.offAllNamed("/loginPage");
        //}, (){

        //}, isCancelable: false);

      } else {
        isLoading = isLoading.toggle();
        if (response.invalid != null) {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.invalid,
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiUpdatePass(body);
          }, () {
            Get.back();
          });
        } else {
          CustomDialog().showDialogConfirms(
              Get.context,
              response.message.toString(),
              "${getTranslated(Get.context, "retry")}",
              "${getTranslated(Get.context, "close")}", () {
            Get.back();
            apiUpdatePass(body);
          }, () {
            Get.back();
          });
        }
      }
    } catch (e) {
      isLoading = isLoading.toggle();
      CustomDialog().showDialogConfirms(
          Get.context,
          "${e.toString()}",
          "${getTranslated(Get.context, "retry")}",
          "${getTranslated(Get.context, "close")}", () {
        Get.back();
        apiUpdatePass(body);
      }, () {
        Get.back();
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
