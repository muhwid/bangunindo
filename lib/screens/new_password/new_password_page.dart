import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/update_password_body.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/component.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'new_pass_controller.dart';

class NewPasswordPage extends StatefulWidget {

  final String phoneNUmber;

  NewPasswordPage({Key key, @required this.phoneNUmber}) : super(key: key);

  @override
  _NewPasswordPageState createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {
  NewPasswordController ctrl = Get.put(NewPasswordController());

  bool _showPassword = true;
  bool _showConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: Obx(() => SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: ScreenUtil().setHeight(75)),
                            Center(
                                child: Image.asset(
                              URL.COMPANY_LOGO,
                              width: 200.0,
                            )),
                            SizedBox(height: ScreenUtil().setHeight(85)),
                            Text(
                                getTranslated(context, "reset_your_password")
                                    .toUpperCase(),
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(18))),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(15),
                                  bottom: ScreenUtil().setHeight(8),
                                  top: ScreenUtil().setHeight(16)),
                              child: Text(
                                  "${getTranslated(context, "password").toUpperCase()}*",
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(10))),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(color: Colors.grey),
                                color: bgPage,
                              ),
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(10),
                                    right: ScreenUtil().setWidth(10)),
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  obscureText: _showPassword,
                                  controller: ctrl.passController.value,
                                  textAlign: TextAlign.left,
                                  style: normalTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12)),
                                  decoration: new InputDecoration(
                                      hintStyle:
                                          TextStyle(color: Color(0xffD1D1D1)),
                                      border: InputBorder.none,
                                      hintText:
                                          "${getTranslated(context, "input_password")}",
                                      fillColor: bgPage,
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _showPassword = !_showPassword;
                                          });
                                        },
                                        child: Icon(
                                          _showPassword
                                              ? Icons.visibility_off_outlined
                                              : Icons.visibility_outlined,
                                          color: primaryColor,
                                        ),
                                      )),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(15),
                                  bottom: ScreenUtil().setHeight(8),
                                  top: ScreenUtil().setHeight(16)),
                              child: Text(
                                  "${getTranslated(context, "confirm_password").toUpperCase()}*",
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(10))),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(color: Colors.grey),
                                color: bgPage,
                              ),
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(10),
                                    right: ScreenUtil().setWidth(10)),
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  obscureText: _showConfirmPassword,
                                  controller: ctrl.confirmPassController.value,
                                  textAlign: TextAlign.left,
                                  style: normalTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12)),
                                  decoration: new InputDecoration(
                                      hintStyle:
                                      TextStyle(color: Color(0xffD1D1D1)),
                                      border: InputBorder.none,
                                      hintText:
                                      "${getTranslated(context, "input_confirm_password")}",
                                      fillColor: bgPage,
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _showConfirmPassword = !_showConfirmPassword;
                                          });
                                        },
                                        child: Icon(
                                          _showConfirmPassword
                                              ? Icons.visibility_off_outlined
                                              : Icons.visibility_outlined,
                                          color: primaryColor,
                                        ),
                                      )),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(15),
                            ),
                            (ctrl.isLoading.value)
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : Row(
                                    children: [
                                      Expanded(
                                        child: WidgetComponent.primaryButton(
                                            onPress: () {
                                              _reqUpdatePass(context);
                                            },
                                            title:
                                                getTranslated(context, "save")
                                                    .toUpperCase()),
                                      )
                                    ],
                                  ),
                            Visibility(
                              child: Text(
                                "${ctrl.errorMessage.value}",
                                style: normalTextFont.copyWith(
                                    color: Colors.red,
                                    fontSize: ScreenUtil().setSp(11)),
                              ),
                              visible: ctrl.errorMessage.value != "",
                            )
                          ],
                        )),
                    SizedBox(
                      height: ScreenUtil().setHeight(50),
                    )
                  ],
                ),
              )),
        ),
      ),
    );
  }

  _reqUpdatePass(BuildContext context) {
    if (ctrl.passController.value.text.isEmpty) {
      FlushBarMessage().error(context, "Oops",
          "${getTranslated(context, "password_cant_empty")}", 2);
      return;
    }

    if (ctrl.confirmPassController.value.text.isEmpty) {
      FlushBarMessage().error(context, "Oops",
          "${getTranslated(context, "password_cant_empty")}", 2);
      return;
    }

    if (ctrl.passController.value.text != ctrl.confirmPassController.value.text) {
      FlushBarMessage().error(context, "Oops",
          "${getTranslated(context, "password_not_same")}", 2);
      return;
    }

    ctrl.apiUpdatePass(UpdatePasswordBody(widget.phoneNUmber,
        ctrl.passController.value.text, ctrl.confirmPassController.value.text));
  }
}
