import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/product/detail_product/detail_product.dart';
import 'package:e_commerce_app/screens/wishlist/bloc/list_wishlist_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_product_grid.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import 'bloc/list_wishlist_bloc.dart';
import 'bloc/list_wishlist_state.dart';

class ListWishlistPage extends StatefulWidget {
  // List<ProductModel> products = List();
  //
  // ListWishlistPage({Key key, this.products, @required this.title, this.id})
  //     : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ListWishlistPageState();
  }
}

class ListWishlistPageState extends State<ListWishlistPage> {
  ListWishlistBloc _bloc;
  ApiDomain _domain;
  List<ProductModel> products = List();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = ListWishlistBloc(domain: _domain);

    _reqWishlist();

    super.initState();
  }

  _reqWishlist() {
    _bloc.add(ReqListWishlist());
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          appBar:
              WidgetComponent.appBar(text: getTranslated(context, "wishlist")),
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<ListWishlistBloc, ListWishlistState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqLoading) {
                  CustomDialog().loading(
                      context, getTranslated(context, "processing_request"));
                } else if (state is ReqWishlistError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqWishlist();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqWishlistSuccess) {
                  products.clear();
                  for (var item in state.resp.data) {
                    products.add(item.product);
                  }
                  Navigator.pop(context);
                }
              },
              child: BlocBuilder<ListWishlistBloc, ListWishlistState>(
                bloc: _bloc,
                builder: (context, state) {
                  return Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: (products.length > 0)
                        ? GridView.count(
                            crossAxisCount: 2,
                            mainAxisSpacing: 3.0,
                            //MENGATUR JARAK ANTARA OBJEK ATAS DAN BAWAH
                            crossAxisSpacing: 3,
                            //MENGATUR JARAK ANTARA OBJEK KIRI DAN KANAN
                            childAspectRatio: 0.6,
                            //A
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            children: List.generate(products.length, (index) {
                              return itemProduct(products[index], context, () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DetailProduct(
                                              id: products[index].id_product,
                                            )));
                              });
                            }),
                          )
                        : Container(
                            margin: EdgeInsets.all(ScreenUtil().setWidth(20)),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: ScreenUtil().setHeight(15),
                                  ),
                                  SvgPicture.asset(
                                      "assets/images/img_empty.svg"),
                                  // SizedBox(
                                  //   height: ScreenUtil().setHeight(10),
                                  // ),
                                  // Text(
                                  //   "${getTranslated(context, "no_address")}",
                                  //   style: boldTextFont.copyWith(
                                  //       fontSize: ScreenUtil().setSp(14)),
                                  // ),
                                  SizedBox(
                                    height: ScreenUtil().setHeight(15),
                                  )
                                ])),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
