import 'package:equatable/equatable.dart';

abstract class ListWishlistEvent extends Equatable {
  const ListWishlistEvent();

  @override
  List<Object> get props => [];
}

class ReqListWishlist extends ListWishlistEvent {
  const ReqListWishlist();

  @override
  List<Object> get props => [];
}
