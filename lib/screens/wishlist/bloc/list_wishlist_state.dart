
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:e_commerce_app/model/response/list_wishlist_response.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:equatable/equatable.dart';

abstract class ListWishlistState extends Equatable {
  const ListWishlistState();
}

class InitPage extends ListWishlistState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends ListWishlistState {
  @override
  List<Object> get props => [];
}

class ReqWishlistSuccess extends ListWishlistState {
  final ListWishlistResponse resp;

  const ReqWishlistSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqWishlistError extends ListWishlistState {
  final String error;

  const ReqWishlistError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqWishlistError { error: $error }';
}