import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/list_wishlist_response.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:flutter/material.dart';

import 'list_wishlist_event.dart';
import 'list_wishlist_state.dart';


class ListWishlistBloc extends Bloc<ListWishlistEvent, ListWishlistState> {
  final ApiDomain domain;

  ListWishlistBloc({@required this.domain}) : assert(domain != null);

  @override
  ListWishlistState get initialState => InitPage();

  @override
  Stream<ListWishlistState> mapEventToState(ListWishlistEvent event) async* {
    if (event is ReqListWishlist) {
      yield ReqLoading();
      try {
        ListWishlistResponse resp = await domain.reqListWishlist();
        if (resp.status.startsWith("20"))
          yield ReqWishlistSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqWishlistError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqWishlistError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqWishlistError(error: e.toString());
      }
    }
  }
}
