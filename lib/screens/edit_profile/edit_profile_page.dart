import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/shared_preferences.dart';
import 'package:e_commerce_app/model/body/edit_profile_body.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/edit_profile/bloc/edit_profile_bloc.dart';
import 'package:e_commerce_app/screens/edit_profile/bloc/edit_profile_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/edit_profile_state.dart';

class EditProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return EditProfilePageState();
  }
}

class EditProfilePageState extends State<EditProfilePage> {
  EditProfileBloc _bloc;
  ApiDomain _domain;
  EditProfileBody body;

  TextEditingController nameCtrl = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  TextEditingController confirmPassController = new TextEditingController();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = EditProfileBloc(domain: _domain);

    _reqGetProfile();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          appBar: WidgetComponent.appBar(
              text: getTranslated(context, "edit_profile")),
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<EditProfileBloc, EditProfileState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqLoading) {
                  CustomDialog().loading(context,
                      "${getTranslated(context, "processing_request")}");
                } else if (state is ReqEditError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqEditProfile();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqEditSuccess) {
                  Navigator.pop(context);
                  FlushBarMessage().toast(
                      context,
                      "${getTranslated(context, "success")}",
                      state.resp.message,
                      Colors.green,
                      2);
                  _reqGetProfile();
                }

                if (state is ReqGetProfileError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqGetProfile();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqGetProfileSuccess) {
                  Navigator.pop(context);
                  SharedPreferencesProvider().setName(state.resp.data.name);
                  nameCtrl.text = "${state.resp.data.name}";
                  phoneController.text = "${state.resp.data.phone}";
                  emailController.text = "${state.resp.data.email}";
                }
              },
              child: BlocBuilder<EditProfileBloc, EditProfileState>(
                bloc: _bloc,
                builder: (context, state) {
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                            margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: ScreenUtil().setHeight(15)),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "name")}*"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: nameCtrl,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_name")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${"${getTranslated(context, "phone_number")}"}*"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: phoneController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_phone_number")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "email").toUpperCase()}*",
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: emailController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_email")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "password")}"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      obscureText: true,
                                      controller: passController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_password")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "confirm_password")}"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      obscureText: true,
                                      controller: confirmPassController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_confirm_password")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: ScreenUtil().setHeight(20),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: WidgetComponent.primaryButton(
                                          onPress: () {
                                            if (nameCtrl.text.isEmpty ||
                                                phoneController.text.isEmpty ||
                                                emailController.text.isEmpty) {
                                              FlushBarMessage().error(
                                                  context,
                                                  "Oops",
                                                  getTranslated(
                                                      context, "fill_all_form"),
                                                  2);
                                              return;
                                            }

                                            if (passController
                                                .text.isNotEmpty) {
                                              if (passController.text !=
                                                  confirmPassController.text) {
                                                FlushBarMessage().error(
                                                    context,
                                                    "Oops",
                                                    "${getTranslated(context, "password_not_same")}",
                                                    2);
                                                return;
                                              }
                                            }

                                            body = EditProfileBody(
                                                nameCtrl.text,
                                                emailController.text,
                                                phoneController.text,
                                                passController.text,
                                                confirmPassController.text);
                                            _reqEditProfile();
                                          },
                                          title: getTranslated(
                                                  context, "edit_profile")
                                              .toUpperCase()),
                                    )
                                  ],
                                ),
                              ],
                            )),
                        SizedBox(
                          height: ScreenUtil().setHeight(25),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _reqEditProfile() {
    _bloc.add(ReqEditProfile(body));
  }

  _reqGetProfile() {
    _bloc.add(ReqGetProfile());
  }
}
