
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/get_profile_response.dart';
import 'package:equatable/equatable.dart';

abstract class EditProfileState extends Equatable {
  const EditProfileState();
}

class InitPage extends EditProfileState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends EditProfileState {
  @override
  List<Object> get props => [];
}

class ReqEditSuccess extends EditProfileState {
  final BaseResponse resp;

  const ReqEditSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqEditError extends EditProfileState {
  final String error;

  const ReqEditError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqEditError { error: $error }';
}

class ReqGetProfileSuccess extends EditProfileState {
  final GetProfileResponse resp;

  const ReqGetProfileSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqGetProfileError extends EditProfileState {
  final String error;

  const ReqGetProfileError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqGetProfileError { error: $error }';
}