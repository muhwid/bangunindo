import 'package:e_commerce_app/model/body/edit_profile_body.dart';
import 'package:equatable/equatable.dart';

abstract class EditProfileEvent extends Equatable {
  const EditProfileEvent();

  @override
  List<Object> get props => [];
}

class ReqEditProfile extends EditProfileEvent {
  final EditProfileBody body;

  const ReqEditProfile(this.body);

  @override
  List<Object> get props => [body];
}

class ReqGetProfile extends EditProfileEvent {
  const ReqGetProfile();

  @override
  List<Object> get props => [];
}
