import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/get_profile_response.dart';
import 'package:flutter/material.dart';

import 'edit_profile_event.dart';
import 'edit_profile_state.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  final ApiDomain domain;

  EditProfileBloc({@required this.domain}) : assert(domain != null);

  @override
  EditProfileState get initialState => InitPage();

  @override
  Stream<EditProfileState> mapEventToState(EditProfileEvent event) async* {
    if (event is ReqEditProfile) {
      yield ReqLoading();
      try {
        BaseResponse resp = await domain.reqEditProfile(event.body);
        if (resp.status.startsWith("20"))
          yield ReqEditSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqEditError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqEditError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqEditError(error: e.toString());
      }
    }

    if (event is ReqGetProfile) {
      yield ReqLoading();
      try {
        GetProfileResponse resp = await domain.reqGetProfile();
        if (resp.status.startsWith("20"))
          yield ReqGetProfileSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqGetProfileError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqGetProfileError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqGetProfileError(error: e.toString());
      }
    }
  }
}
