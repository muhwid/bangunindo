import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/model/alert_model.dart';
import 'package:e_commerce_app/model/object/notification_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/chat/chat_screen.dart';
import 'package:e_commerce_app/screens/main/tab/alert/alert_controller.dart';
import 'package:e_commerce_app/screens/transaction/detail_transaction/detail_transaction.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/widget/refresh_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:path/path.dart';
import 'package:rxdart/streams.dart';

class ChatPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChatPageState();
  }
}

class ChatPageState extends State<ChatPage> {
  final box = GetStorage();
  AlertController ctrl;
  var the_data = [];
  List<AlertModel> alerts = List();

  @override
  void initState() {
    super.initState();
  }

  CombineLatestStream<QuerySnapshot, List<QuerySnapshot>> roomStream() {
    try {
      Stream<QuerySnapshot> s1 = Firestore.instance
          // .collection('rooms').where(field)
          // .document()
          .collection('rooms')
          .where('user_id', isEqualTo: BoxStorage().getId())
          .snapshots();
      Stream<QuerySnapshot> s2 = Firestore.instance
          // .collection('rooms').where(field)
          // .document()
          .collection('rooms')
          .where('store_id', isEqualTo: BoxStorage().getId())
          .snapshots();
      return CombineLatestStream.list<QuerySnapshot>([s1, s2]);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
            backgroundColor: bgPage,
            body: StreamBuilder(
              stream: roomStream(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  the_data = [
                    ...snapshot.data[0].documents,
                    ...snapshot.data[1].documents
                  ];
                }
                return !snapshot.hasData
                    ? Center(child: CircularProgressIndicator())
                    : the_data.length < 1
                        ? Center(
                            child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 30),
                            child: Text(
                              "Belum ada chat. Klik tombol chat di detail produk untuk memulai percakapan",
                              textAlign: TextAlign.center,
                            ),
                          ))
                        : Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(25)),
                            child: ListView.builder(
                              itemCount: the_data.length,
                              itemBuilder: (context, index) {
                                DocumentSnapshot data = the_data[index];
                                if (data.data['user_id'] ==
                                        BoxStorage().getId() &&
                                    data.data['store_id'] ==
                                        BoxStorage().getId()) {
                                  return Container();
                                } else {
                                  return InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ChatScreenPage(
                                                      room: data.documentID,
                                                      store_id:
                                                          data.data['store_id'],
                                                      title: data.data[
                                                                  'store_id'] ==
                                                              BoxStorage()
                                                                  .getId()
                                                          ? data.data['user']
                                                          : data
                                                              .data['store'])));
                                    },
                                    child: Container(
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius
                                                          .circular(ScreenUtil()
                                                              .setWidth(10)),
                                                      color: Colors.white),
                                                  child:
                                                      FadeInImage.assetNetwork(
                                                    placeholder:
                                                        "img_placeholder_profile.png",
                                                    image: data.data[
                                                                'store_id'] ==
                                                            BoxStorage().getId()
                                                        ? 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
                                                        : data.data[
                                                            'store_image'],
                                                    fit: BoxFit.cover,
                                                    height: 50,
                                                    width: 50,
                                                  ),
                                                ),
                                                flex: 1,
                                              ),
                                              SizedBox(
                                                width:
                                                    ScreenUtil().setWidth(10),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      data.data['store_id'] ==
                                                              BoxStorage()
                                                                  .getId()
                                                          ? data.data['user']
                                                          : data.data['store'],
                                                      style:
                                                          boldTextFont.copyWith(
                                                              fontSize:
                                                                  ScreenUtil()
                                                                      .setSp(
                                                                          12)),
                                                    ),
                                                  ],
                                                ),
                                                flex: 6,
                                              ),
                                              SizedBox(
                                                width:
                                                    ScreenUtil().setWidth(10),
                                              ),
                                              Icon(
                                                Icons.arrow_forward_ios,
                                                size: 15,
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: ScreenUtil().setHeight(10),
                                          ),
                                          Divider(),
                                          SizedBox(
                                            height: ScreenUtil().setHeight(10),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ));
              },
            )),
      ),
    );
  }
}
