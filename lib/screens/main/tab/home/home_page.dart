import 'package:e_commerce_app/data/database_helper.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/detail_product_model.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/main/tab/home/home_controller.dart';
import 'package:e_commerce_app/screens/product/detail_product/detail_product.dart';
import 'package:e_commerce_app/screens/product/list_product/list_product_page.dart';
import 'package:e_commerce_app/screens/shopping_cart/shopping_cart.dart';
import 'package:e_commerce_app/screens/search/search_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/rating_stars.dart';
import 'package:e_commerce_app/views/shimmer_menu.dart';
import 'package:e_commerce_app/views/shimmer_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final box = GetStorage();
  DetailProductModel data;
  List<CartDbModel> carts = List();
  int total = 0;

  @override
  void initState() {
    loadAllCarts();
    super.initState();
  }

  loadAllCarts() {
    DatabaseHelper.instance.queryAllRows().then((value) {
      total = 0;
      carts.clear();
      for (var item in value) {
        carts.add(CartDbModel().fromMap(item));
        int harga = item["price"] * item["qty"];
        total = total + harga;
      }

      setState(() {});
    });
  }

  HomeController ctrl = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.red,
      child: SafeArea(
        child: Scaffold(
            backgroundColor: bgPage,
            body: Obx(() => SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          ClipPath(
                              // OvalClipper()
                              clipper: OvalBottomBorderClipper(),
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: ScreenUtil().setHeight(200),
                                color: primaryColor,
                              )),
                          Column(
                            children: [
                              Container(
                                margin: EdgeInsets.all(ScreenUtil().setSp(15)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                                getTranslated(context, 'hello'),
                                                style: boldTextFont.copyWith(
                                                    fontSize:
                                                        ScreenUtil().setSp(14),
                                                    color: Colors.white)),
                                            Text(", ${box.read("name")}",
                                                style: boldTextFont.copyWith(
                                                    fontSize:
                                                        ScreenUtil().setSp(14),
                                                    color: Colors.white)),
                                          ],
                                        ),
                                        SizedBox(
                                          height: ScreenUtil().setHeight(5),
                                        ),
                                        Text(getTranslated(context, 'what'),
                                            style: normalTextFont.copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(10),
                                                color: Colors.white)),
                                      ],
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          right: ScreenUtil().setWidth(8),
                                          top: ScreenUtil().setWidth(10)),
                                      child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ShoppingCart()));
                                          },
                                          child: Stack(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ShoppingCart()));
                                                },
                                                child: Container(
                                                    width: ScreenUtil()
                                                        .setWidth(30),
                                                    height: ScreenUtil()
                                                        .setWidth(30),
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20.0),
                                                    ),
                                                    child: Icon(
                                                        Icons.shopping_cart,
                                                        color: Colors.red)),
                                              ),
                                              // TODO:
                                              // (DatabaseHelper.columnIdProduct
                                              //             .length >=
                                              //         1)
                                              (carts.length >= 1)
                                                  ? Container(
                                                      width: ScreenUtil()
                                                          .setWidth(30),
                                                      height: ScreenUtil()
                                                          .setWidth(30),
                                                      child: Align(
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Container(
                                                            margin: EdgeInsets.only(
                                                                right:
                                                                    ScreenUtil()
                                                                        .setSp(
                                                                            0),
                                                                top: ScreenUtil()
                                                                    .setHeight(
                                                                        3)),
                                                            width: ScreenUtil()
                                                                .setWidth(7),
                                                            height: ScreenUtil()
                                                                .setWidth(7),
                                                            decoration:
                                                                BoxDecoration(
                                                              color: Colors.red,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20.0),
                                                            )),
                                                      ),
                                                    )
                                                  : SizedBox()
                                            ],
                                          )),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(15),
                                      left: ScreenUtil().setWidth(24),
                                      right: ScreenUtil().setWidth(24)),
                                  child: Row(
                                    children: [
                                      RichText(
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                              text: getTranslated(
                                                  context, 'choose'),
                                              style: normalTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(14),
                                                  color: Colors.white)),
                                          TextSpan(
                                              text: getTranslated(
                                                  context, 'best'),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(14),
                                                  color: Colors.white)),
                                        ]),
                                      ),
                                    ],
                                  )),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context)
                                      .push(MaterialPageRoute(builder: (_) {
                                    return SearchPage();
                                  }));
                                },
                                child: Padding(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(15),
                                        right: 8.0,
                                        top: 8.0,
                                        bottom: 8.0),
                                    child: Card(
                                      elevation: 2,
                                      color: Colors.white,
                                      child: TextField(
                                        enabled: false,
                                        style: normalTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(12)),
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText:
                                              getTranslated(context, 'search'),
                                          hintStyle: normalTextFont.copyWith(
                                              fontSize: ScreenUtil().setSp(12),
                                              color: Colors.grey),
                                          contentPadding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(5)),
                                        ),
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(15),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(15),
                            right: ScreenUtil().setWidth(15)),
                        child: SizedBox(
                          height: ScreenUtil().setHeight(120),
                          child: (ctrl.isLoadingCategory.value)
                              ? shimmerMenu()
                              : ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: ctrl.categories.length,
                                  itemBuilder: (BuildContext context,
                                          int index) =>
                                      ItemMenu(ctrl.categories[index], context),
                                ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(35),
                      ),
                      _viewTrending(ctrl, context),
                      SizedBox(
                        height: ScreenUtil().setHeight(35),
                      ),
                      //SLIDER
                      (ctrl.isLoadingCoupon.value)
                          ? loading()
                          : CarouselSlider(
                              options: CarouselOptions(
                                reverse: false,
                                aspectRatio: 2.2,
                              ),
                              items: ctrl.coupons.map((i) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.amber,
                                            borderRadius:
                                                BorderRadius.circular(12.0)),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(12.0),
                                          ),
                                          child: FadeInImage.assetNetwork(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            placeholder:
                                                "assets/images/img_placeholder.png",
                                            image: i.image,
                                            height: ScreenUtil().setHeight(320),
                                            fit: BoxFit.fill,
                                          ),
                                        ));
                                  },
                                );
                              }).toList(),
                            ),
                      SizedBox(
                        height: ScreenUtil().setHeight(35),
                      ),
                      _viewPopular(ctrl, context),
                      SizedBox(
                        height: ScreenUtil().setHeight(35),
                      ),
                      ctrl.trustedSellers.length > 0
                          ? _viewTrustedSeller(ctrl)
                          : Container(),
                      SizedBox(
                        height: ScreenUtil().setHeight(35),
                      ),
                    ],
                  ),
                ))),
      ),
    );
  }

  _viewTrending(HomeController ctrl, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                getTranslated(context, 'trending'),
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(16)),
              ),
              InkWell(
                onTap: () {
                  Get.to(ListProductPage(
                    products: ctrl.trendingItems,
                    title: getTranslated(context, "trending"),
                  ));
                },
                child: Text(
                  getTranslated(context, 'show'),
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12), color: primaryColor),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(10),
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: (ctrl.isLoadingTrending.value)
              ? shimmerProduct(context)
              : Column(
                  children: [
                    for (int i = 0; i < ctrl.limitTrendingItems.length; i++)
                      itemTrending(ctrl.limitTrendingItems[i], context)
                  ],
                ),
        ),
      ],
    );
  }

  Widget loading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _viewPopular(HomeController ctrl, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                getTranslated(context, 'promoted'),
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(16)),
              ),
              InkWell(
                onTap: () {
                  Get.to(ListProductPage(
                    products: ctrl.promotedItems,
                    title: getTranslated(context, "promoted_product"),
                  ));
                },
                child: Text(
                  getTranslated(context, 'show'),
                  style: boldTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(12), color: primaryColor),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(10),
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: (ctrl.isLoadingPromoted.value)
              ? shimmerProduct(context)
              : Column(
                  children: [
                    for (int i = 0; i < ctrl.limitpromotedItems.length; i++)
                      itemTrending(ctrl.limitpromotedItems[i], context)
                  ],
                ),
        ),
      ],
    );
  }

  _viewTrustedSeller(HomeController ctrl) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: Text(
            getTranslated(context, 'trusted_seller'),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(16)),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              left: ScreenUtil().setWidth(15),
              right: ScreenUtil().setWidth(15)),
          child: SizedBox(
            height: ScreenUtil().setHeight(80),
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: ctrl.trustedSellers.length,
              itemBuilder: (BuildContext context, int index) => Card(
                  child: Container(
                      padding: EdgeInsets.all(ScreenUtil().setSp(5)),
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/images/img_placeholder.png",
                        image: ctrl.trustedSellers[index],
                        fit: BoxFit.contain,
                      ))),
            ),
          ),
        ),
      ],
    );
  }

  Widget itemTrending(ProductModel item, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailProduct(
                      id: item.id_product,
                    )));
      },
      child: Container(
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(7)),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    width: ScreenUtil().setWidth(80),
                    height: ScreenUtil().setWidth(80),
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(ScreenUtil().setWidth(10)),
                        color: Colors.white),
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/img_placeholder.png",
                      image: (item.images.length > 0) ? item.images[0] : "",
                      fit: BoxFit.scaleDown,
                    ),
                  ),
                  flex: 2,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        item.name,
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12)),
                      ),
                      Row(
                        children: [
                          Text(
                              "Rp.${GlobalHelper().formattingNumber(int.parse(item.price))}",
                              style: boldTextFont.copyWith(
                                  color: primaryColor,
                                  fontSize: ScreenUtil().setSp(10))),
                          SizedBox(
                            width: ScreenUtil().setWidth(5),
                          ),
                          Visibility(
                            child: Text(
                              "Rp.${GlobalHelper().formattingNumber(int.parse(item.price))}",
                              style: boldTextFont.copyWith(
                                  color: semiTransRed,
                                  fontSize: ScreenUtil().setSp(10)),
                            ),
                            visible: false,
                          )
                        ],
                      ),
                      Row(
                        children: [RatingStars(double.parse(item.review))],
                      )
                    ],
                  ),
                  flex: 6,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Expanded(
                  flex: 2,
                  child: WidgetComponent.shopButton(
                      context: context,
                      onPressed: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => DetailProduct(
                        //               id: item.id_product,
                        //             )));

                        // DatabaseHelper.instance
                        //     .getSpecificDataStore(data.store.id_store)
                        //     .then((value) {
                        //   if (value.isNotEmpty) {
                        //     GlobalHelper().showSnackBar(
                        //         "Tidak bisa melakukan pembelian beda store",
                        //         context);
                        //   } else {
                        DatabaseHelper.instance
                            .getSpecificDataStore(item.store)
                            .then((value) {
                          if (value.isNotEmpty &&
                              ("${item.store}" !=
                                  "${value.first["id_store"]}")) {
                            GlobalHelper().showSnackBar(
                                "${getTranslated(context, "cannot_make_purchase")}",
                                context);
                          } else {
                            DatabaseHelper.instance
                                .getSpecificData(item.id_product)
                                .then((value) {
                              if (value.isNotEmpty) {
                                int currentQty = value[0]["qty"];
                                DatabaseHelper.instance.updateQty(
                                    CartDbModel.insert(
                                        item.id_product,
                                        item.store,
                                        item.name,
                                        item.description,
                                        item.category,
                                        (item.brand == null) ? "" : item.brand,
                                        item.price,
                                        item.stock,
                                        item.min_grosir,
                                        item.grosir,
                                        item.weight,
                                        item.expired,
                                        item.created_at.toIso8601String(),
                                        (item.images.length > 0)
                                            ? item.images[0]
                                            : "",
                                        item.review,
                                        currentQty + 1));
                              } else {
                                DatabaseHelper.instance.insert(
                                    CartDbModel.insert(
                                        item.id_product,
                                        item.store,
                                        item.name,
                                        item.description,
                                        item.category,
                                        (item.brand == null) ? "" : item.brand,
                                        item.price,
                                        item.stock,
                                        item.min_grosir,
                                        item.grosir,
                                        item.weight,
                                        item.expired,
                                        item.created_at.toIso8601String(),
                                        (item.images.length > 0)
                                            ? item.images[0]
                                            : "",
                                        item.review,
                                        1));
                              }
                              loadAllCarts();
                              FlushBarMessage().toast(
                                  context,
                                  "${getTranslated(context, "success")}",
                                  "${getTranslated(context, "success_add_product")} ${item.name}",
                                  Colors.green,
                                  2);
                            });
                          }
                        });
                      }),
                ),
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: ScreenUtil().setWidth(10),
                  right: ScreenUtil().setWidth(10)),
              child: Divider(),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            )
          ],
        ),
      ),
    );
  }

  Widget ItemMenu(CategoryModel item, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ListProductPage(
                      title: item.name,
                      id: item.id_category,
                    )));
      },
      child: Container(
        margin: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
        child: Card(
          child: Container(
            width: ScreenUtil().setWidth(100),
            height: ScreenUtil().setHeight(120),
            color: greyMenu,
            child: Center(
              child: Container(
                margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(35),
                      height: ScreenUtil().setHeight(35),
                      child: (item.image == null || item.image.trim() == "")
                          ? Image.asset("assets/images/apps_logo.jpg")
                          : Image.network(item.image),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(10),
                    ),
                    Center(
                      child: Text(
                        "${item.name} ${item.name}",
                        textAlign: TextAlign.center,
                        style: normalTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12)),
                        maxLines: 2,
                        softWrap: true,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
