import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/coupon_model.dart';
import 'package:e_commerce_app/model/object/detail_product_model.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:e_commerce_app/model/response/coupon_response.dart';
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  RxBool isLoadingCategory = false.obs;
  RxBool isLoadingTrending = false.obs;
  RxBool isLoadingPromoted = false.obs;
  RxBool isLoadingCoupon = false.obs;
  RxBool isLoadingDetail = false.obs;
  RxString errorMessage = "".obs;

  RxList<ProductModel> limitTrendingItems = List<ProductModel>().obs;
  RxList<ProductModel> trendingItems = List<ProductModel>().obs;
  RxList<ProductModel> limitpromotedItems = List<ProductModel>().obs;
  RxList<ProductModel> promotedItems = List<ProductModel>().obs;
  RxList<CouponModel> coupons = List<CouponModel>().obs;
  RxList<String> trustedSellers = List<String>().obs;
  RxList<CategoryModel> categories = List<CategoryModel>().obs;
  Rx<DetailProductModel> dataRes = DetailProductModel().obs;
  Dio dio = ApiServices().launch();

  injectMenu() {
    trustedSellers
        .add("http://muhwid.com/projects/bangunindo/images/img_mitra_io.png");
    trustedSellers.add(
        "http://muhwid.com/projects/bangunindo/images/img_putra_garuda.png");
    trustedSellers.add(
        "http://muhwid.com/projects/bangunindo/images/img_sahabat_bangunan.png");
  }

  @override
  void onInit() {
    injectMenu();
    apiGetCategory();
    apiGetCoupon();
    apiGetPromoted();
    apiGetTrending();
    apiDetail();
    super.onInit();
  }

  void setErrorMessage(String msg) async {
    errorMessage.value = msg;
  }

  void apiGetCategory() async {
    isLoadingCategory = isLoadingCategory.toggle();
    final resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_category}");
    isLoadingCategory = isLoadingCategory.toggle();
    CategoryResponse response = CategoryResponse.fromJson(resp.data);
    if (response.status.startsWith("20")) {
      categories.clear();
      categories.addAll(response.data);
    } else {
      if (response.invalid != null)
        errorMessage.value = response.invalid;
      else
        errorMessage.value = response.message.toString();
    }
  }

  void apiGetTrending() async {
    isLoadingTrending = isLoadingTrending.toggle();
    final resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_trending}");
    isLoadingTrending = isLoadingTrending.toggle();
    ProductResponse response = ProductResponse.fromJson(resp.data);
    if (response.status.startsWith("20")) {
      limitTrendingItems.clear();
      trendingItems.clear();
      if (response.data.length > 5) {
        int i = 1;
        for (ProductModel item in response.data) {
          if (i == 5) return;
          limitTrendingItems.add(item);
          i++;
        }
      } else
        limitTrendingItems.addAll(response.data);

      trendingItems.addAll(response.data);
    } else {
      if (response.invalid != null)
        errorMessage.value = response.invalid;
      else
        errorMessage.value = response.message.toString();
    }
  }

  void apiGetPromoted() async {
    isLoadingPromoted = isLoadingPromoted.toggle();
    final resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_promoted}");
    ProductResponse response = ProductResponse.fromJson(resp.data);
    isLoadingPromoted = isLoadingPromoted.toggle();
    if (response.data.isNotEmpty) {
      limitpromotedItems.clear();
      promotedItems.clear();
      if (response.data.length > 5) {
        int i = 1;
        for (ProductModel item in response.data) {
          if (i == 5) return;
          limitpromotedItems.add(item);
          i++;
        }
      } else
        limitpromotedItems.addAll(response.data);

      promotedItems.addAll(response.data);
    } else {
      errorMessage.value = response.message;
    }
  }

  void apiGetCoupon() async {
    isLoadingCoupon = isLoadingCoupon.toggle();
    final resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_coupon}");
    isLoadingCoupon = isLoadingCoupon.toggle();
    CouponResponse response = CouponResponse.fromJson(resp.data);
    if (response.data.isNotEmpty) {
      coupons.clear();
      coupons.addAll(response.data);
    } else {
      errorMessage.value = response.message;
    }
  }

  void apiDetail() async {
    isLoadingDetail = isLoadingDetail.toggle();
    final res =
        await dio.get("${Endpoint.baseUrl}${Endpoint.api_detail_product}");
    DetailProductResponse response = DetailProductResponse.fromJson(res.data);
    if (response.status.startsWith("20")) {
      dataRes.value = (response.data);
    } else {
      if (response.invalid != null)
        errorMessage.value = response.invalid;
      else
        errorMessage.value = response.message.toString();
    }
  }

  @override
  void onClose() {
    this.dispose();
    super.onClose();
  }
}
