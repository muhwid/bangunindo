import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:flutter/material.dart';

import 'explore_event.dart';
import 'explore_state.dart';

class ExploreBloc extends Bloc<ExploreEvent, ExploreState> {
  final ApiDomain domain;

  ExploreBloc({@required this.domain}) : assert(domain != null);

  @override
  ExploreState get initialState => InitPage();

  @override
  Stream<ExploreState> mapEventToState(ExploreEvent event) async* {
    if (event is ReqExplore) {
      yield ReqLoading();
      try {
        CategoryResponse resp = await domain.reqCategory();
        if (resp.status.startsWith("20"))
          yield ExploreSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ExploreError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ExploreError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ExploreError(error: e.toString());
      }
    }
  }
}
