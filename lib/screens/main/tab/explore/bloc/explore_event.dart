import 'package:equatable/equatable.dart';

abstract class ExploreEvent extends Equatable {
  const ExploreEvent();

  @override
  List<Object> get props => [];
}

class ReqExplore extends ExploreEvent {

  const ReqExplore();

  @override
  List<Object> get props => [];
}
