
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:equatable/equatable.dart';

abstract class ExploreState extends Equatable {
  const ExploreState();
}

class InitPage extends ExploreState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends ExploreState {
  @override
  List<Object> get props => [];
}

class ExploreSuccess extends ExploreState {
  final CategoryResponse resp;

  const ExploreSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ExploreError extends ExploreState {
  final String error;

  const ExploreError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}