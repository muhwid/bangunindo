import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/product/list_product/list_product_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/shimmer_product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/explore_bloc.dart';
import 'bloc/explore_event.dart';
import 'bloc/explore_state.dart';

class ExplorePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ExplorePageState();
  }
}

class ExplorePageState extends State<ExplorePage> {
  var searchCtrl = new TextEditingController();
  List<CategoryModel> resultSearch = List();
  ExploreBloc _bloc;
  ApiDomain _domain;

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = ExploreBloc(domain: _domain);

    _reqGetExplore();
//    injectResult();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<ExploreBloc, ExploreState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqLoading) {
//                  CustomDialog().loading(context, "Processing Request");
                } else if (state is ExploreError) {
//                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqGetExplore();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ExploreSuccess) {
//                  Navigator.pop(context);
                  resultSearch.clear();
                  resultSearch.addAll(state.resp.data);
                }
              },
              child: BlocBuilder<ExploreBloc, ExploreState>(
                bloc: _bloc,
                builder: (context, state) {
                  return Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            getTranslated(context, 'category'),
                            style: boldMuliFont.copyWith(
                                fontSize: ScreenUtil().setSp(16)),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          // Container(
                          //   padding: EdgeInsets.only(
                          //       left: ScreenUtil().setSp(15),
                          //       right: ScreenUtil().setSp(15)),
                          //   decoration: BoxDecoration(
                          //     borderRadius: BorderRadius.circular(10),
                          //     color: Colors.white,
                          //   ),
                          //   child: Container(
                          //     margin: EdgeInsets.only(
                          //         right: ScreenUtil().setWidth(15)),
                          //     child: TextFormField(
                          //       keyboardType: TextInputType.text,
                          //       controller: searchCtrl,
                          //       textAlign: TextAlign.left,
                          //       style: normalTextFont.copyWith(
                          //           fontSize: ScreenUtil().setSp(14)),
                          //       decoration: new InputDecoration(
                          //           hintStyle: normalTextFont.copyWith(
                          //               fontSize: ScreenUtil().setSp(12),
                          //               color: Color(0xffD1D1D1)),
                          //           hintText: getTranslated(context, 'searchf'),
                          //           border: InputBorder.none,
                          //           fillColor: Colors.white),
                          //     ),
                          //   ),
                          // ),
                          (state is ReqLoading)
                              ? shimmerProduct(context)
                              : _viewResult()
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _reqGetExplore() {
    _bloc.add(ReqExplore());
  }

  _viewResult() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [for (CategoryModel item in resultSearch) itemResult(item)],
      ),
    );
  }

  itemResult(CategoryModel item) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ListProductPage(
                      title: item.name,
                      id: item.id_category,
                    )));
      },
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(ScreenUtil().setWidth(10)),
                        color: Colors.white),
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/img_placeholder.png",
                      image: item.image,
                      fit: BoxFit.contain,
                    ),
                  ),
                  flex: 1,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        item.name,
                        style: boldTextFont.copyWith(
                            fontSize: ScreenUtil().setSp(12)),
                      ),
                      Text(
                        "${item.total} Items",
                        style: normalMuliFont.copyWith(
                            fontSize: ScreenUtil().setSp(10)),
                      ),
                    ],
                  ),
                  flex: 6,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Divider(),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
          ],
        ),
      ),
    );
  }
}

class ExploreModel {
  String image;
  String title;
  String count_item;

  ExploreModel(this.image, this.title, this.count_item);
}
