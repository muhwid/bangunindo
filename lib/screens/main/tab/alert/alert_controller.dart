import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/coupon_model.dart';
import 'package:e_commerce_app/model/object/notification_model.dart';
import 'package:e_commerce_app/model/response/Notification_response.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:get/get.dart';

class AlertController extends GetxController {
  RxBool isLoadingNotif = false.obs;
  RxString errorMessage = "".obs;

  RxList<NotificationModel> notif = List<NotificationModel>().obs;
  Dio dio = ApiServices().launch();
  @override
  void onInit() {
    apiGetCategory();
    super.onInit();
  }

  void setErrorMessage(String msg) async{
    errorMessage.value = msg;
  }

  void apiGetCategory() async {
    isLoadingNotif = isLoadingNotif.toggle();
    final resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_notification}");
    isLoadingNotif = isLoadingNotif.toggle();
    NotificationResponse response = NotificationResponse.fromJson(resp.data);
    if(response.status.startsWith("20")){
      notif.clear();
      notif.addAll(response.data);
    }else{
      if(response.invalid != null)
        errorMessage.value = response.invalid;
      else errorMessage.value = response.message.toString();
    }
  }


  @override
  void onClose() {
    this.dispose();
    super.onClose();
  }
}