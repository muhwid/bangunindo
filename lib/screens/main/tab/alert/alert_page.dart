import 'package:e_commerce_app/model/alert_model.dart';
import 'package:e_commerce_app/model/object/notification_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/main/tab/alert/alert_controller.dart';
import 'package:e_commerce_app/screens/transaction/detail_transaction/detail_transaction.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/widget/refresh_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AlertPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AlertPageState();
  }
}

class AlertPageState extends State<AlertPage> {
  final box = GetStorage();
  AlertController ctrl;

  List<AlertModel> alerts = List();

  @override
  void initState() {
    ctrl = Get.put(AlertController());

    refAlert();
    super.initState();
  }

  Future refAlert() async {
    await Future.delayed(Duration(microseconds: 200));

    setState(() {
      ctrl.apiGetCategory();
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: Obx(
            () => RefreshWidget(
              onRefresh: refAlert,
              child: SingleChildScrollView(
                child: Stack(
                  children: [
                    ClipPath(
                        clipper: OvalBottomBorderClipper(),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: ScreenUtil().setHeight(200),
                          color: primaryColor,
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(30),
                              left: ScreenUtil().setWidth(15)),
                          child: Text(
                            getTranslated(context, 'notification'),
                            style: boldMuliFont.copyWith(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(18)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(20),
                              right: ScreenUtil().setWidth(20)),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white),
                            child: IconButton(
                                icon: Icon(Icons.refresh_rounded),
                                color: primaryColor,
                                onPressed: refAlert),
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: ScreenUtil().setHeight(80)),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                left: ScreenUtil().setHeight(15),
                                right: ScreenUtil().setHeight(15),
                                bottom: ScreenUtil().setHeight(15)),
                            child: Card(
                              child: Container(
                                margin:
                                    EdgeInsets.all(ScreenUtil().setWidth(15)),
                                child: (ctrl.isLoadingNotif.value)
                                    ? loading()
                                    : _viewAlert(context),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget loading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _injectAlerts() {
    for (int i = 0; i < 25; i++) {
      alerts.add(AlertModel("Lorem ipsum dolor sit amet", "12 Dec 2020"));
    }
  }

  _viewAlert(BuildContext context) {
    return ctrl.notif.length < 1
        ? Container(
            width: MediaQuery.of(context).size.width,
            child: Text("${getTranslated(context, "empty_nofit")}"),
          )
        : Column(
            children: [
              for (NotificationModel dt in ctrl.notif) _itemAlert(dt, context)
            ],
          );
  }

  _itemAlert(NotificationModel data, BuildContext context) {
    return InkWell(
        onTap: () async {
          if (data.transaction != null) {
            var result = await Navigator.of(context).push(MaterialPageRoute(
                builder: (ctx) => DetailTransactionPage(
                    item: data.transaction, store: false)));
          }
        },
        child: Container(
          margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(20)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.text,
                style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
              Text(
                data.created_at,
                style:
                    normalTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
              Divider()
            ],
          ),
        ));
  }
}
