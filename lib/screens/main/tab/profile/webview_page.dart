import 'package:e_commerce_app/res/theme.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewPage extends StatefulWidget {
  final String title;
  final int id;

  WebviewPage({Key key, @required this.title, this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return WebviewPageState();
  }
}

class WebviewPageState extends State<WebviewPage> {
  _launchURL() async {
    const url = 'https://wa.me/628123456789';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgPage,
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: primaryColor,
      ),
      body: Container(
          padding: EdgeInsets.all(20),
          child: WebView(
            initialUrl: "https://bangunindo.lumira.live/api/page/index/" +
                widget.id.toString(),
          )),
      floatingActionButton: (widget.id == 4)
          ? Container(
              height: 40.0,
              child: FittedBox(
                  child: FloatingActionButton.extended(
                      backgroundColor: Colors.green,
                      label: Text("Whatsapp"),
                      icon: Icon(Icons.chat_bubble_outline_rounded),
                      onPressed: () async {
                        const url = 'http://wa.me/6282216682268';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      })))
          : Container(),
    );
  }
}
