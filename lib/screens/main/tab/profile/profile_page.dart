import 'package:e_commerce_app/base/box_storage.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/helper/shared_preferences.dart';
import 'package:e_commerce_app/main.dart';
import 'package:e_commerce_app/model/body/withdraw_body.dart';
import 'package:e_commerce_app/model/object/store_model.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/list/list_address_page.dart';
import 'package:e_commerce_app/screens/address/store_address_page.dart';
import 'package:e_commerce_app/screens/balance_history/balanceHistory.dart';
import 'package:e_commerce_app/screens/edit_profile/edit_profile_page.dart';
import 'package:e_commerce_app/screens/main/tab/profile/bloc/profile_bloc.dart';
import 'package:e_commerce_app/screens/main/tab/profile/webview_page.dart';
import 'package:e_commerce_app/screens/product/seller_transaction.dart';
import 'package:e_commerce_app/screens/register/register_store.dart';
import 'package:e_commerce_app/screens/store_item/store_item.dart';
import 'package:e_commerce_app/screens/topup/topup_page.dart';
import 'package:e_commerce_app/screens/transaction/list_order.dart';
import 'package:e_commerce_app/screens/user/type_sending.dart';
import 'package:e_commerce_app/screens/wishlist/list_wishlist_page.dart';
import 'package:e_commerce_app/utils/language/lang.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:dio/dio.dart';
import 'bloc/profile_event.dart';
import 'bloc/profile_state.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfilePageState();
  }
}

class ProfilePageState extends State<ProfilePage>
    with TickerProviderStateMixin {
  final box = GetStorage();
  var pembeli = true;
  var selectedTab = 1;
  var name = "";
  StoreModel store;
  int saldo = 0;
  int saldoSeller = 0;
  String errorlog;
  bool loading = false;
  Rx<TextEditingController> wd = TextEditingController().obs;
  Rx<TextEditingController> rek = TextEditingController().obs;
  String bank;
  ProfileBloc _bloc;
  ApiDomain _domain;
  PersistentBottomSheetController _controller; // <------ Instance variable
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Timer _timer;
  Dio dio = ApiServices().launch();

  withdraw(context) async {
    if (int.parse(wd.value.text) < 10000) {
      _controller.setState(() {
        errorlog = "Minimal penarikan adalah Rp 10.000";
        _timer = new Timer(const Duration(milliseconds: 2000), () {
          _controller.setState(() {
            errorlog = null;
          });
        });
      });
    } else if (int.parse(wd.value.text) > saldoSeller) {
      _controller.setState(() {
        errorlog = "Nominal penarikan lebih besar dari saldo yang anda miliki";
        _timer = new Timer(const Duration(milliseconds: 2000), () {
          _controller.setState(() {
            errorlog = null;
          });
        });
      });
    } else {
      loading = true;
      WithdrawBody body = WithdrawBody.fromJson({
        "bank": bank,
        "account_number": rek.value.text,
        "amount": wd.value.text
      });
      final resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_withdraw}",
          data: body.toJson());
      BaseResponse response = BaseResponse.fromJson(resp.data);
      if (response.status.startsWith("20")) {
        Navigator.pop(context, "Hello world");
        _controller.setState(() {
          loading = false;
        });
        setState(() {
          _bloc.add(ReqGetProfile());
        });
        //GlobalHelper().showSnackBar("Pesanan berhasil diterima",context);
      } else {
        _controller.setState(() {
          loading = false;
        });
      }
    }
  }

  void _createBottomSheet() {
    _controller = _scaffoldKey.currentState.showBottomSheet((context) =>
        StatefulBuilder(
          builder: (BuildContext context, setState) => SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                (errorlog != null)
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.red[900],
                        padding: EdgeInsets.all(20),
                        child: Text(
                          errorlog,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    : Container(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[200],
                  padding: EdgeInsets.all(20),
                  child: Text(
                      'Permintaan penarikan dana hanya dapat dilakukan pada jam kerja dengan waktu penanganan 3x24 jam',
                      textAlign: TextAlign.center),
                ),
                Container(
                    padding: EdgeInsets.all(20),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DropdownButton<String>(
                          isExpanded: true,
                          hint: Text('Pilih Bank tujuan'),
                          value: bank,
                          items: <String>['BCA', 'MANDIRI', 'BNI', 'BRI']
                              .map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (velu) {
                            setState(() {
                              bank = velu;
                            });
                          },
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Text("Nomor Rekening"),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: rek.value,
                          textAlign: TextAlign.left,
                          style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                          decoration: new InputDecoration(
                              hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                              hintText: "Masukkan nomor rekening",
                              fillColor: bgPage),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Text("Nominal"),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: wd.value,
                          textAlign: TextAlign.left,
                          style: normalTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                          decoration: new InputDecoration(
                              hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                              hintText: "Masukkan nominal penarikan",
                              fillColor: bgPage),
                        )
                      ],
                    )),
                (loading)
                    ? Center(child: CircularProgressIndicator())
                    : WidgetComponent.primaryButton(
                        onPress: () {
                          withdraw(context);
                        },
                        title: "KIRIM"),
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                )
              ],
            ),
          ),
        ));
  }

  view() {
    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                ClipPath(
                    clipper: OvalBottomBorderClipper(),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: ScreenUtil().setHeight(200),
                      color: primaryColor,
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          top: ScreenUtil().setHeight(30),
                          left: ScreenUtil().setWidth(15)),
                      child: Text(
                        getTranslated(context, "profile"),
                        style: boldMuliFont.copyWith(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(22),
                      child: DropdownButton(
                          onChanged: (Language language) {
                            _changeLanguage(language);
                          },
                          underline: SizedBox(),
                          icon: Icon(Icons.language, color: Colors.white),
                          items: Language.languageList()
                              .map<DropdownMenuItem<Language>>(
                                (lang) => DropdownMenuItem(
                                    value: lang,
                                    child: Row(
                                      children: <Widget>[
                                        Text(lang.flag),
                                        Text(lang.name)
                                      ],
                                    )),
                              )
                              .toList()),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(80)),
                  child: Column(
                    children: [
                      //header
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setHeight(15),
                            right: ScreenUtil().setHeight(15),
                            bottom: ScreenUtil().setHeight(15)),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: Offset(0, 3),
                                color: Colors.grey.withOpacity(0.2),
                              ),
                            ],
                            borderRadius: BorderRadius.circular(12.0)),
                        child: Column(
                          children: [
                            SizedBox(height: 20.0),
                            Row(
                              children: [
                                Expanded(
                                  child: SizedBox(),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      right: ScreenUtil().setWidth(20)),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EditProfilePage()));
                                    },
                                    child: Text("Edit",
                                        style: boldTextFont.copyWith(
                                            fontSize: ScreenUtil().setSp(12),
                                            color: primaryColor)),
                                  ),
                                ),
                              ],
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(
                                  ScreenUtil().setWidth(60)),
                              child: Image.asset(URL.PROFILE_PHOTO,
                                  fit: BoxFit.cover,
                                  width: ScreenUtil().setWidth(80)),
                            ),
                            SizedBox(height: ScreenUtil().setWidth(8)),
                            Text("$name",
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12))),
                            SizedBox(height: 5),
                            Text("Bronze",
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12),
                                    color: bronze)),
                            SizedBox(height: 5),
                            Text(
                                "${getTranslated(context, "saldo")}" +
                                    " : Rp.${GlobalHelper().formattingNumber(saldo)}",
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(10),
                                    color: Colors.black)),
                            SizedBox(height: 16),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(
                                    child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(8.0),
                                    ),
                                  ),
                                  child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          selectedTab = 1;
                                        });
                                      },
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 30.0,
                                            child: Center(
                                              child: Text(
                                                getTranslated(context, "buyer"),
                                                textAlign: TextAlign.center,
                                                style: normalTextFont.copyWith(
                                                    fontSize:
                                                        ScreenUtil().setSp(12)),
                                              ),
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(
                                                  left:
                                                      ScreenUtil().setWidth(20),
                                                  top:
                                                      ScreenUtil().setHeight(5),
                                                  right: ScreenUtil()
                                                      .setWidth(20)),
                                              height: ScreenUtil().setHeight(5),
                                              color: (selectedTab == 1)
                                                  ? primaryColor
                                                  : Colors.white)
                                        ],
                                      )),
                                )),
                                Expanded(
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            selectedTab = 2;
                                          });
                                        },
                                        child: Column(
                                          children: [
                                            Container(
                                              height: 30.0,
                                              child: Center(
                                                child: Text(
                                                    getTranslated(
                                                        context, "seller"),
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        normalTextFont.copyWith(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(
                                                                        12))),
                                              ),
                                            ),
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: ScreenUtil()
                                                        .setWidth(20),
                                                    top: ScreenUtil()
                                                        .setHeight(5),
                                                    right: ScreenUtil()
                                                        .setWidth(20)),
                                                height:
                                                    ScreenUtil().setHeight(5),
                                                color: (selectedTab == 2)
                                                    ? primaryColor
                                                    : Colors.white)
                                          ],
                                        )))
                              ],
                            ),
                          ],
                        ),
                      ),
                      //menu
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setHeight(15),
                            right: ScreenUtil().setHeight(15),
                            bottom: ScreenUtil().setHeight(15)),
                        child: selectedTab == 2
                            ? (store != null)
                                ? Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            spreadRadius: 2,
                                            blurRadius: 5,
                                            offset: Offset(0, 3),
                                            color: Colors.grey.withOpacity(0.2),
                                          ),
                                        ],
                                        borderRadius:
                                            BorderRadius.circular(12.0)),
                                    child: Column(
                                      children: [
                                        itemMenuSaldo(
                                            getTranslated(context, "saldop"),
                                            saldoSeller.toString(), () {
                                          Get.to(BalanceHistory(
                                              saldo: saldoSeller));
                                          // _createBottomSheet();
                                        }),
                                        itemMenu(
                                            getTranslated(context, "penjualan"),
                                            () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          TransactionSeller()));
                                        }),
                                        itemMenu(
                                            getTranslated(context, "listi"),
                                            () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          StoreItem()));
                                        }),
                                        itemMenu(
                                            getTranslated(context, "shipping"),
                                            () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      TypeSending()));
                                        }),
                                        itemMenu(
                                            getTranslated(context, "address"),
                                            () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      StoreAddressPage()));
                                        }),
                                      ],
                                    ),
                                  )
                                : Container(
                                    child: InkWell(
                                      onTap: () async {
                                        final result =
                                            await Get.to(RegisterStorePage());
                                        if (result == true) {
                                          _reqGetProfile();
                                        }
                                      },
                                      child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(
                                              top: 20, bottom: 20),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                  spreadRadius: 3,
                                                  blurRadius: 5,
                                                  offset: Offset(0, 3),
                                                  color: Colors.grey
                                                      .withOpacity(0.5),
                                                ),
                                              ],
                                              borderRadius:
                                                  BorderRadius.circular(12.0)),
                                          child: Column(
                                            children: [
                                              Text(
                                                  getTranslated(
                                                      context, 'regas'),
                                                  style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold))
                                            ],
                                          )),
                                    ),
                                  )
                            : Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        spreadRadius: 3,
                                        blurRadius: 5,
                                        offset: Offset(0, 3),
                                        color: Colors.grey.withOpacity(0.5),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(12.0)),
                                child: Column(
                                  children: [
                                    itemMenu(getTranslated(context, "whislist"),
                                        () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  ListWishlistPage()));
                                    }),
                                    itemMenu(getTranslated(context, "topup"),
                                        () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  TopupPage()));
                                    }),
                                    itemMenu(getTranslated(context, "myorder"),
                                        () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  ListOrder()));
                                    }),
                                    itemMenu(
                                        getTranslated(
                                            context, "delivery_address"), () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ListAddressPage()));
                                    }),
                                  ],
                                ),
                              ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(15),
                            right: ScreenUtil().setWidth(15),
                            bottom: ScreenUtil().setWidth(15)),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  spreadRadius: 3,
                                  blurRadius: 5,
                                  offset: Offset(0, 3),
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                              borderRadius: BorderRadius.circular(12.0)),
                          child: Column(
                            children: [
                              itemMenu(getTranslated(context, "about_us"), () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            WebviewPage(
                                                title: "About Us", id: 3)));
                              }),
                              itemMenu(getTranslated(context, "term"), () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            WebviewPage(
                                                title: "Term of Service",
                                                id: 2)));
                              }),
                              itemMenu(getTranslated(context, "privacy_policy"),
                                  () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            WebviewPage(
                                                title: "Privacy Policy",
                                                id: 1)));
                              }),
                              itemMenu(getTranslated(context, "contact"), () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            WebviewPage(
                                                title: "Contact", id: 4)));
                              }),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(15),
                            right: ScreenUtil().setWidth(15),
                            bottom: ScreenUtil().setWidth(15)),
                        child: Row(
                          children: [
                            Expanded(
                              child: WidgetComponent.primaryButton(
                                  onPress: () {
                                    CustomDialog().showDialogConfirms(
                                        context,
                                        "Are you sure want to logout from this account ?",
                                        "Yes",
                                        "No", () {
                                      Navigator.pop(context);
                                      Get.offAllNamed("/loginPage");
                                      BoxStorage().setLogin(false);
                                    }, () {
                                      Navigator.pop(context);
                                    });
                                  },
                                  title: getTranslated(context, "signout")
                                      .toUpperCase()),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  itemMenu(String title, Function onClick) {
    return InkWell(
      onTap: () {
        onClick();
      },
      child: Container(
        margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(15),
            right: ScreenUtil().setWidth(15),
            top: ScreenUtil().setWidth(20)),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style:
                      boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: ScreenUtil().setWidth(10),
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            Divider()
          ],
        ),
      ),
    );
  }

  itemMenuSaldo(String title, String saldo, Function onClick) {
    return InkWell(
      onTap: () {
        onClick();
      },
      child: Container(
        margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(15),
            right: ScreenUtil().setWidth(15),
            top: ScreenUtil().setWidth(20)),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style:
                      boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                ),
                Text("Rp. ${GlobalHelper().formattingNumber(int.parse(saldo))}")
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            Divider()
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    // SharedPreferencesProvider().getName().then((value) {
    //   setState(() {
    //     name = value;
    //   });
    // });

    _domain = ApiDomain(ApiRepository());
    _bloc = ProfileBloc(domain: _domain);

    _reqGetProfile();

    super.initState();
  }

  _reqGetProfile() {
    _bloc.add(ReqGetProfile());
  }

  _changeLanguage(Language language) {
    Locale _temp;
    switch (language.languageCode) {
      case "id":
        _temp = Locale(language.languageCode, 'ID');
        break;

      case "en":
        _temp = Locale(language.languageCode, 'US');
        break;
      default:
        _temp = Locale(language.languageCode, 'ID');
    }

    MyApp.setLocale(context, _temp);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: bgPage,
      body: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (BuildContext context) {
              return _bloc;
            },
          )
        ],
        child: BlocListener<ProfileBloc, ProfileState>(
          bloc: _bloc,
          listener: (context, state) {
            if (state is ReqLoading) {
              CustomDialog().loading(
                  context, "${getTranslated(context, "processing_request")}");
            }
            if (state is ReqGetProfileError) {
              Navigator.pop(context);
              CustomDialog().showDialogConfirms(
                  context,
                  state.error,
                  "${getTranslated(context, "retry")}",
                  "${getTranslated(context, "close")}", () {
                Navigator.pop(context);
                _reqGetProfile();
              }, () {
                Navigator.pop(context);
              });
            } else if (state is ReqGetProfileSuccess) {
              Navigator.pop(context);
              SharedPreferencesProvider().setName(state.resp.data.name);
              saldo = state.resp.data.balance;
              if (state.resp.data.store != null) {
                saldoSeller = state.resp.data.store.balance;
              }
              name = state.resp.data.name;
              store = state.resp.data.store;
            }
          },
          child: BlocBuilder<ProfileBloc, ProfileState>(
            bloc: _bloc,
            builder: (context, state) {
              return view();
            },
          ),
        ),
      ),
    );
  }

  // void _changeLanguage(Language language) {
  //   print(language.languageCode);
  // }
}
