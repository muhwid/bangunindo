import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/get_profile_response.dart';
import 'package:flutter/material.dart';

import 'profile_event.dart';
import 'profile_state.dart';


class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ApiDomain domain;

  ProfileBloc({@required this.domain}) : assert(domain != null);

  @override
  ProfileState get initialState => InitPage();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is ReqGetProfile) {
      yield ReqLoading();
      try {
        GetProfileResponse resp = await domain.reqGetProfile();
        if (resp.status.startsWith("20"))
          yield ReqGetProfileSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqGetProfileError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqGetProfileError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqGetProfileError(error: e.toString());
      }
    }
  }
}
