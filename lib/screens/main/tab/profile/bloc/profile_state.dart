
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:e_commerce_app/model/response/coupon_response.dart';
import 'package:e_commerce_app/model/response/get_profile_response.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:equatable/equatable.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();
}

class InitPage extends ProfileState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends ProfileState {
  @override
  List<Object> get props => [];
}

class ReqGetProfileSuccess extends ProfileState {
  final GetProfileResponse resp;

  const ReqGetProfileSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqGetProfileError extends ProfileState {
  final String error;

  const ReqGetProfileError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqGetProfileError { error: $error }';
}