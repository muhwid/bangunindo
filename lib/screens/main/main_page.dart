import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/main/tab/alert/alert_page.dart';
import 'package:e_commerce_app/screens/main/tab/chat/chat_page.dart';
import 'package:e_commerce_app/screens/main/tab/explore/explore_page.dart';
import 'package:e_commerce_app/screens/main/tab/home/home_page.dart';
import 'package:e_commerce_app/screens/main/tab/profile/profile_page.dart';
import 'package:e_commerce_app/utils/benindo_icons.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainPage extends StatefulWidget {
  int index;
  MainPage({
    this.index = 0,
  });
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  var selected = 0;
  Widget pages(int index) {
    switch (index) {
      case 0:
        return HomePage();
      case 1:
        return ExplorePage();
      case 2:
        return AlertPage();
      case 3:
        return ChatPage();
      case 4:
        return ProfilePage();
    }
  }

  Widget appBar(int index) {
    switch (index) {
      case 0:
        return Text("Hello Gustav");
      case 1:
        return Text("${getTranslated(context, "explore")}");
      case 2:
        return Text("${getTranslated(context, "notification")}");
      case 3:
        return Text("Chat");
      case 4:
        return Text("${getTranslated(context, "profile")}");
    }
  }

  @override
  void initState() {
    selected = widget.index;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: [pages(selected)],
      ),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (int index) {
            setState(() {
              selected = index;
            });
          },
          currentIndex: selected,
          type: BottomNavigationBarType.fixed,
          // unselectedItemColor: Colours.secondColor,
          selectedItemColor: primaryColor,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(
                  Benindo.ic_home,
                  size: ScreenUtil().setSp(15),
                ),
                title: Text(
                  getTranslated(context, 'home'),
                  style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      color: (selected == 0) ? primaryColor : Colors.grey),
                )),
            BottomNavigationBarItem(
                icon: Icon(Benindo.ic_explore, size: ScreenUtil().setSp(15)),
                title: Text(
                  getTranslated(context, 'ex'),
                  style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      color: (selected == 1) ? primaryColor : Colors.grey),
                )),
            BottomNavigationBarItem(
                icon: Icon(Benindo.ic_alert, size: ScreenUtil().setSp(15)),
                title: Text(
                  getTranslated(context, 'alert'),
                  style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      color: (selected == 2) ? primaryColor : Colors.grey),
                )),
            BottomNavigationBarItem(
                icon:
                    Icon(Icons.message_outlined, size: ScreenUtil().setSp(18)),
                title: Text(
                  'Chat',
                  style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      color: (selected == 3) ? primaryColor : Colors.grey),
                )),
            BottomNavigationBarItem(
                icon: Icon(Benindo.ic_profile, size: ScreenUtil().setSp(15)),
                title: Text(
                  getTranslated(context, 'profile'),
                  style: normalTextFont.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      color: (selected == 4) ? primaryColor : Colors.grey),
                )),
          ]),
    );
  }
}
