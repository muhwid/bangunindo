
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:equatable/equatable.dart';

abstract class SearchState extends Equatable {
  const SearchState();
}

class InitPage extends SearchState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends SearchState {
  @override
  List<Object> get props => [];
}

class SearchSuccess extends SearchState {
  final ProductResponse resp;

  const SearchSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class SearchError extends SearchState {
  final String error;

  const SearchError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}