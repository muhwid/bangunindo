import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:flutter/material.dart';
import 'search_event.dart';
import 'search_state.dart';

class SearchBloc extends Bloc<Searchevent, SearchState> {
  final ApiDomain domain;

  SearchBloc({@required this.domain}) : assert(domain != null);

  @override
  SearchState get initialState => InitPage();

  @override
  Stream<SearchState> mapEventToState(Searchevent event) async* {
    if (event is ReqSearch) {
      yield ReqLoading();
      try {
        ProductResponse resp = await domain.reqSearchProduct(event.q);
        if (resp.status.startsWith("20"))
          yield SearchSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield SearchError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield SearchError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield SearchError(error: e.toString());
      }
    }
  }
}
