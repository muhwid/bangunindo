import 'package:e_commerce_app/model/body/login_body.dart';
import 'package:equatable/equatable.dart';

abstract class Searchevent extends Equatable {
  const Searchevent();

  @override
  List<Object> get props => [];
}

class ReqSearch extends Searchevent {
  final String q;

  const ReqSearch(this.q);

  @override
  List<Object> get props => [q];
}
