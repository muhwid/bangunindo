import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/product/detail_product/detail_product.dart';
import 'package:e_commerce_app/screens/search/bloc/search_bloc.dart';
import 'package:e_commerce_app/screens/search/bloc/search_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/shimmer_product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/search_state.dart';

class SearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchPageState();
  }
}

class SearchPageState extends State<SearchPage> {
  SearchBloc _bloc;
  ApiDomain _domain;
  var searchCtrl = new TextEditingController();
//  List<SearchPageModel> resultSearch = List();
  List<ProductModel> resultSearch = List();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = SearchBloc(domain: _domain);

//    injectResult();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
    ScreenUtil(width: 360, height: 640, allowFontScaling: true)
      ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<SearchBloc, SearchState>(
              bloc: _bloc,
              listener: (context, state) {
                if(state is ReqLoading){
                  // CustomDialog().loading(context, "Search product");
                }else if(state is SearchSuccess){
                  // Navigator.pop(context);
                  resultSearch.addAll(state.resp.data);
                  setState(() {
                  });
                }else if(state is SearchError){
                  // Navigator.pop(context);
                  CustomDialog().showDialogConfirms(context, state.error, "${getTranslated(context, "retry")}", "${getTranslated(context, "close")}", (){
                    Navigator.pop(context);
                    _reqSearch();
                  }, (){
                    Navigator.pop(context);
                  });
                }
              },
              child: BlocBuilder<SearchBloc, SearchState>(
                bloc: _bloc,
                builder: (context, state) {
                  return Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("${getTranslated(context, "searching")}",
                            style: boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(16)),),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: ScreenUtil().setSp(15), right: ScreenUtil().setSp(15)),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Container(
                              margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.search,
                                onFieldSubmitted: (value) {
                                  if(searchCtrl.text.isEmpty){
                                    FlushBarMessage().error(context, "Oops", "${getTranslated(context, "fill_all_form")}", 2);
                                    return;
                                  }

                                  resultSearch.clear();
                                  _reqSearch();
                                },
                                controller: searchCtrl,
                                textAlign: TextAlign.left,
                                style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
                                decoration: new InputDecoration(
                                    hintStyle: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12), color: Color(0xffD1D1D1)),
                                    hintText: "${getTranslated(context, "search_unique_furniture")}",
                                    border: InputBorder.none,
                                    fillColor: Colors.white),
                              ),
                            ),
                          ),
                          (state is ReqLoading) ? shimmerProduct(context) : _viewResultSearch()
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _viewResultSearch() {
    return Container(
      margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [
          for (ProductModel item in resultSearch) itemResult(item)
        ],
      ),
    );
  }

  _reqSearch(){
    _bloc.add(ReqSearch(searchCtrl.text.toString()));
  }

  itemResult(ProductModel item){
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailProduct(id: item.id_product,)));
      },
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(10)), color: Colors.black),
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/img_placeholder.png",
                      image: (item.images.length > 0) ? item.images[0] : "",
                      fit: BoxFit.contain,
                    ),
                  ),
                  flex: 2,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        item.name,
                        style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
                      ),
                      Text(
                        "Rp. ${GlobalHelper().formattingNumber(int.parse(item.price))}",
                        style: normalMuliFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                      ),
                    ],
                  ),
                  flex: 6,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Icon(
                    Icons.arrow_forward_ios,
                  size: 15,
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Divider(),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
          ],
        ),
      )
    ) ;
  }

}
