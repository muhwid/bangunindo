import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:flutter/material.dart';

import 'otp_event.dart';
import 'otp_state.dart';

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  final ApiDomain domain;

  OtpBloc({@required this.domain}) : assert(domain != null);

  @override
  OtpState get initialState => InitPage();

  @override
  Stream<OtpState> mapEventToState(OtpEvent event) async* {
    if (event is ReqOtp) {
      yield ReqLoading();
      try {
        BaseResponse resp = await domain.reqVerifyOtp(event.body);
        if (resp.status.startsWith("20"))
          yield ReqOtpSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqOtpError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqOtpError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqOtpError(error: e.toString());
      }
    }
  }
}
