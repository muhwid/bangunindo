
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:equatable/equatable.dart';

abstract class OtpState extends Equatable {
  const OtpState();
}

class InitPage extends OtpState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends OtpState {
  @override
  List<Object> get props => [];
}

class ReqOtpSuccess extends OtpState {
  final BaseResponse resp;

  const ReqOtpSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqOtpError extends OtpState {
  final String error;

  const ReqOtpError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}