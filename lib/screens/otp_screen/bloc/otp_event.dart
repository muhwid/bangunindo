import 'package:e_commerce_app/model/body/otp_body.dart';
import 'package:equatable/equatable.dart';

abstract class OtpEvent extends Equatable {
  const OtpEvent();

  @override
  List<Object> get props => [];
}

class ReqOtp extends OtpEvent {
  final OtpBody body;

  const ReqOtp(this.body);

  @override
  List<Object> get props => [body];
}
