import 'package:device_info/device_info.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/otp_body.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/login/login_page.dart';
import 'package:e_commerce_app/screens/new_password/new_password_page.dart';
import 'package:e_commerce_app/screens/otp_screen/bloc/otp_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'bloc/otp_bloc.dart';
import 'bloc/otp_state.dart';
import 'package:get/get.dart';

class OtpScreenPage extends StatefulWidget {
  final String phoneNUmber;
  final bool isforgot;

  OtpScreenPage({Key key, @required this.phoneNUmber, this.isforgot})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OtpScreenPageState();
  }
}

class OtpScreenPageState extends State<OtpScreenPage> {
  OtpBloc _bloc;
  ApiDomain _domain;
  OtpBody body;
  String deviceName = "";

  TextEditingController otpController = new TextEditingController();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = OtpBloc(domain: _domain);

    _getDeviceName();
    super.initState();
  }

  _getDeviceName() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceName = androidInfo.model;
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceName = iosInfo.utsname.machine;
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<OtpBloc, OtpState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqLoading) {
                  CustomDialog().loading(context,
                      "${getTranslated(context, "processing_request")}");
                } else if (state is ReqOtpError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqVerifyOtp();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqOtpSuccess) {
                  if (widget.isforgot != null) {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NewPasswordPage(
                                  phoneNUmber: widget.phoneNUmber,
                                )));
                  } else {
                    Navigator.pop(context);
                    Get.offAll(LoginPage());
                  }
                }
              },
              child: BlocBuilder<OtpBloc, OtpState>(
                bloc: _bloc,
                builder: (context, state) {
                  return SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(80)),
                          Center(
                              child: Image.asset(
                            URL.COMPANY_LOGO,
                            width: 200.0,
                          )),
                          SizedBox(height: ScreenUtil().setHeight(80)),
                          Padding(
                            padding: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(8),
                            ),
                            child: Text("OTP",
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(16))),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 12.0),
                            child: Text(
                              "${getTranslated(context, "please_enter_verif_code")}",
                              style: normalTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(10)),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: ScreenUtil().setWidth(10),
                                bottom: ScreenUtil().setHeight(8),
                                top: ScreenUtil().setHeight(16)),
                            child: Text("otp".toUpperCase(),
                                style: boldTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12))),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.grey),
                              color: bgPage,
                            ),
                            child: Container(
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(10),
                                  right: ScreenUtil().setWidth(10)),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                controller: otpController,
                                textAlign: TextAlign.left,
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12)),
                                decoration: new InputDecoration(
                                    hintStyle:
                                        TextStyle(color: Color(0xffD1D1D1)),
                                    border: InputBorder.none,
                                    hintText:
                                        "${getTranslated(context, "enter_otp_code")}",
                                    fillColor: bgPage),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(20),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: WidgetComponent.primaryButton(
                                    onPress: () {
                                      if (otpController.text.isEmpty) {
                                        FlushBarMessage().error(
                                            context,
                                            "Oops",
                                            getTranslated(
                                                context, "fill_all_form"),
                                            2);
                                        return;
                                      }

                                      body = OtpBody(widget.phoneNUmber,
                                          otpController.text, "", deviceName);
                                      _reqVerifyOtp();
                                    },
                                    title: "Submit".toUpperCase()),
                              )
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(80),
                          ),
                          Center(
                              child: InkWell(
                                  onTap: () {
                                    Navigator.pushReplacementNamed(
                                        context, "login");
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 35,
                                    child: Center(
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                            text: getTranslated(context,
                                                "already_have_account"),
                                            style: normalTextFont.copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(10)),
                                          ),
                                          TextSpan(
                                            text: getTranslated(
                                                context, "sign_in"),
                                            style: boldTextFont.copyWith(
                                                color: primaryColor,
                                                fontSize:
                                                    ScreenUtil().setSp(12)),
                                          ),
                                        ]),
                                      ),
                                    ),
                                  ))),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _reqVerifyOtp() {
    _bloc.add(ReqOtp(body));
  }
}
