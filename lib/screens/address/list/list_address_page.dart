import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/add/add_address_page.dart';
import 'package:e_commerce_app/screens/address/list/bloc/list_address_bloc.dart';
import 'package:e_commerce_app/screens/address/list/bloc/list_address_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_address.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';

import 'bloc/list_address_state.dart';

class ListAddressPage extends StatefulWidget {
  bool needRefresh;
  ListAddressPage({Key key, this.needRefresh}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ListAddressPageState();
  }
}

class ListAddressPageState extends State<ListAddressPage> {
  ListAddressBloc _bloc;
  ApiDomain _domain;
  List<AddressesModel> addresses = List();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = ListAddressBloc(domain: _domain);
    _reqPermission();
    _reqListAddress();
    super.initState();
  }

  _reqPermission() async {
    await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
          child: Scaffold(
              backgroundColor: bgPage,
              floatingActionButton: FloatingActionButton(
                  backgroundColor: primaryColor,
                  onPressed: () async {
                    final result = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddAddressPage()));
                    if (result != null) {
                      refreshData();
                    }
                  },
                  child: Icon(Icons.add)),
              appBar: WidgetComponent.appBar(
                  text: "${getTranslated(context, "list_address")}"),
              body: MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (BuildContext context) {
                      return _bloc;
                    },
                  )
                ],
                child: BlocListener<ListAddressBloc, ListAddressState>(
                  bloc: _bloc,
                  listener: (context, state) {
                    if (state is ReqLoading) {
                      CustomDialog().loading(context,
                          "${getTranslated(context, "processing_request")}");
                    } else if (state is ReqListAddressError) {
                      Navigator.pop(context);
                      CustomDialog().showDialogConfirms(
                          context,
                          state.error,
                          "${getTranslated(context, "retry")}",
                          "${getTranslated(context, "close")}", () {
                        Navigator.pop(context);
                        _reqListAddress();
                      }, () {
                        Navigator.pop(context);
                      });
                    } else if (state is ReqListAddressSuccess) {
                      Navigator.pop(context);
                      addresses.clear();
                      if (state.resp.data != null) {
                        addresses.addAll(state.resp.data);
                      }
                    }
                  },
                  child: BlocBuilder<ListAddressBloc, ListAddressState>(
                    bloc: _bloc,
                    builder: (context, state) {
                      return RefreshIndicator(
                        onRefresh: refreshData,
                        child: (state is ReqListAddressSuccess)
                            ? (addresses.length > 0)
                                ? Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(15),
                                        right: ScreenUtil().setWidth(15),
                                        top: ScreenUtil().setHeight(25)),
                                    child: ListView.builder(
                                        itemCount: addresses.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return itemAddress(addresses[index],
                                              () async {
                                            if (widget.needRefresh != null) {
                                              Navigator.pop(
                                                  context, addresses[index]);
                                            } else {
                                              final result = await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          AddAddressPage(
                                                              address:
                                                                  addresses[
                                                                      index])));
                                              _reqListAddress();
                                            }
                                          });
                                        }),
                                  )
                                : emptyAddressView()
                            : Container(),
                      );
                    },
                  ),
                ),
              ))),
    );
  }

  _reqListAddress() {
    _bloc.add(ReqGetListAddress());
  }

  Future refreshData() async {
    addresses.clear();
    _reqListAddress();
  }

  Widget emptyAddressView() {
    return Container(
      margin: EdgeInsets.all(ScreenUtil().setWidth(20)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset("assets/images/img_empty.svg"),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Text(
            "${getTranslated(context, "no_address")}",
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(15),
          ),
          Row(
            children: [
              Expanded(
                child: WidgetComponent.primaryButton(
                    onPress: () async {
                      final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddAddressPage()));
                      if (result != null) {
                        refreshData();
                      }
                    },
                    title: getTranslated(context, "add_address").toUpperCase()),
              )
            ],
          )
        ],
      ),
    );
  }
}
