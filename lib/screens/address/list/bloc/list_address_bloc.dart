import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/addresses_response.dart';
import 'package:flutter/material.dart';

import 'list_address_event.dart';
import 'list_address_state.dart';

class ListAddressBloc extends Bloc<ListAddressEvent, ListAddressState> {
  final ApiDomain domain;

  ListAddressBloc({@required this.domain}) : assert(domain != null);

  @override
  ListAddressState get initialState => InitPage();

  @override
  Stream<ListAddressState> mapEventToState(ListAddressEvent event) async* {
    if (event is ReqGetListAddress) {
      yield ReqLoading();
      try {
        AddressesResponse resp = await domain.reqListAddress();
        if (resp.status.startsWith("20") || resp.status == "404")
          yield ReqListAddressSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqListAddressError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqListAddressError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqListAddressError(error: e.toString());
      }
    }
  }
}
