import 'package:e_commerce_app/model/response/addresses_response.dart';
import 'package:equatable/equatable.dart';

abstract class ListAddressState extends Equatable {
  const ListAddressState();
}

class InitPage extends ListAddressState {
  @override
  List<Object> get props => [];
}

class ReqLoading extends ListAddressState {
  @override
  List<Object> get props => [];
}

class ReqListAddressSuccess extends ListAddressState {
  final AddressesResponse resp;

  const ReqListAddressSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqListAddressError extends ListAddressState {
  final String error;

  const ReqListAddressError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}
