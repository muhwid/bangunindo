import 'package:equatable/equatable.dart';

abstract class ListAddressEvent extends Equatable {
  const ListAddressEvent();

  @override
  List<Object> get props => [];
}

class ReqGetListAddress extends ListAddressEvent {
  const ReqGetListAddress();

  @override
  List<Object> get props => [];
}
