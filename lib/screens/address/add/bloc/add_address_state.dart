import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/city_response.dart';
import 'package:e_commerce_app/model/response/district_response.dart';
import 'package:e_commerce_app/model/response/province_response.dart';
import 'package:equatable/equatable.dart';

abstract class AddAddressState extends Equatable {
  const AddAddressState();
}

class InitPage extends AddAddressState {
  @override
  List<Object> get props => [];
}

class ReqProvinceLoading extends AddAddressState {
  @override
  List<Object> get props => [];
}

class ReqProvinceSuccess extends AddAddressState {
  final ProvinceResponse resp;

  const ReqProvinceSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqProvinceError extends AddAddressState {
  final String error;

  const ReqProvinceError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqCitySuccess extends AddAddressState {
  final CityResponse resp;

  const ReqCitySuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqCityError extends AddAddressState {
  final String error;

  const ReqCityError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqDistrictSuccess extends AddAddressState {
  final DistrictResponse resp;

  const ReqDistrictSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqDistrictError extends AddAddressState {
  final String error;

  const ReqDistrictError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqAddAddressSuccess extends AddAddressState {
  final BaseResponse resp;

  const ReqAddAddressSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqDeleteAddressSuccess extends AddAddressState {
  final BaseResponse resp;

  const ReqDeleteAddressSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqAddAddressError extends AddAddressState {
  final String error;

  const ReqAddAddressError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqAddAddressError { error: $error }';
}
