import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/city_response.dart';
import 'package:e_commerce_app/model/response/district_response.dart';
import 'package:e_commerce_app/model/response/province_response.dart';
import 'package:flutter/material.dart';

import 'add_address_event.dart';
import 'add_address_state.dart';

class AddAddressBloc extends Bloc<AddAddressEvent, AddAddressState> {
  final ApiDomain domain;

  AddAddressBloc({@required this.domain}) : assert(domain != null);

  @override
  AddAddressState get initialState => InitPage();

  @override
  Stream<AddAddressState> mapEventToState(AddAddressEvent event) async* {
    if (event is ReqProvince) {
      yield ReqProvinceLoading();
      try {
        ProvinceResponse resp = await domain.reqProvince();
        if (resp.status.startsWith("20"))
          yield ReqProvinceSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqProvinceError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqProvinceError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqProvinceError(error: e.toString());
      }
    }

    if (event is ReqDistrict) {
      yield ReqProvinceLoading();
      try {
        DistrictResponse resp = await domain.reqDistrict(event.idCity);
        if (resp.status.startsWith("20"))
          yield ReqDistrictSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqDistrictError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqDistrictError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqDistrictError(error: e.toString());
      }
    }

    if (event is ReqCity) {
      yield ReqProvinceLoading();
      try {
        CityResponse resp = await domain.reqCity(event.idProvince);
        if (resp.status.startsWith("20"))
          yield ReqCitySuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqCityError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqCityError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqCityError(error: e.toString());
      }
    }

    if (event is ReqAddAddress) {
      yield ReqProvinceLoading();
      try {
        BaseResponse resp = await domain.reqAddAddress(event.body);
        if (resp.status.startsWith("20"))
          yield ReqAddAddressSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqAddAddressError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqAddAddressError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqAddAddressError(error: e.toString());
      }
    }
    if (event is ReqDeleteAddress) {
      yield ReqProvinceLoading();
      try {
        BaseResponse resp = await domain.reqDeleteAddress(event.idAddress);
        if (resp.status.startsWith("20"))
          yield ReqDeleteAddressSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqAddAddressError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqAddAddressError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqAddAddressError(error: e.toString());
      }
    }
  }
}
