import 'package:e_commerce_app/model/body/add_address_body.dart';
import 'package:equatable/equatable.dart';

abstract class AddAddressEvent extends Equatable {
  const AddAddressEvent();

  @override
  List<Object> get props => [];
}

class ReqProvince extends AddAddressEvent {
  const ReqProvince();

  @override
  List<Object> get props => [];
}

class ReqCity extends AddAddressEvent {
  final String idProvince;
  const ReqCity(this.idProvince);

  @override
  List<Object> get props => [idProvince];
}

class ReqDistrict extends AddAddressEvent {
  final String idCity;
  const ReqDistrict(this.idCity);

  @override
  List<Object> get props => [idCity];
}

class ReqAddAddress extends AddAddressEvent {
  final AddAddressBody body;
  const ReqAddAddress(this.body);

  @override
  List<Object> get props => [body];
}

class ReqDeleteAddress extends AddAddressEvent {
  final String idAddress;
  const ReqDeleteAddress(this.idAddress);

  @override
  List<Object> get props => [idAddress];
}
