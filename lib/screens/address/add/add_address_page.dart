import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/bottom_sheet.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/add_address_body.dart';
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/model/object/shipping_location_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_bloc.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_event.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_state.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';

class AddAddressPage extends StatefulWidget {
  AddressesModel address;

  AddAddressPage({Key key, this.address}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return AddAddressPageState();
  }
}

class AddAddressPageState extends State<AddAddressPage> {
  ShippingLocationModel selectedLocation;

  AddAddressBloc _bloc;
  ApiDomain _domain;

  var nameCtrl = TextEditingController();
  var addressCtrl = TextEditingController();
  var addressManualCtrl = TextEditingController();
  var provinceCtrl = TextEditingController();
  var cityCtrl = TextEditingController();
  var districtCtrl = TextEditingController();
  var phoneCtrl = TextEditingController();
  var searchCtrl = TextEditingController();

  String latitude;
  String longitude;

  ProvinceModel selectedProvince;
  CityModel selectedCity;
  DistrictModel selectedDistrict;

  LocationResult _pickedLocation = LocationResult();

  List<ProvinceModel> provinces = List();
  List<CityModel> cities = List();
  List<DistrictModel> districts = List();

  String address = "";
  Position position;

  AddAddressBody body;
  static const apiKey = "AIzaSyCCVQQw-iYHeqSbqybaXSq0QnlcmZ_e0PE";

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = AddAddressBloc(domain: _domain);

    _reqProvince();
    super.initState();
    if (widget.address != null) {
      nameCtrl.text = widget.address.name;
      addressManualCtrl.text = widget.address.address;
      provinceCtrl.text = widget.address.province.province;
      cityCtrl.text = widget.address.city.city_name;
      districtCtrl.text = widget.address.district.name;
      phoneCtrl.text = widget.address.phone;

      selectedProvince = widget.address.province;
      selectedCity = widget.address.city;
      selectedDistrict = widget.address.district;
      latitude = widget.address.latitude;
      longitude = widget.address.longitude;
      getAddress();
    }
  }

  getAddress() async {
    var add = await Geocoder.local.findAddressesFromCoordinates(Coordinates(
        double.parse(widget.address.latitude),
        double.parse(widget.address.longitude)));

    addressCtrl.text = add.first.addressLine;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          appBar: AppBar(
            backgroundColor: primaryColor,
            actions: <Widget>[
              widget.address != null
                  ? FlatButton(
                      textColor: Colors.white,
                      onPressed: () {
                        CustomDialog().showDialogConfirms(
                            context,
                            "Apakah anda yakin akan menghapus alamat ini ?",
                            "Ya",
                            "Tidak", () {
                          Navigator.pop(context);
                          reqDelete();
                          //_reqProvince();
                        }, () {
                          Navigator.pop(context);
                        });
                      },
                      child: Icon(Icons.delete),
                      shape: CircleBorder(
                          side: BorderSide(color: Colors.transparent)),
                    )
                  : Container(),
            ],
            title: Text(widget.address != null
                ? "Edit Address"
                : getTranslated(context, "add_address")),
          ),
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<AddAddressBloc, AddAddressState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqProvinceLoading) {
                  CustomDialog().loading(context,
                      "${getTranslated(context, "processing_request")}");
                } else if (state is ReqProvinceError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqProvince();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqProvinceSuccess) {
                  provinces.clear();
                  provinces.addAll(state.resp.data);
                  Navigator.pop(context);
                }

                if (state is ReqCityError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqCity();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqCitySuccess) {
                  Navigator.pop(context);
                  cities.clear();
                  cities.addAll(state.resp.data);
                  showCityBottomSheet(
                      "${getTranslated(context, "choose_city")}",
                      searchCtrl,
                      cities, onClick: (dt) {
                    selectedCity = dt;
                    cityCtrl.text = selectedCity.city_name;
                    return;
                  });
                }

                if (state is ReqDistrictError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqDistrict();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqDistrictSuccess) {
                  Navigator.pop(context);
                  districts.clear();
                  districts.addAll(state.resp.data);
                  showDistrictBottomSheet(
                      "${getTranslated(context, "choose_district")}",
                      searchCtrl,
                      districts, onClick: (dt) {
                    selectedDistrict = dt;
                    districtCtrl.text = selectedDistrict.name;
                    return;
                  });
                }

                if (state is ReqAddAddressSuccess) {
                  Navigator.pop(context);
                  CustomDialog().showDialogAlert(context, state.resp.message,
                      getTranslated(context, "close"), () {
                    Navigator.pop(context);
                    Navigator.pop(context, true);
                  });
                } else if (state is ReqAddAddressError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqAddAddress();
                  }, () {
                    Navigator.pop(context);
                  });
                }
                if (state is ReqDeleteAddressSuccess) {
                  Navigator.pop(context);
                  Navigator.pop(context);
                }
              },
              child: BlocBuilder<AddAddressBloc, AddAddressState>(
                bloc: _bloc,
                builder: (context, state) {
                  return Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [_viewForm()],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _viewForm() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            getTranslated(context, "address_name"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            textAlign: TextAlign.left,
            controller: nameCtrl,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: getTranslated(context, "input_address_name"),
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "province"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              FocusManager.instance.primaryFocus.unfocus();
              showProvinceBottomSheet(getTranslated(context, "choose_province"),
                  searchCtrl, provinces, onClick: (dt) {
                selectedProvince = dt;
                provinceCtrl.text = selectedProvince.province;
                return;
              });
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: provinceCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "choose_province"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "city"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              if (selectedProvince == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "choose_province_first"), 2);
                return;
              }

              _reqCity();
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: cityCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "choose_city"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "district"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              if (selectedCity == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "choose_city_first"), 2);
                return;
              }

              _reqDistrict();
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: districtCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "choose_district"),
                  fillColor: bgPage),
            ),
          ),

          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "address"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            textAlign: TextAlign.left,
            controller: addressManualCtrl,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: getTranslated(context, "input_address_name"),
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "choose_location"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          // TODO:
          InkWell(
            onTap: () {
              pickCoordinat();
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: addressCtrl,
              onChanged: (text) {
                if (text.trim() == "") {
                  addressCtrl.text = _pickedLocation.address;
                } else {
                  return;
                }
              },
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "input_address"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "phone_number"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            textAlign: TextAlign.left,
            controller: phoneCtrl,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: getTranslated(context, "input_phone_number"),
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          InkWell(
            onTap: () {
              if (_pickedLocation == null ||
                  selectedProvince == null ||
                  selectedCity == null ||
                  selectedDistrict == null ||
                  nameCtrl.text.isEmpty ||
                  phoneCtrl.text.isEmpty) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "fill_all_form"), 2);
                return;
              }
              if (latitude == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "fill_all_form"), 2);
                return;
              }
              _reqAddAddress();
            },
            child: Row(
              children: [
                Expanded(
                  child: Card(
                    color: primaryColor,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                          child: Text(
                            getTranslated(context, "submit").toUpperCase(),
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: Colors.white),
                          )),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  pickCoordinat() async {
    if (selectedCity == null || selectedCity.city_name == null) {
      CustomDialog()
          .showDialogAlert(context, "Please Choose City First", null, null);
      return;
    }
    LocationResult result = await showLocationPicker(
      context,
      apiKey,
      initialCenter: LatLng(31.1975844, 29.9598339),
      myLocationButtonEnabled: true,
      layersButtonEnabled: true,
    );
    print("result = $result");

    //if (kab.trim() == selectedCity.city_name.toLowerCase().trim()) {
    setState(() => _pickedLocation = result);
    print("picked = $_pickedLocation");
    ShippingLocationModel.insert(_pickedLocation.address,
        _pickedLocation.latLng.latitude, _pickedLocation.latLng.longitude);
    addressCtrl.text = _pickedLocation.address;
    latitude = _pickedLocation.latLng.latitude.toString();
    longitude = _pickedLocation.latLng.longitude.toString();
    // } else {
    //   CustomDialog().showDialogAlert(
    //       context, "Your area must match with Cities you select", null, null);
    // }
  }

  _reqProvince() {
    _bloc.add(ReqProvince());
  }

  reqDelete() {
    _bloc.add(ReqDeleteAddress(widget.address.id_address));
  }

  _reqCity() {
    _bloc.add(ReqCity(selectedProvince.id_province));
  }

  _reqDistrict() {
    _bloc.add(ReqDistrict(selectedCity.id_cities));
  }

  _reqAddAddress() {
    print(latitude);
    body = AddAddressBody(
        widget.address != null ? widget.address.id_address : null,
        nameCtrl.text.toString(),
        addressManualCtrl.text.toString(),
        selectedDistrict.id_district.toString(),
        selectedCity.id_cities.toString(),
        selectedProvince.id_province.toString(),
        phoneCtrl.text.toString(),
        latitude,
        longitude);
    _bloc.add(ReqAddAddress(body));
  }
}
