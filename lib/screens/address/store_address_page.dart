import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/bottom_sheet.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/add_address_body.dart';
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/model/object/shipping_location_model.dart';
import 'package:e_commerce_app/model/object/store_model.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/store_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_bloc.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_event.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_state.dart';
import 'package:e_commerce_app/screens/pick_map/pick_map_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:dio/dio.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class StoreAddressPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StoreAddressPageState();
  }
}

class StoreAddressPageState extends State<StoreAddressPage> {
  ShippingLocationModel selectedLocation;
  AddAddressBloc _bloc;
  ApiDomain _domain;

  var nameCtrl = TextEditingController();
  var addressCtrl = TextEditingController();
  var addressManualCtrl = TextEditingController();
  var provinceCtrl = TextEditingController();
  var cityCtrl = TextEditingController();
  var districtCtrl = TextEditingController();
  var phoneCtrl = TextEditingController();
  var searchCtrl = TextEditingController();

  ProvinceModel selectedProvince;
  CityModel selectedCity;
  DistrictModel selectedDistrict;
  StoreModel store;
  List<ProvinceModel> provinces = List();
  List<CityModel> cities = List();
  List<DistrictModel> districts = List();

  AddAddressBody body;
  RxBool isLoading = false.obs;
  Dio dio = ApiServices().launch();
  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = AddAddressBloc(domain: _domain);
    _reqProvince();
    _reqAddress();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          appBar: WidgetComponent.appBar(
              text: "${getTranslated(context, "store_address")}"),
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<AddAddressBloc, AddAddressState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqProvinceLoading) {
                  CustomDialog().loading(
                      context, getTranslated(context, "processing_request"));
                } else if (state is ReqProvinceError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqProvince();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqProvinceSuccess) {
                  provinces.clear();
                  provinces.addAll(state.resp.data);
                  Navigator.pop(context);
                }

                if (state is ReqCityError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqCity();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqCitySuccess) {
                  Navigator.pop(context);
                  cities.clear();
                  cities.addAll(state.resp.data);
                  showCityBottomSheet(
                      "${getTranslated(context, "choose_city")}",
                      searchCtrl,
                      cities, onClick: (dt) {
                    selectedCity = dt;
                    cityCtrl.text = selectedCity.city_name;
                    return;
                  });
                }

                if (state is ReqDistrictError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqDistrict();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqDistrictSuccess) {
                  Navigator.pop(context);
                  districts.clear();
                  districts.addAll(state.resp.data);
                  showDistrictBottomSheet(
                      "${getTranslated(context, "choose_district")}",
                      searchCtrl,
                      districts, onClick: (dt) {
                    selectedDistrict = dt;
                    districtCtrl.text = selectedDistrict.name;
                    return;
                  });
                }

                if (state is ReqAddAddressSuccess) {
                  Navigator.pop(context);
                  CustomDialog().showDialogAlert(
                      context, state.resp.message, "Close", () {
                    Navigator.pop(context);
                    Navigator.pop(context, true);
                  });
                } else if (state is ReqAddAddressError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    //_reqAddAddress();
                  }, () {
                    Navigator.pop(context);
                  });
                }
              },
              child: BlocBuilder<AddAddressBloc, AddAddressState>(
                bloc: _bloc,
                builder: (context, state) {
                  return Container(
                    margin: EdgeInsets.all(ScreenUtil().setSp(20)),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [_viewForm(context)],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _viewForm(context) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            getTranslated(context, "address"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            textAlign: TextAlign.left,
            controller: addressManualCtrl,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
            decoration: new InputDecoration(
                hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                hintText: getTranslated(context, "input_address"),
                fillColor: bgPage),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "choose_location"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () async {
              ShippingLocationModel result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PickMapPage()),
              );
              if (result != null) {
                selectedLocation = result;
                setState(() {
                  addressCtrl.text =
                      "${selectedLocation.lat}, ${selectedLocation.lng}";
                });
              }
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: addressCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "input_address"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "province"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              showProvinceBottomSheet(getTranslated(context, "choose_province"),
                  searchCtrl, provinces, onClick: (dt) {
                selectedProvince = dt;
                provinceCtrl.text = selectedProvince.province;
                return;
              });
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: provinceCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "choose_province"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "city"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              if (selectedProvince == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "choose_province_first"), 2);
                return;
              }

              _reqCity();
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: cityCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "choose_city"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          Text(
            getTranslated(context, "district"),
            style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          ),
          InkWell(
            onTap: () {
              if (selectedCity == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "choose_city_first"), 2);
                return;
              }

              _reqDistrict();
            },
            child: TextFormField(
              keyboardType: TextInputType.text,
              textAlign: TextAlign.left,
              controller: districtCtrl,
              enabled: false,
              style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
              decoration: new InputDecoration(
                  hintStyle: TextStyle(color: Color(0xffD1D1D1)),
                  hintText: getTranslated(context, "choose_district"),
                  fillColor: bgPage),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(25),
          ),
          // Text(phone_number, style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),),
          // TextFormField(
          //   keyboardType: TextInputType.number,
          //   textAlign: TextAlign.left,
          //   controller: phoneCtrl,
          //   style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
          //   decoration: new InputDecoration(
          //       hintStyle: TextStyle(color: Color(0xffD1D1D1)),
          //       hintText: input_phone_number,
          //       fillColor: bgPage),
          // ),
          // SizedBox(
          //   height: ScreenUtil().setHeight(25),
          // ),
          InkWell(
            onTap: () {
              if (selectedLocation == null ||
                  selectedProvince == null ||
                  selectedCity == null ||
                  selectedDistrict == null) {
                FlushBarMessage().error(context, "Oops",
                    getTranslated(context, "fill_all_form"), 2);
                return;
              }

              _reqAddAddress(context);
            },
            child: Row(
              children: [
                Expanded(
                  child: Card(
                    color: primaryColor,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                          child: (isLoading.value)
                              ? CircularProgressIndicator()
                              : Text(
                                  getTranslated(context, "submit")
                                      .toUpperCase(),
                                  style: boldTextFont.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      color: Colors.white),
                                )),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _reqAddress() async {
    final resp =
        await dio.get("${Endpoint.baseUrl}${Endpoint.api_get_store_address}");
    StoreResponse response = StoreResponse.fromJson(resp.data);
    if (response.status.startsWith("20")) {
      setState(() {
        addressManualCtrl.text = response.data.address;
        addressCtrl.text =
            "${response.data.latitude}, ${response.data.longitude}";

        var lt = {
          'address': response.data.address,
          'lat': double.parse(response.data.latitude),
          'lng': double.parse(response.data.longitude)
        };
        selectedLocation = ShippingLocationModel.fromJson(lt);
        provinceCtrl.text = provinces
            .where((element) => element.id_province == response.data.province)
            .first
            .province;
        var json = {'id_province': response.data.province};
        selectedProvince = ProvinceModel.fromJson(json);
        var ct = {'id_cities': response.data.city};
        selectedCity = CityModel.fromJson(ct);
        var ds = {'id_district': response.data.city};
        selectedDistrict = DistrictModel.fromJson(ds);
        cityCtrl.text = response.data.city_name;
        districtCtrl.text = response.data.district_name;
      });
    } else {}
  }

  _reqProvince() {
    _bloc.add(ReqProvince());
  }

  _reqCity() {
    _bloc.add(ReqCity(selectedProvince.id_province));
  }

  _reqDistrict() {
    _bloc.add(ReqDistrict(selectedCity.id_cities));
  }

  _reqAddAddress(context) async {
    body = AddAddressBody(
        null,
        nameCtrl.text.toString(),
        addressManualCtrl.text.toString(),
        selectedDistrict.id_district,
        selectedCity.id_cities.toString(),
        selectedProvince.id_province.toString(),
        phoneCtrl.text.toString(),
        "${selectedLocation.lat}",
        "${selectedLocation.lng}");

    setState(() {
      isLoading = isLoading.toggle();
    });

    final resp = await dio.post(
        "${Endpoint.baseUrl}${Endpoint.api_store_address}",
        data: body.toJson());
    BaseResponse response = BaseResponse.fromJson(resp.data);
    if (response.status.startsWith("20")) {
      setState(() {
        isLoading = isLoading.toggle();
      });
    } else {
      setState(() {
        isLoading = isLoading.toggle();
      });
    }
    FlushBarMessage().toast(context, "${getTranslated(context, "success")}",
        response.message, Colors.green, 2);
    print(response);
  }
}
