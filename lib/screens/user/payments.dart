import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';

class PaymentMethod extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PaymentMethodeState();
  }
}

class PaymentMethodeState extends State<PaymentMethod> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: WidgetComponent.appBar(text: "${getTranslated(context, "pmethod")}"),
      body: Stack(
        children: [WidgetComponent.clipPath(context : context)],
      ),
    );
  }
}
