import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';

class EditDelivery extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return EditDeliveryState();
  }
}

class EditDeliveryState extends State<EditDelivery> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: WidgetComponent.appBar(text: "${getTranslated(context, "edit_address")}"),
      body: ListView(
        children: [],
      ),
    );
  }
}
