import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_bloc.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_address.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../res/theme.dart';
import 'edit_delivery.dart';

class DeliveryOrderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DeliveryOrderPageState();
  }
}

class DeliveryOrderPageState extends State<DeliveryOrderPage> {
  List<AddressesModel> address = List();
  AddAddressBloc _bloc;
  ApiDomain _domain;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            backgroundColor: primaryColor,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => EditDelivery()));
            },
            child: Icon(Icons.edit)),
        appBar: WidgetComponent.appBar(text: "${getTranslated(context, "delivery_address")}"),
        body: ListView.builder(itemBuilder: (BuildContext context, int index) {
          return itemAddress(address[index], (){});
        }));
  }


}
