import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/api_services.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/shipping_service_body.dart';
import 'package:e_commerce_app/model/delivery_model.dart';
import 'package:e_commerce_app/model/object/shipping_service_model.dart';
import 'package:e_commerce_app/model/response/shipping_service_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

class TypeSending extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TypeSendingState();
  }
}

class TypeSendingState extends State<TypeSending> {
  bool isLoading = false;
  Dio dio = ApiServices().launch();
  List<ShippingServiceModel> shippingService = [];
  RxString errorMessage = "".obs;

  // injectResult() {
  //   deliveries.add(DeliveryModel("SiCepat REG", false));
  //   deliveries.add(DeliveryModel("SiCepat BEST", false));
  //   deliveries.add(DeliveryModel("J&T REG", false));
  //   deliveries.add(DeliveryModel("JNE REG", false));
  //   deliveries.add(DeliveryModel("POS KILAT KHUSUS", false));
  //   deliveries.add(DeliveryModel("Kurir Pribadi", false));
  // }
  void apiGetCategory() async {
    isLoading = true;
    final resp = await dio.get("${Endpoint.baseUrl}${Endpoint.api_shipping_service}");
    ShippingServiceResponse response = ShippingServiceResponse.fromJson(resp.data);
    if(response.status.startsWith("20")){
      //shippingService.clear();
      setState(() {
        isLoading = false;
        shippingService.addAll(response.data);
      });
    }else{
      if(response.invalid != null)
        errorMessage.value = response.invalid;
      else errorMessage.value = response.message.toString();
    }
  }
  void _reqSave(context) async{
    setState(() {
      isLoading = true;
    });
    
    ShippingServiceBody body;
    body = ShippingServiceBody(shippingService);
    final resp = await dio.post("${Endpoint.baseUrl}${Endpoint.api_shipping_service}", data: body.toJson());
    ShippingServiceResponse response = ShippingServiceResponse.fromJson(resp.data);
    if(response.status.startsWith("20")){
      //shippingService.clear();
      setState(() {
        isLoading = false;
         FlushBarMessage().toast(context, "${getTranslated(context,"success")}", "${getTranslated(context, "success_save_data")}", Colors.green, 3);
      });
    }else{
      if(response.invalid != null)
        errorMessage.value = response.invalid;
      else errorMessage.value = response.message.toString();
    }
  }
  @override
  void initState() {
    apiGetCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: (isLoading) ? Center(child: CircularProgressIndicator(),) : Container(
            margin: EdgeInsets.all(ScreenUtil().setSp(20)),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${getTranslated(context, "shipping_service")}",
                    style:
                        boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(16)),
                  ),
                  _viewDeliveries(),
                  Center(
                    child: WidgetComponent.primaryButton(
                    onPress: () {
                      _reqSave(context);
                    },
                    title: "${getTranslated(context, "submit").toUpperCase()}")
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _viewDeliveries() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [
          for (int i = 0; i < shippingService.length; i++)
            itemDelivery(i, shippingService[i])
        ],
      ),
    );
  }

  itemDelivery(int index, ShippingServiceModel item) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  item.name,
                  style:
                      boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
                ),
                flex: 5,
              ),
              Expanded(
                child: CupertinoSwitch(
                  value: item.isactive,
                  activeColor: cyan,
                  onChanged: (value) {
                    setState(() {
                      setState(() {
                        shippingService[index].isactive = value;
                      });
                    });
                  },
                ),
              )
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}
