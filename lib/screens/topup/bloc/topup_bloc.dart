import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/va_response.dart';
import 'package:flutter/material.dart';

import 'topup_event.dart';
import 'topup_state.dart';

class TopupBloc extends Bloc<TopupEvent, TopupState> {
  final ApiDomain domain;

  TopupBloc({@required this.domain}) : assert(domain != null);

  @override
  TopupState get initialState => InitPage();

  @override
  Stream<TopupState> mapEventToState(TopupEvent event) async* {
    if (event is ReqVa) {
      yield ReqLoading();
      try {
        VaResponse resp = await domain.reqListVa();
        if (resp.status.startsWith("20"))
          yield ReqVaSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqVaError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqVaError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqVaError(error: e.toString());
      }
    }
  }
}
