
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:e_commerce_app/model/response/va_response.dart';
import 'package:equatable/equatable.dart';

abstract class TopupState extends Equatable {
  const TopupState();
}

class InitPage extends TopupState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends TopupState {
  @override
  List<Object> get props => [];
}

class ReqVaSuccess extends TopupState {
  final VaResponse resp;

  const ReqVaSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqVaError extends TopupState {
  final String error;

  const ReqVaError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqVaError { error: $error }';
}