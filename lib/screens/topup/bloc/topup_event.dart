import 'package:equatable/equatable.dart';

abstract class TopupEvent extends Equatable {
  const TopupEvent();

  @override
  List<Object> get props => [];
}

class ReqVa extends TopupEvent {
  const ReqVa();

  @override
  List<Object> get props => [];
}
