import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/object/va_model.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/topup/bloc/topup_bloc.dart';
import 'package:e_commerce_app/screens/topup/bloc/topup_state.dart';
import 'package:e_commerce_app/screens/topup/detail_topup/detail_va_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/topup_event.dart';

class TopupPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TopupPagetate();
  }
}

class TopupPagetate extends State<TopupPage> {

  TopupBloc _bloc;
  ApiDomain _domain;
  List<VaModel> transactions = List();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = TopupBloc(domain: _domain);

    _reqVa();
    super.initState();
  }

  _reqVa(){
    _bloc.add(ReqVa());
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
    ScreenUtil(width: 360, height: 640, allowFontScaling: true)
      ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: Container(
            margin: EdgeInsets.all(ScreenUtil().setSp(20)),
            child:  MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (BuildContext context) {
                    return _bloc;
                  },
                )
              ],
              child: BlocListener<TopupBloc, TopupState>(
                bloc: _bloc,
                listener: (context, state) {
                  if (state is ReqLoading) {
                    CustomDialog().loading(context, "${getTranslated(context, "wait_a_moment")}");
                  }else if(state is ReqVaError){
                    Navigator.pop(context);
                    CustomDialog().showDialogConfirms(context, state.error, "${getTranslated(context, "retry")}", "${getTranslated(context, "close")}", (){
                      Navigator.pop(context);
                      _reqVa();
                    }, (){
                      Navigator.pop(context);
                    });
                  }else if (state is ReqVaSuccess){
                    Navigator.pop(context);
                    transactions.clear();
                    transactions.addAll(state.resp.data);
                  }
                },
                child: BlocBuilder<TopupBloc, TopupState>(
                  bloc: _bloc,
                  builder: (context, state) {
                    return SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: (){
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(10),),
                              Text("${getTranslated(context, "topup")}",
                                style: boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(16)),)
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          _viewListVa()
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _viewListVa() {
    return Container(
      margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(25)),
      child: Column(
        children: [
          for (VaModel item in transactions) itemMenu(item)
        ],
      ),
    );
  }

  itemMenu(VaModel data) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailVaPage(codeName: data.bank_code)));
      },
      child: Container(
        margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(15),
            right: ScreenUtil().setWidth(15),
            top: ScreenUtil().setWidth(20)),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  data.bank_code,
                  style:
                  boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: ScreenUtil().setWidth(10),
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            Divider()
          ],
        ),
      ),
    );
  }
}
