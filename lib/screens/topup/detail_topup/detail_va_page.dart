import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/model/object/instruction_model.dart';
import 'package:e_commerce_app/model/object/va_model.dart';
import 'package:e_commerce_app/model/response/detail_va_response.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/topup/detail_topup/bloc/detail_va_bloc.dart';
import 'package:e_commerce_app/screens/topup/detail_topup/bloc/detail_va_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/detail_va_state.dart';

class DetailVaPage extends StatefulWidget {

  String codeName;
  DetailVaPage({Key key, @required this.codeName}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DetailVaPagetate();
  }
}

class DetailVaPagetate extends State<DetailVaPage> {

  DetailVaBloc _bloc;
  ApiDomain _domain;
  DetailVaResponse resp;
  List<InstructionModel> instructions = List();

  @override
  void initState() {
    _domain = ApiDomain(ApiRepository());
    _bloc = DetailVaBloc(domain: _domain);

    _reqVa();
    super.initState();
  }

  _reqVa(){
    _bloc.add(ReqDetailVa(name: widget.codeName));
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
    ScreenUtil(width: 360, height: 640, allowFontScaling: true)
      ..init(context);

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: Container(
            margin: EdgeInsets.all(ScreenUtil().setSp(20)),
            child:  MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (BuildContext context) {
                    return _bloc;
                  },
                )
              ],
              child: BlocListener<DetailVaBloc, DetailVaState>(
                bloc: _bloc,
                listener: (context, state) {
                  if (state is ReqLoading) {
                    CustomDialog().loading(context, "${getTranslated(context, "wait_a_moment")} ...");
                  }else if(state is ReqDetailVaError){
                    Navigator.pop(context);
                    CustomDialog().showDialogConfirms(context, state.error, "${getTranslated(context, "retry")}", "${getTranslated(context, "close")}", (){
                      Navigator.pop(context);
                      _reqVa();
                    }, (){
                      Navigator.pop(context);
                    });
                  }else if (state is ReqDetailVaSuccess){
                    Navigator.pop(context);
                    resp = state.resp;
                    instructions.clear();
                    instructions.addAll(state.resp.data.instruction);
                  }
                },
                child: BlocBuilder<DetailVaBloc, DetailVaState>(
                  bloc: _bloc,
                  builder: (context, state) {
                    return SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: (){
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(10),),
                              Text("${getTranslated(context, "topup")}",
                                style: boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(16)),)
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          (state is ReqLoading) ? Center(child: CircularProgressIndicator(),) : _viewVa(),
                          SizedBox(
                            height: ScreenUtil().setHeight(35),
                          ),
                          Text("${getTranslated(context, "follow_instruction_below")}", style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14), color: Color(0xFFC4C4C4)),),
                          SizedBox(
                            height: ScreenUtil().setHeight(10),
                          ),
                          _instructionView()
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _viewVa() {
    return Container(
      margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(25)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("${resp.data.va.bank_code}", style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(18)),),
          SizedBox(height: ScreenUtil().setHeight(10),),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("${resp.data.va.account_number}", style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(18), color: accentColor),),
              SizedBox(width: ScreenUtil().setWidth(5),),
              InkWell(
                onTap: (){},
                child: Icon(Icons.copy_outlined, color: accentColor,),
              ),],
          ),
          SizedBox(height: ScreenUtil().setHeight(10),),
          Text("${getTranslated(context, "account_name")} : ${resp.data.va.name}", style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),),
        ],
      ),
    );
  }

  _instructionView() {
    return Container(
      child: Column(
        children: [
          for (InstructionModel item in instructions) itemMenu(item)
        ],
      ),
    );
  }

  itemMenu(InstructionModel data) {
    return InkWell(
      onTap: () {
        _showDialogInstruction(data.text);
      },
      child: Container(
        margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(15),
            right: ScreenUtil().setWidth(15),
            top: ScreenUtil().setWidth(20)),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  data.name,
                  style:
                  boldMuliFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: ScreenUtil().setWidth(10),
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            Divider()
          ],
        ),
      ),
    );
  }

  void _showDialogInstruction(String content) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (builder) {
          return new SingleChildScrollView(
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return Container(
                    color: transparent,
                    child: new Container(
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.only(
                                topLeft: const Radius.circular(15),
                                topRight: const Radius.circular(15))),
                        child: Padding(
                          padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: ScreenUtil().setHeight(25),
                              ),
                              Html(
                                data: "$content",
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(25),
                              ),
                            ],
                          ),
                        )),
                  );
                }),
          );
        });
  }
}
