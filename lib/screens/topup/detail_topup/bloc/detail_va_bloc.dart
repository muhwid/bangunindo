import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/detail_va_response.dart';
import 'package:flutter/material.dart';

import 'detail_va_event.dart';
import 'detail_va_state.dart';

class DetailVaBloc extends Bloc<DetailVaEvent, DetailVaState> {
  final ApiDomain domain;

  DetailVaBloc({@required this.domain}) : assert(domain != null);

  @override
  DetailVaState get initialState => InitPage();

  @override
  Stream<DetailVaState> mapEventToState(DetailVaEvent event) async* {
    if (event is ReqDetailVa) {
      yield ReqLoading();
      try {
        DetailVaResponse resp = await domain.reqDetailVa(event.name);
        if (resp.status.startsWith("20"))
          yield ReqDetailVaSuccess(resp: resp);
        else {
          if (resp.invalid != null) {
            yield ReqDetailVaError(error: resp.invalid.toString());
          } else {
            print("error success 0 ${resp.message.toString()}");
            yield ReqDetailVaError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqDetailVaError(error: e.toString());
      }
    }
  }
}
