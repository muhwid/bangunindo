
import 'package:e_commerce_app/model/response/detail_va_response.dart';
import 'package:equatable/equatable.dart';

abstract class DetailVaState extends Equatable {
  const DetailVaState();
}

class InitPage extends DetailVaState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends DetailVaState {
  @override
  List<Object> get props => [];
}

class ReqDetailVaSuccess extends DetailVaState {
  final DetailVaResponse resp;

  const ReqDetailVaSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqDetailVaError extends DetailVaState {
  final String error;

  const ReqDetailVaError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReqDetailVaError { error: $error }';
}