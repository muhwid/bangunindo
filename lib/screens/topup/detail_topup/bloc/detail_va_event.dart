import 'package:equatable/equatable.dart';

abstract class DetailVaEvent extends Equatable {
  const DetailVaEvent();

  @override
  List<Object> get props => [];
}

class ReqDetailVa extends DetailVaEvent {
  final String name;
  const ReqDetailVa({this.name});

  @override
  List<Object> get props => [name];
}
