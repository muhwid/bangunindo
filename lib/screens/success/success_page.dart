import 'package:clipboard/clipboard.dart';
import 'package:e_commerce_app/data/database_helper.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/order_body.dart';
import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/model/object/bank_model.dart';
import 'package:e_commerce_app/model/object/data_shipping_model.dart';
import 'package:e_commerce_app/model/object/master_cost_model.dart';
import 'package:e_commerce_app/model/object/product_cart_model.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/response/order_response.dart';
import 'package:e_commerce_app/model/response/shipping_response.dart';
import 'package:e_commerce_app/res/strings.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/address/list/list_address_page.dart';
import 'package:e_commerce_app/screens/checkout/bloc/checkout_bloc.dart';
import 'package:e_commerce_app/screens/main/main_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';


class SuccessPage extends StatefulWidget {
  final DataListTransaction item;

  SuccessPage({Key key, @required this.item}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SuccessPageState();
  }
}

class SuccessPageState extends State<SuccessPage> {

  final f = new DateFormat('dd-MM-yyyy hh:mm');
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: SingleChildScrollView(
            child :Container(
              margin: EdgeInsets.only(top: ScreenUtil().setHeight(25)),
              child: Column(
                children: [
                  Container(
                    child: Column(
                      children: [
                        //header
                        Container(
                          padding: EdgeInsets.all(ScreenUtil().setSp(15)),
                          margin: EdgeInsets.only(
                              left: ScreenUtil().setHeight(15),
                              right: ScreenUtil().setHeight(15),
                              bottom: ScreenUtil().setHeight(15)),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  spreadRadius: 2,
                                  blurRadius: 5,
                                  offset: Offset(0, 3),
                                  color: Colors.grey.withOpacity(0.2),
                                ),
                              ],
                              borderRadius: BorderRadius.circular(12.0)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: ScreenUtil().setHeight(25),
                              ),
                              Text(
                                widget.item.payment,
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(15),
                              ),
                              Text("${getTranslated(context, "complete_order_before")}",
                                  style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12),
                                  )),
                              SizedBox(
                                height: ScreenUtil().setHeight(10),
                              ),
                              Text(
                                f.format(DateTime.parse(widget.item.expired)),
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(15),
                              ),
                              Text("${getTranslated(context, "virtual_account_number")}",
                                  style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12),
                                  )),
                              SizedBox(
                                height: ScreenUtil().setHeight(10),
                              ),
                              Text(
                                widget.item.bank_account_number,
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(15),
                              ),
                              InkWell(
                                onTap: (){
                                  FlutterClipboard.copy(widget.item.bank_account_number).then(( value ) => FlushBarMessage().toast(context, "${getTranslated(context, "success")}",
                                  "Berhasil disalin", Colors.green, 2));
                                },
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10,5,10,5),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.red),
                                  ),
                                  child: Text('Salin'),
                                ),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(15),
                              ),
                              Text("${getTranslated(context, "amount_should_paid")}",
                                  style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(12),
                                  )),
                              SizedBox(
                                height: ScreenUtil().setHeight(10),
                              ),
                              Text(
                                "Rp.${GlobalHelper().formattingNumber(int.parse(widget.item.total))}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 22,
                                    color: Colors.red),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(15),
                              ),
                              InkWell(
                                onTap: (){
                                  FlutterClipboard.copy(widget.item.total).then(( value ) => FlushBarMessage().toast(context, "${getTranslated(context, "success")}",
                                  "Berhasil disalin", Colors.green, 2));
                                },
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10,5,10,5),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.red),
                                  ),
                                  child: Text('Salin'),
                                ),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(15),
                              ),
                            ],
                          ),
                        ),
                        for ( DetailInstruction how in widget.item.instruction )
                        Container(
                          padding: EdgeInsets.all(ScreenUtil().setSp(15)),
                          margin: EdgeInsets.only(
                              left: ScreenUtil().setHeight(15),
                              right: ScreenUtil().setHeight(15),
                              bottom: ScreenUtil().setHeight(5)),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  spreadRadius: 2,
                                  blurRadius: 5,
                                  offset: Offset(0, 3),
                                  color: Colors.grey.withOpacity(0.2),
                                ),
                              ],
                              borderRadius: BorderRadius.circular(12.0)),
                          child: ExpandablePanel(
                            header: Text(how.name,style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                            expanded: Html(data:how.text),
                            tapHeaderToExpand: true,
                            hasIcon: false,
                          ),
                        ),
                      ],
                    ),
                  )
                  
                ],
              ),
            ),
          )
        ),
      ),
    );
  }
}