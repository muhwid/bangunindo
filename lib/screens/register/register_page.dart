import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/model/body/register_body.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/main/tab/profile/webview_page.dart';
import 'package:e_commerce_app/screens/otp_screen/otp_screen_page.dart';
import 'package:e_commerce_app/screens/register/bloc/register_bloc.dart';
import 'package:e_commerce_app/screens/register/bloc/register_event.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/register_state.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterPageState();
  }
}

class RegisterPageState extends State<RegisterPage> {
  TextEditingController nameCtrl = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  TextEditingController confirmPassController = new TextEditingController();

  bool _showPassword = true;
  bool _showConfirmPassword = true;
  bool isChecked = false;

  RegisterBloc _bloc;
  ApiDomain _domain;

  RegisterBody body;

  @override
  void initState() {
    super.initState();
    _domain = ApiDomain(ApiRepository());
    _bloc = RegisterBloc(domain: _domain);
  }

  validator(String value) {
    if (value.isEmpty) {
      return false;
    } else {
      //bool mobileValid = RegExp(r"^(?:\+88||01)?(?:\d{10}|\d{13})$").hasMatch(value);

      bool mobileValid = RegExp(r'^(?:[+0]8)?[0-9]{9}$').hasMatch(value);
      return mobileValid ? true : false;
    }
  }

  emailValidator(String value) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<RegisterBloc, RegisterState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqLoading) {
                  CustomDialog().loading(context,
                      "${getTranslated(context, "processing_request")}");
                } else if (state is RegisterError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqRegister();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is RegisterSuccess) {
                  Navigator.pop(context);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => OtpScreenPage(
                                phoneNUmber: phoneController.text,
                              )));
                }
              },
              child: BlocBuilder<RegisterBloc, RegisterState>(
                bloc: _bloc,
                builder: (context, state) {
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                            margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: ScreenUtil().setHeight(75)),
                                Center(
                                    child: Image.asset(
                                  URL.COMPANY_LOGO,
                                  width: 200.0,
                                )),
                                SizedBox(height: ScreenUtil().setHeight(85)),
                                Text(
                                    getTranslated(context, "sign_up")
                                        .toUpperCase(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0)),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "name").toUpperCase()}*"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: nameCtrl,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_name")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "phone_number")}*"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: phoneController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_phone_number")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "email").toUpperCase()}*",
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: emailController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_email")}",
                                          fillColor: bgPage),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "password").toUpperCase()}*",
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      obscureText: _showPassword,
                                      controller: passController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_password")}",
                                          fillColor: bgPage,
                                          suffixIcon: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _showPassword = !_showPassword;
                                              });
                                            },
                                            child: Icon(
                                              _showPassword
                                                  ? Icons
                                                      .visibility_off_outlined
                                                  : Icons.visibility_outlined,
                                              color: primaryColor,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(15),
                                      bottom: ScreenUtil().setHeight(8),
                                      top: ScreenUtil().setHeight(16)),
                                  child: Text(
                                      "${getTranslated(context, "confirm_password")}*"
                                          .toUpperCase(),
                                      style: boldTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(10))),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(color: Colors.grey),
                                    color: bgPage,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(10),
                                        right: ScreenUtil().setWidth(10)),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      obscureText: _showConfirmPassword,
                                      controller: confirmPassController,
                                      textAlign: TextAlign.left,
                                      style: normalTextFont.copyWith(
                                          fontSize: ScreenUtil().setSp(12)),
                                      decoration: new InputDecoration(
                                          hintStyle: TextStyle(
                                              color: Color(0xffD1D1D1)),
                                          border: InputBorder.none,
                                          hintText:
                                              "${getTranslated(context, "input_confirm_password")}",
                                          fillColor: bgPage,
                                          suffixIcon: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _showConfirmPassword =
                                                    !_showConfirmPassword;
                                              });
                                            },
                                            child: Icon(
                                              _showConfirmPassword
                                                  ? Icons
                                                      .visibility_off_outlined
                                                  : Icons.visibility_outlined,
                                              color: primaryColor,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: ScreenUtil().setHeight(20),
                                ),
                                Wrap(children: [
                                  Row(
                                    children: [
                                      Checkbox(
                                        value: isChecked,
                                        onChanged: (bool b) {
                                          setState(() {
                                            isChecked = b;
                                          });
                                        },
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      WebviewPage(
                                                          title:
                                                              "Term of Service",
                                                          id: 2)));
                                        },
                                        child: Text(
                                          "${getTranslated(context, "terms")}",
                                          style: normalTextFont.copyWith(
                                            fontSize: 12,
                                            color: Colors.blue,
                                            decoration:
                                                TextDecoration.underline,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                        ),
                                      )
                                    ],
                                  ),
                                ]),
                                Row(
                                  children: [
                                    Expanded(
                                      child: WidgetComponent.primaryButton(
                                          onPress: () {
                                            if (nameCtrl.text.isEmpty ||
                                                phoneController.text.isEmpty ||
                                                emailController.text.isEmpty ||
                                                passController.text.isEmpty ||
                                                confirmPassController
                                                    .text.isEmpty) {
                                              FlushBarMessage().error(
                                                  context,
                                                  "Oops",
                                                  getTranslated(
                                                      context, "fill_all_form"),
                                                  2);
                                              return;
                                            }

                                            if (nameCtrl.text.length < 5) {
                                              FlushBarMessage().error(
                                                  context,
                                                  "Oops",
                                                  getTranslated(
                                                      context, "invalid_name"),
                                                  2);
                                              return;
                                            }

                                            if (passController.text !=
                                                confirmPassController.text) {
                                              FlushBarMessage().error(
                                                  context,
                                                  "Oops",
                                                  "${getTranslated(context, "password_not_same")}",
                                                  2);
                                              return;
                                            }

                                            if (!validator(
                                                phoneController.text)) {
                                              FlushBarMessage().error(
                                                  context,
                                                  "Oops",
                                                  getTranslated(
                                                      context, "invalid_phone"),
                                                  2);
                                            }
                                            if (!emailValidator(
                                                emailController.text)) {
                                              FlushBarMessage().error(
                                                  context,
                                                  "Oops",
                                                  getTranslated(
                                                      context, "invalid_email"),
                                                  2);
                                            }
                                            if (!isChecked) {
                                              FlushBarMessage().toast(
                                                  context,
                                                  "Terms and conditions",
                                                  "${getTranslated(context, "disagree_terms")}",
                                                  transparent,
                                                  4);
                                            }

                                            if (isChecked) {
                                              // FlushBarMessage().toast(
                                              //     context,
                                              //     "Terms and conditions",
                                              //     "${getTranslated(context, "agree_terms")}",
                                              //     primaryColor,
                                              //     6);

                                              body = RegisterBody(
                                                  nameCtrl.text,
                                                  phoneController.text,
                                                  emailController.text,
                                                  passController.text,
                                                  passController.text);
                                              _reqRegister();
                                            }
                                          },
                                          title:
                                              getTranslated(context, "sign_up")
                                                  .toUpperCase()),
                                    )
                                  ],
                                ),
                              ],
                            )),
                        SizedBox(
                          height: ScreenUtil().setHeight(25),
                        ),
                        Center(
                            child: InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 35,
                                  child: Center(
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                            text: getTranslated(context,
                                                "already_have_account"),
                                            style: normalTextFont.copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(10))),
                                        TextSpan(
                                            text: getTranslated(
                                                    context, "sign_in")
                                                .toUpperCase(),
                                            style: boldTextFont.copyWith(
                                                color: primaryColor,
                                                fontSize:
                                                    ScreenUtil().setSp(10))),
                                      ]),
                                    ),
                                  ),
                                ))),
                        SizedBox(
                          height: ScreenUtil().setHeight(50),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _reqRegister() {
    _bloc.add(ReqRegister(body));
  }
}
