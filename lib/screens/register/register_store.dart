import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/helper/bottom_sheet.dart';
import 'package:e_commerce_app/helper/dialog_widget.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/body/register_store_body.dart';
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/model/object/shipping_location_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/pick_map/pick_map_page.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'bloc/register_store_bloc.dart';
import 'bloc/register_store_event.dart';
import 'bloc/register_store_state.dart';

class RegisterStorePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterStorePageState();
  }
}

class RegisterStorePageState extends State<RegisterStorePage> {
  TextEditingController nameCtrl = new TextEditingController();
  TextEditingController bidangCtrl = new TextEditingController();
  TextEditingController jenisCtrl = new TextEditingController();
  var addressCtrl = TextEditingController();
  var provinceCtrl = TextEditingController();
  var cityCtrl = TextEditingController();
  var districtCtrl = TextEditingController();
  var searchCtrl = TextEditingController();

  File selectedSiup;
  File selectedTdp;
  File selectedNpwp;
  File selectedKtp;
  File selectedFotoUsaha;

  ProvinceModel selectedProvince;
  CityModel selectedCity;
  DistrictModel selectedDistrict;
  ShippingLocationModel selectedLocation;

  List<ProvinceModel> provinces = List();
  List<CityModel> cities = List();
  List<DistrictModel> districts = List();

  final picker = ImagePicker();

  RegisterStoreBloc _bloc;
  ApiDomain _domain;

  RegisterStoreBody body;

  @override
  void initState() {
    super.initState();
    _domain = ApiDomain(ApiRepository());
    _bloc = RegisterStoreBloc(domain: _domain);

    _reqProvince();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 360, height: 640, allowFontScaling: true)
          ..init(context);

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: bgPage,
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) {
                  return _bloc;
                },
              )
            ],
            child: BlocListener<RegisterStoreBloc, RegisterStoreState>(
              bloc: _bloc,
              listener: (context, state) {
                if (state is ReqStoreLoading) {
                  CustomDialog().loading(context,
                      "${getTranslated(context, "processing_request")}");
                } else if (state is RegisterStoreError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqRegisterStore();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is RegisterStoreSuccess) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      "${getTranslated(context, "message_success_register_seller")}",
                      "Ok",
                      "", () {
                    Get.back(result: true);
                    Get.back(result: true);
                  }, () {
                    Get.back(result: true);
                    Get.back(result: true);
                  });
                } else if (state is ReqProvinceSuccess) {
                  Navigator.pop(context);
                  provinces.clear();
                  provinces.addAll(state.resp.data);
                  setState(() {});
                } else if (state is ReqCitySuccess) {
                  Navigator.pop(context);
                  cities.clear();
                  cities.addAll(state.resp.data);
                  setState(() {});
                } else if (state is ReqDistrictSuccess) {
                  Navigator.pop(context);
                  districts.clear();
                  districts.addAll(state.resp.data);
                  setState(() {});
                } else if (state is ReqProvinceError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqProvince();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqCityError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqCity();
                  }, () {
                    Navigator.pop(context);
                  });
                } else if (state is ReqDistrictError) {
                  Navigator.pop(context);
                  CustomDialog().showDialogConfirms(
                      context,
                      state.error,
                      "${getTranslated(context, "retry")}",
                      "${getTranslated(context, "close")}", () {
                    Navigator.pop(context);
                    _reqDistrict();
                  }, () {
                    Navigator.pop(context);
                  });
                }
              },
              child: BlocBuilder<RegisterStoreBloc, RegisterStoreState>(
                bloc: _bloc,
                builder: (context, state) {
                  return Column(
                    children: [
                      Container(
                        height: ScreenUtil().setHeight(55),
                        padding: EdgeInsets.only(
                            left: ScreenUtil().setSp(20),
                            right: ScreenUtil().setSp(20)),
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(Icons.arrow_back_ios),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(15),
                            ),
                            Text(
                              "${getTranslated(context, "register_store")}",
                              style: boldMuliFont.copyWith(
                                  fontSize: ScreenUtil().setSp(16)),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Container(
                                  margin:
                                      EdgeInsets.all(ScreenUtil().setWidth(15)),
                                  child: Form(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(5)),
                                          child: Text(
                                              "${getTranslated(context, "store_name")}*"
                                                  .toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            border:
                                                Border.all(color: Colors.grey),
                                            color: bgPage,
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: ScreenUtil().setWidth(10),
                                                right:
                                                    ScreenUtil().setWidth(10)),
                                            child: TextFormField(
                                              keyboardType: TextInputType.text,
                                              controller: nameCtrl,
                                              textAlign: TextAlign.left,
                                              style: normalTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(12)),
                                              decoration: new InputDecoration(
                                                  hintStyle: TextStyle(
                                                      color: Color(0xffD1D1D1)),
                                                  border: InputBorder.none,
                                                  hintText:
                                                      "${getTranslated(context, "input_store_name")}",
                                                  fillColor: bgPage),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "field")}*"
                                                  .toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            border:
                                                Border.all(color: Colors.grey),
                                            color: bgPage,
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: ScreenUtil().setWidth(10),
                                                right:
                                                    ScreenUtil().setWidth(10)),
                                            child: TextFormField(
                                              keyboardType: TextInputType.text,
                                              textInputAction:
                                                  TextInputAction.next,
                                              controller: bidangCtrl,
                                              textAlign: TextAlign.left,
                                              style: normalTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(12)),
                                              decoration: new InputDecoration(
                                                  hintStyle: TextStyle(
                                                      color: Color(0xffD1D1D1)),
                                                  border: InputBorder.none,
                                                  hintText:
                                                      "${getTranslated(context, "input_field")}",
                                                  fillColor: bgPage),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "company_type")}*"
                                                  .toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            border:
                                                Border.all(color: Colors.grey),
                                            color: bgPage,
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: ScreenUtil().setWidth(10),
                                                right:
                                                    ScreenUtil().setWidth(10)),
                                            child: TextFormField(
                                              keyboardType: TextInputType.text,
                                              controller: jenisCtrl,
                                              textAlign: TextAlign.left,
                                              style: normalTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(12)),
                                              decoration: new InputDecoration(
                                                  hintStyle: TextStyle(
                                                      color: Color(0xffD1D1D1)),
                                                  border: InputBorder.none,
                                                  hintText:
                                                      "${getTranslated(context, "input_company_type")}",
                                                  fillColor: bgPage),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "province").toUpperCase()}*",
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  .unfocus();
                                              showProvinceBottomSheet(
                                                  getTranslated(context,
                                                      "choose_province"),
                                                  searchCtrl,
                                                  provinces, onClick: (dt) {
                                                selectedProvince = dt;
                                                provinceCtrl.text =
                                                    selectedProvince.province;
                                                _reqCity();
                                                return;
                                              });
                                            },
                                            child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  border: Border.all(
                                                      color: Colors.grey),
                                                  color: bgPage,
                                                ),
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      left: ScreenUtil()
                                                          .setWidth(10),
                                                      right: ScreenUtil()
                                                          .setWidth(10)),
                                                  child: TextFormField(
                                                    keyboardType:
                                                        TextInputType.text,
                                                    textAlign: TextAlign.left,
                                                    controller: provinceCtrl,
                                                    enabled: false,
                                                    style:
                                                        normalTextFont.copyWith(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(12)),
                                                    decoration: new InputDecoration(
                                                        hintStyle: TextStyle(
                                                            color: Color(
                                                                0xffD1D1D1)),
                                                        hintText: getTranslated(
                                                            context,
                                                            "choose_province"),
                                                        border:
                                                            InputBorder.none,
                                                        fillColor: bgPage),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "city").toUpperCase()}*",
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  .unfocus();
                                              if (selectedProvince == null) {
                                                FlushBarMessage().error(
                                                    context,
                                                    "Oops",
                                                    getTranslated(context,
                                                        "choose_province_first"),
                                                    2);
                                                return;
                                              }
                                              showCityBottomSheet(
                                                  "${getTranslated(context, "choose_city")}",
                                                  searchCtrl,
                                                  cities, onClick: (dt) {
                                                selectedCity = dt;
                                                cityCtrl.text =
                                                    selectedCity.city_name;
                                                _reqDistrict();
                                                return;
                                              });
                                            },
                                            child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  border: Border.all(
                                                      color: Colors.grey),
                                                  color: bgPage,
                                                ),
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      left: ScreenUtil()
                                                          .setWidth(10),
                                                      right: ScreenUtil()
                                                          .setWidth(10)),
                                                  child: TextFormField(
                                                    keyboardType:
                                                        TextInputType.text,
                                                    textAlign: TextAlign.left,
                                                    controller: cityCtrl,
                                                    enabled: false,
                                                    style:
                                                        normalTextFont.copyWith(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(12)),
                                                    decoration: new InputDecoration(
                                                        hintStyle: TextStyle(
                                                            color: Color(
                                                                0xffD1D1D1)),
                                                        hintText: getTranslated(
                                                            context,
                                                            "choose_city"),
                                                        border:
                                                            InputBorder.none,
                                                        fillColor: bgPage),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "district").toUpperCase()}*",
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  .unfocus();
                                              if (selectedCity == null) {
                                                FlushBarMessage().error(
                                                    context,
                                                    "Oops",
                                                    getTranslated(context,
                                                        "choose_city_first"),
                                                    2);
                                                return;
                                              }

                                              showDistrictBottomSheet(
                                                  "${getTranslated(context, "choose_district")}",
                                                  searchCtrl,
                                                  districts, onClick: (dt) {
                                                selectedDistrict = dt;
                                                districtCtrl.text =
                                                    selectedDistrict.name;
                                                return;
                                              });
                                            },
                                            child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  border: Border.all(
                                                      color: Colors.grey),
                                                  color: bgPage,
                                                ),
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      left: ScreenUtil()
                                                          .setWidth(10),
                                                      right: ScreenUtil()
                                                          .setWidth(10)),
                                                  child: TextFormField(
                                                    keyboardType:
                                                        TextInputType.text,
                                                    textAlign: TextAlign.left,
                                                    controller: districtCtrl,
                                                    enabled: false,
                                                    style:
                                                        normalTextFont.copyWith(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(12)),
                                                    decoration: new InputDecoration(
                                                        hintStyle: TextStyle(
                                                            color: Color(
                                                                0xffD1D1D1)),
                                                        hintText: getTranslated(
                                                            context,
                                                            "choose_district"),
                                                        border:
                                                            InputBorder.none,
                                                        fillColor: bgPage),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "address").toUpperCase()}*",
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                            onTap: () async {
                                              FocusManager.instance.primaryFocus
                                                  .unfocus();
                                              ShippingLocationModel result =
                                                  await Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PickMapPage()),
                                              );
                                              if (result != null) {
                                                selectedLocation = result;
                                                setState(() {
                                                  addressCtrl.text =
                                                      "${selectedLocation.address}";
                                                });
                                              }
                                            },
                                            child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  border: Border.all(
                                                      color: Colors.grey),
                                                  color: bgPage,
                                                ),
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      left: ScreenUtil()
                                                          .setWidth(10),
                                                      right: ScreenUtil()
                                                          .setWidth(10)),
                                                  child: TextFormField(
                                                    keyboardType:
                                                        TextInputType.text,
                                                    textAlign: TextAlign.left,
                                                    controller: addressCtrl,
                                                    enabled: false,
                                                    style:
                                                        normalTextFont.copyWith(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(12)),
                                                    decoration: new InputDecoration(
                                                        hintStyle: TextStyle(
                                                            color: Color(
                                                                0xffD1D1D1)),
                                                        hintText:
                                                            "${getTranslated(context, "input_address")}",
                                                        border:
                                                            InputBorder.none,
                                                        fillColor: bgPage),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text("SIUP*".toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            FocusManager.instance.primaryFocus
                                                .unfocus();
                                            showChooseImageDialog("siup");
                                          },
                                          child: selectedSiup == null
                                              ? Card(
                                                  child: Container(
                                                    margin: EdgeInsets.all(
                                                        ScreenUtil()
                                                            .setWidth(25)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        SvgPicture.asset(
                                                            "assets/images/ic_add_image.svg"),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(10),
                                                        ),
                                                        Text(
                                                            "${getTranslated(context, "additional_image")}",
                                                            style: boldTextFont.copyWith(
                                                                fontSize:
                                                                    ScreenUtil()
                                                                        .setSp(
                                                                            10))),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(5),
                                                        ),
                                                        Text(
                                                          "${getTranslated(context, "additional_image_desc")}",
                                                          style: normalTextFont
                                                              .copyWith(
                                                                  fontSize:
                                                                      ScreenUtil()
                                                                          .setSp(
                                                                              10)),
                                                          textAlign:
                                                              TextAlign.center,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              : Card(
                                                  child: Container(
                                                    width: Get.width,
                                                    height: ScreenUtil()
                                                        .setHeight(150),
                                                    child: Image.file(
                                                      selectedSiup,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text("TDP*".toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            FocusManager.instance.primaryFocus
                                                .unfocus();
                                            showChooseImageDialog("tdp");
                                          },
                                          child: selectedTdp == null
                                              ? Card(
                                                  child: Container(
                                                    margin: EdgeInsets.all(
                                                        ScreenUtil()
                                                            .setWidth(25)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        SvgPicture.asset(
                                                            "assets/images/ic_add_image.svg"),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(10),
                                                        ),
                                                        Text(
                                                            getTranslated(
                                                                context,
                                                                "additional_image"),
                                                            style: boldTextFont.copyWith(
                                                                fontSize:
                                                                    ScreenUtil()
                                                                        .setSp(
                                                                            10))),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(5),
                                                        ),
                                                        Text(
                                                          getTranslated(context,
                                                              "additional_image_desc"),
                                                          style: normalTextFont
                                                              .copyWith(
                                                                  fontSize:
                                                                      ScreenUtil()
                                                                          .setSp(
                                                                              10)),
                                                          textAlign:
                                                              TextAlign.center,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              : Card(
                                                  child: Container(
                                                    width: Get.width,
                                                    height: ScreenUtil()
                                                        .setHeight(150),
                                                    child: Image.file(
                                                      selectedTdp,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text("NPWP*".toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            FocusManager.instance.primaryFocus
                                                .unfocus();
                                            showChooseImageDialog("npwp");
                                          },
                                          child: selectedNpwp == null
                                              ? Card(
                                                  child: Container(
                                                    margin: EdgeInsets.all(
                                                        ScreenUtil()
                                                            .setWidth(25)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        SvgPicture.asset(
                                                            "assets/images/ic_add_image.svg"),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(10),
                                                        ),
                                                        Text(
                                                            getTranslated(
                                                                context,
                                                                "additional_image"),
                                                            style: boldTextFont.copyWith(
                                                                fontSize:
                                                                    ScreenUtil()
                                                                        .setSp(
                                                                            10))),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(5),
                                                        ),
                                                        Text(
                                                          getTranslated(context,
                                                              "additional_image_desc"),
                                                          style: normalTextFont
                                                              .copyWith(
                                                                  fontSize:
                                                                      ScreenUtil()
                                                                          .setSp(
                                                                              10)),
                                                          textAlign:
                                                              TextAlign.center,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              : Card(
                                                  child: Container(
                                                    width: Get.width,
                                                    height: ScreenUtil()
                                                        .setHeight(150),
                                                    child: Image.file(
                                                      selectedNpwp,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text("KTP*".toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            FocusManager.instance.primaryFocus
                                                .unfocus();
                                            showChooseImageDialog("ktp");
                                          },
                                          child: selectedKtp == null
                                              ? Card(
                                                  child: Container(
                                                    margin: EdgeInsets.all(
                                                        ScreenUtil()
                                                            .setWidth(25)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        SvgPicture.asset(
                                                            "assets/images/ic_add_image.svg"),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(10),
                                                        ),
                                                        Text(
                                                            getTranslated(
                                                                context,
                                                                "additional_image"),
                                                            style: boldTextFont.copyWith(
                                                                fontSize:
                                                                    ScreenUtil()
                                                                        .setSp(
                                                                            10))),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(5),
                                                        ),
                                                        Text(
                                                          getTranslated(context,
                                                              "additional_image_desc"),
                                                          style: normalTextFont
                                                              .copyWith(
                                                                  fontSize:
                                                                      ScreenUtil()
                                                                          .setSp(
                                                                              10)),
                                                          textAlign:
                                                              TextAlign.center,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              : Card(
                                                  child: Container(
                                                    width: Get.width,
                                                    height: ScreenUtil()
                                                        .setHeight(150),
                                                    child: Image.file(
                                                      selectedKtp,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(15),
                                              bottom: ScreenUtil().setHeight(8),
                                              top: ScreenUtil().setHeight(16)),
                                          child: Text(
                                              "${getTranslated(context, "business_photo")}*"
                                                  .toUpperCase(),
                                              style: boldTextFont.copyWith(
                                                  fontSize:
                                                      ScreenUtil().setSp(10))),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            FocusManager.instance.primaryFocus
                                                .unfocus();
                                            showChooseImageDialog("foto usaha");
                                          },
                                          child: selectedFotoUsaha == null
                                              ? Card(
                                                  child: Container(
                                                    margin: EdgeInsets.all(
                                                        ScreenUtil()
                                                            .setWidth(25)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        SvgPicture.asset(
                                                            "assets/images/ic_add_image.svg"),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(10),
                                                        ),
                                                        Text(
                                                            getTranslated(
                                                                context,
                                                                "additional_image"),
                                                            style: boldTextFont.copyWith(
                                                                fontSize:
                                                                    ScreenUtil()
                                                                        .setSp(
                                                                            10))),
                                                        SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(5),
                                                        ),
                                                        Text(
                                                          getTranslated(context,
                                                              "additional_image_desc"),
                                                          style: normalTextFont
                                                              .copyWith(
                                                                  fontSize:
                                                                      ScreenUtil()
                                                                          .setSp(
                                                                              10)),
                                                          textAlign:
                                                              TextAlign.center,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              : Card(
                                                  child: Container(
                                                    width: Get.width,
                                                    height: ScreenUtil()
                                                        .setHeight(150),
                                                    child: Image.file(
                                                      selectedFotoUsaha,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                        SizedBox(
                                          height: ScreenUtil().setHeight(20),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child:
                                                  WidgetComponent.primaryButton(
                                                      onPress: () {
                                                        if (nameCtrl
                                                                .text.isEmpty ||
                                                            bidangCtrl
                                                                .text.isEmpty ||
                                                            jenisCtrl
                                                                .text.isEmpty ||
                                                            selectedProvince ==
                                                                null ||
                                                            selectedCity ==
                                                                null ||
                                                            selectedDistrict ==
                                                                null ||
                                                            selectedLocation ==
                                                                null ||
                                                            selectedSiup ==
                                                                null ||
                                                            selectedTdp ==
                                                                null ||
                                                            selectedNpwp ==
                                                                null ||
                                                            selectedKtp ==
                                                                null ||
                                                            selectedFotoUsaha ==
                                                                null) {
                                                          FlushBarMessage().error(
                                                              context,
                                                              "Oops",
                                                              getTranslated(
                                                                  context,
                                                                  "fill_all_form"),
                                                              2);
                                                          return;
                                                        }

                                                        body = RegisterStoreBody(
                                                            nameCtrl.text,
                                                            bidangCtrl.text,
                                                            jenisCtrl.text,
                                                            GlobalHelper()
                                                                .fileToBase64(
                                                                    selectedSiup),
                                                            GlobalHelper()
                                                                .fileToBase64(
                                                                    selectedTdp),
                                                            GlobalHelper()
                                                                .fileToBase64(
                                                                    selectedNpwp),
                                                            GlobalHelper()
                                                                .fileToBase64(
                                                                    selectedKtp),
                                                            GlobalHelper()
                                                                .fileToBase64(
                                                                    selectedFotoUsaha),
                                                            addressCtrl.text,
                                                            selectedDistrict
                                                                .id_district,
                                                            selectedCity
                                                                .id_cities,
                                                            selectedProvince
                                                                .id_province,
                                                            "${selectedLocation.lat}",
                                                            "${selectedLocation.lng}");
                                                        _reqRegisterStore();
                                                      },
                                                      title: getTranslated(
                                                              context,
                                                              "register")
                                                          .toUpperCase()),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  )),
                              SizedBox(
                                height: ScreenUtil().setHeight(50),
                              )
                            ],
                          ),
                        ),
                        flex: 1,
                      )
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  showChooseImageDialog(source) {
    Get.bottomSheet(Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(ScreenUtil().setWidth(15)),
              topRight: Radius.circular(ScreenUtil().setWidth(15)))),
      child: Container(
        margin: EdgeInsets.all(ScreenUtil().setWidth(25)),
        child: Wrap(
          children: [
            Column(
              children: [
                InkWell(
                  onTap: () {
                    Get.back();
                    getImage(1, source);
                  },
                  child: Row(
                    children: [
                      SizedBox(
                        width: ScreenUtil().setWidth(15),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${getTranslated(context, "camera")}',
                              style: boldTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(12),
                                  color: Colors.black)),
                          Text('${getTranslated(context, "camera_desc")}',
                              style: normalTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(12),
                                  color: Colors.black)),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(15),
                ),
                InkWell(
                  onTap: () {
                    Get.back();
                    getImage(2, source);
                  },
                  child: Row(
                    children: [
                      SizedBox(
                        width: ScreenUtil().setWidth(15),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${getTranslated(context, "gallery")}',
                              style: boldTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(12),
                                  color: Colors.black)),
                          Text('${getTranslated(context, "gallery_desc")}',
                              style: normalTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(12),
                                  color: Colors.black)),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }

  Future getImage(type, source) async {
    final pickedFile = await picker.getImage(
        source: type == 1 ? ImageSource.camera : ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        if (source == 'siup') {
          selectedSiup = File(pickedFile.path);
        } else if (source == "tdp") {
          selectedTdp = File(pickedFile.path);
        } else if (source == "npwp") {
          selectedNpwp = File(pickedFile.path);
        } else if (source == "ktp") {
          selectedKtp = File(pickedFile.path);
        } else if (source == "foto usaha") {
          selectedFotoUsaha = File(pickedFile.path);
        }
      } else {
        print('No image selected.');
      }
    });
  }

  _reqRegisterStore() {
    _bloc.add(ReqStoreRegister(body));
  }

  _reqProvince() {
    _bloc.add(ReqProvince());
  }

  _reqCity() {
    _bloc.add(ReqCity(selectedProvince.id_province));
  }

  _reqDistrict() {
    _bloc.add(ReqDistrict(selectedCity.id_cities));
  }
}
