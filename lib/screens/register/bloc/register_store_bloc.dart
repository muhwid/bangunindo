import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/city_response.dart';
import 'package:e_commerce_app/model/response/district_response.dart';
import 'package:e_commerce_app/model/response/province_response.dart';
import 'package:flutter/material.dart';

import 'register_store_event.dart';
import 'register_store_state.dart';

class RegisterStoreBloc extends Bloc<RegisterStoreEvent, RegisterStoreState> {
  final ApiDomain domain;

  RegisterStoreBloc({@required this.domain}) : assert(domain != null);

  @override
  RegisterStoreState get initialState => InitPage();

  @override
  Stream<RegisterStoreState> mapEventToState(RegisterStoreEvent event) async* {
    if (event is ReqStoreRegister) {
      yield ReqStoreLoading();
      try {
        BaseResponse resp = await domain.reqStoreRegister(event.body);
        if(resp.status != null){
          if (resp.status.startsWith("20"))
            yield RegisterStoreSuccess(resp: resp);
          else {
            yield RegisterStoreError(error: resp.message.toString());
          }
        }
        else {
          if(resp.invalid != null){
            yield RegisterStoreError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield RegisterStoreError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield RegisterStoreError(error: e.toString());
      }
    }

    if (event is ReqProvince) {
      yield ReqStoreLoading();
      try {
        ProvinceResponse resp = await domain.reqProvince();
        if (resp.status.startsWith("20"))
          yield ReqProvinceSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqProvinceError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqProvinceError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqProvinceError(error: e.toString());
      }
    }

    if (event is ReqDistrict) {
      yield ReqStoreLoading();
      try {
        DistrictResponse resp = await domain.reqDistrict(event.idCity);
        if (resp.status.startsWith("20"))
          yield ReqDistrictSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqDistrictError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqDistrictError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqDistrictError(error: e.toString());
      }
    }

    if (event is ReqCity) {
      yield ReqStoreLoading();
      try {
        CityResponse resp = await domain.reqCity(event.idProvince);
        if (resp.status.startsWith("20"))
          yield ReqCitySuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqCityError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqCityError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqCityError(error: e.toString());
      }
    }

    if (event is ReqAddStore) {
      yield ReqStoreLoading();
      try {
        BaseResponse resp = await domain.reqStoreRegister(event.body);
        if (resp.status.startsWith("20"))
          yield ReqAddStoreSuccess(resp: resp);
        else {
          if(resp.invalid != null){
            yield ReqAddStoreError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield ReqAddStoreError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield ReqAddStoreError(error: e.toString());
      }
    }
  }
}
