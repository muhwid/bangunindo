import 'package:e_commerce_app/model/body/register_body.dart';
import 'package:e_commerce_app/model/body/register_store_body.dart';
import 'package:e_commerce_app/screens/address/add/bloc/add_address_event.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterStoreEvent extends Equatable {
  const RegisterStoreEvent();

  @override
  List<Object> get props => [];
}

class ReqStoreRegister extends RegisterStoreEvent {
  final RegisterStoreBody body;

  const ReqStoreRegister(this.body);

  @override
  List<Object> get props => [body];
}

class ReqProvince extends RegisterStoreEvent {
  const ReqProvince();

  @override
  List<Object> get props => [];
}

class ReqCity extends RegisterStoreEvent {
  final String idProvince;
  const ReqCity(this.idProvince);

  @override
  List<Object> get props => [idProvince];
}

class ReqDistrict extends RegisterStoreEvent {
  final String idCity;
  const ReqDistrict(this.idCity);

  @override
  List<Object> get props => [idCity];
}

class ReqAddStore extends RegisterStoreEvent {
  final RegisterStoreBody body;

  const ReqAddStore(this.body);

  @override
  List<Object> get props => [body];
}


