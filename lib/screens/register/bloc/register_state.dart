
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class InitPage extends RegisterState{
  @override
  List<Object> get props => [];

}

class ReqLoading extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterSuccess extends RegisterState {
  final BaseResponse resp;

  const RegisterSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class RegisterError extends RegisterState {
  final String error;

  const RegisterError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}