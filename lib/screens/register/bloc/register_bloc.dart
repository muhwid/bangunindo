import 'package:bloc/bloc.dart';
import 'package:e_commerce_app/data/domain/api_domain.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:flutter/material.dart';

import 'register_event.dart';
import 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final ApiDomain domain;

  RegisterBloc({@required this.domain}) : assert(domain != null);

  @override
  RegisterState get initialState => InitPage();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is ReqRegister) {
      yield ReqLoading();
      try {
        BaseResponse resp = await domain.reqRegister(event.body);
        if(resp.status != null){
          if (resp.status.startsWith("20"))
            yield RegisterSuccess(resp: resp);
          else {
            yield RegisterError(error: resp.message.toString());
          }
        }
        else {
          if(resp.invalid != null){
            yield RegisterError(error: resp.invalid.toString());
          }else{
            print("error success 0 ${resp.message.toString()}");
            yield RegisterError(error: resp.message.toString());
          }
        }
      } catch (e) {
        print("error error ${e.toString()}");
        yield RegisterError(error: e.toString());
      }
    }
  }
}
