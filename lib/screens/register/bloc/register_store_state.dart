
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/city_response.dart';
import 'package:e_commerce_app/model/response/district_response.dart';
import 'package:e_commerce_app/model/response/login_response.dart';
import 'package:e_commerce_app/model/response/province_response.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterStoreState extends Equatable {
  const RegisterStoreState();
}

class InitPage extends RegisterStoreState{
  @override
  List<Object> get props => [];

}

class ReqStoreLoading extends RegisterStoreState {
  @override
  List<Object> get props => [];
}

class RegisterStoreSuccess extends RegisterStoreState {
  final BaseResponse resp;

  const RegisterStoreSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class RegisterStoreError extends RegisterStoreState {
  final String error;

  const RegisterStoreError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqProvinceLoading extends RegisterStoreState {
  @override
  List<Object> get props => [];
}

class ReqProvinceSuccess extends RegisterStoreState {
  final ProvinceResponse resp;

  const ReqProvinceSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqProvinceError extends RegisterStoreState {
  final String error;

  const ReqProvinceError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqCitySuccess extends RegisterStoreState {
  final CityResponse resp;

  const ReqCitySuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqCityError extends RegisterStoreState {
  final String error;

  const ReqCityError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqDistrictSuccess extends RegisterStoreState {
  final DistrictResponse resp;

  const ReqDistrictSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqDistrictError extends RegisterStoreState {
  final String error;

  const ReqDistrictError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ReqAddStoreSuccess extends RegisterStoreState {
  final BaseResponse resp;

  const ReqAddStoreSuccess({this.resp});

  @override
  List<Object> get props => [resp];
}

class ReqAddStoreError extends RegisterStoreState {
  final String error;

  const ReqAddStoreError({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}