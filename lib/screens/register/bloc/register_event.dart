import 'package:e_commerce_app/model/body/register_body.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];
}

class ReqRegister extends RegisterEvent {
  final RegisterBody body;

  const ReqRegister(this.body);

  @override
  List<Object> get props => [body];
}
