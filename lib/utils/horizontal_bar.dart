import 'package:e_commerce_app/res/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_animations/simple_animations.dart';

class HorizontalBar extends StatelessWidget {
  final double width;
  final String label;
  final Color color;

  final int _baseDurationMs = 1000;
  final double _maxElementHeight = 100;

  HorizontalBar(this.width, this.label, this.color);

  @override
  Widget build(BuildContext context) {
    return ControlledAnimation(
      duration: Duration(milliseconds: (width * _baseDurationMs).round()),
      tween: Tween(begin: 0.0, end: width),
      builder: (context, animatedHeight) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Text(label, style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),),
            ),
            Expanded(
              flex: 8,
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(ScreenUtil().setWidth(20))
                      ),
                      color: line,
                    ),
                    width: MediaQuery.of(context).size.width,
                    height: ScreenUtil().setWidth(10),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(ScreenUtil().setWidth(20))
                      ),
                      color: color,
                    ),
                    width: animatedHeight * _maxElementHeight,
                    height: ScreenUtil().setWidth(10),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
  }
}