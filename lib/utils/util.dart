class URL {
  static const String PRODUCT_NOT_FOUND = "assets/images/image_not_found.jpg";
  static const String COMPANY_LOGO = "assets/images/logo.png";
  static const String PROFILE_PHOTO =
      "assets/images/img_placeholder_profile.png";
  static const String SPLASHS = "assets/images/splashscreen.jpg";
  static const String EMPTY_CART = "assets/images/empty_cart.jpg";
  static const String IMG_NOT_FOUND = "assets/images/image_not_found.png";
}

class StoreKeyConstants {
  static const String FCM_TOKEN = ".fcmToken";
}

enum MediaSource { image, video }
