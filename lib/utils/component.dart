import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

import 'language/localization/localization.dart';

class WidgetComponent {
  static MaterialButton primaryButton({
    @required VoidCallback onPress,
    @required String title,
  }) =>
      MaterialButton(
        onPressed: onPress,
        color: primaryColor,
        elevation: 0,
        child: Text(title, style: boldTextFont.copyWith(color: Colors.white)),
      );

  static AppBar appBar({
    @required String text,
  }) =>
      AppBar(
        centerTitle: true,
        backgroundColor: primaryColor,
        elevation: 0,
        title: Text(text),
      );

  static ClipPath clipPath({
    @required BuildContext context,
    double height = 200,
  }) =>
      ClipPath(
        clipper: OvalBottomBorderClipper(),
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: primaryColor,
          height: height,
        ),
      );

  static ListTile listUser({
    @required VoidCallback onPressed,
    @required String title,
  }) =>
      ListTile(
        onTap: onPressed,
        title: Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
        trailing: Icon(Icons.arrow_forward_ios),
      );

  static Container shopButton(
          {@required VoidCallback onPressed, @required BuildContext context}) =>
      Container(
          width: MediaQuery.of(context).size.width / 6,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0), color: Colors.black),
          child: InkWell(
            child: Center(
              child: Container(
                padding: EdgeInsets.only(
                    left: ScreenUtil().setHeight(4),
                    top: ScreenUtil().setHeight(2),
                    bottom: ScreenUtil().setHeight(2),
                    right: ScreenUtil().setHeight(2)),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    getTranslated(context, 'shop'),
                    textAlign: TextAlign.center,
                    style: boldTextFont.copyWith(
                        fontSize: ScreenUtil().setSp(10), color: Colors.white),
                  ),
                ),
              ),
            ),
            onTap: onPressed,
          ));

  static ListTile listItems({
    @required VoidCallback onPressed,
    @required String judul,
    @required String linkImage,
    @required String harga,
  }) =>
      ListTile(
        onTap: onPressed,
        leading: Image.asset(linkImage, width: 60, height: 60),
        title: Text(judul, style: TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Text(harga, style: TextStyle(color: primaryColor)),
        trailing: Icon(Icons.arrow_forward_ios),
      );

  static ListTile productSeller({
    @required String linkImage,
    @required VoidCallback onPressed,
    @required String judul,
    @required String tanggal,
    @required String harga,
  }) =>
      ListTile(
        onTap: onPressed,
        leading: Image.asset(linkImage, fit: BoxFit.cover),
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.grey,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Selesai", style: TextStyle(color: Colors.green)),
                )),
            Text(judul,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0)),
            Text(tanggal, style: TextStyle(color: Colors.grey, fontSize: 12.0)),
          ],
        ),
        subtitle: Text("Rp. $harga", style: TextStyle(color: Colors.red)),
        trailing: Icon(Icons.arrow_forward_ios),
      );
  static ListTile products({
    @required String linkImage,
    @required String title,
  }) =>
      ListTile(
        leading: Image.asset(linkImage, height: 50, width: 50),
        title: Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
        trailing: Icon(Icons.arrow_forward_ios),
      );

  static ListTile productTile({
    @required String linkImage,
    @required String title,
    @required String tag,
    @required String price,
    @required String prices,
    @required double rating,
    @required BuildContext context,
    @required VoidCallback onPressed,
    @required VoidCallback onTap,
  }) =>
      ListTile(
        onTap: onTap,
        leading: Container(
            width: 50.0,
            height: 50.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.asset(
                linkImage,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.fill,
              ),
            )),
        title: Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                Text("\$$price",
                    style: TextStyle(color: Colors.red, fontSize: 12.0)),
                SizedBox(width: 6.0),
                Text("\$$prices",
                    style: TextStyle(
                        color: Colors.red.withOpacity(0.6), fontSize: 12.0))
              ],
            ),
            Row(
              children: [
                RatingBar.builder(
                  itemSize: 12.0,
                  initialRating: rating,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemBuilder: (context, _) {
                    return Icon(Icons.star, color: Colors.amber);
                  },
                  onRatingUpdate: (double value) {
                    print(value);
                  },
                ),
                SizedBox(width: 8),
                Text(rating.toString(),
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ],
            )
          ],
        ),
        trailing: WidgetComponent.shopButton(onPressed: onPressed),
      );

  static InkWell btnCategory({
    @required String images,
    @required String text,
    @required VoidCallback onPressed,
  }) =>
      InkWell(
          onTap: onPressed,
          child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Container(
                  width: 55,
                  height: 55,
                  decoration: BoxDecoration(
                      color: Colors.blueGrey[100],
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Image.asset(images, width: 30, height: 30),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                        child: Text(text,
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.bold)),
                      )
                    ],
                  ))));
}
