import 'package:e_commerce_app/utils/language/localization/localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String LAGUAGE_CODE = 'languageCode';

//languages code
const String ENGLISH = 'en';
const String INDONESIA = 'id';

Future<Locale> setLocale(String languageCode) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setString(LAGUAGE_CODE, languageCode);
  return _locale(languageCode);
}

Future<Locale> getLocale() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String languageCode = _prefs.getString(LAGUAGE_CODE) ?? 'id';
  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  switch (languageCode) {
    case INDONESIA:
      return Locale(INDONESIA, 'ID');
    case ENGLISH:
      return Locale(ENGLISH, 'US');
    default:
      return Locale(INDONESIA, 'ID');
  }
}

String getTranslated(BuildContext context, String key) {
  return NewLocalizations.of(context).getTranslatedValue(key);
}
