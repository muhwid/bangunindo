import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NewLocalizations {
  final Locale locale;

  NewLocalizations(this.locale);

  static NewLocalizations of(BuildContext context) {
    return Localizations.of<NewLocalizations>(context, NewLocalizations);
  }

  Map<String, String> _localizedValues;

  Future load() async {
    String jsonStringValues = await rootBundle
        .loadString('lib/utils/language/lang/${locale.languageCode}.json');

    Map<String, dynamic> mappedJson = json.decode(jsonStringValues);

    _localizedValues =
        mappedJson.map((key, value) => MapEntry(key, value.toString()));
  }

  String getTranslatedValue(String key) {
    return _localizedValues[key];
  }

  static const LocalizationsDelegate<NewLocalizations> delegate =
      _NewLocalizationsDelegate();
}

class _NewLocalizationsDelegate
    extends LocalizationsDelegate<NewLocalizations> {
  const _NewLocalizationsDelegate();
  @override
  bool isSupported(Locale locale) {
    return ['id', 'en'].contains(locale.languageCode);
  }

  @override
  Future<NewLocalizations> load(Locale locale) async {
    NewLocalizations localization = new NewLocalizations(locale);
    await localization.load();
    return localization;
  }

  @override
  bool shouldReload(LocalizationsDelegate<NewLocalizations> old) => false;
}
