import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/item/item_region.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

void showProvinceBottomSheet(String title, TextEditingController searchCtrl, List<ProvinceModel> data, {Function onClick(ProvinceModel dt)}) {
  searchCtrl.text = "";
  List<ProvinceModel> filterData = List();
  filterData.addAll(data);

  showModalBottomSheet(
      context: Get.context,
      isScrollControlled: true,
      builder: (builder) {
        return new StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: transparent,
                child: new Container(
                    decoration: new BoxDecoration(
                        color: Color(0xFFF7F9FD),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15))),
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                          Text(
                            "$title",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(16),
                                color: primaryColor),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: ScreenUtil().setSp(15),
                                right: ScreenUtil().setSp(15)),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Container(
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                controller: searchCtrl,
                                textAlign: TextAlign.left,
                                onChanged: (text) {
                                  filterData.clear();
                                  if(text.trim() == ""){
                                    filterData.addAll(data);
                                  }else{
                                    for(ProvinceModel item in data){
                                      if(item.province.toLowerCase().contains(text.toLowerCase())){
                                        filterData.add(item);
                                      }
                                    }
                                  }
                                  setState((){});
                                },
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(14)),
                                decoration: new InputDecoration(
                                    hintStyle: normalTextFont.copyWith(
                                        fontSize: ScreenUtil().setSp(12),
                                        color: Color(0xffD1D1D1)),
                                    hintText: getTranslated(context, 'input_keyword'),
                                    border: InputBorder.none,
                                    suffixIcon: Icon(Icons.search),
                                    fillColor: Colors.grey),
                              ),
                            ),
                          ),
                          Expanded(child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                for(ProvinceModel dt in filterData) itemRegion(dt, (){
                                  onClick(dt);
                                  Navigator.pop(context);
                                })
                              ],
                            ),
                          ), flex: 1,),
                        ],
                      ),
                    )),
              );
            });
      });
}

void showCityBottomSheet(String title, TextEditingController searchCtrl, List<CityModel> data, {Function onClick(CityModel dt)}) {
  searchCtrl.text = "";
  List<CityModel> filterData = List();
  filterData.addAll(data);

  showModalBottomSheet(
      context: Get.context,
      isScrollControlled: true,
      builder: (builder) {
        return new StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: transparent,
                child: new Container(
                    decoration: new BoxDecoration(
                        color: Color(0xFFF7F9FD),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15))),
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                          Text(
                            "$title",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(16),
                                color: primaryColor),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: ScreenUtil().setSp(15),
                                right: ScreenUtil().setSp(15)),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Container(
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                controller: searchCtrl,
                                textAlign: TextAlign.left,
                                onChanged: (text) {
                                  filterData.clear();
                                  if(text.trim() == ""){
                                    filterData.addAll(data);
                                  }else{
                                    for(CityModel item in data){
                                      if(item.city_name.toLowerCase().contains(text.toLowerCase())){
                                        filterData.add(item);
                                      }
                                    }
                                  }
                                  setState((){});
                                },
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(14)),
                                decoration: new InputDecoration(
                                    hintStyle: normalTextFont.copyWith(
                                        fontSize: ScreenUtil().setSp(12),
                                        color: Color(0xffD1D1D1)),
                                    hintText: getTranslated(context, 'input_keyword'),
                                    border: InputBorder.none,
                                    suffixIcon: Icon(Icons.search),
                                    fillColor: Colors.grey),
                              ),
                            ),
                          ),
                          Expanded(child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                for(CityModel dt in filterData) itemRegion(dt, (){
                                  onClick(dt);
                                  Navigator.pop(context);
                                })
                              ],
                            ),
                          ), flex: 1,),
                        ],
                      ),
                    )),
              );
            });
      });
}

void showDistrictBottomSheet(String title, TextEditingController searchCtrl, List<DistrictModel> data, {Function onClick(DistrictModel dt)}) {
  searchCtrl.text = "";
  List<DistrictModel> filterData = List();
  filterData.addAll(data);

  showModalBottomSheet(
      context: Get.context,
      isScrollControlled: true,
      builder: (builder) {
        return new StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: transparent,
                child: new Container(
                    decoration: new BoxDecoration(
                        color: Color(0xFFF7F9FD),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15))),
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: ScreenUtil().setHeight(25),
                          ),
                          Text(
                            "$title",
                            style: boldTextFont.copyWith(
                                fontSize: ScreenUtil().setSp(16),
                                color: primaryColor),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(15),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: ScreenUtil().setSp(15),
                                right: ScreenUtil().setSp(15)),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Container(
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                controller: searchCtrl,
                                textAlign: TextAlign.left,
                                onChanged: (text) {
                                  filterData.clear();
                                  if(text.trim() == ""){
                                    filterData.addAll(data);
                                  }else{
                                    for(DistrictModel item in data){
                                      if(item.name.toLowerCase().contains(text.toLowerCase())){
                                        filterData.add(item);
                                      }
                                    }
                                  }
                                  setState((){});
                                },
                                style: normalTextFont.copyWith(
                                    fontSize: ScreenUtil().setSp(14)),
                                decoration: new InputDecoration(
                                    hintStyle: normalTextFont.copyWith(
                                        fontSize: ScreenUtil().setSp(12),
                                        color: Color(0xffD1D1D1)),
                                    hintText: getTranslated(context, 'input_keyword'),
                                    border: InputBorder.none,
                                    suffixIcon: Icon(Icons.search),
                                    fillColor: Colors.grey),
                              ),
                            ),
                          ),
                          Expanded(child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                for(DistrictModel dt in filterData) itemRegion(dt, (){
                                  onClick(dt);
                                  Navigator.pop(context);
                                })
                              ],
                            ),
                          ), flex: 1,),
                        ],
                      ),
                    )),
              );
            });
      });
}