import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class FlushBarMessage {
  Widget error(
      BuildContext context, String title, String message, int duration) {
    return Flushbar(
      title: title,
      message: message,
      backgroundColor: Colors.red,
      duration: Duration(seconds: duration),
    )..show(context);
  }

  Widget toast(
      BuildContext context, String title, String message, Color bgColor, int duration) {
    return Flushbar(
      title: title,
      message: message,
      backgroundColor: bgColor,
      duration: Duration(seconds: duration),
    )..show(context);
  }
}
