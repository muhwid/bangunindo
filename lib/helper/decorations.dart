import 'package:flutter/material.dart';

class CustomDecoration {
  static BoxDecoration createBox() {
    return BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10)));
  }

  static BoxDecoration createCardView() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.black12,
          blurRadius: 20.0, // has the effect of softening the shadow
          spreadRadius: 2.0, // has the effect of extending the shadow
          offset: Offset(
            10.0, // horizontal, move right 10
            10.0, // vertical, move down 10
          ),
        )
      ],
    );
  }
}
