import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesProvider {
  static final SharedPreferencesProvider _singleton =
      SharedPreferencesProvider._internal();

  factory SharedPreferencesProvider() {
    return _singleton;
  }

  SharedPreferencesProvider._internal();

  saveUserId(String userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user_id', userId);
  }

  clearUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user_id', "");
  }

  Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_id");
  }

  setFirst(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('is_first', value);
  }

  Future<String> isFirst() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("is_first");
  }

  setLogin(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('is_login', value);
  }

  Future<String> isLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("is_login");
  }

  setToken(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', value);
  }

  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("token");
  }

  setName(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('name', value);
  }

  Future<String> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("name");
  }

  setUser(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user', value);
  }

  Future<String> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user");
  }

  setFcmToken(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('fcm_token', value);
  }

  Future<String> getFcmToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("fcm_token");
  }
}
