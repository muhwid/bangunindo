import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomDialog {
  showDialogConfirms(BuildContext context, String title, String btnYes,
      String btnNo, Function onYesClicked, Function onNoClicked,
      {bool isCancelable}) {
    return showDialog<void>(
      barrierDismissible: (isCancelable == null) ? true : isCancelable,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 0.0,
          backgroundColor: Colors.white,
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          title,
                          textAlign: TextAlign.center,
                          style: normalTextFont.copyWith(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(12)),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: WidgetComponent.primaryButton(
                                onPress: () {
                                  onYesClicked();
                                },
                                title: btnYes),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        btnNo != null
                            ? InkWell(
                                onTap: () {
                                  onNoClicked();
                                },
                                child: Text(
                                  btnNo,
                                  style: normalTextFont.copyWith(
                                      color: Colors.black26,
                                      fontSize: ScreenUtil().setSp(12)),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  showDialogAlert(BuildContext context, String title, String btnYes,
      Function onYesClicked) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 0.0,
          backgroundColor: Colors.white,
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          title,
                          textAlign: TextAlign.center,
                          style: normalTextFont.copyWith(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(12)),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        btnYes == null
                            ? Container()
                            : Container(
                                width: MediaQuery.of(context).size.width,
                                child: Center(
                                  child: WidgetComponent.primaryButton(
                                      onPress: () {
                                        onYesClicked();
                                      },
                                      title: btnYes),
                                ),
                              ),
                        SizedBox(
                          height: 15,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  loading(BuildContext context, String text) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 0.0,
          backgroundColor: Colors.white,
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Center(
                          child: CircularProgressIndicator(
                            backgroundColor: primaryColor,
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(15),
                        ),
                        Text(
                          text,
                          style: boldTextFont.copyWith(
                              fontSize: ScreenUtil().setSp(12)),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
