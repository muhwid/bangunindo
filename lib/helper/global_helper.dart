import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';

class GlobalHelper {
  static final GlobalHelper _singleton = GlobalHelper._internal();

  factory GlobalHelper() {
    return _singleton;
  }

  GlobalHelper._internal();

  int getCurrentTimestamp() {
    return new DateTime.now().millisecondsSinceEpoch;
  }

  String fileToBase64(File finalFile) {
    final bytes = finalFile.readAsBytesSync();
    return base64Encode(bytes);
  }

  Future<File> compressAndGetFile(File file, String targetPath) async {
    print("file path ${file.absolute.path} $targetPath");
    if (Platform.isIOS) {
      if (!basename(targetPath).contains(".png")) {
        print("target path 1 $targetPath");
        var result = await FlutterImageCompress.compressAndGetFile(
          file.absolute.path,
          targetPath,
          quality: 88,
        );

        // print(file.lengthSync());
        return result;
      } else {
        return file;
      }
    } else {
      print("target path 2 $targetPath");
      var result = await FlutterImageCompress.compressAndGetFile(
        file.absolute.path,
        targetPath,
        quality: 88,
      );

      // print(file.lengthSync());
      return result;
    }
  }

  static Future<bool> saveFcmToken(String fcmToken) async {
    GetStorage box = GetStorage();
    await box.write(StoreKeyConstants.FCM_TOKEN, fcmToken);
    return true;
  }

  String formattingDate(String oriDate, String output) {
    var dateFormat;
    if (Platform.localeName.contains("id"))
      dateFormat = new DateFormat('yyyy-MM-ddTHH:mm:ssZ', 'id');
    else
      dateFormat = new DateFormat('yyyy-MM-ddTHH:mm:ssZ', 'en');

    DateTime dateTime = dateFormat.parse(oriDate);
    String formattedDate = DateFormat(output).format(dateTime);
    return formattedDate;
  }

  String convertDateFromString(String strDate) {
    DateTime todayDate = DateTime.parse(strDate);
    return formatDate(todayDate, [dd, ' ', MM, ' ', yyyy]);
  }

  String formattingDateTime(DateTime oriDate, String output) {
    var dateFormat;
    if (Platform.localeName.contains("id"))
      dateFormat = new DateFormat(output, 'id');
    else
      dateFormat = new DateFormat(output, 'en');
    return dateFormat.format(oriDate);
  }

  String formattingDateCustom(String oriDate, String input, String output) {
    var dateFormat;
    if (Platform.localeName.contains("id"))
      dateFormat = new DateFormat(input, 'id');
    else
      dateFormat = new DateFormat(input, 'en');

    DateTime dateTime = dateFormat.parse(oriDate);
    String formattedDate = DateFormat(output).format(dateTime);
    return formattedDate;
  }

  showSnackBar(String content, BuildContext context) {
    final snackBar = SnackBar(content: Text(content));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  String formattingNumber(int number) {
    var formatter;
    if (Platform.localeName.contains("id"))
      formatter = new NumberFormat("#.###", "id_ID");
    else
      formatter = new NumberFormat("#,###", "en_US");

    return formatter.format(number);
  }

  String formatTimestamp(int timestamp, String outpuFormat) {
    var format = new DateFormat(outpuFormat);
    var date = new DateTime.fromMillisecondsSinceEpoch(timestamp);
    return format.format(date);
  }

  DateTime formatTimestampToDate(int timestamp) {
//    var format = new DateFormat(outpuFormat);
    var date = new DateTime.fromMillisecondsSinceEpoch(timestamp);
    return date;
  }

  DateTime convertDateFormat(
      String inFormat, String outFormat, String inputDate) {
    DateFormat inputFormat = DateFormat(inFormat);
    DateTime dateTime = inputFormat.parse(inputDate);
    DateFormat outputFormat = DateFormat(outFormat);
    return outputFormat.parse(dateTime.toString());
  }
}

showSnackError(String title, String content) {
  Get.snackbar(title, content,
      snackPosition: SnackPosition.BOTTOM,
      margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
      colorText: Colors.white,
      backgroundColor: Colors.red);
}

showSnackSuccess(String title, String content) {
  Get.snackbar(title, content,
      snackPosition: SnackPosition.BOTTOM,
      margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
      colorText: Colors.white,
      backgroundColor: Colors.green);
}
