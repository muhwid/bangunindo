// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginBody _$LoginBodyFromJson(Map<String, dynamic> json) {
  return LoginBody(
    json['phone'] as String,
    json['password'] as String,
    json['fcm'] as String,
    json['device'] as String,
  );
}

Map<String, dynamic> _$LoginBodyToJson(LoginBody instance) => <String, dynamic>{
      'phone': instance.phone,
      'password': instance.password,
      'fcm': instance.fcm,
      'device': instance.device,
    };
