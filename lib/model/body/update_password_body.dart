import 'package:json_annotation/json_annotation.dart';

part 'update_password_body.g.dart';

@JsonSerializable()
class UpdatePasswordBody {
  String phone;
  String password;
  String repassword;


  UpdatePasswordBody(this.phone, this.password, this.repassword);

  factory UpdatePasswordBody.fromJson(Map<String, dynamic> json) =>
      _$UpdatePasswordBodyFromJson(json);

  Map<String, dynamic> toJson() => _$UpdatePasswordBodyToJson(this);
}
