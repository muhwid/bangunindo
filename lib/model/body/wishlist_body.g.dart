// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wishlist_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WishlistBody _$WishlistBodyFromJson(Map<String, dynamic> json) {
  return WishlistBody(
    json['product'] as String,
  );
}

Map<String, dynamic> _$WishlistBodyToJson(WishlistBody instance) =>
    <String, dynamic>{
      'product': instance.product,
    };
