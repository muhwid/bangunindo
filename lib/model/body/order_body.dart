import 'package:e_commerce_app/model/object/product_cart_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_body.g.dart';

@JsonSerializable()
class OrderBody {
  String address;
  List<ProductCartModel> detail;
  String payment;
  String coupon;
  String subtotal;
  String shipping;
  String shipping_service;
  String total;
  String delivery;


  OrderBody(this.address, this.detail, this.payment, this.coupon,
      this.subtotal, this.shipping, this.shipping_service,this.total, this.delivery);

  factory OrderBody.fromJson(Map<String, dynamic> json) =>
      _$OrderBodyFromJson(json);

  Map<String, dynamic> toJson() => _$OrderBodyToJson(this);
}
