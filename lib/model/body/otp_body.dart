import 'package:json_annotation/json_annotation.dart';

part 'otp_body.g.dart';

@JsonSerializable()
class OtpBody {
  String phone;
  String otp;
  String fcm;
  String device;

  OtpBody(this.phone, this.otp, this.fcm, this.device);

  factory OtpBody.fromJson(Map<String, dynamic> json) =>
      _$OtpBodyFromJson(json);

  Map<String, dynamic> toJson() => _$OtpBodyToJson(this);
}
