// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_store_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterStoreBody _$RegisterStoreBodyFromJson(Map<String, dynamic> json) {
  return RegisterStoreBody(
    json['name'] as String,
    json['bidang'] as String,
    json['jenis_perusahaan'] as String,
    json['siup'] as String,
    json['tdp'] as String,
    json['npwp'] as String,
    json['ktp'] as String,
    json['photo'] as String,
    json['address'] as String,
    json['district'] as String,
    json['city'] as String,
    json['province'] as String,
    json['latitude'] as String,
    json['longitude'] as String,
  );
}

Map<String, dynamic> _$RegisterStoreBodyToJson(RegisterStoreBody instance) =>
    <String, dynamic>{
      'name': instance.name,
      'bidang': instance.bidang,
      'jenis_perusahaan': instance.jenis_perusahaan,
      'siup': instance.siup,
      'tdp': instance.tdp,
      'npwp': instance.npwp,
      'ktp': instance.ktp,
      'photo': instance.photo,
      'address': instance.address,
      'district': instance.district,
      'city': instance.city,
      'province': instance.province,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
