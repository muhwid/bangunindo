import 'package:json_annotation/json_annotation.dart';

part 'add_product_body.g.dart';

@JsonSerializable()
class AddProductBody {
  String name;
  String description;
  String category;
  String price;
  String stock;
  String min_grosir;
  String grosir;
  String weight;
  String images;
  String video;


  AddProductBody(
      this.name,
      this.description,
      this.category,
      this.price,
      this.stock,
      this.min_grosir,
      this.grosir,
      this.weight,
      this.images,
      this.video);


  factory AddProductBody.fromJson(Map<String, dynamic> json) =>
      _$AddProductBodyFromJson(json);

  Map<String, dynamic> toJson() => _$AddProductBodyToJson(this);
}
