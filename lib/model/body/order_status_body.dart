import 'package:json_annotation/json_annotation.dart';

part 'order_status_body.g.dart';

@JsonSerializable()
class OrderStatusBody {
  String id;
  String status;
  String reason;
  String shipping;
  String shipping_permit;
  String shipping_photo;
  String shipping_number;

  OrderStatusBody(this.id, this.status, this.reason,
      this.shipping,
      [this.shipping_permit = null,
      this.shipping_photo = null,
      this.shipping_number = null]);

  factory OrderStatusBody.fromJson(Map<String, dynamic> json) =>
      _$OrderStatusBodyFromJson(json);

  Map<String, dynamic> toJson() => _$OrderStatusBodyToJson(this);
}
