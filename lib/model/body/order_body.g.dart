// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderBody _$OrderBodyFromJson(Map<String, dynamic> json) {
  return OrderBody(
    json['address'] as String,
    (json['detail'] as List)
        ?.map((e) => e == null
            ? null
            : ProductCartModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['payment'] as String,
    json['coupon'] as String,
    json['subtotal'] as String,
    json['shipping'] as String,
    json['shipping_service'] as String,
    json['total'] as String,
    json['delivery'] as String,
  );
}

Map<String, dynamic> _$OrderBodyToJson(OrderBody instance) => <String, dynamic>{
      'address': instance.address,
      'detail': instance.detail,
      'payment': instance.payment,
      'coupon': instance.coupon,
      'subtotal': instance.subtotal,
      'shipping': instance.shipping,
      'shipping_service': instance.shipping_service,
      'total': instance.total,
      'delivery': instance.delivery,
    };
