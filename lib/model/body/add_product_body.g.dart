// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_product_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddProductBody _$AddProductBodyFromJson(Map<String, dynamic> json) {
  return AddProductBody(
    json['name'] as String,
    json['description'] as String,
    json['category'] as String,
    json['price'] as String,
    json['stock'] as String,
    json['min_grosir'] as String,
    json['grosir'] as String,
    json['weight'] as String,
    json['images'] as String,
    json['video'] as String,
  );
}

Map<String, dynamic> _$AddProductBodyToJson(AddProductBody instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'category': instance.category,
      'price': instance.price,
      'stock': instance.stock,
      'min_grosir': instance.min_grosir,
      'grosir': instance.grosir,
      'weight': instance.weight,
      'images': instance.images,
      'video': instance.video,
    };
