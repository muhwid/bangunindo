// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'otp_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OtpBody _$OtpBodyFromJson(Map<String, dynamic> json) {
  return OtpBody(
    json['phone'] as String,
    json['otp'] as String,
    json['fcm'] as String,
    json['device'] as String,
  );
}

Map<String, dynamic> _$OtpBodyToJson(OtpBody instance) => <String, dynamic>{
      'phone': instance.phone,
      'otp': instance.otp,
      'fcm': instance.fcm,
      'device': instance.device,
    };
