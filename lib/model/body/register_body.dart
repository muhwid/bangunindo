import 'package:json_annotation/json_annotation.dart';

part 'register_body.g.dart';

@JsonSerializable()
class RegisterBody{

  String name;
  String phone;
  String email;
  String password;
  String repassword;


  RegisterBody(
      this.name, this.phone, this.email, this.password, this.repassword);

  factory RegisterBody.fromJson(Map<String, dynamic> json) => _$RegisterBodyFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterBodyToJson(this);

}