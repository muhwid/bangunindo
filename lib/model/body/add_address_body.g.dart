// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_address_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddAddressBody _$AddAddressBodyFromJson(Map<String, dynamic> json) {
  return AddAddressBody(
    json['id'] as String,
    json['name'] as String,
    json['address'] as String,
    json['district'] as String,
    json['city'] as String,
    json['province'] as String,
    json['phone'] as String,
    json['latitude'] as String,
    json['longitude'] as String,
  );
}

Map<String, dynamic> _$AddAddressBodyToJson(AddAddressBody instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'district': instance.district,
      'city': instance.city,
      'province': instance.province,
      'phone': instance.phone,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
