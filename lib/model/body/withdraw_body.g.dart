// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'withdraw_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WithdrawBody _$WithdrawBodyFromJson(Map<String, dynamic> json) {
  return WithdrawBody(
    json['bank'] as String,
    json['account_number'] as String,
    json['amount'] as String,
  );
}

Map<String, dynamic> _$WithdrawBodyToJson(WithdrawBody instance) =>
    <String, dynamic>{
      'bank': instance.bank,
      'account_number': instance.account_number,
      'amount': instance.amount,
    };
