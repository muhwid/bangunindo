import 'package:json_annotation/json_annotation.dart';

part 'review_body.g.dart';

@JsonSerializable()
class ReviewBody {
  String product;
  String rate;
  String review;
  String photo;
  String video;

  //for helping list only
  String type;

  ReviewBody(this.product, this.rate, this.review, this.photo, this.video);

  ReviewBody.static(this.product, this.rate, this.review, this.photo, this.video, this.type);

  factory ReviewBody.fromJson(Map<String, dynamic> json) =>
      _$ReviewBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewBodyToJson(this);
}
