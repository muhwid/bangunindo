import 'package:json_annotation/json_annotation.dart';

part 'edit_product_body.g.dart';

@JsonSerializable()
class EditProductBody {
  String id;
  String name;
  String description;
  String category;
  String price;
  String stock;
  String min_grosir;
  String grosir;
  String weight;
  String images;
  String video;

  
  EditProductBody(
      this.id,
      this.name,
      this.description,
      this.category,
      this.price,
      this.stock,
      this.min_grosir,
      this.grosir,
      this.weight,
      this.images,
      this.video);

  factory EditProductBody.fromJson(Map<String, dynamic> json) =>
      _$EditProductBodyFromJson(json);

  Map<String, dynamic> toJson() => _$EditProductBodyToJson(this);
}
