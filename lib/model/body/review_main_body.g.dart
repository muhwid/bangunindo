// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_main_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReviewMainBody _$ReviewMainBodyFromJson(Map<String, dynamic> json) {
  return ReviewMainBody(
    json['id_transaction'] as String,
    (json['product'] as List)
        ?.map((e) =>
            e == null ? null : ReviewBody.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ReviewMainBodyToJson(ReviewMainBody instance) =>
    <String, dynamic>{
      'id_transaction': instance.id_transaction,
      'product': instance.product,
    };
