import 'package:json_annotation/json_annotation.dart';

part 'withdraw_body.g.dart';

@JsonSerializable()
class WithdrawBody {
  String bank;
  String account_number;
  String amount;

  WithdrawBody(this.bank, this.account_number, this.amount);

  factory WithdrawBody.fromJson(Map<String, dynamic> json) =>
      _$WithdrawBodyFromJson(json);

  Map<String, dynamic> toJson() => _$WithdrawBodyToJson(this);
}
