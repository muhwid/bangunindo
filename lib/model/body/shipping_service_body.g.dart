// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipping_service_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShippingServiceBody _$ShippingServiceBodyFromJson(Map<String, dynamic> json) {
  return ShippingServiceBody(
    (json['product'] as List)
        ?.map((e) => e == null
            ? null
            : ShippingServiceModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ShippingServiceBodyToJson(
        ShippingServiceBody instance) =>
    <String, dynamic>{
      'product': instance.product,
    };
