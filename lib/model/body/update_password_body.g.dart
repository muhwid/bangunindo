// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_password_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdatePasswordBody _$UpdatePasswordBodyFromJson(Map<String, dynamic> json) {
  return UpdatePasswordBody(
    json['phone'] as String,
    json['password'] as String,
    json['repassword'] as String,
  );
}

Map<String, dynamic> _$UpdatePasswordBodyToJson(UpdatePasswordBody instance) =>
    <String, dynamic>{
      'phone': instance.phone,
      'password': instance.password,
      'repassword': instance.repassword,
    };
