import 'package:json_annotation/json_annotation.dart';

part 'register_store_body.g.dart';

@JsonSerializable()
class RegisterStoreBody{

  String name;
  String bidang;
  String jenis_perusahaan;
  String siup;
  String tdp;
  String npwp;
  String ktp;
  String photo;
  String address;
  String district;
  String city;
  String province;
  String latitude;
  String longitude;


  RegisterStoreBody(
      this.name, this.bidang, this.jenis_perusahaan, this.siup, this.tdp, this.npwp, this.ktp, this.photo, this.address, this.district, this.city, this.province, this.latitude, this.longitude);

  factory RegisterStoreBody.fromJson(Map<String, dynamic> json) => _$RegisterStoreBodyFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterStoreBodyToJson(this);

}