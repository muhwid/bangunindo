// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_status_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderStatusBody _$OrderStatusBodyFromJson(Map<String, dynamic> json) {
  return OrderStatusBody(
    json['id'] as String,
    json['status'] as String,
    json['reason'] as String,
    json['shipping'] as String,
    json['shipping_permit'] as String,
    json['shipping_photo'] as String,
    json['shipping_number'] as String,
  );
}

Map<String, dynamic> _$OrderStatusBodyToJson(OrderStatusBody instance) =>
    <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'reason': instance.reason,
      'shipping': instance.shipping,
      'shipping_permit': instance.shipping_permit,
      'shipping_photo': instance.shipping_photo,
      'shipping_number': instance.shipping_number,
    };
