// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_profile_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EditProfileBody _$EditProfileBodyFromJson(Map<String, dynamic> json) {
  return EditProfileBody(
    json['name'] as String,
    json['email'] as String,
    json['phone'] as String,
    json['password'] as String,
    json['repassword'] as String,
  );
}

Map<String, dynamic> _$EditProfileBodyToJson(EditProfileBody instance) =>
    <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'phone': instance.phone,
      'password': instance.password,
      'repassword': instance.repassword,
    };
