import 'package:json_annotation/json_annotation.dart';

part 'wishlist_body.g.dart';

@JsonSerializable()
class WishlistBody {
  String product;


  WishlistBody(this.product);

  factory WishlistBody.fromJson(Map<String, dynamic> json) =>
      _$WishlistBodyFromJson(json);

  Map<String, dynamic> toJson() => _$WishlistBodyToJson(this);
}
