// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_product_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EditProductBody _$EditProductBodyFromJson(Map<String, dynamic> json) {
  return EditProductBody(
    json['id'] as String,
    json['name'] as String,
    json['description'] as String,
    json['category'] as String,
    json['price'] as String,
    json['stock'] as String,
    json['min_grosir'] as String,
    json['grosir'] as String,
    json['weight'] as String,
    json['images'] as String,
    json['video'] as String,
  );
}

Map<String, dynamic> _$EditProductBodyToJson(EditProductBody instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'category': instance.category,
      'price': instance.price,
      'stock': instance.stock,
      'min_grosir': instance.min_grosir,
      'grosir': instance.grosir,
      'weight': instance.weight,
      'images': instance.images,
      'video': instance.video,
    };
