import 'package:json_annotation/json_annotation.dart';

part 'add_address_body.g.dart';

@JsonSerializable()
class AddAddressBody {
  String id;
  String name;
  String address;
  String district;
  String city;
  String province;
  String phone;
  String latitude;
  String longitude;

  AddAddressBody(this.id, this.name, this.address, this.district, this.city,
      this.province, this.phone, this.latitude, this.longitude);

  factory AddAddressBody.fromJson(Map<String, dynamic> json) =>
      _$AddAddressBodyFromJson(json);

  Map<String, dynamic> toJson() => _$AddAddressBodyToJson(this);
}
