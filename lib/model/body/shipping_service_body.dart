import 'package:e_commerce_app/model/object/shipping_service_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'shipping_service_body.g.dart';

@JsonSerializable()
class ShippingServiceBody {
  List<ShippingServiceModel> product;
  ShippingServiceBody(this.product);

  factory ShippingServiceBody.fromJson(Map<String, dynamic> json) =>
      _$ShippingServiceBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ShippingServiceBodyToJson(this);
}
