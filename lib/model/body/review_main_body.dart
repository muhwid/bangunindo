import 'package:e_commerce_app/model/body/review_body.dart';
import 'package:json_annotation/json_annotation.dart';

part 'review_main_body.g.dart';

@JsonSerializable()
class ReviewMainBody {
  String id_transaction;
  List<ReviewBody> product;

  ReviewMainBody(this.id_transaction, this.product);

  factory ReviewMainBody.fromJson(Map<String, dynamic> json) =>
      _$ReviewMainBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewMainBodyToJson(this);
}
