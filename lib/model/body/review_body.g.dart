// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReviewBody _$ReviewBodyFromJson(Map<String, dynamic> json) {
  return ReviewBody(
    json['product'] as String,
    json['rate'] as String,
    json['review'] as String,
    json['photo'] as String,
    json['video'] as String,
  )..type = json['type'] as String;
}

Map<String, dynamic> _$ReviewBodyToJson(ReviewBody instance) =>
    <String, dynamic>{
      'product': instance.product,
      'rate': instance.rate,
      'review': instance.review,
      'photo': instance.photo,
      'video': instance.video,
      'type': instance.type,
    };
