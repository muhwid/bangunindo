import 'package:json_annotation/json_annotation.dart';

part 'edit_profile_body.g.dart';

@JsonSerializable()
class EditProfileBody {
  String name;
  String email;
  String phone;
  String password;
  String repassword;


  EditProfileBody(
      this.name, this.email, this.phone, this.password, this.repassword);

  factory EditProfileBody.fromJson(Map<String, dynamic> json) =>
      _$EditProfileBodyFromJson(json);

  Map<String, dynamic> toJson() => _$EditProfileBodyToJson(this);
}
