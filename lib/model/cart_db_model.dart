class CartDbModel {
  String id_product;
  String id_store;
  String name;
  String description;
  String category;
  String brand;
  String price;
  String stock;
  String min_grosir;
  String grosir;
  String weight;
  String expired;
  String created_at;
  String image;
  String review;
  int qty;


  CartDbModel();

  CartDbModel.insert(
      this.id_product,
      this.id_store,
      this.name,
      this.description,
      this.category,
      this.brand,
      this.price,
      this.stock,
      this.min_grosir,
      this.grosir,
      this.weight,
      this.expired,
      this.created_at,
      this.image,
      this.review,
      this.qty);

  Map<String, dynamic> toMap() {
    return {
      'id_product': id_product,
      'id_store': id_store,
      'name': name,
      'description': description,
      'category': category,
      'brand': brand,
      'price': price,
      'stock': stock,
      'min_grosir': min_grosir,
      'grosir': grosir,
      'weight': weight,
      'expired': expired,
      'created_at': created_at,
      'image': image,
      'review': review,
      'qty': qty
    };
  }

  CartDbModel fromMap(Map<String, dynamic> map) {
    return CartDbModel.insert(
      id_product = map['id_product'].toString(),
      id_store = map['id_store'].toString(),
      name = map['name'].toString(),
      description = map['description'].toString(),
      category = map['category'].toString(),
      brand = map['brand'].toString(),
      price = map['price'].toString(),
      stock = map['stock'].toString(),
      min_grosir = map['min_grosir'].toString(),
      grosir = map['grosir'].toString(),
      weight = map['weight'].toString(),
      expired = map['expired'].toString(),
      created_at = map['created_at'].toString(),
      image = map['image'].toString(),
      review = map['review'].toString(),
      qty = map['qty']
    );
  }
}
