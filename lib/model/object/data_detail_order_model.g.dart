// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_detail_order_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataDetailOrderModel _$DataDetailOrderModelFromJson(Map<String, dynamic> json) {
  return DataDetailOrderModel()
    ..id_transaction = json['id_transaction'] as String
    ..user = json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>)
    ..store = json['store'] as String
    ..discount = json['discount'] as String
    ..subtotal = json['subtotal'] as String
    ..shipping = json['shipping'] as String
    ..shipping_service = json['shipping_service'] as String
    ..total = json['total'] as String
    ..delivery = json['delivery'] as String
    ..address = json['address'] == null
        ? null
        : AddressModel.fromJson(json['address'] as Map<String, dynamic>)
    ..payment = json['payment'] as String
    ..status = json['status'] as String
    ..coupon = json['coupon'] as String
    ..courier = json['courier'] == null
        ? null
        : CourierModel.fromJson(json['courier'] as Map<String, dynamic>)
    ..shipping_number = json['shipping_number'] as String
    ..shipping_photo = json['shipping_photo'] as String
    ..shipping_permit = json['shipping_permit'] as String
    ..xendit = json['xendit'] as String
    ..accepted = json['accepted'] as String
    ..reason = json['reason'] as String
    ..created_at = json['created_at'] as String
    ..detail = (json['detail'] as List)
        ?.map((e) =>
            e == null ? null : DetailModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$DataDetailOrderModelToJson(
        DataDetailOrderModel instance) =>
    <String, dynamic>{
      'id_transaction': instance.id_transaction,
      'user': instance.user,
      'store': instance.store,
      'discount': instance.discount,
      'subtotal': instance.subtotal,
      'shipping': instance.shipping,
      'shipping_service': instance.shipping_service,
      'total': instance.total,
      'delivery': instance.delivery,
      'address': instance.address,
      'payment': instance.payment,
      'status': instance.status,
      'coupon': instance.coupon,
      'courier': instance.courier,
      'shipping_number': instance.shipping_number,
      'shipping_photo': instance.shipping_photo,
      'shipping_permit': instance.shipping_permit,
      'xendit': instance.xendit,
      'accepted': instance.accepted,
      'reason': instance.reason,
      'created_at': instance.created_at,
      'detail': instance.detail,
    };

DetailModel _$DetailModelFromJson(Map<String, dynamic> json) {
  return DetailModel()
    ..id_detail_transaction = json['id_detail_transaction'] as String
    ..transaction = json['transaction'] as String
    ..product = json['product'] as String
    ..qty = json['qty'] as String
    ..price = json['price'] as String
    ..total = json['total'] as String
    ..created_at = json['created_at'] as String
    ..productID = json['productID'] as String;
}

Map<String, dynamic> _$DetailModelToJson(DetailModel instance) =>
    <String, dynamic>{
      'id_detail_transaction': instance.id_detail_transaction,
      'transaction': instance.transaction,
      'product': instance.product,
      'qty': instance.qty,
      'price': instance.price,
      'total': instance.total,
      'created_at': instance.created_at,
      'productID': instance.productID,
    };

AddressModel _$AddressModelFromJson(Map<String, dynamic> json) {
  return AddressModel()
    ..id_address = json['id_address'] as String
    ..user = json['user'] as String
    ..name = json['name'] as String
    ..address = json['address'] as String
    ..district = json['district'] as String
    ..city = json['city'] as String
    ..province = json['province'] as String
    ..phone = json['phone'] as String
    ..latitude = json['latitude'] as String
    ..longitude = json['longitude'] as String
    ..created_at = json['created_at'] as String
    ..provinceName = json['provinceName'] as String
    ..districtName = json['districtName'] as String
    ..cityName = json['cityName'] as String;
}

Map<String, dynamic> _$AddressModelToJson(AddressModel instance) =>
    <String, dynamic>{
      'id_address': instance.id_address,
      'user': instance.user,
      'name': instance.name,
      'address': instance.address,
      'district': instance.district,
      'city': instance.city,
      'province': instance.province,
      'phone': instance.phone,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'created_at': instance.created_at,
      'provinceName': instance.provinceName,
      'districtName': instance.districtName,
      'cityName': instance.cityName,
    };

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..idUser = json['idUser'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..email = json['email'] as String
    ..password = json['password'] as String
    ..otp = json['otp'] as String
    ..status = json['status'] as String
    ..createdAt = json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'idUser': instance.idUser,
      'name': instance.name,
      'phone': instance.phone,
      'email': instance.email,
      'password': instance.password,
      'otp': instance.otp,
      'status': instance.status,
      'createdAt': instance.createdAt?.toIso8601String(),
    };
