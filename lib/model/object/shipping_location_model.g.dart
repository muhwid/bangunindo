// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipping_location_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShippingLocationModel _$ShippingLocationModelFromJson(
    Map<String, dynamic> json) {
  return ShippingLocationModel()
    ..address = json['address'] as String
    ..lat = (json['lat'] as num)?.toDouble()
    ..lng = (json['lng'] as num)?.toDouble();
}

Map<String, dynamic> _$ShippingLocationModelToJson(
        ShippingLocationModel instance) =>
    <String, dynamic>{
      'address': instance.address,
      'lat': instance.lat,
      'lng': instance.lng,
    };
