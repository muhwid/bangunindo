
import 'package:json_annotation/json_annotation.dart';

part 'cost_model.g.dart';

@JsonSerializable()
class CostModel {
  int value;
  String etd;
  String note;

  CostModel();

  factory CostModel.fromJson(Map<String, dynamic> json) =>
      _$CostModelFromJson(json);

  Map<String, dynamic> toJson() => _$CostModelToJson(this);
}
