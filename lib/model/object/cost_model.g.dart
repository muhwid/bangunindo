// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cost_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CostModel _$CostModelFromJson(Map<String, dynamic> json) {
  return CostModel()
    ..value = json['value'] as int
    ..etd = json['etd'] as String
    ..note = json['note'] as String;
}

Map<String, dynamic> _$CostModelToJson(CostModel instance) => <String, dynamic>{
      'value': instance.value,
      'etd': instance.etd,
      'note': instance.note,
    };
