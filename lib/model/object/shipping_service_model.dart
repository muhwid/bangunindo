import 'package:json_annotation/json_annotation.dart';

part 'shipping_service_model.g.dart';

@JsonSerializable()
class ShippingServiceModel {
  String id_shipping_service;
  String name;
  String code;
  bool isactive;

  ShippingServiceModel();

  factory ShippingServiceModel.fromJson(Map<String, dynamic> json) =>
      _$ShippingServiceModelFromJson(json);

  Map<String, dynamic> toJson() => _$ShippingServiceModelToJson(this);
}
