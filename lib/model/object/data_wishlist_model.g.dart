// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_wishlist_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataWishlistModel _$DataWishlistModelFromJson(Map<String, dynamic> json) {
  return DataWishlistModel()
    ..id_favorite = json['id_favorite'] as String
    ..user = json['user'] as String
    ..product = json['product'] == null
        ? null
        : ProductModel.fromJson(json['product'] as Map<String, dynamic>)
    ..created_at = json['created_at'] as String;
}

Map<String, dynamic> _$DataWishlistModelToJson(DataWishlistModel instance) =>
    <String, dynamic>{
      'id_favorite': instance.id_favorite,
      'user': instance.user,
      'product': instance.product,
      'created_at': instance.created_at,
    };
