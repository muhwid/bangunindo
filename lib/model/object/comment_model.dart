
import 'package:e_commerce_app/model/object/user_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'comment_model.g.dart';

@JsonSerializable()
class CommentModel {

  String id_review;
  String transaction;
  String product;
  String rate;
  UserModel user;
  String review;
  String created_at;
  String photo;
  String video;

  CommentModel();

  factory CommentModel.fromJson(Map<String, dynamic> json) =>
      _$CommentModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentModelToJson(this);
}
