// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipping_service_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShippingServiceModel _$ShippingServiceModelFromJson(Map<String, dynamic> json) {
  return ShippingServiceModel()
    ..id_shipping_service = json['id_shipping_service'] as String
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..isactive = json['isactive'] as bool;
}

Map<String, dynamic> _$ShippingServiceModelToJson(
        ShippingServiceModel instance) =>
    <String, dynamic>{
      'id_shipping_service': instance.id_shipping_service,
      'name': instance.name,
      'code': instance.code,
      'isactive': instance.isactive,
    };
