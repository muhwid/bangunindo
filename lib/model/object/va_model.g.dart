// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'va_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VaModel _$VaModelFromJson(Map<String, dynamic> json) {
  return VaModel()
    ..is_closed = json['is_closed'] as bool
    ..status = json['status'] as String
    ..owner_id = json['owner_id'] as String
    ..external_id = json['external_id'] as String
    ..bank_code = json['bank_code'] as String
    ..merchant_code = json['merchant_code'] as String
    ..name = json['name'] as String
    ..account_number = json['account_number'] as String
    ..is_single_use = json['is_single_use'] as bool
    ..expiration_date = json['expiration_date'] as String
    ..id = json['id'] as String;
}

Map<String, dynamic> _$VaModelToJson(VaModel instance) => <String, dynamic>{
      'is_closed': instance.is_closed,
      'status': instance.status,
      'owner_id': instance.owner_id,
      'external_id': instance.external_id,
      'bank_code': instance.bank_code,
      'merchant_code': instance.merchant_code,
      'name': instance.name,
      'account_number': instance.account_number,
      'is_single_use': instance.is_single_use,
      'expiration_date': instance.expiration_date,
      'id': instance.id,
    };
