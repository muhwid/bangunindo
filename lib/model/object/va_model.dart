
import 'package:json_annotation/json_annotation.dart';

part 'va_model.g.dart';

@JsonSerializable()
class VaModel {
 
  bool is_closed;
  String status;
  String owner_id;
  String external_id;
  String bank_code;
  String merchant_code;
  String name;
  String account_number;
  bool is_single_use;
  String expiration_date;
  String id;

  VaModel();

  factory VaModel.fromJson(Map<String, dynamic> json) =>
      _$VaModelFromJson(json);

  Map<String, dynamic> toJson() => _$VaModelToJson(this);
}
