
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:e_commerce_app/model/object/instruction_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/model/object/va_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'data_detail_va_model.g.dart';

@JsonSerializable()
class DataDetailVaModel {

 VaModel va;
 List<InstructionModel> instruction;

  DataDetailVaModel();

  factory DataDetailVaModel.fromJson(Map<String, dynamic> json) =>
      _$DataDetailVaModelFromJson(json);

  Map<String, dynamic> toJson() => _$DataDetailVaModelToJson(this);
}
