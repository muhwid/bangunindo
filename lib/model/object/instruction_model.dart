import 'package:json_annotation/json_annotation.dart';

part 'instruction_model.g.dart';

@JsonSerializable()
class InstructionModel {
  String id_instruction;
  String code;
  String name;
  String text;

  InstructionModel();

  factory InstructionModel.fromJson(Map<String, dynamic> json) =>
      _$InstructionModelFromJson(json);

  Map<String, dynamic> toJson() => _$InstructionModelToJson(this);
}
