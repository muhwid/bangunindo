// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponModel _$CouponModelFromJson(Map<String, dynamic> json) {
  return CouponModel()
    ..id_coupon = json['id_coupon'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..code = json['code'] as String
    ..discount = json['discount'] as String
    ..image = json['image'] as String
    ..expired = json['expired'] as String
    ..created_at = json['created_at'] as String;
}

Map<String, dynamic> _$CouponModelToJson(CouponModel instance) =>
    <String, dynamic>{
      'id_coupon': instance.id_coupon,
      'name': instance.name,
      'description': instance.description,
      'code': instance.code,
      'discount': instance.discount,
      'image': instance.image,
      'expired': instance.expired,
      'created_at': instance.created_at,
    };
