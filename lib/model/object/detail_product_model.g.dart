// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_product_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailProductModel _$DetailProductModelFromJson(Map<String, dynamic> json) {
  return DetailProductModel()
    ..id_product = json['id_product'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..category = json['category'] == null
        ? null
        : CategoryModel.fromJson(json['category'] as Map<String, dynamic>)
    ..store = json['store'] == null
        ? null
        : StoreModel.fromJson(json['store'] as Map<String, dynamic>)
    ..brand = json['brand'] as String
    ..price = json['price'] as String
    ..stock = json['stock'] as String
    ..min_grosir = json['min_grosir'] as String
    ..grosir = json['grosir'] as String
    ..weight = json['weight'] as String
    ..video = json['video'] as String
    ..expired = json['expired'] as String
    ..created_at = json['created_at'] as String
    ..images = (json['images'] as List)?.map((e) => e as String)?.toList()
    ..review = json['review'] as String
    ..comments = (json['comments'] as List)
        ?.map((e) =>
            e == null ? null : CommentModel.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..bad = (json['bad'] as num)?.toDouble()
    ..average = (json['average'] as num)?.toDouble()
    ..very = (json['very'] as num)?.toDouble()
    ..excelent = (json['excelent'] as num)?.toDouble();
}

Map<String, dynamic> _$DetailProductModelToJson(DetailProductModel instance) =>
    <String, dynamic>{
      'id_product': instance.id_product,
      'name': instance.name,
      'description': instance.description,
      'category': instance.category,
      'store': instance.store,
      'brand': instance.brand,
      'price': instance.price,
      'stock': instance.stock,
      'min_grosir': instance.min_grosir,
      'grosir': instance.grosir,
      'weight': instance.weight,
      'video': instance.video,
      'expired': instance.expired,
      'created_at': instance.created_at,
      'images': instance.images,
      'review': instance.review,
      'comments': instance.comments,
      'bad': instance.bad,
      'average': instance.average,
      'very': instance.very,
      'excelent': instance.excelent,
    };
