
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'addresses_model.g.dart';

@JsonSerializable()
class AddressesModel {

  String id_address;
  String user;
  String name;
  String address;
  DistrictModel district;
  CityModel city;
  ProvinceModel province;
  String phone;
  String latitude;
  String longitude;
  String created_at;

  AddressesModel();

  factory AddressesModel.fromJson(Map<String, dynamic> json) =>
      _$AddressesModelFromJson(json);

  Map<String, dynamic> toJson() => _$AddressesModelToJson(this);
}
