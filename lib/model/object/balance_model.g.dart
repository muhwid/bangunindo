// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'balance_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BalanceModel _$BalanceModelFromJson(Map<String, dynamic> json) {
  return BalanceModel()
    ..id_balance_store = json['id_balance_store'] as String
    ..store = json['store'] as String
    ..amount = json['amount'] as String
    ..transaction = json['transaction'] as String
    ..description = json['description'] as String
    ..created_at = json['created_at'] as String;
}

Map<String, dynamic> _$BalanceModelToJson(BalanceModel instance) =>
    <String, dynamic>{
      'id_balance_store': instance.id_balance_store,
      'store': instance.store,
      'amount': instance.amount,
      'transaction': instance.transaction,
      'description': instance.description,
      'created_at': instance.created_at,
    };
