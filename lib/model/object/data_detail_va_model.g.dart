// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_detail_va_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataDetailVaModel _$DataDetailVaModelFromJson(Map<String, dynamic> json) {
  return DataDetailVaModel()
    ..va = json['va'] == null
        ? null
        : VaModel.fromJson(json['va'] as Map<String, dynamic>)
    ..instruction = (json['instruction'] as List)
        ?.map((e) => e == null
            ? null
            : InstructionModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$DataDetailVaModelToJson(DataDetailVaModel instance) =>
    <String, dynamic>{
      'va': instance.va,
      'instruction': instance.instruction,
    };
