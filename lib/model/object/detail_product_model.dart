import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/store_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'comment_model.dart';

part 'detail_product_model.g.dart';

@JsonSerializable()
class DetailProductModel {
  String id_product;
  String name;
  String description;
  CategoryModel category;
  StoreModel store;
  String brand;
  String price;
  String stock;
  String min_grosir;
  String grosir;
  String weight;
  String video;
  String expired;
  String created_at;
  List<String> images;
  String review;
  List<CommentModel> comments;
  double bad;
  double average;
  double very;
  double excelent;

  // var isWishlist = false.obs;

  DetailProductModel();

  factory DetailProductModel.fromJson(Map<String, dynamic> json) =>
      _$DetailProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductModelToJson(this);
}
