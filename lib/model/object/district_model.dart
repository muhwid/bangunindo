
import 'package:json_annotation/json_annotation.dart';

part 'district_model.g.dart';

@JsonSerializable()
class DistrictModel {

  String id_district;
  String name;
  String id_province;
  String province;
  String id_cities;
  String city;
  String type;

  DistrictModel();

  factory DistrictModel.fromJson(Map<String, dynamic> json) =>
      _$DistrictModelFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictModelToJson(this);
}
