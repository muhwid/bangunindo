// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'courier_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CourierModel _$CourierModelFromJson(Map<String, dynamic> json) {
  return CourierModel()
    ..id_courier = json['id_courier'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..password = json['password'] as String
    ..image = json['image'] as String
    ..latitude = json['latitude'] as String
    ..longitude = json['longitude'] as String
    ..status = json['status'] as String
    ..created_at = json['created_at'] as String
    ..last_online = json['last_online'] as String;
}

Map<String, dynamic> _$CourierModelToJson(CourierModel instance) =>
    <String, dynamic>{
      'id_courier': instance.id_courier,
      'name': instance.name,
      'phone': instance.phone,
      'password': instance.password,
      'image': instance.image,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'status': instance.status,
      'created_at': instance.created_at,
      'last_online': instance.last_online,
    };
