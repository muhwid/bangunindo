import 'package:json_annotation/json_annotation.dart';

part 'store_model.g.dart';

@JsonSerializable()
class StoreModel {
  String id_store;
  String name;
  String user;
  String address;
  String province;
  String district;
  String city;
  String city_name;
  String district_name;
  String latitude;
  String longitude;
  String photo;
  String last_seen;
  int balance;

  StoreModel();

  factory StoreModel.fromJson(Map<String, dynamic> json) =>
      _$StoreModelFromJson(json);

  Map<String, dynamic> toJson() => _$StoreModelToJson(this);
}
