
import 'package:json_annotation/json_annotation.dart';

import 'master_cost_model.dart';

part 'data_shipping_model.g.dart';

@JsonSerializable()
class DataShippingModel {

  String code;
  String name;
  List<MasterCostModel> costs;

  DataShippingModel();

  factory DataShippingModel.fromJson(Map<String, dynamic> json) =>
      _$DataShippingModelFromJson(json);

  Map<String, dynamic> toJson() => _$DataShippingModelToJson(this);
}
