// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductModel _$ProductModelFromJson(Map<String, dynamic> json) {
  return ProductModel(
    id_product: json['id_product'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    store: json['store'] as String,
    brand: json['brand'] as String,
    category: json['category'] as String,
    price: json['price'] as String,
    stock: json['stock'] as String,
    min_grosir: json['min_grosir'] as String,
    grosir: json['grosir'] as String,
    weight: json['weight'] as String,
    expired: json['expired'] as String,
    images: (json['images'] as List)?.map((e) => e as String)?.toList(),
    video: json['video'] as String,
    created_at: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    review: json['review'] as String,
  );
}

Map<String, dynamic> _$ProductModelToJson(ProductModel instance) =>
    <String, dynamic>{
      'id_product': instance.id_product,
      'name': instance.name,
      'description': instance.description,
      'store': instance.store,
      'brand': instance.brand,
      'category': instance.category,
      'price': instance.price,
      'stock': instance.stock,
      'min_grosir': instance.min_grosir,
      'grosir': instance.grosir,
      'weight': instance.weight,
      'expired': instance.expired,
      'images': instance.images,
      'video': instance.video,
      'created_at': instance.created_at?.toIso8601String(),
      'review': instance.review,
    };
