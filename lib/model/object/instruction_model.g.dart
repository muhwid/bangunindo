// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instruction_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstructionModel _$InstructionModelFromJson(Map<String, dynamic> json) {
  return InstructionModel()
    ..id_instruction = json['id_instruction'] as String
    ..code = json['code'] as String
    ..name = json['name'] as String
    ..text = json['text'] as String;
}

Map<String, dynamic> _$InstructionModelToJson(InstructionModel instance) =>
    <String, dynamic>{
      'id_instruction': instance.id_instruction,
      'code': instance.code,
      'name': instance.name,
      'text': instance.text,
    };
