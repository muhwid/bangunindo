// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StoreModel _$StoreModelFromJson(Map<String, dynamic> json) {
  return StoreModel()
    ..id_store = json['id_store'] as String
    ..name = json['name'] as String
    ..user = json['user'] as String
    ..address = json['address'] as String
    ..province = json['province'] as String
    ..district = json['district'] as String
    ..city = json['city'] as String
    ..city_name = json['city_name'] as String
    ..district_name = json['district_name'] as String
    ..latitude = json['latitude'] as String
    ..longitude = json['longitude'] as String
    ..photo = json['photo'] as String
    ..last_seen = json['last_seen'] as String
    ..balance = json['balance'] as int;
}

Map<String, dynamic> _$StoreModelToJson(StoreModel instance) =>
    <String, dynamic>{
      'id_store': instance.id_store,
      'name': instance.name,
      'user': instance.user,
      'address': instance.address,
      'province': instance.province,
      'district': instance.district,
      'city': instance.city,
      'city_name': instance.city_name,
      'district_name': instance.district_name,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'photo': instance.photo,
      'last_seen': instance.last_seen,
      'balance': instance.balance,
    };
