
import 'package:json_annotation/json_annotation.dart';

part 'shipping_location_model.g.dart';

@JsonSerializable()
class ShippingLocationModel {

  String address;
  double lat;
  double lng;


  ShippingLocationModel();

  ShippingLocationModel.insert(this.address, this.lat, this.lng);

  factory ShippingLocationModel.fromJson(Map<String, dynamic> json) =>
      _$ShippingLocationModelFromJson(json);

  Map<String, dynamic> toJson() => _$ShippingLocationModelToJson(this);
}
