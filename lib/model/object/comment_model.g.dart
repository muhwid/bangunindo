// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentModel _$CommentModelFromJson(Map<String, dynamic> json) {
  return CommentModel()
    ..id_review = json['id_review'] as String
    ..transaction = json['transaction'] as String
    ..product = json['product'] as String
    ..rate = json['rate'] as String
    ..user = json['user'] == null
        ? null
        : UserModel.fromJson(json['user'] as Map<String, dynamic>)
    ..review = json['review'] as String
    ..created_at = json['created_at'] as String
    ..photo = json['photo'] as String
    ..video = json['video'] as String;
}

Map<String, dynamic> _$CommentModelToJson(CommentModel instance) =>
    <String, dynamic>{
      'id_review': instance.id_review,
      'transaction': instance.transaction,
      'product': instance.product,
      'rate': instance.rate,
      'user': instance.user,
      'review': instance.review,
      'created_at': instance.created_at,
      'photo': instance.photo,
      'video': instance.video,
    };
