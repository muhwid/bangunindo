import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'courier_model.dart';

part 'data_detail_order_model.g.dart';

@JsonSerializable()
class DataDetailOrderModel {
  String id_transaction;
  User user;
  String store;
  String discount;
  String subtotal;
  String shipping;
  String shipping_service;
  String total;
  String delivery;
  AddressModel address;
  String payment;
  String status;
  String coupon;
  CourierModel courier;
  String shipping_number;
  String shipping_photo;
  String shipping_permit;
  String xendit;
  String accepted;
  String reason;
  String created_at;
  List<DetailModel> detail;

  DataDetailOrderModel();

  factory DataDetailOrderModel.fromJson(Map<String, dynamic> json) =>
      _$DataDetailOrderModelFromJson(json);

  Map<String, dynamic> toJson() => _$DataDetailOrderModelToJson(this);
}

@JsonSerializable()
class DetailModel {
  String id_detail_transaction;
  String transaction;
  String product;
  String qty;
  String price;
  String total;
  String created_at;
  String productID;

  DetailModel();

  factory DetailModel.fromJson(Map<String, dynamic> json) =>
      _$DetailModelFromJson(json);

  Map<String, dynamic> toJson() => _$DetailModelToJson(this);
}

@JsonSerializable()
class AddressModel {
  String id_address;
  String user;
  String name;
  String address;
  String district;
  String city;
  String province;
  String phone;
  String latitude;
  String longitude;
  String created_at;
  String provinceName;
  String districtName;
  String cityName;

  AddressModel();

  factory AddressModel.fromJson(Map<String, dynamic> json) =>
      _$AddressModelFromJson(json);

  Map<String, dynamic> toJson() => _$AddressModelToJson(this);
}

@JsonSerializable()
class User {
  String idUser;
  String name;
  String phone;
  String email;
  String password;
  String otp;
  String status;
  DateTime createdAt;

  User();

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  // factory User.fromJson(Map<String, dynamic> json) => User(
  //     idUser: json["id_user"],
  //     name: json["name"],
  //     phone: json["phone"],
  //     email: json["email"],
  //     password: json["password"],
  //     otp: json["otp"],
  //     status: json["status"],
  //     createdAt: DateTime.parse(json["created_at"]),
  // );

  // Map<String, dynamic> toJson() => {
  //     "id_user": idUser,
  //     "name": name,
  //     "phone": phone,
  //     "email": email,
  //     "password": password,
  //     "otp": otp,
  //     "status": status,
  //     "created_at": createdAt.toIso8601String(),
  // };
}
