import 'package:json_annotation/json_annotation.dart';

part 'courier_model.g.dart';

@JsonSerializable()
class CourierModel {
  String id_courier;
  String name;
  String phone;
  String password;
  String image;
  String latitude;
  String longitude;
  String status;
  String created_at;
  String last_online;

  CourierModel();

  factory CourierModel.fromJson(Map<String, dynamic> json) =>
      _$CourierModelFromJson(json);

  Map<String, dynamic> toJson() => _$CourierModelToJson(this);
}
