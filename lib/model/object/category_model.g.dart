// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryModel _$CategoryModelFromJson(Map<String, dynamic> json) {
  return CategoryModel()
    ..id_category = json['id_category'] as String
    ..name = json['name'] as String
    ..image = json['image'] as String
    ..parent = json['parent'] as String
    ..created_at = json['created_at'] as String
    ..total = json['total'] as int;
}

Map<String, dynamic> _$CategoryModelToJson(CategoryModel instance) =>
    <String, dynamic>{
      'id_category': instance.id_category,
      'name': instance.name,
      'image': instance.image,
      'parent': instance.parent,
      'created_at': instance.created_at,
      'total': instance.total,
    };
