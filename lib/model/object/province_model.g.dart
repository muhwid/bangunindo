// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'province_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProvinceModel _$ProvinceModelFromJson(Map<String, dynamic> json) {
  return ProvinceModel()
    ..id_province = json['id_province'] as String
    ..province = json['province'] as String;
}

Map<String, dynamic> _$ProvinceModelToJson(ProvinceModel instance) =>
    <String, dynamic>{
      'id_province': instance.id_province,
      'province': instance.province,
    };
