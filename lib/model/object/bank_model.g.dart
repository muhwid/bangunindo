// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bank_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankModel _$BankModelFromJson(Map<String, dynamic> json) {
  return BankModel()
    ..name = json['name'] as String
    ..code = json['code'] as String;
}

Map<String, dynamic> _$BankModelToJson(BankModel instance) => <String, dynamic>{
      'name': instance.name,
      'code': instance.code,
    };
