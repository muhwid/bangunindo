
import 'package:json_annotation/json_annotation.dart';

part 'payment_model.g.dart';

@JsonSerializable()
class PaymentModel {
  String bank_code;
  String collection_type;
  String bank_account_number;
  int transfer_amount;
  String bank_branch;
  String account_holder_name;

  PaymentModel();

  factory PaymentModel.fromJson(Map<String, dynamic> json) =>
      _$PaymentModelFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentModelToJson(this);
}
