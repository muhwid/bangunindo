// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'addresses_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressesModel _$AddressesModelFromJson(Map<String, dynamic> json) {
  return AddressesModel()
    ..id_address = json['id_address'] as String
    ..user = json['user'] as String
    ..name = json['name'] as String
    ..address = json['address'] as String
    ..district = json['district'] == null
        ? null
        : DistrictModel.fromJson(json['district'] as Map<String, dynamic>)
    ..city = json['city'] == null
        ? null
        : CityModel.fromJson(json['city'] as Map<String, dynamic>)
    ..province = json['province'] == null
        ? null
        : ProvinceModel.fromJson(json['province'] as Map<String, dynamic>)
    ..phone = json['phone'] as String
    ..latitude = json['latitude'] as String
    ..longitude = json['longitude'] as String
    ..created_at = json['created_at'] as String;
}

Map<String, dynamic> _$AddressesModelToJson(AddressesModel instance) =>
    <String, dynamic>{
      'id_address': instance.id_address,
      'user': instance.user,
      'name': instance.name,
      'address': instance.address,
      'district': instance.district,
      'city': instance.city,
      'province': instance.province,
      'phone': instance.phone,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'created_at': instance.created_at,
    };
