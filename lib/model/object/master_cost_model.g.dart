// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'master_cost_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MasterCostModel _$MasterCostModelFromJson(Map<String, dynamic> json) {
  return MasterCostModel()
    ..service = json['service'] as String
    ..description = json['description'] as String
    ..cost = (json['cost'] as List)
        ?.map((e) =>
            e == null ? null : CostModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$MasterCostModelToJson(MasterCostModel instance) =>
    <String, dynamic>{
      'service': instance.service,
      'description': instance.description,
      'cost': instance.cost,
    };
