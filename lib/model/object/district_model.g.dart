// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'district_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DistrictModel _$DistrictModelFromJson(Map<String, dynamic> json) {
  return DistrictModel()
    ..id_district = json['id_district'] as String
    ..name = json['name'] as String
    ..id_province = json['id_province'] as String
    ..province = json['province'] as String
    ..id_cities = json['id_cities'] as String
    ..city = json['city'] as String
    ..type = json['type'] as String;
}

Map<String, dynamic> _$DistrictModelToJson(DistrictModel instance) =>
    <String, dynamic>{
      'id_district': instance.id_district,
      'name': instance.name,
      'id_province': instance.id_province,
      'province': instance.province,
      'id_cities': instance.id_cities,
      'city': instance.city,
      'type': instance.type,
    };
