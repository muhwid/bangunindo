// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel()
    ..id_user = json['id_user'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..email = json['email'] as String
    ..status = json['status'] as String
    ..created_at = json['created_at'] as String
    ..balance = json['balance'] as int
    ..store = json['store'] == null
        ? null
        : StoreModel.fromJson(json['store'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id_user': instance.id_user,
      'name': instance.name,
      'phone': instance.phone,
      'email': instance.email,
      'status': instance.status,
      'created_at': instance.created_at,
      'balance': instance.balance,
      'store': instance.store,
    };
