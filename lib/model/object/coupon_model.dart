
import 'package:json_annotation/json_annotation.dart';

part 'coupon_model.g.dart';

@JsonSerializable()
class CouponModel {

  String id_coupon;
  String name;
  String description;
  String code;
  String discount;
  String image;
  String expired;
  String created_at;

  CouponModel();

  factory CouponModel.fromJson(Map<String, dynamic> json) =>
      _$CouponModelFromJson(json);

  Map<String, dynamic> toJson() => _$CouponModelToJson(this);
}
