// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_shipping_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataShippingModel _$DataShippingModelFromJson(Map<String, dynamic> json) {
  return DataShippingModel()
    ..code = json['code'] as String
    ..name = json['name'] as String
    ..costs = (json['costs'] as List)
        ?.map((e) => e == null
            ? null
            : MasterCostModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$DataShippingModelToJson(DataShippingModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'costs': instance.costs,
    };
