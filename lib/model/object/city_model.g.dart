// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CityModel _$CityModelFromJson(Map<String, dynamic> json) {
  return CityModel()
    ..id_cities = json['id_cities'] as String
    ..city_name = json['city_name'] as String
    ..id_province = json['id_province'] as String
    ..province = json['province'] as String
    ..type = json['type'] as String
    ..postal_code = json['postal_code'] as String;
}

Map<String, dynamic> _$CityModelToJson(CityModel instance) => <String, dynamic>{
      'id_cities': instance.id_cities,
      'city_name': instance.city_name,
      'id_province': instance.id_province,
      'province': instance.province,
      'type': instance.type,
      'postal_code': instance.postal_code,
    };
