import 'package:json_annotation/json_annotation.dart';

part 'product_cart_model.g.dart';

@JsonSerializable()
class ProductCartModel {
  String product;
  String qty;
  String store;
  String price;

  ProductCartModel();

  ProductCartModel.insert(this.product, this.store, this.qty, this.price);

  factory ProductCartModel.fromJson(Map<String, dynamic> json) =>
      _$ProductCartModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductCartModelToJson(this);
}
