// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationModel _$NotificationModelFromJson(Map<String, dynamic> json) {
  return NotificationModel()
    ..id_notification = json['id_notification'] as String
    ..text = json['text'] as String
    ..created_at = json['created_at'] as String
    ..transaction = json['transaction'] == null
        ? null
        : DataListTransaction.fromJson(
            json['transaction'] as Map<String, dynamic>);
}

Map<String, dynamic> _$NotificationModelToJson(NotificationModel instance) =>
    <String, dynamic>{
      'id_notification': instance.id_notification,
      'text': instance.text,
      'created_at': instance.created_at,
      'transaction': instance.transaction,
    };
