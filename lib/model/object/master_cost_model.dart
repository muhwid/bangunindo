
import 'package:json_annotation/json_annotation.dart';

import 'cost_model.dart';

part 'master_cost_model.g.dart';

@JsonSerializable()
class MasterCostModel {
  String service;
  String description;
  List<CostModel> cost;

  MasterCostModel();

  factory MasterCostModel.fromJson(Map<String, dynamic> json) =>
      _$MasterCostModelFromJson(json);

  Map<String, dynamic> toJson() => _$MasterCostModelToJson(this);
}
