import 'package:json_annotation/json_annotation.dart';

part 'product_model.g.dart';

@JsonSerializable()
class ProductModel {
  ProductModel({
    this.id_product,
    this.name,
    this.description,
    this.store,
    this.brand,
    this.category,
    this.price,
    this.stock,
    this.min_grosir,
    this.grosir,
    this.weight,
    this.expired,
    this.images,
    this.video,
    this.created_at,
    this.review,
  });

  String id_product;
  String name;
  String description;
  String store;
  String brand;
  String category;
  String price;
  String stock;
  String min_grosir;
  String grosir;
  String weight;
  String expired;
  List<String> images;
  String video;
  DateTime created_at;
  String review;

  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}
