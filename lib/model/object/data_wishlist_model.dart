import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'data_wishlist_model.g.dart';

@JsonSerializable()
class DataWishlistModel {
  String id_favorite;
  String user;
  ProductModel product;
  String created_at;

  DataWishlistModel();

  factory DataWishlistModel.fromJson(Map<String, dynamic> json) =>
      _$DataWishlistModelFromJson(json);

  Map<String, dynamic> toJson() => _$DataWishlistModelToJson(this);
}
