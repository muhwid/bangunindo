
import 'package:e_commerce_app/model/object/store_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {

  String id_user;
  String name;
  String phone;
  String email;
  String status;
  String created_at;
  int balance;
  StoreModel store;
  UserModel();

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
