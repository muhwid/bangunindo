// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentModel _$PaymentModelFromJson(Map<String, dynamic> json) {
  return PaymentModel()
    ..bank_code = json['bank_code'] as String
    ..collection_type = json['collection_type'] as String
    ..bank_account_number = json['bank_account_number'] as String
    ..transfer_amount = json['transfer_amount'] as int
    ..bank_branch = json['bank_branch'] as String
    ..account_holder_name = json['account_holder_name'] as String;
}

Map<String, dynamic> _$PaymentModelToJson(PaymentModel instance) =>
    <String, dynamic>{
      'bank_code': instance.bank_code,
      'collection_type': instance.collection_type,
      'bank_account_number': instance.bank_account_number,
      'transfer_amount': instance.transfer_amount,
      'bank_branch': instance.bank_branch,
      'account_holder_name': instance.account_holder_name,
    };
