// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_cart_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductCartModel _$ProductCartModelFromJson(Map<String, dynamic> json) {
  return ProductCartModel()
    ..product = json['product'] as String
    ..qty = json['qty'] as String
    ..store = json['store'] as String
    ..price = json['price'] as String;
}

Map<String, dynamic> _$ProductCartModelToJson(ProductCartModel instance) =>
    <String, dynamic>{
      'product': instance.product,
      'qty': instance.qty,
      'store': instance.store,
      'price': instance.price,
    };
