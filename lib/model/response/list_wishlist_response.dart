import 'package:e_commerce_app/model/object/data_wishlist_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'list_wishlist_response.g.dart';

@JsonSerializable()
class ListWishlistResponse extends BaseResponse {
  List<DataWishlistModel> data = List();

  ListWishlistResponse();

  factory ListWishlistResponse.fromJson(Map<String, dynamic> json) =>
      _$ListWishlistResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ListWishlistResponseToJson(this);
}
