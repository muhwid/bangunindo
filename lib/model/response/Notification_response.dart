
import 'package:e_commerce_app/model/object/notification_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'Notification_response.g.dart';

@JsonSerializable()
class NotificationResponse extends BaseResponse {

  List<NotificationModel> data = List();

  NotificationResponse();

  factory NotificationResponse.fromJson(Map<String, dynamic> json) =>
      _$NotificationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationResponseToJson(this);
}
