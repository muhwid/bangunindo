import 'package:e_commerce_app/model/object/payment_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';
import 'list_order_response.dart';

part 'order_response.g.dart';

@JsonSerializable()
class OrderResponse extends BaseResponse {
  String expired;
  String url;
  PaymentModel payment;
  List<DetailInstruction> instruction;

  OrderResponse();

  factory OrderResponse.fromJson(Map<String, dynamic> json) =>
      _$OrderResponseFromJson(json);
  Map<String, dynamic> toJson() => _$OrderResponseToJson(this);
}
