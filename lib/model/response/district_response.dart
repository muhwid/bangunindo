import 'package:e_commerce_app/model/object/district_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'district_response.g.dart';

@JsonSerializable()
class DistrictResponse extends BaseResponse {
  List<DistrictModel> data = List();

  DistrictResponse();

  factory DistrictResponse.fromJson(Map<String, dynamic> json) =>
      _$DistrictResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictResponseToJson(this);
}
