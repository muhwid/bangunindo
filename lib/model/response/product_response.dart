
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'product_response.g.dart';

@JsonSerializable()
class ProductResponse extends BaseResponse {

  List<ProductModel> data = List();

  ProductResponse();

  factory ProductResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductResponseToJson(this);
}
