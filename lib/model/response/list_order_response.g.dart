// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_order_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListOrderResponse _$ListOrderResponseFromJson(Map<String, dynamic> json) {
  return ListOrderResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : DataListTransaction.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ListOrderResponseToJson(ListOrderResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };

DetailTransaction _$DetailTransactionFromJson(Map<String, dynamic> json) {
  return DetailTransaction()
    ..id_product = json['id_product'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..category = json['category'] as String
    ..price = json['price'] as String
    ..stock = json['stock'] as String
    ..min_grosir = json['min_grosir'] as String
    ..grosir = json['grosir'] as String
    ..weight = json['weight'] as String
    ..expired = json['expired'] as String
    ..images = (json['images'] as List)?.map((e) => e as String)?.toList()
    ..created_at = json['created_at'] as String
    ..qty = json['qty'] as String
    ..reason = json['reason'] as String;
}

Map<String, dynamic> _$DetailTransactionToJson(DetailTransaction instance) =>
    <String, dynamic>{
      'id_product': instance.id_product,
      'name': instance.name,
      'description': instance.description,
      'category': instance.category,
      'price': instance.price,
      'stock': instance.stock,
      'min_grosir': instance.min_grosir,
      'grosir': instance.grosir,
      'weight': instance.weight,
      'expired': instance.expired,
      'images': instance.images,
      'created_at': instance.created_at,
      'qty': instance.qty,
      'reason': instance.reason,
    };

DetailInstruction _$DetailInstructionFromJson(Map<String, dynamic> json) {
  return DetailInstruction()
    ..id_instruction = json['id_instruction'] as String
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..text = json['text'] as String;
}

Map<String, dynamic> _$DetailInstructionToJson(DetailInstruction instance) =>
    <String, dynamic>{
      'id_instruction': instance.id_instruction,
      'name': instance.name,
      'code': instance.code,
      'text': instance.text,
    };

DataListTransaction _$DataListTransactionFromJson(Map<String, dynamic> json) {
  return DataListTransaction()
    ..id_transaction = json['id_transaction'] as String
    ..user = json['user'] as String
    ..discount = json['discount'] as String
    ..subtotal = json['subtotal'] as String
    ..shipping = json['shipping'] as String
    ..shipping_photo = json['shipping_photo'] as String
    ..shipping_permit = json['shipping_permit'] as String
    ..shipping_number = json['shipping_number'] as String
    ..shipping_service = json['shipping_service'] as String
    ..total = json['total'] as String
    ..delivery = json['delivery'] as String
    ..address = json['address'] as String
    ..payment = json['payment'] as String
    ..status = json['status'] as String
    ..coupon = json['coupon'] as String
    ..courier = json['courier'] as String
    ..accepted = json['accepted'] as String
    ..created_at = json['created_at'] as String
    ..expired = json['expired'] as String
    ..status_text = json['status_text'] as String
    ..reason = json['reason'] as String
    ..bank_account_number = json['bank_account_number'] as String
    ..account_holder_name = json['account_holder_name'] as String
    ..instruction = (json['instruction'] as List)
        ?.map((e) => e == null
            ? null
            : DetailInstruction.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..detail = (json['detail'] as List)
        ?.map((e) => e == null
            ? null
            : DetailTransaction.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$DataListTransactionToJson(
        DataListTransaction instance) =>
    <String, dynamic>{
      'id_transaction': instance.id_transaction,
      'user': instance.user,
      'discount': instance.discount,
      'subtotal': instance.subtotal,
      'shipping': instance.shipping,
      'shipping_photo': instance.shipping_photo,
      'shipping_permit': instance.shipping_permit,
      'shipping_number': instance.shipping_number,
      'shipping_service': instance.shipping_service,
      'total': instance.total,
      'delivery': instance.delivery,
      'address': instance.address,
      'payment': instance.payment,
      'status': instance.status,
      'coupon': instance.coupon,
      'courier': instance.courier,
      'accepted': instance.accepted,
      'created_at': instance.created_at,
      'expired': instance.expired,
      'status_text': instance.status_text,
      'reason': instance.reason,
      'bank_account_number': instance.bank_account_number,
      'account_holder_name': instance.account_holder_name,
      'instruction': instance.instruction,
      'detail': instance.detail,
    };
