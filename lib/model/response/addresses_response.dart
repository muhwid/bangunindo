
import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'addresses_response.g.dart';

@JsonSerializable()
class AddressesResponse extends BaseResponse {

  List<AddressesModel> data = List();

  AddressesResponse();

  factory AddressesResponse.fromJson(Map<String, dynamic> json) =>
      _$AddressesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AddressesResponseToJson(this);
}
