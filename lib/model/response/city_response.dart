import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'city_response.g.dart';

@JsonSerializable()
class CityResponse extends BaseResponse {
  List<CityModel> data = List();

  CityResponse();

  factory CityResponse.fromJson(Map<String, dynamic> json) =>
      _$CityResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CityResponseToJson(this);
}
