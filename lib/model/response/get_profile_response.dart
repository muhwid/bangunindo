
import 'package:e_commerce_app/model/object/user_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'get_profile_response.g.dart';

@JsonSerializable()
class GetProfileResponse extends BaseResponse {

  UserModel data;

  GetProfileResponse();

  factory GetProfileResponse.fromJson(Map<String, dynamic> json) =>
      _$GetProfileResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetProfileResponseToJson(this);
}
