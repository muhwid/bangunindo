import 'package:e_commerce_app/model/object/data_shipping_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'shipping_response.g.dart';

@JsonSerializable()
class ShippingResponse extends BaseResponse {
  List<DataShippingModel> data = List();

  ShippingResponse();

  factory ShippingResponse.fromJson(Map<String, dynamic> json) =>
      _$ShippingResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ShippingResponseToJson(this);
}
