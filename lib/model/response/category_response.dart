
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'category_response.g.dart';

@JsonSerializable()
class CategoryResponse extends BaseResponse {

  List<CategoryModel> data = List();

  CategoryResponse();

  factory CategoryResponse.fromJson(Map<String, dynamic> json) =>
      _$CategoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryResponseToJson(this);
}
