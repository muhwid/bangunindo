
import 'package:e_commerce_app/model/object/shipping_service_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'shipping_service_response.g.dart';

@JsonSerializable()
class ShippingServiceResponse extends BaseResponse {

  List<ShippingServiceModel> data = List();

  ShippingServiceResponse();

  factory ShippingServiceResponse.fromJson(Map<String, dynamic> json) =>
      _$ShippingServiceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ShippingServiceResponseToJson(this);
}
