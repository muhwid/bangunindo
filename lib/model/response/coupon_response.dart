import 'package:e_commerce_app/model/object/coupon_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'coupon_response.g.dart';

@JsonSerializable()
class CouponResponse extends BaseResponse {
  List<CouponModel> data = List();

  CouponResponse();

  factory CouponResponse.fromJson(Map<String, dynamic> json) =>
      _$CouponResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CouponResponseToJson(this);
}
