// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_product_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailProductResponse _$DetailProductResponseFromJson(
    Map<String, dynamic> json) {
  return DetailProductResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = json['data'] == null
        ? null
        : DetailProductModel.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DetailProductResponseToJson(
        DetailProductResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
