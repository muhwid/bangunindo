// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_order_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailOrderResponse _$DetailOrderResponseFromJson(Map<String, dynamic> json) {
  return DetailOrderResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = json['data'] == null
        ? null
        : DataDetailOrderModel.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DetailOrderResponseToJson(
        DetailOrderResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
