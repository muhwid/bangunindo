import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'list_order_response.g.dart';

@JsonSerializable()
class ListOrderResponse extends BaseResponse {
  List<DataListTransaction> data;

  ListOrderResponse();

  factory ListOrderResponse.fromJson(Map<String, dynamic> json) =>
      _$ListOrderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ListOrderResponseToJson(this);
}

@JsonSerializable()
class DetailTransaction {
  String id_product;
  String name;
  String description;
  String category;
  String price;
  String stock;
  String min_grosir;
  String grosir;
  String weight;
  String expired;
  List<String> images;
  String created_at;
  String qty;
  String reason;

  DetailTransaction();

  factory DetailTransaction.fromJson(Map<String, dynamic> json) =>
      _$DetailTransactionFromJson(json);

  Map<String, dynamic> toJson() => _$DetailTransactionToJson(this);
}
@JsonSerializable()
class DetailInstruction {
  String id_instruction;
  String name;
  String code;
  String text;

  DetailInstruction();

  factory DetailInstruction.fromJson(Map<String, dynamic> json) =>
      _$DetailInstructionFromJson(json);

  Map<String, dynamic> toJson() => _$DetailInstructionToJson(this);
}
@JsonSerializable()
class DataListTransaction {
  String id_transaction;
  String user;
  String discount;
  String subtotal;
  String shipping;
  String shipping_photo;
  String shipping_permit;
  String shipping_number;
  String shipping_service;
  String total;
  String delivery;
  String address;
  String payment;
  String status;
  String coupon;
  String courier;
  String accepted;
  String created_at;
  String expired;
  String status_text;
  String reason;
  String bank_account_number;
  String account_holder_name;
  List<DetailInstruction> instruction;
  List<DetailTransaction> detail;

  DataListTransaction();

  factory DataListTransaction.fromJson(Map<String, dynamic> json) =>
      _$DataListTransactionFromJson(json);

  Map<String, dynamic> toJson() => _$DataListTransactionToJson(this);
}
