import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'province_response.g.dart';

@JsonSerializable()
class ProvinceResponse extends BaseResponse {
  List<ProvinceModel> data = List();

  ProvinceResponse();

  factory ProvinceResponse.fromJson(Map<String, dynamic> json) =>
      _$ProvinceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProvinceResponseToJson(this);
}
