// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return LoginResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = json['data'] == null
        ? null
        : DataLogin.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };

DataLogin _$DataLoginFromJson(Map<String, dynamic> json) {
  return DataLogin()
    ..token = json['token'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..id = json['id'] as String;
}

Map<String, dynamic> _$DataLoginToJson(DataLogin instance) => <String, dynamic>{
      'token': instance.token,
      'name': instance.name,
      'phone': instance.phone,
      'id': instance.id,
    };
