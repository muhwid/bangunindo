// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'balance_history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BalanceHistoryResponse _$BalanceHistoryResponseFromJson(
    Map<String, dynamic> json) {
  return BalanceHistoryResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) =>
            e == null ? null : BalanceModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$BalanceHistoryResponseToJson(
        BalanceHistoryResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
