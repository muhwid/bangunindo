// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipping_service_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShippingServiceResponse _$ShippingServiceResponseFromJson(
    Map<String, dynamic> json) {
  return ShippingServiceResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ShippingServiceModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ShippingServiceResponseToJson(
        ShippingServiceResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
