import 'package:e_commerce_app/model/object/data_detail_va_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'detail_va_response.g.dart';

@JsonSerializable()
class DetailVaResponse extends BaseResponse {
  DataDetailVaModel data;

  DetailVaResponse();

  factory DetailVaResponse.fromJson(Map<String, dynamic> json) =>
      _$DetailVaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DetailVaResponseToJson(this);
}
