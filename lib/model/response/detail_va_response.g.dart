// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_va_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailVaResponse _$DetailVaResponseFromJson(Map<String, dynamic> json) {
  return DetailVaResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = json['data'] == null
        ? null
        : DataDetailVaModel.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DetailVaResponseToJson(DetailVaResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
