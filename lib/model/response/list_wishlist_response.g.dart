// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_wishlist_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListWishlistResponse _$ListWishlistResponseFromJson(Map<String, dynamic> json) {
  return ListWishlistResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : DataWishlistModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ListWishlistResponseToJson(
        ListWishlistResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
