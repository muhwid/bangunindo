// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderResponse _$OrderResponseFromJson(Map<String, dynamic> json) {
  return OrderResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..expired = json['expired'] as String
    ..url = json['url'] as String
    ..payment = json['payment'] == null
        ? null
        : PaymentModel.fromJson(json['payment'] as Map<String, dynamic>)
    ..instruction = (json['instruction'] as List)
        ?.map((e) => e == null
            ? null
            : DetailInstruction.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$OrderResponseToJson(OrderResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'expired': instance.expired,
      'url': instance.url,
      'payment': instance.payment,
      'instruction': instance.instruction,
    };
