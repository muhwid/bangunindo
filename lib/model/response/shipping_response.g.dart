// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipping_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShippingResponse _$ShippingResponseFromJson(Map<String, dynamic> json) {
  return ShippingResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : DataShippingModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ShippingResponseToJson(ShippingResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
