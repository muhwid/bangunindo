// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_bank_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListBankResponse _$ListBankResponseFromJson(Map<String, dynamic> json) {
  return ListBankResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) =>
            e == null ? null : BankModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ListBankResponseToJson(ListBankResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
