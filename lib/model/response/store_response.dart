import 'package:e_commerce_app/model/object/store_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'store_response.g.dart';

@JsonSerializable()
class StoreResponse extends BaseResponse {
  StoreModel data;

  StoreResponse();

  factory StoreResponse.fromJson(Map<String, dynamic> json) =>
      _$StoreResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StoreResponseToJson(this);
}
