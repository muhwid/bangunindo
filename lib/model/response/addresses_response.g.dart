// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'addresses_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressesResponse _$AddressesResponseFromJson(Map<String, dynamic> json) {
  return AddressesResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : AddressesModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$AddressesResponseToJson(AddressesResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
