import 'package:e_commerce_app/model/object/va_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'va_response.g.dart';

@JsonSerializable()
class VaResponse extends BaseResponse {
  List<VaModel> data = List();

  VaResponse();

  factory VaResponse.fromJson(Map<String, dynamic> json) =>
      _$VaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$VaResponseToJson(this);
}
