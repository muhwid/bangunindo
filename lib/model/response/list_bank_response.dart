
import 'package:e_commerce_app/model/object/bank_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'list_bank_response.g.dart';

@JsonSerializable()
class ListBankResponse extends BaseResponse {

  List<BankModel> data = List();

  ListBankResponse();

  factory ListBankResponse.fromJson(Map<String, dynamic> json) =>
      _$ListBankResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ListBankResponseToJson(this);
}
