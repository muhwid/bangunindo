import 'package:e_commerce_app/model/object/data_detail_order_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'detail_order_response.g.dart';

@JsonSerializable()
class DetailOrderResponse extends BaseResponse {
  DataDetailOrderModel data;

  DetailOrderResponse();

  factory DetailOrderResponse.fromJson(Map<String, dynamic> json) =>
      _$DetailOrderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DetailOrderResponseToJson(this);
}
