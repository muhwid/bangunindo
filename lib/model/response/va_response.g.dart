// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'va_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VaResponse _$VaResponseFromJson(Map<String, dynamic> json) {
  return VaResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..invalid = json['invalid'] as String
    ..data = (json['data'] as List)
        ?.map((e) =>
            e == null ? null : VaModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$VaResponseToJson(VaResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'invalid': instance.invalid,
      'data': instance.data,
    };
