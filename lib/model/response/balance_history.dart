import 'package:e_commerce_app/model/object/balance_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'balance_history.g.dart';

@JsonSerializable()
class BalanceHistoryResponse extends BaseResponse {
  List<BalanceModel> data = List();

  BalanceHistoryResponse();

  factory BalanceHistoryResponse.fromJson(Map<String, dynamic> json) =>
      _$BalanceHistoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BalanceHistoryResponseToJson(this);
}
