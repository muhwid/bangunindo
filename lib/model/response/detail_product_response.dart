import 'package:e_commerce_app/model/object/detail_product_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'detail_product_response.g.dart';

@JsonSerializable()
class DetailProductResponse extends BaseResponse {
  DetailProductModel data;

  DetailProductResponse();

  factory DetailProductResponse.fromJson(Map<String, dynamic> json) =>
      _$DetailProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductResponseToJson(this);
}
