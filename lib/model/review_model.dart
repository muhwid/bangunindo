class ReviewModel{
  String image;
  String name;
  String time;
  String date;
  double rate;
  String review;

  ReviewModel(
      this.image, this.name, this.time, this.date, this.rate, this.review);
}