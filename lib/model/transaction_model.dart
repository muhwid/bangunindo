class TransactionModel{
  String image;
  String status;
  String name;
  String date;
  double price;

  TransactionModel(this.image, this.status, this.name, this.date, this.price);
}