class ImageModel{
  String name;
  String path;
  String base64;

  ImageModel({this.name, this.path, this.base64});
}