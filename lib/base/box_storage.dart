import 'package:get_storage/get_storage.dart';

class BoxStorage {
  final box = GetStorage();

  void setFirst(bool value) {
    box.write("first", value);
  }

  bool getFirst() {
    return box.read("first");
  }

  void setLogin(bool value) {
    box.write("login", value);
  }

  bool getLogin() {
    return box.read("login");
  }

  void setToken(String value) {
    box.write("token", value);
  }

  String getToken() {
    return box.read("token");
  }

  void setName(String value) {
    box.write("name", value);
  }

  String getName() {
    return box.read("name");
  }

  void setId(String value) {
    box.write("id", value);
  }

  String getId() {
    return box.read("id");
  }
}
