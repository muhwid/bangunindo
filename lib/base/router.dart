import 'package:e_commerce_app/screens/intro/intro_page.dart';
import 'package:e_commerce_app/screens/login/login_page.dart';
import 'package:e_commerce_app/screens/main/main_page.dart';
import 'package:e_commerce_app/screens/new_password/new_password_page.dart';
import 'package:e_commerce_app/screens/splash/splash_screen.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

class Router {
  static final route = [
    GetPage(
      name: '/splashPage',
      page: () => SplashPage(),
    ),
    GetPage(
      name: '/loginPage',
      page: () => LoginPage(),
    ),
    GetPage(
      name: '/mainPage',
      page: () => MainPage(),
    ),
    GetPage(
      name: '/introPage',
      page: () => IntroPage(),
    ),
    GetPage(
      name: '/newPassPage',
      page: () => NewPasswordPage(),
    ),
  ];
}