import 'package:e_commerce_app/res/theme.dart';
import 'package:flutter/material.dart';

class Indicator extends StatelessWidget {
  final int positionIndex, currentIndex;
  const Indicator({this.currentIndex, this.positionIndex});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 12,
      width: 12,
      decoration: BoxDecoration(
          border: Border.all(color: primaryColor),
          color: positionIndex == currentIndex
              ? primaryColor
              : Colors.transparent,
          borderRadius: BorderRadius.circular(100)),
    );
  }
}