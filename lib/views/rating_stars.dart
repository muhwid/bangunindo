import 'package:e_commerce_app/res/theme.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class RatingStars extends StatelessWidget {
  final double rate;

  RatingStars(this.rate);

  @override
  Widget build(BuildContext context) {
    int numberOfStars = rate.round();

    return Row(
        children: List<Widget>.generate(
                5,
                (index) => Icon(
                      (index < numberOfStars)
                          ? MdiIcons.star
                          : MdiIcons.starOutline,
                      size: 16,
                      color: primaryColor,
                    )) +
            [
              SizedBox(width: 4),
              Text(
                rate.toString(),
                style: normalTextFont.copyWith(fontSize: 12),
              )
            ]);
  }
}
