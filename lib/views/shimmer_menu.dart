import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/shimmer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget shimmerMenu(){
  return shimmering(
    Expanded(
      flex: 1,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Row(
            children: [
              Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setHeight(80),
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                color: Colors.grey,
              ),
              Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setHeight(80),
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                color: Colors.grey,
              ),
              Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setHeight(80),
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                color: Colors.grey,
              ),
              Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setHeight(80),
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
                color: Colors.grey,
              ),
            ],
          )
        ],
      ),
    )
  );
}