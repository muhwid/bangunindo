import 'package:e_commerce_app/res/theme.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget shimmering(Widget child){
  return Shimmer.fromColors(child: child, baseColor: greyMenu, highlightColor: Colors.white);
}