import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/shimmer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget shimmerProduct(BuildContext context){
  return shimmering(
    Column(
      children: [
        SizedBox(
          height: ScreenUtil().setHeight(25),
        ),
        Row(
          children: [
            Container(
              width: ScreenUtil().setWidth(40),
              height: ScreenUtil().setHeight(40),
              margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
              color: Colors.grey,
            ),
            Expanded(child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width - ScreenUtil().setWidth(55),
                  height: ScreenUtil().setHeight(15),
                  color: Colors.grey,
                ),
                SizedBox(height: ScreenUtil().setHeight(10),),
                Container(
                  width: MediaQuery.of(context).size.width - ScreenUtil().setWidth(55),
                  height: ScreenUtil().setHeight(15),
                  color: Colors.grey,
                )
              ],
            ), flex: 1,)
          ],
        ),
        SizedBox(
          height: ScreenUtil().setHeight(25),
        ),
        Row(
          children: [
            Container(
              width: ScreenUtil().setWidth(40),
              height: ScreenUtil().setHeight(40),
              margin: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
              color: Colors.grey,
            ),
            Expanded(child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width - ScreenUtil().setWidth(55),
                  height: ScreenUtil().setHeight(15),
                  color: Colors.grey,
                ),
                SizedBox(height: ScreenUtil().setHeight(10),),
                Container(
                  width: MediaQuery.of(context).size.width - ScreenUtil().setWidth(55),
                  height: ScreenUtil().setHeight(15),
                  color: Colors.grey,
                )
              ],
            ), flex: 1,)
          ],
        ),
      ],
    ),
  );
}