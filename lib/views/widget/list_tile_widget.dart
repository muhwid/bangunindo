import 'package:e_commerce_app/res/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ListTileWidget extends StatelessWidget {
  final String text;
  final IconData icon;
  final VoidCallback onClicked;

  const ListTileWidget(
      {@required this.text, @required this.icon, @required this.onClicked});

  @override
  Widget build(BuildContext context) => ListTile(
        contentPadding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16),
          vertical: ScreenUtil().setHeight(8),
        ),
        title: Text(
          text,
          style: boldTextFont,
        ),
        leading: Icon(
          icon,
          size: ScreenUtil().setHeight(28),
          color: Colors.black,
        ),
        onTap: onClicked,
      );
}
