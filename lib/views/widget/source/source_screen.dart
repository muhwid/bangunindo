import 'package:e_commerce_app/views/widget/gallery_button_widget.dart';
import 'package:flutter/material.dart';

import '../camera_button_widget.dart';

class SourceScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: [CameraButtonWidget(), GalleryButtonWidget()]),
    );
  }
}
