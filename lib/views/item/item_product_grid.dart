import 'package:e_commerce_app/data/database_helper.dart';
import 'package:e_commerce_app/helper/flushbar_message.dart';
import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:e_commerce_app/model/object/product_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget itemProduct(ProductModel data, BuildContext context, Function onClick) {
  return InkWell(
    onTap: () {
      onClick();
    },
    child: Card(
      child: Container(
        padding: EdgeInsets.all(ScreenUtil().setSp(10)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: ScreenUtil().setHeight(100),
              width: MediaQuery.of(context).size.width,
              child: FadeInImage.assetNetwork(
                placeholder: "assets/images/img_placeholder.png",
                image: (data.images.length > 0)
                    ? data.images[0]
                    : "assets/images/img_placeholder.png",
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Text(
              "${data.name}",
              maxLines: 2,
              style: normalTextFont.copyWith(
                  fontSize: ScreenUtil().setSp(10), color: Color(0xff979797)),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Row(
              children: [
                Text(
                    "Rp.${GlobalHelper().formattingNumber(int.parse(data.price))}",
                    style: boldTextFont.copyWith(
                        color: primaryColor, fontSize: ScreenUtil().setSp(10))),
                SizedBox(
                  width: ScreenUtil().setWidth(5),
                ),
                Visibility(
                  child: Text(
                    "Rp.${GlobalHelper().formattingNumber(int.parse(data.price))}",
                    style: boldTextFont.copyWith(
                        color: semiTransRed, fontSize: ScreenUtil().setSp(10)),
                  ),
                  visible: false,
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            Row(
              children: [
                Wrap(
                  children: [
                    Container(
                        width: ScreenUtil().setWidth(50),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(color: Colors.black),
                          color: Color(0xffEEBD57),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.star,
                              size: ScreenUtil().setSp(12),
                            ),
                            Text(
                              "${data.review}",
                              style: boldTextFont.copyWith(
                                  fontSize: ScreenUtil().setSp(10)),
                            )
                          ],
                        ))
                  ],
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(
                        left: ScreenUtil().setWidth(10),
                        right: ScreenUtil().setWidth(10)),
                    child: WidgetComponent.shopButton(
                        context: context,
                        onPressed: () {
                          DatabaseHelper.instance.getSpecificDataStore(data.store).then((value) {
                          if (value.isNotEmpty && ("${data.store}" != "${value.first["id_store"]}")) {
                              GlobalHelper().showSnackBar("${getTranslated(context, "cannot_make_purchase")}", context);
                            } else {
                              DatabaseHelper.instance
                                  .getSpecificData(data.id_product)
                                  .then((value) {
                                if (value.isNotEmpty) {
                                  int currentQty = value[0]["qty"];
                                  DatabaseHelper.instance.updateQty(
                                      CartDbModel.insert(
                                          data.id_product,
                                          data.store,
                                          data.name,
                                          data.description,
                                          data.category,
                                          (data.brand == null)
                                              ? ""
                                              : data.brand,
                                          data.price,
                                          data.stock,
                                          data.min_grosir,
                                          data.grosir,
                                          data.weight,
                                          data.expired,
                                          data.created_at.toIso8601String(),
                                          (data.images.length > 0)
                                              ? data.images[0]
                                              : "",
                                          data.review,
                                          currentQty + 1));
                                } else {
                                  var i = DatabaseHelper.instance.insert(
                                      CartDbModel.insert(
                                          data.id_product,
                                          data.store,
                                          data.name,
                                          data.description,
                                          data.category,
                                          (data.brand == null)
                                              ? ""
                                              : data.brand,
                                          data.price,
                                          data.stock,
                                          data.min_grosir,
                                          data.grosir,
                                          data.weight,
                                          data.expired,
                                          data.created_at.toIso8601String(),
                                          (data.images.length > 0)
                                              ? data.images[0]
                                              : "",
                                          data.review,
                                          1));
                                }
                                FlushBarMessage().toast(
                                    context,
                                    "${getTranslated(context, "success")}",
                                    "${getTranslated(context, "success")} Add ${data.name} to cart",
                                    Colors.green,
                                    2);
                              });
                            }
                          });
                        }),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    ),
  );
}
