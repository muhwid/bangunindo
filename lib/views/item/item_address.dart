import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget itemAddress(AddressesModel data, Function onClick) {
  return InkWell(
    onTap: (){
      onClick();
    },
    child: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("${data.name}", style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12), color: Colors.black),),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Text("${data.phone}", style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(10), color: Color(0xff979797)),),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Text("${data.address}", style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(10), color: Colors.black),),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Divider(),
          SizedBox(
            height: ScreenUtil().setHeight(35),
          )
        ],
      ),
    ),
  );
}