import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

Widget itemRegion(dynamic data, Function onClick) {
  return Container(
    width: Get.width,
    margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
    child: InkWell(
      onTap: () {
        onClick();
      },
      child: Text(
        (data is ProvinceModel)
            ? "${data.province}"
            : (data is CityModel)
                ? "${data.type} ${data.city_name}"
                : "${data.name}",
        style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
      ),
    ),
  );
}
