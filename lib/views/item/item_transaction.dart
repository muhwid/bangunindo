import 'package:e_commerce_app/helper/global_helper.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/screens/transaction/detail_transaction/detail_transaction.dart';
import 'package:e_commerce_app/utils/language/localization/language_constant.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

itemTransaction(DataListTransaction item, BuildContext context){
  return InkWell(
    onTap: () async {
     var result = await Navigator.of(context).push(MaterialPageRoute(
            builder: (ctx) => DetailTransactionPage(item:item,store:false)
          ));
    },
    child: Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(10)), color: Colors.black),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/img_placeholder.png",
                    image: (item.detail != null) ? (item.detail[0].images.length > 0) ? item.detail[0].images[0] : "" : "",
                    fit: BoxFit.contain,
                  ),
                ),
                flex: 2,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Card(
                      color: greyMenu,
                      child: Container(
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(15), right:  ScreenUtil().setWidth(15), top:  ScreenUtil().setWidth(2), bottom:  ScreenUtil().setWidth(2)),
                        child: Text(
                          "${item.status_text}",
                          style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10), color: green),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    Text(
                      (item.detail != null) ? item.detail[0].name : "${getTranslated(context, "product_has_deleted")}",
                      style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    Text(
                      "${GlobalHelper().convertDateFromString(item.created_at)}",
                      style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10), color: subtitle),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    Text(
                      "Rp. ${GlobalHelper().formattingNumber(int.parse(item.total))}",
                      style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10), color: primaryColor),
                    ),
                  ],
                ),
                flex: 6,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 15,
              )
            ],
          ),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Divider(),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
        ],
      ),
    ),
  );
}