import 'package:e_commerce_app/model/object/addresses_model.dart';
import 'package:e_commerce_app/model/object/category_model.dart';
import 'package:e_commerce_app/model/object/city_model.dart';
import 'package:e_commerce_app/model/object/province_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

Widget itemCategoryHorizontal(CategoryModel data, Function onClick){
  return Container(
    margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
    child: InkWell(
      onTap: (){
        onClick();
      },
      child: Row(
        children: [
          Container(
            height: ScreenUtil().setHeight(15),
            width: ScreenUtil().setHeight(15),
            child: FadeInImage.assetNetwork(
              placeholder: "assets/images/img_placeholder.png",
              image: data.image,
              fit: BoxFit.contain,
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          Text(
            data.name,
            style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(14)),
          )
        ],
      ),
    ),
  );
}