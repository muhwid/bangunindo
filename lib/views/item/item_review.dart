import 'package:e_commerce_app/model/object/comment_model.dart';
import 'package:e_commerce_app/res/theme.dart';
import 'package:e_commerce_app/utils/module.dart';
import 'package:e_commerce_app/views/widget/video_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget itemReviews(CommentModel data) {
  return Container(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.user.name,
                      style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(12)),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(2),
                    ),
                    Text(
                      data.created_at,
                      style:
                      normalTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),
                    ),
                     SizedBox(
                      height: ScreenUtil().setHeight(2),
                    ),
                    Wrap(
                      children: [
                        Container(
                            margin: EdgeInsets.only(top:1),
                            width: ScreenUtil().setWidth(50),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.black),
                              color: Color(0xffEEBD57),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(Icons.star, size: ScreenUtil().setSp(12),),
                                Text(data.rate, style: boldTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),)
                              ],
                            ))
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                  ],
                )),
          ],
        ),
        SizedBox(
          height: ScreenUtil().setHeight(10),
        ),
        Text(data.review, style: normalTextFont.copyWith(fontSize: ScreenUtil().setSp(10)),),
        SizedBox(
          height: ScreenUtil().setHeight(10),
        ),
        Row(
          children: [
            data.photo == null || data.photo == "" ? Container() : Container(
              child: Image.network(data.photo, fit: BoxFit.cover,),
              width: ScreenUtil().setWidth(30),height: ScreenUtil().setHeight(30),
            ),
            SizedBox(width: ScreenUtil().setWidth(5),),
            data.video == null || data.video == "" ? Container() : Container(
              child: VideoWidget(url: data.video,),
              width: ScreenUtil().setWidth(30),height: ScreenUtil().setHeight(30),
            )
          ],
        ),
        Divider(),
        SizedBox(
          height: ScreenUtil().setHeight(35),
        )
      ],
    ),
  );
}