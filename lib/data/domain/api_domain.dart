import 'package:e_commerce_app/data/repository/api_repository.dart';
import 'package:e_commerce_app/model/body/add_address_body.dart';
import 'package:e_commerce_app/model/body/edit_profile_body.dart';
import 'package:e_commerce_app/model/body/order_body.dart';
import 'package:e_commerce_app/model/body/otp_body.dart';
import 'package:e_commerce_app/model/body/register_body.dart';
import 'package:e_commerce_app/model/body/register_store_body.dart';
import 'package:e_commerce_app/model/body/update_password_body.dart';
import 'package:e_commerce_app/model/body/wishlist_body.dart';
import 'package:e_commerce_app/model/response/addresses_response.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:e_commerce_app/model/response/city_response.dart';
import 'package:e_commerce_app/model/response/coupon_response.dart';
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:e_commerce_app/model/response/detail_va_response.dart';
import 'package:e_commerce_app/model/response/district_response.dart';
import 'package:e_commerce_app/model/response/get_profile_response.dart';
import 'package:e_commerce_app/model/response/list_bank_response.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/response/list_wishlist_response.dart';
import 'package:e_commerce_app/model/response/order_response.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:e_commerce_app/model/response/province_response.dart';
import 'package:e_commerce_app/model/response/shipping_response.dart';
import 'package:e_commerce_app/model/response/va_response.dart';

class ApiDomain {
  final ApiRepository repository;

  ApiDomain(this.repository);

  Future<BaseResponse> reqRegister(RegisterBody body) {
    return repository.reqRegister(body);
  }

  Future<BaseResponse> reqStoreRegister(RegisterStoreBody body) {
    return repository.reqStoreRegister(body);
  }

  Future<BaseResponse> reqForgotPass(String phoneNumber) {
    return repository.reqForgotPass(phoneNumber);
  }

  Future<GetProfileResponse> reqGetProfile() {
    return repository.reqGetProfile();
  }

  Future<ProductResponse> reqSearchProduct(String q) {
    return repository.reqSearchProduct(q);
  }

  Future<CategoryResponse> reqCategory() {
    return repository.reqCategory();
  }

  Future<DetailProductResponse> reqDetailProduct(String id) {
    return repository.reqDetailProduct(id);
  }

  Future<ProductResponse> reqTrendingItem() {
    return repository.reqTrendingItem();
  }

  Future<CouponResponse> reqCoupon() {
    return repository.reqCoupon();
  }

  Future<ProductResponse> reqPromotedItem() {
    return repository.reqPromotedItem();
  }

  Future<ListBankResponse> reqListBank() {
    return repository.reqListBank();
  }

  Future<ShippingResponse> reqShipping(String kecamatan, String store) {
    return repository.reqShipping(kecamatan, store);
  }

  Future<AddressesResponse> reqListAddress() {
    return repository.reqListAddress();
  }

  Future<ProvinceResponse> reqProvince() {
    return repository.reqProvince();
  }

  Future<CityResponse> reqCity(String idProvince) {
    return repository.reqCity(idProvince);
  }

  Future<DistrictResponse> reqDistrict(String idCity) {
    return repository.reqDistrict(idCity);
  }

  Future<BaseResponse> reqAddAddress(AddAddressBody body) {
    return repository.reqAddAddress(body);
  }

  Future<BaseResponse> reqDeleteAddress(String idAddress) {
    return repository.reqDeleteAddress(idAddress);
  }

  Future<OrderResponse> reqOrder(OrderBody body) {
    return repository.reqOrder(body);
  }

  Future<ProductResponse> reqProductByCat(String id) {
    return repository.reqProductByCat(id);
  }

  Future<ProductResponse> reqProductByStore(String id) {
    return repository.reqProductByStore(id);
  }

  Future<ProductResponse> reqProductTrending() {
    return repository.reqProductTrending();
  }

  Future<BaseResponse> reqVerifyOtp(OtpBody body) {
    return repository.reqVerifyOtp(body);
  }

  Future<BaseResponse> reqEditProfile(EditProfileBody body) {
    return repository.reqEditProfile(body);
  }

  Future<BaseResponse> reqAddWishlist(WishlistBody body) {
    return repository.reqAddWishlist(body);
  }

  Future<ListWishlistResponse> reqListWishlist() {
    return repository.reqListWishlist();
  }

  Future<VaResponse> reqListVa() {
    return repository.reqListVa();
  }

  Future<DetailVaResponse> reqDetailVa(String name) {
    return repository.reqDetailVa(name);
  }

  Future<ListOrderResponse> reqListOrder() {
    return repository.reqListOrder();
  }

  Future<ListOrderResponse> reqSellerTransaction() {
    return repository.reqSellerTransaction();
  }

  Future<ProductResponse> reqStoreItem() {
    return repository.reqStoreItem();
  }

  Future<BaseResponse> reqUpdatePass(UpdatePasswordBody body) {
    return repository.reqUpdatePass(body);
  }
}
