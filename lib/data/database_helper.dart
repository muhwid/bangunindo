import 'package:e_commerce_app/model/cart_db_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _databaseName = "cart.db";
  static final _databaseVersion = 1;

  static final table = 'cart';

  static final columnId = 'id';
  static final columnIdProduct = 'id_product';
  static final columnIdStore = 'id_store';
  static final columnName = 'name';
  static final columnDesc = 'description';
  static final columnCategory = 'category';
  static final columnBrand = 'brand';
  static final columnPrice = 'price';
  static final columnStock = 'stock';
  static final columnMinGrosir = 'min_grosir';
  static final columnGrosir = 'grosir';
  static final columnWeight = 'weight';
  static final columnExpired = 'expired';
  static final columnCreatedAt = 'created_at';
  static final columnImage = 'image';
  static final columnReview = 'review';
  static final columnQty = 'qty';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
            $columnIdProduct STRING NOT NULL,
            $columnIdStore STRING NOT NULL,
            $columnName STRING NOT NULL,
            $columnDesc STRING NOT NULL,
            $columnCategory STRING NOT NULL,
            $columnBrand STRING NULL,
            $columnPrice STRING NOT NULL,
            $columnStock STRING NOT NULL,
            $columnMinGrosir STRING NOT NULL,
            $columnGrosir STRING NOT NULL,
            $columnWeight STRING NOT NULL,
            $columnExpired STRING NOT NULL,
            $columnCreatedAt STRING NOT NULL,
            $columnImage STRING NOT NULL,
            $columnReview STRING NOT NULL,
            $columnQty INTEGER NOT NULL
          )
          ''');
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    if (oldVersion < newVersion) {
      // you can execute drop table and create table
      db.execute("DELETE TABLE $table;");
    }
  }


  Future<int> insert(CartDbModel cart) async {
    Database db = await instance.database;
    var res = await db.insert(table, cart.toMap());
    return res;
  }

  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await instance.database;
    var res = await db.query(table, orderBy: "$columnId DESC");
    return res;
  }

  Future<List<Map<String, dynamic>>> getSpecificData(String idProduct) async {
    Database db = await instance.database;
    var res = await db.query(table, where: '$columnIdProduct = ?', whereArgs: [idProduct]);
    return res;
  }

  Future<List<Map<String, dynamic>>> getSpecificDataStore(String idStore) async {
    Database db = await instance.database;
    var res = await db.query(table, where: '$columnIdStore = ?', whereArgs: [idStore]);
    return res;
  }

  Future<int> delete(String id) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$columnIdProduct = ?', whereArgs: [id]);
  }

  Future<int> updateQty(CartDbModel cart) async {
    Database db = await instance.database;
    return await db.update(table,cart.toMap(),  where: '$columnIdProduct = ?', whereArgs: [cart.id_product]);
  }

  Future<void> clearTable(String table) async {
    Database db = await instance.database;
    return await db.rawQuery("DELETE FROM $table");
  }

}