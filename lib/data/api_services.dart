import 'package:dio/dio.dart';

import '../base/box_storage.dart';

class ApiServices {
  Dio launch() {
    Dio dio = new Dio();
    dio.interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
    dio.options.headers["accept"] = 'application/json';
    var token = BoxStorage().getToken();
    print(token);
    dio.options.headers["Authorization"] = '$token';
    dio.options.followRedirects = false;
    dio.options.validateStatus = (s) {
      return s < 500;
    };

    return dio;
  }
}
