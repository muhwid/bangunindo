import 'package:dio/dio.dart';
import 'package:e_commerce_app/data/endpoint.dart';
import 'package:e_commerce_app/model/body/add_address_body.dart';
import 'package:e_commerce_app/model/body/edit_profile_body.dart';
import 'package:e_commerce_app/model/body/order_body.dart';
import 'package:e_commerce_app/model/body/otp_body.dart';
import 'package:e_commerce_app/model/body/register_body.dart';
import 'package:e_commerce_app/model/body/register_store_body.dart';
import 'package:e_commerce_app/model/body/update_password_body.dart';
import 'package:e_commerce_app/model/body/wishlist_body.dart';
import 'package:e_commerce_app/model/response/addresses_response.dart';
import 'package:e_commerce_app/model/response/base_response.dart';
import 'package:e_commerce_app/model/response/category_response.dart';
import 'package:e_commerce_app/model/response/city_response.dart';
import 'package:e_commerce_app/model/response/coupon_response.dart';
import 'package:e_commerce_app/model/response/detail_order_response.dart';
import 'package:e_commerce_app/model/response/detail_product_response.dart';
import 'package:e_commerce_app/model/response/detail_va_response.dart';
import 'package:e_commerce_app/model/response/district_response.dart';
import 'package:e_commerce_app/model/response/get_profile_response.dart';
import 'package:e_commerce_app/model/response/list_bank_response.dart';
import 'package:e_commerce_app/model/response/list_order_response.dart';
import 'package:e_commerce_app/model/response/list_wishlist_response.dart';
import 'package:e_commerce_app/model/response/order_response.dart';
import 'package:e_commerce_app/model/response/product_response.dart';
import 'package:e_commerce_app/model/response/province_response.dart';
import 'package:e_commerce_app/model/response/shipping_response.dart';
import 'package:e_commerce_app/model/response/va_response.dart';

import '../api_services.dart';

class ApiRepository {
  Dio dio = ApiServices().launch();

  Future<BaseResponse> reqRegister(RegisterBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_register}",
          data: body.toJson());
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqStoreRegister(RegisterStoreBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_register_store}",
          data: body.toJson());
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqForgotPass(String phoneNumber) async {
    try {
      //FormData formData = FormData.fromMap({"phone": phoneNumber});

      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_forgot_pass}",
          data: {"phone": phoneNumber});
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<GetProfileResponse> reqGetProfile() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_get_profile}");
      return GetProfileResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqSearchProduct(String q) async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_search}$q");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<CategoryResponse> reqCategory() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_category}");
      return CategoryResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<DetailProductResponse> reqDetailProduct(String id) async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_detail_product}$id");
      return DetailProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqTrendingItem() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_trending}");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<CouponResponse> reqCoupon() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_coupon}");
      return CouponResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqPromotedItem() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_promoted}");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ListBankResponse> reqListBank() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_banks}");
      return ListBankResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ShippingResponse> reqShipping(String kecamatan, String store) async {
    try {
      final response = await dio
          .get("${Endpoint.baseUrl}${Endpoint.api_shipping}$kecamatan/$store");
      return ShippingResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<AddressesResponse> reqListAddress() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_address}");
      return AddressesResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProvinceResponse> reqProvince() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_province}");
      return ProvinceResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<CityResponse> reqCity(String idProvince) async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_city}/$idProvince");
      return CityResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<DistrictResponse> reqDistrict(String idCity) async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_district}/$idCity");
      return DistrictResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqAddAddress(AddAddressBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_address}",
          data: body.toJson());
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqDeleteAddress(String idAddress) async {
    try {
      var body = {"id": idAddress};
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_address}/delete",
          data: body);
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<OrderResponse> reqOrder(OrderBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_order}",
          data: body.toJson());
      return OrderResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ListOrderResponse> reqListOrder() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_order}");
      return ListOrderResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ListOrderResponse> reqSellerTransaction() async {
    try {
      final response = await dio
          .get("${Endpoint.baseUrl}${Endpoint.api_seller_transaction}");
      return ListOrderResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqStoreItem() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_store_item}");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqProductByCat(String id) async {
    try {
      final response = await dio
          .get("${Endpoint.baseUrl}${Endpoint.api_product_by_cat}/$id");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqProductByStore(String id) async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}api/product/bystore/$id");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ProductResponse> reqProductTrending() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_trending}");
      return ProductResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqVerifyOtp(OtpBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_verify_otp}",
          data: body.toJson());
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqEditProfile(EditProfileBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_edit_profile}",
          data: body.toJson());
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqAddWishlist(WishlistBody body) async {
    try {
      final response = await dio.post(
          "${Endpoint.baseUrl}${Endpoint.api_favorite}",
          data: body.toJson());
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<ListWishlistResponse> reqListWishlist() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_favorite}");
      return ListWishlistResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<VaResponse> reqListVa() async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_list_va}");
      return VaResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<DetailVaResponse> reqDetailVa(String name) async {
    try {
      final response =
          await dio.get("${Endpoint.baseUrl}${Endpoint.api_list_va}/$name");
      return DetailVaResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<DetailOrderResponse> reqDetailOrder(String id) async {
    try {
      final response = await dio.get("${Endpoint.api_detail_order}");
      return DetailOrderResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseResponse> reqUpdatePass(UpdatePasswordBody body) async {
    try {
      final response = await dio.post("${Endpoint.api_user_update}");
      return BaseResponse.fromJson(response.data);
    } catch (e) {
      return e;
    }
  }
}
