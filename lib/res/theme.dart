import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color bgSplash = Color(0xffFBF5EA);
Color primaryColor = Color(0xFFd63031);
Color btnShopColor = Color(0xFFd63031);
Color accentColor = Color(0xFFD98E00);
Color greyMenu = Color(0xFFDFE6E9);
Color semiTransRed = Color(0xFFF9A2AB);
Color bgPage = Color(0xFFF7F9FD);
Color line = Color(0xffECECEC);
Color subtitle = Color(0xff5B606B);
Color bgCard = Color(0xff412C39);
Color green = Color(0xff00A65F);
Color cyan = Color(0xff00BCD4);
Color bronze = Color(0xff7E5900);
Color transparent = Color(0xFF737373);

TextStyle boldTextFont = GoogleFonts.poppins().copyWith(color: Colors.black, fontWeight: FontWeight.w900);
TextStyle normalTextFont = GoogleFonts.poppins().copyWith(color: Colors.black, fontWeight: FontWeight.normal);
TextStyle normalMuliFont = GoogleFonts.muli().copyWith(color: Colors.black, fontWeight: FontWeight.normal);
TextStyle boldMuliFont = GoogleFonts.muli().copyWith(color: Colors.black, fontWeight: FontWeight.w900);

